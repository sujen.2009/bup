BUP App
=====================


## Using this project

We recommend using the `ionic` utility to create new Ionic projects that are based on this project but use a ready-made starter template.

For example, to start a new Ionic project with the default tabs interface, make sure the `ionic` utility is installed:

```bash
$ sudo npm install -g ionic bower gulp grunt-cli
$ sudo npm install
$ sudo bower install

```

to run in browser  

```bash
$ gulp
$ ionic serve
```