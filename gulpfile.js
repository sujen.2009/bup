var gulp = require('gulp');
require('gulp-grunt')(gulp);
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var path = require('path');
var paths = {
  sass: ['./scss/**/*.scss']
};





var minifyHtml    = require('gulp-minify-html'),
    templateCache = require('gulp-angular-templatecache');

gulp.task('cache_templates', function() {
  gulp.src('www/html/**/*.html')
    //.pipe(minifyHtml({empty: true}))
    .pipe(templateCache({
      standalone: true,
      root: 'html'
    }))
    .pipe(gulp.dest('www'));
})


gulp.task('buildFresh',['grunt-build:development']);

//'grunt-watch','watch'
gulp.task('cache_templates',['buildFresh'], function() {
  gulp.src('www/html/**/*.html')
    //.pipe(minifyHtml({empty: true}))
    .pipe(templateCache({
      standalone: true,
      root: 'html'
    }))
    .pipe(gulp.dest('www'));
})


gulp.task('code',['cache_templates'],function(){
  gulp.run('grunt-watch');
  gulp.run('watch');
 
})

gulp.task('default', ['code']);
gulp.task('defaultt',['grunt-watch','watch'], function(){
  // run complete grunt tasks
    //gulp.run('grunt-build:development',function(){
      console.log('finished');
      gulp.run('cache_templates');
      //gulp.run('grunt-watch');
      // gulp.run('watch');


    //});

    ////gulp.run('cache_templates');
    //gulp.run('cache_templates');

    
    
    // or run specific targets
    //gulp.run('grunt-sass:dist');
    //gulp.run('grunt-browserify:dev');

});

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  // gulp.watch(paths.sass, ['sass']);
  gulp.watch('./www/html/**/*.html', ['cache_templates']);
});


//todo use https://www.npmjs.org/package/gulp-grunt or https://www.npmjs.org/package/grunt-gulp to control centrally




gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
