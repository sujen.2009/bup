/*
 * environments/production.js
 * This file defines globals that are used in the
 * production environment.
 *
 * Example:
 * var _BASE_API_URL = 'http://production.example.org/api/v1';
 *
 */
//var _BASE_API_URL = 'https://skills.lul.se/webservice',
var _BASE_API_URL = 'http://109.74.12.180/HemuppgiftenDev/webservice',
	//var _BASE_API_URL = 'http://109.74.12.180/Hemuppgiften2.0/webservice',
	_APP_VERSION = '2.0.0',
	_APP_DEVELOPMENT_MODE = false,
	debugMode = false,
	images
	;

    