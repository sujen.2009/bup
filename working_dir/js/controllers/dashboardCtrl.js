CONTROLLER_MODULE
    .controller('dashboardCtrl', [
        '$scope', '$rootScope', '$state','$translate' ,'$timeout', '$ionicViewService', '$ionicTabsDelegate', '$ionicScrollDelegate', '$ionicModal', '$ionicPopup', 'AuthenticationService', 'BUPWebservice', 'BUPUser', 'BUPActivities', 'BUPSync', 'BUPInteraction','DynamicTextService',
        function($scope, $rootScope, $state,$translate, $timeout, $ionicViewService, $ionicTabsDelegate, $ionicScrollDelegate, $ionicModal, $ionicPopup, AuthenticationService, BUPWebservice, BUPUser, BUPActivities, BUPSync, BUPInteraction,DynamicTextService) {


            //$ionicScrollDelegate.forgetScrollPosition();
            window.sh = $ionicScrollDelegate;
            window.s = $scope;
            $scope.scrollHandle = $ionicScrollDelegate;//.$getByHandle('dashboardScroll');
            // $scope.scrollHandle = $ionicScrollDelegate.$getByHandle('dashboardScroll');
            window.sH = $scope.scrollHandle;

            $timeout(function() {

                $scope.scrollHandle.forgetScrollPosition();

            }, 1000);





            //console.log($rootScope.currentUser);
            //$ionicViewService.clearHistory();
            $scope.$on('modal.shown', function() {
                /**
                 * a bug in current version of ionic in which
                 * after a modal open (probably with combination of ionic loading)
                 * body has a class "loading-active" which disables the entire screen and thus
                 * freezed the app
                 * @return {[type]} [description]
                 */

                //function defined in the rootscope
                $scope.fixIonicModalDisabledBug();
            });
            $ionicModal.fromTemplateUrl('html/partials/modals/about-the-app-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.title = DynamicTextService.getTitle('ABOUT_THE_APP');
                $scope.modal.content = DynamicTextService.getContent('ABOUT_THE_APP');
                //console.log($scope.modal)
            });
            $scope.openAboutAppModal = function() {
                $scope.modal.show();
            };
            $scope.closeAboutAppModal = function() {
                $scope.modal.hide();
            };
            //Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function() {
                $scope.modal.remove();
            });
            // Execute action on hide modal
            $scope.$on('modal.hide', function() {
                // Execute action
            });
            // Execute action on remove modal
            $scope.$on('modal.removed', function() {
                // Execute action
            });


            $scope.taskGroups = function() {
                var practices = $scope.practices;


                if(practices && practices.length && practices.length ===4) {
                    return _.groupBy(practices, function(element, index){
                      return Math.floor(index/2);
                    });

                }else {
                    return [practices];
                }

                //var practices = angular.copy(tpractices);
                window.p = practices;
                //var practices = practices || $scope.practices;
                if(!practices ||  !practices.length || practices.length !=4) return [practices];
                var chunkSize = practices.length;
                switch(practices.length) {
                    case 4:
                    chunkSize = 2;
                    break;
                }
                var groups = [];
                for (var i=0; i<practices.length; i+=chunkSize)
                    // groups.push(practices.slice(i,i+chunkSize));
                    groups.push(angular.copy(practices.slice(i,i+chunkSize)) ) ;
                return groups;


                // groups = _.groupBy(practices, function(element, index){
                //   return Math.floor(index/chunks);
                // });
                //console.log(groups);
                return groups;

                //return [practices];
                
                
            }
            $scope.linkToPractice = function(practice) {
                switch (practice.type) {
                    case 'sound':
                        // $state.go('practice', {
                        //     'type': 'sound',
                        //     'id': practice.id
                        // });
                        // 
                        $state.go('sound', {
                            id: practice.id,
                        });
                        break;

                    case 'exposure':
                        //$state.go('practice',{type:'exposure',id:practice.id});
                        $state.go('exposure.intro', {
                            'id': practice.id,
                            //'step':1
                        });

                        break;

                    case 'homework':
                        // $state.go('practice', {
                        //     type: 'homework',
                        //     id: practice.id
                        // });
                        $state.go('homework', {
                            //type: 'homework',
                            id: practice.id,
                            isRepetitive: 'yes'
                        });
                        break;

                }
            };

            $scope.set_icon_bg = function(practice) {
                if (practice.icon) {
                    //return { color: "red" }
                    return {
                        backgroundImage: 'url(' + practice.icon + ')'
                    }

                }
            }
            /**
             * to know whether the practice is untouched or not
             * @param  {[type]}  practice [description]
             * @return {Boolean}          [description]
             */
            $scope.isVirgin = function(practice) {
                return 0 === BUPActivities.getActivitiesCountByTaskId(practice.id);
            }

            $scope.isHomeworkDone = function(homework) {
                //console.log('checking homework'+homework.id)
                return BUPActivities.isHomeworkDone(homework.id);
            }

            //console.log('am in the dashboard. lets see if we can find the user details here');
            //console.log($rootScope.currentUser);


            /*
            $scope.logout = function() {

                $ionicPopup.confirm({
                    title: 'Hemuppgiften',
                    content: 'Are you sure you want to log out?',
                    buttons: [{
                        text: 'Cancel',
                        type: 'button-red'
                    }, {
                        text: 'Ok',
                        type: 'button-positive',
                        onTap: function(res) {
                            $state.go('signin');
                            $ionicViewService.clearHistory();
                            AuthenticationService.logout();
                        }

                    }, ]
                }).then();
            };
            */

            $scope.logout = function() {

                $translate(['LABEL_CONFIRM_LOGOUT', 'LABEL_LOG_ME_OUT', 'LABEL_CANCEL'])
                .then(function (translations) {
                    BUPInteraction.confirm({
                        destructiveText: translations.LABEL_LOG_ME_OUT,
                        titleText: translations.LABEL_CONFIRM_LOGOUT,//'Are you sure you want to log out?',
                        cancelText: translations.LABEL_CANCEL,//'Cancel',
                        destructiveButtonClicked: function() {
                            $state.go('signin');
                            $ionicViewService.clearHistory();
                            AuthenticationService.logout();
                             //console.log('logged out!');
                            $timeout(function () {
                                //$state.reload();
                                //console.warn('refreshing!')
                                $state.transitionTo('signin', {}, { reload: true });
                            }, 100);
                            return true;
                        }
                    });

                });


            }

            $scope.sync = function() {
                BUPSync.manualSync();
            };

            //$scope.practicesBeingLoaded = true;

            // BUPWebservice.getAllTasks().then(function(response) {
            //     //console.log(response);
            //     $scope.practices = angular.copy(response.practices.filter(function(p) {
            //         return typeof p === 'object' && p != null; // && p.hasOwnProperty('id');
            //     }));
  
            //     $scope.homeworks = response.homeworks;
            //     $scope.practicesBeingLoaded = false;

            //     $scope.usePeriodsGraphs = response.usePeriodsGraphs;
            //     if (response.RoleId && response.RoleId == 3) {
            //         $rootScope.meetingId = response.meeting_id;  
            //         $scope.totalPracticesToBeMade = response.total_practice;
            //     }
            //     $rootScope.$emit('activitiesInit');

            // });

            //@todo intelligent syncing
            //keep all the user added information
            //
            
            var loadTasksAndHomeworks = function(freshTasks,isBackgroundTask,obtainedFromServer) {
                
                if( obtainedFromServer ) {
                    
                }
                if (!angular.equals(freshTasks.practices.filter(function(p) {
                    return typeof p === 'object' && p != null; // && p.hasOwnProperty('id');
                }), $scope.practices)) {
                    $scope.practices = angular.copy(freshTasks.practices);
                    //isBackgroundTask === false && BUPInteraction.toast('practices updated');
                }

                if (!angular.equals(freshTasks.homeworks, $scope.homeworks)) {
                    $scope.homeworks = freshTasks.homeworks;
                    //isBackgroundTask === false && BUPInteraction.toast('homeworks updated');
                }


                $rootScope.RoleId = freshTasks.RoleId;
                if (freshTasks.RoleId && freshTasks.RoleId == 3) {
                    $rootScope.meetingId = freshTasks.meeting_id;  
                    $scope.totalPracticesToBeMade = freshTasks.total_practice;
                }

                $scope.usePeriodsGraphs = freshTasks.usePeriodsGraphs;

                $scope.showHomeworkOneByOne = freshTasks.showHomeworkOneByOne || false;

                $rootScope.$emit('activitiesInit');
                $scope.scrollHandle.resize();
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.update');
                $scope.$broadcast('scroll.resize');

            }

            BUPWebservice.getAllTasks({cachePreferred:true}).then(function(response) {
                //immediately get data from cache if any    
                loadTasksAndHomeworks(response, false,false);

                //also not if user is online the data is being fetched in the background from server
                //on the event "app.userready" 
            });

            $scope.$on('BUP.tasks_updated', function(event, freshTasks, isBackgroundTask,obtainedFromServer) {
                loadTasksAndHomeworks(freshTasks, isBackgroundTask,obtainedFromServer);
            });

            $scope.$on('BUP.tasks_loaded', function(event, freshTasks) {
                //obtained when no internet from the cache
                loadTasksAndHomeworks(freshTasks, false,false);
            });







            $scope.updateGraph = function() {
                //if group user (data saved locally only)
                //if ($rootScope.currentUser.saveDataInServer === true) {
                
                //console.debug($rootScope.currentUser);
                if ($rootScope.currentUser && $rootScope.currentUser.useMeeting) {
                    //$scope.totalPracticesToBeMade
                    var practiceActivities = BUPActivities.getRepetitiveActivities();
                    var practicePercent = 0;
                    if (practiceActivities.length && practiceActivities.length >= $scope.totalPracticesToBeMade ) {
                        practicePercent = 100;
                    }else {
                        practicePercent = (practiceActivities.length/$scope.totalPracticesToBeMade)*100;
                    }
                    //$scope.practiceLevels = _.range(1, $scope.totalPracticesToBeMade, 1);
                    $scope.setMeetingGraphStyle = function() {
                        
                        // $scope.totalPracticesToBeMade;

                        // return {
                        //         height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.weekly * 100) + '%')
                        // }
                        // 
                        return {
                            width: practicePercent+'%'
                        };
                    };
                }
                else {
                //if ($rootScope.currentUser.saveDataInServer === false) {
                    
                    $scope.practiceRange = {
                        max: {
                            daily: 0,
                            weekly: 0
                        },
                        min: {
                            daily: 0,
                            weekly: 0
                        }
                    };

                    $scope.maxPracticeNum = {
                        daily: 0,
                        weekly: 0
                    };

                    var graphTabsHandle = $ionicTabsDelegate.$getByHandle('graphTabs');

                    $scope.dailyGraphTabSelected = function() {
                        $timeout(function() {

                            $scope.currentGraphScrollHandle = $ionicScrollDelegate.$getByHandle('dailyGraphScroll');

                            if (1 || typeof $scope.dailyGraphTabSelected.executionCount == "undefined") {
                                //this block is executed exactly once
                                $scope.dailyGraphTabSelected.executionCount = 0;
                                
                                //BUPActivities.groupActivitiesByDay();
                                //$scope.dailyActivities = BUPActivities.dailyActivities();
                                $scope.dailyActivities = BUPActivities.groupActivitiesByDay();

                                $scope.dailyActivitiesCount = _.size($scope.dailyActivities);
                                //console.log($scope.dailyActivities);




                                var dailyPracticeCountsArray = Object.keys($scope.dailyActivities).map(function(key) {
                                    return $scope.dailyActivities[key].count
                                });

                                $scope.maxPracticeNum.daily = Math.max.apply(null, dailyPracticeCountsArray);
                                if($scope.maxPracticeNum.daily<5) $scope.maxPracticeNum.daily = 5;

                                //know if it is the first time click or switched later
                                if ($scope.dailyActivitiesCount > 6) {
                                    $scope.currentGraphScrollHandle.scrollBottom();
                                } else {
                                    //items less than 6, add empty boxes

                                    $scope.futureDailyActivities = BUPActivities.futureDailyActivities(7 - $scope.dailyActivitiesCount);
                                    //console.log($scope.futureDailyActivities );

                                }

                                $scope.currentGraphScrollHandle.rememberScrollPosition('dailyScrollRememberId');
                                //at first go to last ie current day

                            } else {
                                //this block is executed everyting daily tab is selected
                                //when switched across daily and weekly remember the last position
                                $scope.currentGraphScrollHandle.rememberScrollPosition('dailyScrollRememberId');
                                $scope.currentGraphScrollHandle.scrollToRememberedPosition();
                                $scope.dailyGraphTabSelected.executionCount++;
                            }

                        }, 0);

                    };




                    /**
                     * function that run when the
                     * @return {[type]} [description]
                     */
                    $scope.weeklyGraphTabSelected = function() {
                        //$scope.weeklyGraphTabSelected = ionic.debounce(function() {

                        $timeout(function() {

                            $scope.currentGraphScrollHandle = $ionicScrollDelegate.$getByHandle('weeklyGraphScroll');
                            if (typeof $scope.weeklyGraphTabSelected.executionCount == "undefined") {
                                $scope.weeklyGraphTabSelected.executionCount = 0;

                                BUPActivities.groupActivitiesByWeek();
                                $scope.weeklyActivities = BUPActivities.weeklyActivities();
                                $scope.weeklyActivitiesCount = _.size($scope.weeklyActivities);

                                var weeklyPracticeCountsArray = Object.keys($scope.weeklyActivities).map(function(key) {
                                    return $scope.weeklyActivities[key].count
                                });

                                $scope.maxPracticeNum.weekly = Math.max.apply(null, weeklyPracticeCountsArray);
                                if($scope.maxPracticeNum.weekly<5) $scope.maxPracticeNum.weekly = 5;


                                //on first time click to weekly tab go to last (current week)
                                if ($scope.weeklyActivitiesCount > 4) {
                                    $scope.currentGraphScrollHandle.scrollBottom();
                                } else {

                                    $scope.futureWeeklyActivities = BUPActivities.futureWeeklyActivities(4 - $scope.weeklyActivitiesCount);
                                }

                                $scope.currentGraphScrollHandle.rememberScrollPosition('weeklyScrollRememberId');


                            } else {

                                //when switched across daily and weekly remember the last position
                                $scope.currentGraphScrollHandle.rememberScrollPosition('weeklyScrollRememberId');
                                $scope.currentGraphScrollHandle.scrollToRememberedPosition();
                                $scope.weeklyGraphTabSelected.executionCount++;
                            }


                        }, 0);

                    };
                    ////}, 100);

                    $scope.$on("$destroy", function() {
                        //forget the scroll position once user navigates to other pages
                        $scope.currentGraphScrollHandle.forgetScrollPosition();
                    });


                    ionic.DomUtil.ready(function() {

                        var tabsNode = angular.element(document.querySelector('#fixedgraph'));
                        //var tabWidth = ionic.DomUtil.getTextBounds(tabsNode[0]);
                        $scope.tabsWidth = tabsNode[0].offsetWidth;
                        $scope.dailyGraphTabSelected();

                    });

                    /**
                     * to scroll the graph to previous view
                     * @uses $scope.tabsWidth which is precalculated once
                     * shifts left by $scope.tabsWidth value
                     * @return void
                     */
                    $scope.graphScrollLeft = function() {

                        $scope.currentGraphScrollHandle.scrollBy(-$scope.tabsWidth, 0, true);

                    }

                    /**
                     * to show/hide the graphScrollLeft button/link based on the current
                     * scroll position of the scroller
                     *
                     * called with throttle function to rate limit the execution of this function
                     * not used debounce here since we want to the graph scroll fn to execute every 300 millisecond durting
                     * the scrolling movement
                     *
                     * with time 300 millisecond minimum time before next execution will be 300 milisecond
                     *
                     * updates the $scope var hideScrollLeft
                     *
                     * @return void
                     */
                    $scope.graphScrolled = ionic.throttle(function() {
                        //
                        //
                        var currentPos = $scope.currentGraphScrollHandle.getScrollPosition();

                        var currentlyAtBeginningOfGraph = currentPos.left < 10;
                        //this is to prevent update of scope.hideScrollLeft every time
                        if (currentlyAtBeginningOfGraph !== $scope.hideScrollLeft) {
                            $scope.hideScrollLeft = currentlyAtBeginningOfGraph;
                        }
                    }, 300);


                    /**
                     * set the maximum height of the bar graph (daily) based on the maximum value
                     * @param [object] {height:'Inpx or %'}
                     */
                    $scope.set_daily_graph_height = function(activity) {
                        if (parseInt(activity.count, 10) > -1) {
                            return {
                                height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.daily * 100) + '%')

                            }

                        }
                    }

                    /**
                     * set the maximum height of the bar graph (weekly) based on the maximum value
                     * @param [object] {height:'Inpx or %'}
                     */
                    $scope.set_weekly_graph_height = function(activity) {
                        if (parseInt(activity.count, 10) > -1) {
                            return {
                                height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.weekly * 100) + '%')
                            }
                        }
                    }


                }
            };

            $scope.$on('event:auth-loginConfirmed', function(event, data) {
                $scope.updateGraph();
            });
            //$scope.updateGraph();

            //$scope.$watch('totalPracticesToBeMade', $scope.updateGraph , true);
            //$scope.$watch('dailyActivities', $scope.updateGraph , true);

            $scope.$on('graphInit',function(event,data) {
                //console.debug('graphInit fired');
                $scope.updateGraph();

            });

            //$scope.activities = $scope.dailyActivities();


        }
    ]);