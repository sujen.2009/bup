CONTROLLER_MODULE
/**
 * Repetitive practice of type exposure
 * may or may not have the anxiety rating
 * may or may not have the feeback
 */
.controller('exponeraCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$timeout', 'BUPInteraction', 'BUPActivities', 'BUPWebservice',
    function($rootScope, $scope, $state, $stateParams, $timeout, BUPInteraction, BUPActivities, BUPWebservice) {
        /**
         * step to determine which substep of expousre is the current page
         * intro is step 1
         * rating is step 2
         * graph is step 3
         */
        $scope.step = $state.current.data.step;
        $scope.practice = {};

        //user contributed values
        $scope.userResponse = {};

        //the default rating values for the user;
        $scope.rating = {};
        $scope.rating.value = null;
        $scope.feedBackShowDuration = 900;


        BUPWebservice.getPracticeById($stateParams.id).then(function(response) {
            $scope.practice = response;
            //mocking test data
            //$scope.practice.anxiety_question = 'what is your problem?';
            //$scope.audio_bgcolor = $scope.practice.audio_bgcolor;

            $scope.practice.has_anxiety_rating = $scope.practice.has_anxiety_rating || false;
            //update the userResponse id same as the practice id
            $scope.userResponse.id = $scope.practice.id;
            $scope.userResponse.type = 'practices';

            $scope.userResponse.rating = $scope.practice.rating || {};

            /*
                if (false === $scope.practice.has_anxiety_rating) {
                    //if there is no rating mark this task as done immediately after this page is opened

                    BUPActivities.addActivity({
                        taskid: Number($stateParams.id)
                    });

                }
            */

        });

        //make this method available to go to slider
        $scope.goToSlider = function() {
            if ($scope.practice.has_anxiety_rating !== false) {
                $state.transitionTo('exposure.rating', {
                    'id': $stateParams.id,
                    'step': 2
                });
            } else {

                //

                BUPActivities.addActivity({
                    taskid: Number($stateParams.id)
                });

                // BUPInteraction.toast($scope.feedbackMessage(), $scope.feedBackShowDuration);
                // $timeout(function() {
                //     //make user properly seee (feel) the feedback message
                //     $state.go('home');
                // }, $scope.feedBackShowDuration);

                BUPInteraction.alert({
                    title: "Feedback",
                    message:$scope.feedbackMessage(),
                    callback:function() {
                        $state.go('home');
                    }
                });


            }
        };



        $scope.feedbackMessage = function() {
            var message = $scope.practice.general_feedback || "Bra jobbat";

            //make sure the rating value is integer so the comparing would give the correct result!
            $scope.rating.value = parseInt($scope.rating.value, 10);

            $scope.practice.feedback_option = $scope.practice.feedback_option || {
                compare: [2, 5, 8, 10],
                message: ["<=2 feedback message 1", "> 2 & <=5feedback message 2", " > 5 & <=8 feedback message 3", "> 8 feedbackmessage 4"]
            };

            //if feed back is enabled and feedback options are correctly configured 
            //value to compare against with logic <= (less than or equal to)
            //minimum have one value so that we have two range at minimum
            //condtional feedback message
            //if number of items in compare is n then there should be (n+1) messages

            if (!!$scope.practice.feedback && $scope.practice.feedback_option.compare instanceof Array && $scope.practice.feedback_option.message instanceof Array && ($scope.practice.feedback_option.compare.length === $scope.practice.feedback_option.message.length)) {
                var i, compareLength = $scope.practice.feedback_option.compare.length;


                /*
            //compare with first item
            if ($scope.rating.value <= $scope.practice.feedback_option.compare[0]) {
                //console.log('first case');
                message = $scope.practice.feedback_option.message[0];
            } else if ($scope.rating.value > $scope.practice.feedback_option.compare[compareLength - 1]) {
                //console.log('last case');

                message = $scope.practice.feedback_option.message[compareLength];

            } else {
                for (i = 1; i < compareLength; i++) {
                    if ($scope.rating.value > $scope.practice.feedback_option.compare[i - 1] &&
                        $scope.rating.value <= $scope.practice.feedback_option.compare[i]
                    ) {
                        // console.log("\n" + 'i = ' + i);
                        // console.log("actual rating value=" + $scope.rating.value);
                        // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                        // console.log("next=" + $scope.practice.feedback_option.compare[i]);

                        message = $scope.practice.feedback_option.message[i];
                        break;
                    }
                    // console.log('???i=' + i);
                    // console.log("actual rating value=" + $scope.rating.value);
                    // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                    // console.log("next=" + $scope.practice.feedback_option.compare[i]);


                }

            }
            */
                if ($scope.rating.value <= $scope.practice.feedback_option.compare[0]) {
                    //console.log('first case');
                    message = $scope.practice.feedback_option.message[0];
                } else {
                    for (i = 1; i < compareLength; i++) {
                        if ($scope.rating.value > $scope.practice.feedback_option.compare[i - 1] &&
                            $scope.rating.value <= $scope.practice.feedback_option.compare[i]
                        ) {
                            // console.log("\n" + 'i = ' + i);
                            // console.log("actual rating value=" + $scope.rating.value);
                            // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                            // console.log("next=" + $scope.practice.feedback_option.compare[i]);

                            message = $scope.practice.feedback_option.message[i];
                            break;
                        }
                        // console.log('???i=' + i);
                        // console.log("actual rating value=" + $scope.rating.value);
                        // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                        // console.log("next=" + $scope.practice.feedback_option.compare[i]);


                    }
                }

            }
            return message;
        }
        //save the rating in step 2
        $scope.save = function() {
            console.log($scope.rating.value);
            if (undefined == $scope.rating.value) {

                BUPInteraction.toast('Ange ett värde!', 'short'); //please set a value

                return false;
            } else {

                /*
                    BUPInteraction.toast($scope.feedbackMessage(), $scope.feedBackShowDuration);
                    
                    $timeout(function() {
                        //make user properly seee (feel) the feedback message
                        $state.go('home');
                    }, $scope.feedBackShowDuration);

                */
               $scope.$on('newactivity',function(){
                    BUPInteraction.alert({
                        title: "Feedback",
                        message:$scope.feedbackMessage(),
                        callback:function() {
                            $state.go('home');
                        }
                    });
                }) 

                BUPActivities.addActivity({
                    taskid: Number($stateParams.id),
                    rating: $scope.rating.value
                });
                
                //$rootScope.$broadcast('newactivity', newActivity,activities);  
                
                
                    
                  

            }



        }

        $scope.hide_rating_graph = false;
        if ($scope.practice.has_anxiety_rating !== false) {
            $scope.activities = BUPActivities.getExposureRatingActivities($stateParams.id);
            if ($scope.activities.length === 0) {
                $scope.hide_rating_graph = true;
            }

        }

        if ($scope.step === 3) {

            console.log("in graph step");

            $scope.today = moment().startOf('day').format('YYYY-MM-DD');





            console.log("activities for the current graph only");
            console.log($scope.activities);
            //$scope.startedDate = _.min($scope.activities, 'date');
            $scope.numberOfEstimates = _.size($scope.activities);
            if ($scope.numberOfEstimates > 0) {

                $scope.startedDate = _.min($scope.activities, function(act) {
                    return moment(act.date).valueOf();
                }).date;

                $scope.totalDays = 1 + moment().startOf('day').diff($scope.startedDate, 'days');


            } else {
                $scope.startedDate = '-';
                $scope.totalDays = '-';
            }
            console.log($scope.activities);
        }


    }
])
.controller('ratingTest', ['$scope', function ($scope) {
    
}])
;
