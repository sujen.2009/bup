CONTROLLER_MODULE
.controller('homeworkCtrl', [
    '$scope', '$rootScope', '$state', '$stateParams','$translate', '$ionicModal', 'BUPWebservice', 'BUPInteraction', 'BUPActivities', '$ionicSlideBoxDelegate','$ionicScrollDelegate','$sce','$timeout',
    function($scope, $rootScope, $state, $stateParams,$translate, $ionicModal, BUPWebservice, BUPInteraction, BUPActivities, $ionicSlideBoxDelegate,$ionicScrollDelegate,$sce,$timeout) {
        $scope.practice = {};
        $scope.practice.homeworkSlides = [];

        var isRepetitiveType = $stateParams.isRepetitive == 'yes';
        //console.log("is repetitive = " + isRepetitiveType);

        /*
            if (isRepetitiveType) {
                BUPWebservice.getPracticeById($stateParams.id).then(function(response) {
                    $scope.homeworkSlides = response.homeworkSlides;`~~
                    $scope.totalNumberOfSlides = $scope.homeworkSlides.length;
                    if( $scope.totalNumberOfSlides === 1) {

                        $scope.$broadcast('save_homework_activity'); 

                    }
                });
            } else {
                BUPWebservice.getHomeworkById($stateParams.id).then(function(response) {
                  $scope.homeworkSlides = response.homeworkSlides;
                  $scope.totalNumberOfSlides = $scope.homeworkSlides.length;
                  if( $scope.totalNumberOfSlides === 1) {

                        $scope.$broadcast('save_homework_activity'); 

                    }
                });

            }
        */
        window.sb = $ionicSlideBoxDelegate;
        //var to hold the promise
        var homeworkSlidesReceived,practiceType;
        
        if (isRepetitiveType) {

            homeworkSlidesReceived = BUPWebservice.getPracticeById($stateParams.id);
            practiceType =  'practices';

        } else {

            homeworkSlidesReceived = BUPWebservice.getHomeworkById($stateParams.id);
            practiceType =  'homeworks';

        }

         
        // $scope.$watch('practice', function(newVal,oldVal){
        //     console.debug('inside watch');      
        //     console.log(newVal);
        //     var type;
        //     practices = isRepetitiveType? 'practices':'homeworks';
        //     //BUPWebservice.updateTaskById($stateParams.id,practices,newVal);
        //     BUPActivities.updateUserResponseByTaskId($stateParams.id,practices,newVal);

        // }, true);



       $scope.$watch('practice',  ionic.throttle(function(newVal,oldVal){
        console.debug('inside watch');      
        
            //var type;
         
            //console.log(newVal);
            if( newVal !== oldVal) {
                //console.log('updating user response');
                BUPActivities.updateUserResponseByTaskId($stateParams.id,practiceType,newVal);
            }
            
      
            //BUPWebservice.updateTaskById($stateParams.id,practices,newVal);
        },5000), true);


        $scope.canSave = false;
        /**
         * The purpose of this merging is
         * 1. show the latest update form the server as possible
         * 2. retain the user changes, eg form value, list order, new list, list edits etc
         * 3. 
         * @param  {[type]} server [description]
         * @param  {[type]} local  [description]
         * @return {[type]}        [description]
         */
        var intelligentlyMerge = function(server,local){
            // var ultimate = _.clone(server);
           // console.log(server);
            var ultimate = server;
            //console.log('local list');
            //console.log(local);
            
            //make backward compatible
            //earlier locally saved hoemwork slides didn't have slidId
            //so consider only those slides which have sildeId
            if (local.homeworkSlides && local.homeworkSlides.length ) {
                local.homeworkSlides = _.filter(local.homeworkSlides,function(theSlide){
                    return theSlide.slidId;
                });
            }

            //if there is no data in local (no data from the user side) no need to merge
            if (local.homeworkSlides && local.homeworkSlides.length ) {
                
                //Index the user homework slide content by the user so that they can be 
                //easily referenced by id
                var userSlides = _.indexBy(local.homeworkSlides,'slidId');
                //console.log(userSlides);

                _.forEach(ultimate.homeworkSlides,function(slide,homeworkSlideIndex){
                    var slideId = slide.slidId;
                    switch(slide.type) {
                        //Form type
                        //bring the from value
                        case 'normal-text':
                            
                            //console.log('normal-text');
                            if (userSlides[slideId] && userSlides[slideId].value) {
                                slide.value = userSlides[slideId].value;
                                //console.log('user content updated');
                            }

                        break;

                        /**
                         * data structure 
                         * {
                         *     homeworkSlides:[
                         *     {
                         *         slidId: <id>,
                         *         list:[
                         *             {
                         *                 id: <id>
                         *                 list: <list label>
                         *                 reordered:true,//if the list has been updated
                         *                 added:true,//if the list was added later on by the user
                         *                 updated:true //if the user updated the list
                         *             }
                         *         ]
                         *     }
                         *     
                         *     ]
                         * }
                         *
                         *
                         * Challenges:
                         * 1. delete the list that has been deleted from the server but they might be present in the local
                               delete those list
                         * 2.Updated list reordered as in the server but  maintain the list order if list reordered by the user
                         * 3.Do not show the list if the user has deleted it even if the admin makes some changes to it
                         * 4.Show the updated  text if the text has been updated by the user
                         *
                         * Solution Approach:
                         * 1. start from the list taken from the local list so that the users order is maintained
                         * 2. by comparing with local and remote list determine
                         *    i. which list are newly added in the server
                         *    ii. which list have been deleted from the server
                         * 3. for each list in the local
                         *     if the list item has been removed from the server remove it
                         *     if the local list has been marked as added or updated ( user added that list or updated that list) then user the same version
                         *     otherwise user the list label from the admin so that update made by the admin would be refledted
                         * 4  for each new list added in the server also add it     
                         *        
                         */
                        case "reorder":
                            var userList = _.indexBy(userSlides[slideId].list,'id');
                            var serverList = _.indexBy(slide.list,'id');
                            var finalList = [];
                            //get the array of ids of list from the server
                            var serverListIds = _.map(_.keys(serverList),function(a){return parseInt(a)});
                            var localListIds = _.map(_.keys(userList),function(a){return parseInt(a)});

                            var serverRemovedList = _.difference(localListIds,serverListIds);
                            var serverAddedList = _.difference(serverListIds,localListIds);

                            //constraint always added newly added list from the server at the end
                            
                            

                            //get started with local user list
                            _.forEach(userSlides[slideId].list,function(item,key){

                                if (_.indexOf(serverRemovedList,item.id) < 0) {//not removed from the server
                                    var aList = item;
                                    aList.id = item.id;
                                    //if the user has not made any changes to the list or if it is not the one created by the user itself
                                    //let the list be updated if admin has changged in the server
                                    if ( !(item.added===true || item.updated===true ) ) {
                                        aList.list = serverList[item.id].list;
                                    }
                                    finalList.push(aList);

                                } else if( item.added) {
                                    finalList.push(item);
                                }

                                
                            });



                            //new items from the server that are not present in the local
                            _.forEach( serverAddedList, function(listId){
                                //add in the list
                                var aList = serverList[listId];
                                finalList.push(aList);
                            });

                            slide.list = finalList;

                            // Super Awesome!

         
                        break;
                    }
                });    
            }
            

            return ultimate;

        };

        homeworkSlidesReceived.then(function(response) {

            
            //console.log('fresh task from the server');
            //console.log(response);

            var userExtendedTask = BUPActivities.getUserResponseByTaskId($stateParams.id,practiceType);
            //console.log('pure user data');

            //console.log(userExtendedTask);
            
            // $scope.practice = response;
            $scope.practice = intelligentlyMerge(response,userExtendedTask);

            //console.log('merged user data');
            
           // console.log($scope.practice);
            

            //var deletedHomeworkSlideIds = _.filter()
            //$scope.practice.homeworkSlides[$scope.slideIndex].list.splice(index, 1);





            //$scope.homeworkSlides = response.homeworkSlides;

            //$scope.totalNumberOfSlides = $scope.homeworkSlides.length;
            $scope.totalNumberOfSlides = $scope.practice.homeworkSlides.length;

            //$scope.homeworkSlideScroll = $ionicScrollDelegate.$getByHandle('homeworkSlideScroll');
            //$scope.homeworkSlideScroll = $ionicScrollDelegate.$getByHandle('homeworkSlideScroll');
            //window.hs = $scope.homeworkSlideScroll;
            //$scope.homeworkSlideScroll.forgetScrollPosition();
            //$scope.homeworkSlideScroll.resize();
            if ($scope.totalNumberOfSlides === 1) {
                $scope.canSave = true;
                // $scope.$broadcast('save_homework_activity', {
                //     'isRepetitiveType': isRepetitiveType
                // });

            }

                        $ionicSlideBoxDelegate.update();
        });

        $ionicModal.fromTemplateUrl('html/partials/modals/homework_add_edit_list.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }, {

            focusFirstInput: true

        }).then(function(modal) {
            $scope.modal = modal;
            //console.log("modal loaded");
        });

        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        $scope.data = {
            showDelete:false,
            showReorder:true,
            showEdit: false

        };
        //data.showDelete=false;data.showReorder=true; data.showEdit=true;



        $scope.taskAlreadyCompleted = false; //$scope.totalNumberOfSlides > 0;
        

        $scope.slideIndex = 0;
        $scope.next = function() {
            $ionicSlideBoxDelegate.next();
        };
        $scope.previous = function() {
            $ionicSlideBoxDelegate.previous();
        };
        $scope.currSlide = 0;//$ionicSlideBoxDelegate.currentIndex();
        //console.debug('current slide is'+$scope.currSlide);



        // Called each time the slide changes
        $scope.slideChanged = function(index) {
            $scope.slideIndex = index;
            $scope.currSlide = $ionicSlideBoxDelegate.currentIndex();
            //console.debug('slide changed new current slide is'+$scope.currSlide);
            //$scope.$broadcast('scroll.resize');
            //$scope.$broadcast('scroll.scrollTop');
            //$scope.homeworkSlideScroll.resize();
            //console.log('slide changed');
            if ($scope.taskAlreadyCompleted === false && (Number(index) === Number($scope.totalNumberOfSlides - 1))) {
                $scope.canSave = true;
                // $scope.$broadcast('save_homework_activity', {
                //     'isRepetitiveType': isRepetitiveType
                // });
                // $scope.taskAlreadyCompleted = true;
            }

            $timeout( function() {
              $ionicScrollDelegate.resize();
              $ionicScrollDelegate.scrollTop(false);
              $ionicSlideBoxDelegate.update();
            }, 50);
            

            //updateTask on the slide Change
        };

        $scope.$on('save_homework_activity', function(event, info) {
            
            if (info.isRepetitiveType === true) {
                //only keep save the activities if it is a repetitive one
                BUPActivities.addActivity({
                    taskid: Number($stateParams.id),
                    type: 'homework'
                    //date: moment().startOf('day').add('day',-7).format('YYYY-MM-DD'),//
                });


            } else {
                //if not repetitive task we do not need to keep track of every practices
                //but we need to keep track if the homework has been done once or not
                //alert('non repetitive type')
                //first get its previous activity count
                //if this has not been practiced previously then add the activity
                //alert(BUPActivities.getActivitiesCountByTaskId($stateParams.id))

                if (!BUPActivities.isHomeworkDone($stateParams.id)) {
                    BUPActivities.addActivity({
                        homeworkid: Number($stateParams.id),
                        //type: 'homework',
                        //repetitive: false
                        //date: moment().startOf('day').add('day',-7).format('YYYY-MM-DD'),//
                    });
                }else{

                    $rootScope.$broadcast('homeworkdoneagain', {
                        homeworkid: Number($stateParams.id)
                    });

                }

            }
        })
        $scope.showSaveButton = function() {
            return $scope.canSave===true;// = true;
        };

        $scope.save = function(){
            $scope.$broadcast('save_homework_activity', {
                'isRepetitiveType': isRepetitiveType
            });
            $scope.taskAlreadyCompleted = true;
            $scope.canSave=false;
            
        };
        $scope.theReorderItem = {
            list: "",
        };
        $scope.updateTheList = function() {
            if (!$scope.theReorderItem.list.length) {
                $translate('INFO_LIST_CANNOT_BE_EMPTY'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });
            }

            if ($scope.theReorderItem.task == 'add') {
                $scope.practice.homeworkSlides[$scope.slideIndex].list.push({
                    list: $scope.theReorderItem.list,
                    added:true,
                    reordered:true,
                    id: (new Date).getTime()
                });
                //when added the newly added list appears at the bottom,but since the order is not saved 
                //next time while loading the just added list appears on the top
                //so instead mark all the list as reordered so that the order would be maintained
                _.map($scope.practice.homeworkSlides[$scope.slideIndex].list,function(a){
                    a.reordered=true;
                    return a;
                });

                $translate('LABEL_LIST_INSERT_SUCCESS'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                });

                $scope.$broadcast('scroll.resize');


            } else if ($scope.theReorderItem.task == 'edit') {
                $scope.practice.homeworkSlides[$scope.slideIndex].list[$scope.theReorderItem.index].list = $scope.theReorderItem.list;
                $scope.practice.homeworkSlides[$scope.slideIndex].list[$scope.theReorderItem.index].updated = true;
                //BUPInteraction.toast('Updated!');
                $translate('LABEL_LIST_UPDATE_SUCCESS'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                });
            }

            $scope.closeListModal();

        }

        $scope.deleteTheList = function(index) {
            // BUPInteraction.confirm({
            //     destructiveButtonClicked: function() {
            //         console.log($scope.practice.homeworkSlides[$scope.slideIndex].list[index]);
            //         $scope.practice.homeworkSlides[$scope.slideIndex].list.splice(index, 1);
            //         BUPInteraction.toast('Deleted!');
            //         return true;
            //     }
            // });

            $translate(['LABEL_CONFIRM_LIST_DELETE', 'LABEL_LIST_DELETE', 'LABEL_CANCEL','LABEL_LIST_DELETE_SUCCESS'])
            .then(function (translations) {
                BUPInteraction.confirm({
                    destructiveText: translations.LABEL_LIST_DELETE,
                    titleText: translations.LABEL_CONFIRM_LIST_DELETE,//'Are you sure you want to log out?',
                    cancelText: translations.LABEL_CANCEL,//'Cancel',
                    destructiveButtonClicked: function() {
                        //$scope.practice.homeworkSlides[$scope.slideIndex].list.splice(index, 1);
                        $scope.practice.homeworkSlides[$scope.slideIndex].list[index].trashed = true;
                        BUPInteraction.toast(translations.LABEL_LIST_DELETE_SUCCESS);//'Deleted!'
                        return true;
                    }
                });

            });

        };



        $scope.reorderList = function(item, fromIndex, toIndex) {
            //Move the item in the array
            //console.log(item);
            //console.log($scope.practice.homeworkSlides[$scope.slideIndex].list);
            $scope.practice.homeworkSlides[$scope.slideIndex].list.splice(fromIndex, 1); //remove from
            $scope.practice.homeworkSlides[$scope.slideIndex].list.splice(toIndex, 0, item); //add item to
            //console.log("reorder from " + fromIndex + 'to ' + toIndex);
            //keep the flat reordered
            
            $scope.practice.homeworkSlides[$scope.slideIndex].list[fromIndex].reordered = true;
            $scope.practice.homeworkSlides[$scope.slideIndex].list[toIndex].reordered = true;
        };




        $scope.updateListModal = function(index) {

            angular.copy($scope.practice.homeworkSlides[$scope.slideIndex].list[index], $scope.theReorderItem);

            $scope.theReorderItem.task = "edit";

            //var oldList = $scope.theReorderItem.list;

            $scope.theReorderItem.oldList = $scope.theReorderItem.list;
            $scope.theReorderItem.index = index;

            //angular.copy($scope.master, $scope.user);
            $scope.$broadcast('addOrUpdateList'); //to focus on the textarea

            $scope.modal.show();
        };

        $scope.openListModal = function() {
            $scope.theReorderItem = {
                list: "",
                oldList: "",
                task: "add"
            };
            $scope.$broadcast('addOrUpdateList'); //to focus on the textarea

            $scope.modal.show();
        };

        $scope.closeListModal = function() {
            $scope.modal.hide();
        };


        /**
         * dynamic button action
         */
        $scope.button_action = function(dynamicButton) {
            console.log(dynamicButton);
            if (dynamicButton.link) {


                if (dynamicButton.contentbuttontypeid==1) {

                    window.open(encodeURI(dynamicButton.link), '_system');

                }else if (dynamicButton.contentbuttontypeid == 2) {
                  
                    BUPInteraction.alert({message:dynamicButton.link});
                
                }

                //window.open(encodeURI($attrs.browseTo), '_system');
                
                //window.open(encodeURI(dynamicButton.link), '_blank','location=yes');
            }
        };

        $scope.$on('modal.shown', function() {
            /**
             * a bug in current version of ionic in which
             * after a modal open (probably with combination of ionic loading)
             * body has a class "loading-active" which disables the entire screen and thus
             * freezed the app
             * @return {[type]} [description]
             */

            //function defined in the rootscope
            $scope.fixIonicModalDisabledBug();
        });

        $scope.htmlSafe = function(html){
            //return html;
            return $sce.trustAsHtml(html);
        }

        $scope.$on('native.keyboardshown',function(event){
            //disable the slide feature
            $ionicSlideBoxDelegate.enableSlide(false);
        });
        $scope.$on('native.keyboardhidden',function(event){
            //enbale the slide feature
            $ionicSlideBoxDelegate.enableSlide(true);

        });

        



    }
]);
