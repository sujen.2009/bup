CONTROLLER_MODULE
.controller('SignInCtrl', [
    '$scope',
    '$rootScope', '$document', '$state', '$http','$translate', 'BUPUser', 'AuthService', 'AuthenticationService', 'BUPWebservice', 'BUPInteraction', 'BUPLoader', 'BUPSync', '$ionicModal', '$ionicPlatform', '$ionicViewService', '$ionicPopup', '$ionicLoading', 'BUPUtility',
    function($scope, $rootScope, $document, $state, $http,$translate, BUPUser, AuthService, AuthenticationService, BUPWebservice, BUPInteraction, BUPLoader, BUPSync, $ionicModal, $ionicPlatform, $ionicViewService, $ionicPopup, $ionicLoading, BUPUtility) {

        // var random_users = [{
        //     Username: "patient21",
        //     UserPassword: "123456"
        // }];
        // rand_index = Math.floor(Math.random() * (random_users.length));

        //$scope.user = random_users[rand_index];


        //if a user is alreay logged in then don't waste his/her time    
        if (BUPUser.isLoggedIn()) {
            $state.go('home');
            //BUPInteraction.toast('already logged in', 'short');
        }
        console.log($scope.currentUser);
        $scope.user = {};
        $scope.user = {
            Username: String($scope.currentUser ? $scope.currentUser.username : ""),
            UserPassword: ""
        };
        console.log(BUPUser);

        $ionicPlatform.ready().then(function() {

            $scope.user = angular.extend($scope.user, BUPUtility.deviceInfo());
            console.debug($scope.user);

        });


        $scope.signIn = function() {

            if (!$scope.user.Username) {
                $translate('INFO_USERNAME_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });


            } else if (!$scope.user.UserPassword) {
                $translate('INFO_PASSWORD_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });
            } else {

                AuthenticationService.login($scope.user);
            }
        };

        $scope.$on('event:auth-loginConfirmed', function(event, data) {
            //console.debug(data);
            
            // BUPSync.manualSync({
            //     silentSync: true
            // });

            console.debug('event fired: event:auth-loginConfirmed');

            $translate('INFO_LOGIN_SUCCESS'). then ( function  (text) {
                BUPInteraction.toast(text, 'short');
            });

            try {

                cordova.plugins.Keyboard.close();

            } catch (err) {
                
                //console.error(err);

            }
            /**
             * A decisional point in the app
             * Direction the user according to the role
             * For Individual user and custom group user redirect to the home page
             * ie
             * for roleId in (3 or 5 ) (individual or custom group user)
             *
             * But if roleId == 4 ( general group user)
             * then
             *     redirect to a user setting page
             *     where they must choose a custom username and password
             *     Then they they are logged out and should login as roleId 4
             *
             */

            if (data.roleId !== 4) {

                if ($scope.settingModal) {
                    //$scope.settingModal.isShown()
                    $scope.settingModal.hide();
                    $scope.settingModal.remove();

                }
                $rootScope.$emit('activitiesInit');
                $state.go('home');
                $rootScope.$broadcast('app.userready',{manualLogin:true});

            } else {
                /**
                 * @todo create a ionic popup modal
                 * for choosing custom username and password
                 */
                //$scope.generalGroupUserAction();

                //consider that the user is NOT logged in since 
                //the user is never going to use the app with this username
                var user = BUPUser.getCurrentUser();
                user.isLoggedIn = false;
                BUPUser.setCurrentUserInfo(user);
                $rootScope.currentUser = user;


                $scope.customGroupUser = {
                    GroupUserName: data.username,
                    UserId: data.id,
                    Username: "",
                    UserPassword: ""
                };
                $ionicPlatform.ready().then(function() {

                    $scope.customGroupUser = angular.extend($scope.customGroupUser, BUPUtility.deviceInfo());
                    console.debug($scope.customGroupUser);

                });
                console.log("generalGroupUserAction");
                $ionicModal.fromTemplateUrl('html/partials/modals/setting.html', function(modal) {
                    $scope.settingModal = modal;
                    $scope.settingModal.show();

                    $scope.$on('$destroy', function() {
                        $scope.settingModal.remove();
                    });
                    // Execute action on hide modal
                    $scope.$on('modal.hide', function() {
                        $scope.settingModal.remove();
                    });


                }, {
                    scope: $scope,
                    backdropClickToClose: false,
                    hardwareBackButtonClose: false,
                    animation: 'slide-in-up',
                    focusFirstInput: false,
                });
            }

        });



        $scope.$on('event:auth-login-failed', function(e, status) {

            console.log('event fired: auth-login-failed');
            console.log(status);
            $translate('INFO_LOGIN_FAILED'). then ( function  (text) {
                BUPInteraction.toast(text, 'short');
                return false;
            });

        });

        $scope.$on('modal.shown', function() {
            console.log('modal shown');
        });
        $scope.register = function(user) {

            if (!$scope.customGroupUser.Username) {
                //BUPInteraction.toast('Please choose a cutom username', 'short');
                //return false;
                $translate('INFO_USERNAME_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });


            } else if (!$scope.customGroupUser.UserPassword) {
                //BUPInteraction.toast('Please choose a cutom password', 'short');
                //return false;
                $translate('INFO_PASSWORD_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });
            } else {

                //$scope.customGroupUser = angular.extend($scope.customGroupUser, BUPUtility.deviceInfo());


                BUPWebservice.registerCustomGroupUser($scope.customGroupUser).then(function(response) {
                    if (response.status === "Success") {
                        //alert('welcome');
                        console.log(response);

                        AuthenticationService.login($scope.customGroupUser);

                        $scope.settingModal.hide();
                    } else {

                        BUPInteraction.toast(response.Message, 'short');

                    }

                });
            }
        };

        // $scope.$on('event:auth-logout-complete', function() {
        //     console.log('event fired: event:auth-logout-complete');
        //     console.log('time to refresh');
        //     $state.go('app.home', {}, {
        //         reload: true,
        //         inherit: false
        //     });
        // });

        // $scope.signIn = function() {
        //     AuthService.login($scope.user);
        // };

        $scope.$on('modal.shown', function() {
            /**
             * a bug in current version of ionic in which
             * after a modal open (probably with combination of ionic loading)
             * body has a class "loading-active" which disables the entire screen and thus
             * freezed the app
             * @return {[type]} [description]
             */

            //function defined in the rootscope
            $scope.fixIonicModalDisabledBug();
        });

    }
]);