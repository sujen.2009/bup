var BUPAudioAdapter = function(){
};


DIRECTIVE_MODULE
//BUPAudio
.directive('bupAudio', ['$rootScope', '$interval',
    function($rootScope, $interval) {


        //var $seekbar = 
        var timeoutId;
        //var audioElement = $document[0].createElement('audio');
        return {
            restrict: 'E',
            replace: false,
            //transculde: true,
            scope: {},
            //link: link,
            controller: function($scope, $element) {

                console.log($scope);
                //console.log(audioElement);
                //console.log('up');
                $scope.audio = new Audio();
                //$scope.seekbar = angular.element(seekbar);
                //$scope.currentNum = 0;
                $scope.playpause = function() {
                    var a = $scope.audio.paused ? $scope.audio.play() : $scope.audio.pause();
                };

                $scope.toggle = function() {

                    $scope.playing = !$scope.playing;
                    $scope.playing ? $scope.audio.play() : $scope.audio.pause();

                };

                $scope.playAudio = function() {
                    //BUPAudio.play($scope.practice.audio_file);
                    //BUPAudio.play();
                    $scope.audio.play();
                    $scope.playing = true;
                    //audioElement.play();

                };

                $scope.pauseAudio = function() {
                    // BUPAudio.pause();
                    $scope.audio.pause();
                    $scope.playing = false;
                    //audioElement.pause();

                };


                //$scope.stopAudio = function() {};



                //listen for audio-element events
                // $scope.audio.addEventListener('play', function() {
                //     $rootScope.$broadcast('audio.play', this);
                // });
                // $scope.audio.addEventListener('pause', function() {
                //     $rootScope.$broadcast('audio.pause', this);
                // });
                $scope.played = 0;
                /*
                $scope.audio.addEventListener('timeupdate', function() {
                    $rootScope.$broadcast('audio.time', this);
                    console.log('timeupdate fired');
                    $scope.played = $scope.audio.currentTime;
                    window.clearTimeout($scope.checkLoading);

                    if( $scope.audio.ended === true) {
                        $scope.audioEnded();
                    }
                    
                    //console.log($scope.audio.currentTime);
                });
                */

             //improved
                $scope.onTimeUpdate = ionic.throttle(function() {

                    $rootScope.$broadcast('audio.time', this);
                    //console.log('timeupdate fired');
                    
                    

                    $scope.played = $scope.audio.currentTime;
                    if( $scope.audio.ended ) {
                       
                        $scope.audioEnded();
                    }

                    window.clearTimeout($scope.checkLoading);

                    
                    
                },500);

             

                $scope.audio.addEventListener('timeupdate', $scope.onTimeUpdate);
               

                $scope.seekbarChanged = function(a){
                    //console.log(a);
                    $scope.audio.currentTime = $scope.played;

                    //console.log('seekbar changed');
                };

               
               
               $scope.audioEnded = _.debounce(function() {
                    $rootScope.$broadcast('audio.ended', this);
                    console.log('audio.ended fired');
                    $scope.playing = false;
                    $scope.played = 0;
                    //if( !$scope.audio.loop)
                      //  $scope.audio.removeEventListener('ended', $scope.audioEnded);
                    if ($scope.loop) {
                        $scope.audio.currentTime = 0;
                        $scope.playAudio();
                    }  
                   
                    //@todo
                    //if has option restart replay the audio
                },5000);


                $scope.audio.addEventListener('ended', $scope.audioEnded);




                //console.log('here audio.set');

                $scope.checkLoading = null;
                $scope.loadingStarted = function() {
                    $scope.loading = true;
                    //console.log('loadstart fired');

                    $scope.audio.removeEventListener('ended', $scope.audioEnded);

                    $scope.checkLoading = setTimeout(function() {
                        //$scope.audio.play();
                        //$scope.audio.play();
                        $scope.playAudio();
                    }, 50);

                }
                $scope.audio.addEventListener('loadstart', $scope.loadingStarted);

                

                $scope.audio.addEventListener('loadeddata', function() {
                    //console.log('loadeddata fired');
                    $scope.loading = false;

                });

                $scope.audio.addEventListener('canplay', function() {
                    //console.log('can play');
                    $scope.loading = false;

                });




                var unregisterAudioSetListener = $rootScope.$on('audio.set', function(event, obj) {
                    //console.log('audio.set called');
                    var playing = !$scope.audio.paused;
                    //console.log(playing);
                    //console.log(obj)
                    //$scope.loading = false;
                    $scope.audio.src = obj.file;
                    $scope.loop = Boolean(parseInt(obj.loop,10));

                    //$scope.audio.loop = obj.loop;

                    unregisterAudioSetListener();

              
                });





                timeoutId = $interval(function() {
                    //if ( !! !$scope.$$phase) {
                    //$digest or $apply
                    //console.log("being applied");
                    //$scope.$apply();
                    //$scope.$$phase || $scope.$apply();
                    // }

                }, 0);

                $scope.$on('$destroy', function() {

                    $scope.audio.pause();
                    $interval.cancel(timeoutId);
                    delete $scope.audio;
                    $scope.audio =  {};
                    // $scope.audio.src = null;

                })

                //update display of things - makes scrub work
                // setInterval(function() {
                //     //console.log('scope applied')
                //     $scope.$apply();
                // }, 500);
            },
            //$scope.audio.currentTime
            template: '<div class="audiocontrol">\
                            <button ng-class="{\'ion-play\':!playing,\'ion-pause\':playing,\'audio-loading ion-loading-c\':loading}" class="circled" ng-click="toggle()" > </button>\
                            <div id="audio_timeline">\
                                <div class="playingtime">{{played | number:2 | audiotime}}</div>\
                                <div class="range2">\
                                    <input ng-change="seekbarChanged();" ng-model="played"  type="range" precision="float" step="any" min="0" max="{{audio.duration}}"  id="seekbar">\
                                </div>\
                                <div class="totaltime">{{audio.duration | number:2 | audiotime}}</div>\
                            </div>\
                        </div>'
            //templateUrl: 'templates/audio-player.html'
            // link: function($scope, $element, attrs) {
            //     scope.timeElapsed

            // }
        };
    }
]);

        // function ctrl($scope, $element) {
        //     console.log('here');
        //     $scope.audio = new Audio();
        //     $scope.currentNum = 0;
        //     $scope.playpause = function() {
        //         var a = $scope.audio.paused ? $scope.audio.play() : $scope.audio.pause();
        //     };

        //     //listen for audio-element events
        //     $scope.audio.addEventListener('play', function() {
        //         $rootScope.$broadcast('audio.play', this);
        //     });
        //     $scope.audio.addEventListener('pause', function() {
        //         $rootScope.$broadcast('audio.pause', this);
        //     });
        //     $scope.audio.addEventListener('timeupdate', function() {
        //         $rootScope.$broadcast('audio.time', this);
        //     });
        //     $scope.audio.addEventListener('ended', function() {
        //         $rootScope.$broadcast('audio.ended', this);
        //         //goto next practice???
        //         //$scope.next();
        //     });

        //     $rootScope.$on('audio.set', function(obj) {
        //         console.log('audio.set called');
        //         console.log(obj);
        //         var playing = !$scope.audio.paused;
        //         $scope.audio.src = obj.file;
        //         var a = playing ? $scope.audio.play() : $scope.audio.pause();
        //         $scope.info = obj.info;
        //         //set other audio related inforation from source to the audio object
        //     });

        //     //update display of things - makes scrub work
        //     // setInterval(function() {
        //     //     //console.log('scope applied')
        //     //     $scope.$apply();
        //     // }, 500);
        // }
