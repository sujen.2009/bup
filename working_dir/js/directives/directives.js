DIRECTIVE_MODULE

/**
 * directive to make autofocus work in the modal box
 */


.directive('focusOn', function() {
    return function(scope, elem, attr) {
        scope.$on(attr.focusOn, function(e) {
            //elem[0].focus();
            //elem[0].select();
        });
    };
})

.directive('dipeshSlider', [function () {
  return {
    restrict: 'A',
    scope: {
      rating: '='
    },
    require: '?ngModel',
    link: function (scope, $element, $attrs) {
        
     var sliderDimensions = {};
     var trackContainer =  $element;
     var min,max,step;

     function refreshSliderDimensions() {
        sliderDimensions = trackContainer[0].getBoundingClientRect();
     }
     refreshSliderDimensions();
     console.log(sliderDimensions);
     console.log("<- slider dimensions");

      var hammertime = new Hammer($element[0], {
        recognizers: [
          [Hammer.Pan, { direction: Hammer.DIRECTION_VERTICAL }]
        ]
      });
      hammertime.on('hammer.input', onInput);
      hammertime.on('panstart', onPanStart);
      hammertime.on('pan', onPan);
      //hammertime.on('panend', onPanEnd);


          /**
           * Slide listeners
           */
          var isSliding = false;
          var rating;
          var isDiscrete = angular.isDefined($attrs.mdDiscrete);
          function updateSlider(y) {
            y = y-30;
            

            var point = Math.round( (sliderDimensions.bottom-y)/20);
            if (point < 0){
              rating = '0';
            }else if (point>10) {
              rating = 10
            }else{
              rating = point;
            }
            if(rating==0){
              rating = '0';
            }

            //console.log(''+(y+30)+', '+y+', '+sliderDimensions.bottom+', '+point+', '+rating);

            scope.$apply(function() {
              scope.rating = rating;
            });
            //console.log($attrs.whenSlidePan);
          }
          function onInput(ev) {
            if (!isSliding && ev.eventType === Hammer.INPUT_START &&
                !$element[0].hasAttribute('disabled')) {

              isSliding = true;

              $element.addClass('active');
              $element[0].focus();
              //refreshSliderDimensions();

              onPan(ev);

              ev.srcEvent.stopPropagation();

            } else if (isSliding && ev.eventType === Hammer.INPUT_END) {

              if ( isSliding && isDiscrete ) onPanEnd(ev);
              isSliding = false;

              $element.removeClass('panning active');
            }
          }
          function onPanStart() {
            if (!isSliding) return;
            $element.addClass('panning');
          }
          function onPan(ev) {
            if (!isSliding) return;

            // While panning discrete, update only the
            // visual positioning but not the model value.

            if ( isDiscrete ) {
              //adjustThumbPosition( ev.center.x );
              console.log('discrete')
              //console.log(ev.center.y);
              updateSlider(ev.center.y);
            }
            else{
              //doSlide( ev.center.x );
              //console.log(ev.center.y);
              updateSlider(ev.center.y);
            }              

            ev.preventDefault();
            ev.srcEvent.stopPropagation();
          }

          function onPanEnd(ev) {
            console.log('onpanend');
            if ( isDiscrete && !$element[0].hasAttribute('disabled') ) {
              // Convert exact to closest discrete value.
              // Slide animate the thumb... and then update the model value.

              var exactVal = percentToValue( positionToPercent( ev.center.x ));
              var closestVal = minMaxValidator( stepValidator(exactVal) );

              setSliderPercent( valueToPercent(closestVal));
              $$rAF(function(){
                setModelValue( closestVal );
              });

              ev.preventDefault();
              ev.srcEvent.stopPropagation();
            }
          }



    }
  };
}])

.directive('slider', function () {
  return {
    restrict: 'A',
    scope: {
      start: '@',
      step: '@',
      end: '@',
      callback: '@',
      margin: '@',
      orientation: '=',
      ngModel: '=',
      ngFrom: '=',
      ngTo: '='
    },
    link: function (scope, element, attrs) {
      var callback, fromParsed, parsedValue, slider, toParsed;
      slider = $(element);
      callback = scope.callback ? scope.callback : 'slide';
      if (scope.ngFrom != null && scope.ngTo != null) {
        fromParsed = null;
        toParsed = null;
        slider.noUiSlider({
          start: [
            scope.ngFrom || scope.start,
            scope.ngTo || scope.end
          ],
          step: parseFloat(scope.step || 1),
          orientation: scope.orientation || 'horizontal',
          margin: parseFloat(scope.margin || 0),
          range: {
            min: [parseFloat(scope.start)],
            max: [parseFloat(scope.end)]
          }
        });
        slider.on(callback, function () {
          var from, to, _ref;
          _ref = slider.val(), from = _ref[0], to = _ref[1];
          fromParsed = parseFloat(from);
          toParsed = parseFloat(to);
          return scope.$apply(function () {
            scope.ngFrom = fromParsed;
            return scope.ngTo = toParsed;
          });
        });
        scope.$watch('ngFrom', function (newVal, oldVal) {
          if (newVal !== fromParsed) {
            return slider.val([
              newVal,
              null
            ]);
          }
        });
        return scope.$watch('ngTo', function (newVal, oldVal) {
          if (newVal !== toParsed) {
            return slider.val([
              null,
              newVal
            ]);
          }
        });
      } else {
        parsedValue = null;
        slider.noUiSlider({
          start: [scope.ngModel || scope.start],
          step: parseFloat(scope.step || 1),
          orientation: scope.orientation || 'vertical',
          direction: 'rtl',
          connect: 'lower',
          range: {
            min: [parseFloat(scope.start)],
            max: [parseFloat(scope.end)]
          }
        });
        slider.on(callback, function () {
          parsedValue = parseFloat(slider.val());
          return scope.$apply(function () {
            return scope.ngModel = parsedValue;
          });
        });
        return scope.$watch('ngModel', function (newVal, oldVal) {
          if (newVal !== parsedValue) {
            return slider.val(newVal);
          }
        });
      }
    }
  };
})
.directive('sglide', [function () {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            //alert('linked');
            jQuery(document).ready(function(){
                //(iElement).sGlide({
                jQuery('#anxietyLevel').sGlide({
                'startAt': 0,
                'flat': true,
                'width': 218,
                'unit': 'px',
                'snap': {
                    'points': 11,
                    'markers': true,
                    'hard': true,
                    'onlyOnDrop':false
                },
                'vertical': true,
                'showKnob': false
            });

            });
            
        }
    };
}])

    .directive("dynamicBackground", function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var el = element[0],
                    attr = el.getAttribute('style');

                el.setAttribute('style', attr);

                // We need to watch for changes in the style in case required data is not yet ready when compiling
                attrs.$observe('style', function() {
                    attr = el.getAttribute('style');

                    if (attr) {
                        el.setAttribute('style', attr);
                    }
                });
            }
        };
    })

.directive('selectOnClick', function() {
    // Linker function
    return function(scope, element, attrs) {
        element.bind('click', function() {
            this.select();
        });
    };
})

.directive('openUrl', function() {
    // Linker function
    return function(scope, element, attrs) {

        var target = attrs.target || "_blank";

        element.bind('click', function() {
            window.open(attrs.openUrl, target, 'location=yes');
        });
    };
})
// .directive('ionNetwork', function($interval) {
//     return {
//         restrict: 'A',
//         scope: {
//             interval: '@?ionNetwork'
//         },
//         link: function(scope, element) {
//             if (window.cordova) {
//                 var allowedNetworkStates = [Connection.WIFI, Connection.CELL_4G, Connection.CELL_3G, Connection.CELL_2G];
//                 var disabledTags = ['input', 'button', 'textarea', 'select'];
//                 var tag = element[0].tagName.toLowerCase();
//                 scope.interval = parseInt(scope.interval) || 500;

//                 function checkNetworkState() {
//                     if (allowedNetworkStates.indexOf(navigator.connection.type) === -1) {
//                         if (disabledTags.indexOf(tag) !== -1) {
//                             element[0].disabled = true;
//                         }
//                         element.removeClass('online');
//                         element.addClass('offline');
//                     } else {
//                         if (disabledTags.indexOf(tag) !== -1) {
//                             element[0].disabled = false;
//                         }
//                         element.removeClass('offline');
//                         element.addClass('online');
//                     }
//                 }

//                 checkNetworkState();
//                 stop = $interval(checkNetworkState, scope.interval);

//                 scope.$on('$destroy', function() {
//                     $interval.cancel(stop);
//                 });
//             }
//         }
//     };
// })

.directive('kcNetwork', function($rootScope, $interval, CordovaNetwork) {
    return {
        restrict: 'A',
        scope: {
            interval: '@?kcNetwork'
        },
        link: function(scope, element) {
            return;
            //if (window.cordova) {
            var disabledTags = ['input', 'button', 'textarea', 'select'];
            var tag = element[0].tagName.toLowerCase();
            scope.interval = parseInt(scope.interval) || 500;

            var whenOnline = function() {
                if (disabledTags.indexOf(tag) !== -1) {
                    element[0].disabled = false;
                }
                element.removeClass('offline');
                element.addClass('online');
            }

            var whenOffline = function() {
                if (disabledTags.indexOf(tag) !== -1) {
                    element[0].disabled = true;
                }
                element.removeClass('online');
                element.addClass('offline');
            }

            function checkNetworkState() {

                // if (CordovaNetwork.isOnline() === false) {
                //     whenOffline();
                // } else {
                //     whenOnline();
                // }
                if ($rootScope.isOnline) {
                    whenOnline();
                } else {
                    whenOffline();
                }
            }

            checkNetworkState();
            stop = $interval(checkNetworkState, scope.interval);

            scope.$on('$destroy', function() {
                $interval.cancel(stop);
            });

            $rootScope.$on('Cordova.NetworkStatus.Offline', function() {
                whenOffline();
            });
            $rootScope.$on('Cordova.NetworkStatus.Online', function() {
                whenOnline();
            });
            //}
        }
    };
})
