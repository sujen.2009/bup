angular.module(_FILTERS_, [])

/**
 * angularjs filter to convert the audio seconds to minutes:seconds
 * return in two digit
 * eg 00:00
 * @return {[type]} [description]
 */
.filter('audiotime', function() {
    return function(input) {
        input = Math.round(input) || 0;
        var minute = Math.floor(input / 60); //% 60 show actual minute for now assuming there will be no greater thant 60 minutes audio

        var sec = Math.floor(input % 60);


        //var hh = Math.floor(seconds / 3600),

        return ('00' + minute).substr(-2) + ':' + ('00' + sec).substr(-2);
    }
})

.filter('group', function(){
   return _.memoize(function(items, groupSize) {
        if(!items) return items;

      var groups = [],
         inner;
      for(var i = 0; i < items.length; i++) {
         if(i % groupSize === 0) {
            inner = [];
            groups.push(inner);
         }
         inner.push(items[i]);
      }
      return groups;
   });
})

.filter('groupBy', function() {


    return function(list, groupBy) {
        var filtered = [],
            byday = [],
            byweek = [],
            bymonth = [];
        var groupday = function(value, index, array) {
            //console.log('inside groupday filter');
            //console.log(value);
            //console.log('value');
            d = new Date(value['date']);
            d = Math.floor(d.getTime() / (1000 * 60 * 60 * 24));
            //console.log(d);
            byday[d] = byday[d] || [];
            byday[d].push(value);
            //console.log(byday);
        };

        var groupweek = function(value, index, array) {
            d = new Date(value['date']);
            d = Math.floor(d.getTime() / (1000 * 60 * 60 * 24 * 7));
            byweek[d] = byweek[d] || [];
            byweek[d].push(value);
        };

        var groupmonth = function(value, index, array) {
            d = new Date(value['date']);
            d = (d.getFullYear() - 1970) * 12 + d.getMonth();
            bymonth[d] = bymonth[d] || [];
            bymonth[d].push(value);
        };
        if (list === null) return [];

        // angular.forEach(list, function(item) {

        // });

        console.log('all data');
        //console.log(list);
        //console.log(groupBy);




        switch (groupBy) {
            case "day":
                //console.log("before applying the map");
                list.map(groupday);
                //console.log("after applying map")
                console.log(byday);
                return [];
                //return byday;
                break;

            case "week":
                list.map(groupweek);
                return byweek;
                break;

            case "month":
                list.map(groupmonth);
                break;
        }



        //return filtered;

    }
})

// myApp.filter('groupBy',
//             function () {
//                 return function (collection, key) {
//                     if (collection === null) return;
//                     return uniqueItems(collection, key);
//         };
//     });