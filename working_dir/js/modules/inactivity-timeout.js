/**
 * Module to do sessionTimeout after certain time of inactivity
 *
 */
MODULES_MODULE

    .run(['$rootScope','AuthenticationService','$state',
            function($rootScope,AuthenticationService,$state) {
            	
            	var timeoutDuration = 30*60*1000;//30 minutes
           
            	
            	//var timeoutDuration = 6*1000;//6 seconds
            	var inactivityTimer = null;


            	var inactivityExecution = function() {
            		//console.error('inactive executed!');
            		$rootScope.$broadcast('inactivity_timeout');
            		AuthenticationService.logout();
            		$state.transitionTo('signin', {}, { reload: true });
            		clearInterval(inactivityTimer);
            	};

            	var resetTimer = function() {
            		//console.error('timer reset!');
            		clearInterval(inactivityTimer);
            		inactivityTimer = setInterval(function() {
                        inactivityExecution();
                    }, timeoutDuration);
            	};

            	//ionic.EventController.on('touchstart',resetTimer);
            	resetTimer();
            	ionic.EventController.on('touchend',resetTimer);
            	ionic.EventController.on('touchend',resetTimer);
            	ionic.EventController.on('mousemove',resetTimer);
            	ionic.EventController.on('keypress',resetTimer);
            	//ionic.EventController.on('load',resetTimer);
    
            }
      ]);