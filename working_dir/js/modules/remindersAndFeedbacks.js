var ReminderClass = function(entry) {
    //the constructor function

    if (entry.id) this.id = "reminder_"+String(entry.id);
    //if (entry.Code) this.code = entry.Code;
    //
    if (entry.OverallTextName)  this.title = he.decode((entry.OverallTextName||'').replace(/(<([^>]+)>)/ig,"") );
    if (entry.OverallTextDescription)    this.message = he.decode((entry.OverallTextDescription||'').replace(/(<([^>]+)>)/ig,"") );

    
    if (entry.days)    this.days = entry.days;

    return this;
};

var FeedbackClass = function(entry) {
    //the constructor function

    if (entry.id) this.id = "feedback_"+String(entry.id);
    //if (entry.Code) this.code = entry.Code;
    if (entry.overallTextName)  this.title = he.decode((entry.overallTextName||'').replace(/(<([^>]+)>)/ig,"") );
    if (entry.overallTextDescription)    this.message = he.decode((entry.overallTextDescription||'').replace(/(<([^>]+)>)/ig,"") );
    if (entry.practiceCount)    this.practiceCount = entry.practiceCount;
    if (entry.type) {
    	switch(entry.type) {
    		case 'intotal':
    		this.type = 'total';
    		break;

    		case 'thisweek':
    		this.type = 'week';
    		break;
    	}
    }    

    return this;
};

MODULES_MODULE
.factory('BUPReminders', ['$rootScope','$cordovaLocalNotification','$moment','BUPInteraction',function ($rootScope, $cordovaLocalNotification, $moment, BUPInteraction) {
	var service  =  {

		getAll: function() {
			var reminders =[];
			var data = false;
			try{

				var data = JSON.parse(window.localStorage['tasks']);

			}catch(e){
				
			}

			if(data && data.reminders && data.reminders.length  ) {
				for (var i=0; i<data.reminders.length; i++) {
					var reminder = new ReminderClass(data.reminders[i]);
                    reminders.push(reminder);
				}
			}

			return reminders;

		},
		prepare: function() {
			if ( ionic.Platform.isWebView()){
				 window.plugin.notification.local.hasPermission(function(granted) {

	                if (granted) {

	                    //$('#NoRightsWarning').hide();       // Hide the warning message
	                    console.log('Push Notification is disabled');

	                } else {
	                	
	                	window.plugin.notification.local.promptForPermission();
	                	console.log('Push Notification is disabled');

	                    // Alas, no permission granted (yet)
	                    /*
	                    if (appData.getUser().getHasBeenAsked4Permission()) {

	                        // The user has been asked for permission already but refused... :-(
	                        $('#NoRightsWarning').show();       // Show the warning message

	                    } else {

	                        window.plugin.notification.local.promptForPermission();
	                        appData.getUser().setHasBeenAsked4Permission();

	                        appData.checkPermissions.call(appData);    // Recursive call to check the answer given by the user

	                    }
	                    */
	                }
	            });

				window.plugin.notification.local.ontrigger = function (id, state, json) {
					console.log(state);
					console.log(json);
					try{
						var message = JSON.parse(json);
						if (message && message.type=='reminder' && message.message) {
							console.log('notification '+id+' has been triggered');
							BUPInteraction.alert({
								title: message.title,
								message: message.message
							});
						}
					}catch(e){
						console.error(e);
					}

					//alert('notification '+id+' has been triggered');


				};


			}
		},
		cancelAllPreviousReminders: function() {
			try{
	        	console.log('push notification');
	        	$cordovaLocalNotification.cancelAll($rootScope);
	        }catch(e) {
	        	console.error(e);
	        }
		},
		reSchedule: function() {
			console.log('rescheduling the notifications');
			//cancel all previously scheduled reminders
			if (ionic.Platform.isWebView()){
				//cancel all previous scheduled reminders and schedule it again
				service.cancelAllPreviousReminders();

				//read the latest reminder settings
				var reminders = service.getAll();


				console.debug(reminders);
				var allReminders = [];
				_.forEach(reminders,function(reminder){
					if (reminder.days && reminder.message) {

						var date = $moment().add(reminder.days,'d').toDate();//seconds
						// s = seconds
						// d = days
						// m = minutes                  
						var data = {
							//user: $rootScope.currentUser,
							id: reminder.id,
							title:reminder.title,
							message:reminder.message,
							type: 'reminder'
						}
						var notificationSetting = {
							id:      reminder.id,
						    title:   reminder.title,
						    message: reminder.message,
						    json:       JSON.stringify(data),
						    date:  date
						}
						allReminders.push(notificationSetting);
						console.debug(notificationSetting);
						window.ars = allReminders;
						try{
							
							$cordovaLocalNotification.add(notificationSetting,$rootScope);

						}catch(e){
							
							console.error(e);

						}

						//return;

						

					}
				});

				window.ars = allReminders;
			}
			

		}


	};
	window.rem = service;
	return service;
}])

.factory('BUPFeedbacks', ['$rootScope','$cordovaLocalNotification','$moment','BUPInteraction','BUPActivities','$timeout',function ($rootScope, $cordovaLocalNotification, $moment, BUPInteraction, BUPActivities, $timeout) {
	var service  =  {

		getAll: function() {
			var feedbacks =[];
			var data = false;
			try{

				var data = JSON.parse(window.localStorage['tasks']);

			}catch(e){
				
			}

			if(data && data.feedbacks && data.feedbacks.length  ) {
				for (var i=0; i<data.feedbacks.length; i++) {
					var feedback = new FeedbackClass(data.feedbacks[i]);
                    feedbacks.push(feedback);
				}
			}

			return feedbacks;

		},
		giveFeedback: function(feedbackObject) {
			console.log('executing feedback message:');
			console.log(feedbackObject);
			if (feedbackObject &&  feedbackObject.message) {
				console.log('notification '+feedbackObject.id+' has been triggered');
				$timeout(function(){
					BUPInteraction.alert({
						title: feedbackObject.title,
						message: feedbackObject.message,
						//autoClose: 3000
					});
					
				},2000);
				
			}
			if (ionic.Platform.isWebView() && feedbackObject.message){
				console.log(feedbackObject);
				var date = $moment().add(1,'s');//seconds
				// s = seconds
				// d = days
				// m = minutes                  
				var data = {
					id: date.valueOf(),
					title:feedbackObject.title,
					message:feedbackObject.message
				}
				var notificationSetting = {
					id:      date.valueOf(),
				    title:   feedbackObject.title,
				    message: feedbackObject.message,
				    autoCancel:false,
				    //date:  date.toDate(),
				    json: JSON.stringify(data)
				};

				window.ns = notificationSetting;
				console.debug(notificationSetting);
				
				try{
					
					$cordovaLocalNotification.add(notificationSetting,$rootScope);

				}catch(e){
					
					console.error(e);

				}

				//return;

					

			
				
			}
			

		},
		checkFeedbackCondition: function(newPracticeActivity) {
			console.log('checking the feedback setting for the user');
			var feedbackSetting = service.getAll();
			window.fs = feedbackSetting;	
			//sort by practiceCount in descending order and group by type
			//var groupedSettings = _(feedbackSetting).sortBy('practiceCount').reverse().groupBy('type').valueOf();
			var groupedSettings = _(feedbackSetting).groupBy('type').valueOf();

			console.debug(groupedSettings);
			console.log('currentPracticeActivity');
			console.log(newPracticeActivity);
			/*
			var groupedSettings = _.groupBy(feedbackSetting,'type');
			*/
			// var reading = {
			// 	totalMatchIndex : -1,
			// 	weekMatchIndex: -1
			// };
			var reading = BUPActivities.getCurrentUserPracticeCount();
			reading.totalMatchIndex = -1;
			reading.weekMatchIndex = -1;4

			
			//type = total
			if (groupedSettings.total && groupedSettings.total.length) {
				//the setting should be in descending order of practiceCount;
				console.log('checking total');
				console.time('getPracticeCountInTotal');
				//reading.total = BUPActivities.getPracticeCountInTotal();
				console.timeEnd('getPracticeCountInTotal');
				
				if (reading.total > 0) {
					reading.totalMatchIndex = _.findIndex(groupedSettings.total,function(feedbackOption) {
						return feedbackOption.practiceCount == reading.total;
					});	
				}

				


				
				//calculate the total number of practices made by the current User in total from the beginning
			}

			if (groupedSettings.week && groupedSettings.week.length) {
				//the setting should be in descending order of practiceCount;
				console.time('getPracticeCountThisWeek');
				//reading.week = BUPActivities.getPracticeCountThisWeek();
				console.timeEnd('getPracticeCountThisWeek');

				//calculate the  number of practices made by the current User in this week
				
				
				if (reading.week > 0) {
					reading.weekMatchIndex = _.findIndex(groupedSettings.week,function(feedbackOption) {
						return feedbackOption.practiceCount == reading.week;
					});	
				}

				
			}
			
			console.debug(reading);

			if (reading.totalMatchIndex > -1) {
				//time to give the feedback!
				console.log('practiceCount '+reading.total+ ' matched to index:'+reading.totalMatchIndex);
				console.log(groupedSettings.total[reading.totalMatchIndex]);
				service.giveFeedback(groupedSettings.total[reading.totalMatchIndex]);
			}
			if (reading.weekMatchIndex > -1) {
				//time to give the feedback!
				console.log('practiceCount '+reading.week+ ' matched to index:'+reading.weekMatchIndex);
				console.log(groupedSettings.week[reading.weekMatchIndex]);
				service.giveFeedback(groupedSettings.week[reading.weekMatchIndex]);
			}

				
			

			//console.log(feedbackSetting);


		} 


	};
	window.rem = service;
	return service;
}])


.run(['$rootScope','BUPUser','BUPConfig','$moment','BUPReminders','BUPFeedbacks',function($rootScope,BUPUser,BUPConfig,$moment, BUPReminders, BUPFeedbacks) {
     
    var giveFeedbackIfRequired = function(event,newEvent) {
    	var isOfTypeHomework = newEvent.hasOwnProperty('homeworkid');
     	if(isOfTypeHomework) return;
     	console.log(newEvent);
     	BUPFeedbacks.checkFeedbackCondition(newEvent);
    };

    $rootScope.$on('newactivity',giveFeedbackIfRequired);


 	var updateAppLastPracticed = _.debounce(function(){
        if(BUPUser.isLoggedIn()){
            $rootScope.currentUserConfig.lastUsed = $moment().valueOf();
            BUPConfig.updateCurrentUserConfig($rootScope.currentUserConfig);
            console.debug($rootScope.currentUserConfig);
            console.debug('last practiced date logged');
            BUPReminders.reSchedule();
        }
	}, 10000);//60000

    //update when a user started using the app as logged in user
    //$rootScope.$on('app.userready',updateAppLastUsed);

    //update when a user has made a practice
    $rootScope.$on('newactivity',updateAppLastPracticed);
    $rootScope.$on('homeworkdoneagain',updateAppLastPracticed);

    BUPReminders.prepare();


}]);