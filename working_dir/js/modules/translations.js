MAIN_MODULE
.config(function($translateProvider) {
  $translateProvider

  /************************
   * English translation
   ***********************/
  .translations('en', {
		
	///global translations:
	LOADING_TEXT:'Laddar',


	//^^^^^^^^^[ Login screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
	HEADLINE_LOGIN:'Login',
	LABEL_BUTTON_LOGIN: 'Login',
	PLACEHOLDER_USERNAME:'Username',
	PLACEHOLDER_PASSWORD:'Password',
    INFO_LOGIN_FAILED:'Login failed',
    INFO_LOGIN_SUCCESS:'Login success',
    INFO_USERNAME_MISSING:'Please provide the username',
    INFO_PASSWORD_MISSING:'Please enter your password',
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

    //^^^^^^^^^[ Dashboard screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
    HEADLINE_DASHBOARD: 'Start',
    HEADLINE_ABOUT_APP_MODAL: 'About Hemuppgiften',
    LABEL_CONFIRM_LOGOUT:'',
    LABEL_BUTTON_LOGOUT:'',
    HEADLINE_FEEDBACK: 'there, This is my awesome app!',
    HEADLINE_ABOUT: 'About Hemuppgiften',
    LABEL_DELETE:'',
    LABEL_CANCEL:'',
    LOADING:'Loading',
    INFO_HOMEWORKS_UPDATED:'',
    INFO_PRACTICES_UPDATED:'',
    LABEL_LIST_UPDATED:'',

  })

  /**********************
   * Swedish translation
   ***********************/

  .translations('sv', {
	
  	///global translations:
	LOADING_TEXT:'Laddar',

	
	//^^^^^^^^^[ Login screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
	HEADLINE_LOGIN:'Logga in',
	LABEL_BUTTON_LOGIN: 'Logga in',
	PLACEHOLDER_USERNAME:'Användarnamn',
	PLACEHOLDER_PASSWORD:'Lösenord',
    INFO_LOGIN_FAILED:'Inloggningen misslyckades',
    INFO_LOGIN_SUCCESS:'Lyckad inloggning',
    INFO_USERNAME_MISSING:'Var vänlig ange användarnamn',
    INFO_PASSWORD_MISSING:'Du måste fylla i lösenord',
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    
    //CUSTOM LOGIN SETTING
    HEADLINE_CUSTOM_LOGIN:'Välj inställningar',
    INFO_USERNAME_ALREADY_TAKEN:'Användarnamnet finns redan, var vänlig välj ett nytt',


	//^^^^^^^^^[ Login screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
    LABEL_GRAPH_PREVIOUS:'Tidigare',
    LABEL_GRAPH_DAILY:'Dagar',
    LABEL_GRAPH_WEEKLY:'Veckor',
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	

    //LOGOUT
    HEADLINE_DASHBOARD: 'Start',
    HEADLINE_ABOUT_APP_MODAL: 'Om hemuppgiften',
   

    LABEL_BUTTON_LOGOUT:'Logga ut',
    LABEL_CONFIRM_LOGOUT:'Är du säker på att du vill logga ut? ',
    LABEL_LOG_ME_OUT:'Ja, logga ut mig!',

    LABEL_CANCEL:'Avbryt',//common for all confirms

    LABEL_LIST_DELETE:'Radera',
    LABEL_CONFIRM_LIST_DELETE:'Är du säker på att du vill radera? ',
    LABEL_LIST_DELETE_SUCCESS:'Raderad!',//@todo
    LABEL_LIST_UPDATE_SUCCESS:'Uppdaterad!',
    LABEL_LIST_INSERT_SUCCESS:'Tillagt!',//@todo
	INFO_LIST_CANNOT_BE_EMPTY:'Fyll i text',//@todo


	INFO_OFFLINE_SYNC_NOT_POSSIBLE: 'Du måste vara online för att synka',


    HEADLINE_FEEDBACK: '',
   

    LOADING:'Loading',

    INFO_HOMEWORKS_UPDATED:'',
    INFO_PRACTICES_UPDATED:''


  })
  ;

  if (_APP_DEVELOPMENT_MODE) {
  	//$translateProvider.preferredLanguage('en');
  	$translateProvider.preferredLanguage('sv');
  }else {
  	$translateProvider.preferredLanguage('sv');
  }
  

});