SERVICE_MODULE

.factory('BUPActivities', ['BUPConfig', 'BUPUtility', 'BUPUser', 'BUPLoader', '$rootScope', '$filter', 'authService', '$moment','UserAssignment','$timeout',
    function(BUPConfig, BUPUtility, BUPUser, BUPLoader, $rootScope, $filter, authService, $moment,UserAssignment,$timeout) {
        //var currentUserID = BUPUser.getCurrentUser().id;
        //console.log(currentUserID + " <-");


        var allActivities = [];
        var activities = [];
      
        //console.debug('setting the current activities');
        //console.debug(BUPUser.getCurrentUser());
        //console.log("all activities");
        //console.log(allActivities);



        //if individual user
        

        
 

        /*
        var activities = _.filter(allActivities, {
            //userid: 11 //currentUserID
            userid: BUPUser.getCurrentUser() && BUPUser.getCurrentUser().id
            //userid:Number($rootScope.currentUser.id)
        });
        */
        //var activities = _.filter(allActivities, activitiesFilterCriteria);
        //console.log("activities from the current user only");
        //console.log(activities);


        var usersFeedbacks = {
            practices: [],
            homeworks: []
        };





        var daily = [];
        window.daily = daily;

        var weekly = [];    
        window.weekly= weekly;

        /**
         * @todo
         * Questions: is  the saved activities only related to the repetitive practices???
         * what abou the homework list added???
         */

        var service = {

            initActivities: function() {
                var currentUser = BUPUser.getCurrentUser();
                if (!currentUser || !currentUser.isLoggedIn) return;
                
                if (window.localStorage['activities']) {
                    try {
                        allActivities = JSON.parse(window.localStorage['activities']);
                    } catch (e) {

                    }
                }
                

                var activitiesFilterCriteria = {};
                if($rootScope.RoleId && $rootScope.RoleId == 3){
                    activitiesFilterCriteria.meetingId = $rootScope.meetingId;
                }
                if (BUPUser.getCurrentUser() ) {
                     activitiesFilterCriteria.userid = parseInt(BUPUser.getCurrentUser().id,10);
                }

                window.activitiesFilterCriteria=activitiesFilterCriteria;

                activities = _.filter(allActivities, activitiesFilterCriteria);

                if (window.localStorage['usersFeedbacks']) {
                    try {
                        usersFeedbacks = angular.extend({
                            practices: [],
                            homeworks: []
                        }, JSON.parse(window.localStorage['usersFeedbacks']));
                    } catch (e) {

                    }
                }
                window.usersFeedbacks = usersFeedbacks;


                window.activities = activities;


                //if daily and weekly have already been calculate , they might need to be recalculated!
                //if (daily.length) {
                
                    service.groupActivitiesByDay();
                //}

                //if (weekly.length) {
                    service.groupActivitiesByWeek();
                //}
                /*
                console.error('Good now initialized');
                console.log('>>>>>>>>>>>>>>>>>>>>>>>>>');
                console.log('all activities');
                console.debug(allActivities);
                console.log('activities');
                console.debug(activities);
                console.log('daily');
                console.debug(daily);
                */
                // this.listen = function($scope) {
                //     $scope.$watch(function(){return someScopeValue},function(){
                //        //$scope.dosomestuff();
                //     });
                // };
                $rootScope.$broadcast('graphInit');

            },
            getAll: function(opt) {
                opt = opt || {};
                if (typeof activities == "object" && activities != null && activities.hasOwnProperty("length")) { //make sure it is a valid array
                    //controller is configured to expect a promist
                    var deferred = $q.defer();
                    deferred.resolve(activities);
                    // !opt.hideLoader && BUPLoader.show({
                    //     delay: 0
                    // });
                    return deferred.promise;

                } else {
                    return []; //return the empty array
                }
            },

            getCurrentUserActivities: function() {
                var user = BUPUser.getCurrentUser();
                return activities;
                var cua = activities.filter(function(activity) {
                    return activity.userid == user.id;
                });
                return cua;
            },
            dailyActivities: function() {
                //console.warn('daily data requested');
                //console.log(daily);
                return daily; 
                
            },
            //returns data in same format as dailyActivities but with remaining dates
            futureDailyActivities: function(daysCount) {
                daysCount = daysCount || 4;
                var today = $moment().startOf('day');
                var graphTomorrow = {
                    day: today.dayOfYear()+1,
                    week: today.isoWeek() +1
                };

                //great!!! in same year
                //just create empty array from graphStart.day to graphToday.day
                var futureDaily = _(graphTomorrow.day).range(graphTomorrow.day + daysCount).map(function(ind) {
                    var obj = {};
                    obj['count'] = 0;
                    obj['date'] = $moment().dayOfYear(ind).startOf('day').valueOf();
                    obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                    obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                    return obj;

                }).indexBy('dayIndex')
                .value();


                return futureDaily;
            },
            weeklyActivities: function() {
                return weekly;
            },
            futureWeeklyActivities: function(weeksCount) {
                weeksCount = weeksCount || 0;

                var today = $moment().startOf('day');


            
                  var graphStart = {
                    year: today.get('year'),
                    day: today.dayOfYear()+1,
                    week: today.isoWeek() +1
                };


                var futureWeekly = _(graphStart.week).range(graphStart.week + weeksCount).map(function(ind) {
                        var obj = {};
                        obj['count'] = 0;
                        obj['date'] = $moment().isoWeek(ind).startOf('day').valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('weekIndex')
                        .value();
                return futureWeekly;
            },
            /**
             * adds the activity to the persistent storage
             * 
             * @param {object} newActivity  
             * a compulsory paramter used to identify:
             *     1. how many times a particular task has been done 
             *     2. other user contributed values which needs to be tracked for each activity
             *
             * For eg rating rating value has to be saved for every entry, ie has to be tracked 
             * independently for each activity even for the same task
             *  
             *]
             * @param {[object]} userResponse [description]
             * Saved in one place for one task
             * eg when a task is done for first time it is saved, when the same task 
             * is done second time, it overwrites the old values
             */
            
            addActivity: function(newActivity,userResponse) {
                console.log(newActivity);

                // obj['date'] = $moment().isoWeek(ind).startOf('day').valueOf();
                //         obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                //         obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);

                var currDate =$moment().startOf('day');
                var defaultValues = {
                    date: currDate.format('YYYY-MM-DD'),
                    day: BUPUtility.dayIndex(currDate.valueOf()),
                    week: BUPUtility.weekIndex(currDate.valueOf()),
                    userid: Number(BUPUser.getCurrentUser().id),
                };



                //if individual user
                if($rootScope.RoleId && $rootScope.RoleId == 3){
                    defaultValues.meetingId = $rootScope.meetingId;
                }
                newActivity = angular.extend(defaultValues, newActivity);
                //console.log(newActivity);
                activities.push(newActivity);
                allActivities.push(newActivity);
                this.saveToLocalStorage();
                
                // if( BUPUser.getCurrentUser().saveDataInServer ) {
                //     newActivity
                // }

                //$timeout(function(){
                    $rootScope.$broadcast('newactivity', newActivity,activities);    
                //},1);
                

            },

            getActivitiesCountByTaskId: function(taskId) {
                //return _.filter(activities, { 'taskid': taskId }).length;
                return _.filter(activities, function(activity) {
                    return activity.taskid === Number(taskId);
                }).length;
                //function(num) { return num % 2 == 0; });
            },
            //
            getActivitiesCountByMeeting: function(meetingId) {
                //return _.filter(activities, { 'taskid': taskId }).length;
                return _.filter(activities, function(activity) {
                    return activity.meetingId === Number(meetingId);
                }).length;
                //function(num) { return num % 2 == 0; });
            },

            isHomeworkDone: function(homeworkId) {
                //console.warn('homework check request received: '+homeworkId);
                //console.log(activities);
                return _.filter(activities, function(activity) {
                    return activity.homeworkid && activity.homeworkid === Number(homeworkId);
                }).length !== 0;
            },

            getExposureRatingActivities: function(taskId) {

                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });

                return _.filter(repetitiveActivities, function(activity) {
                    return activity.taskid === Number(taskId);
                });
            },

            getActivityStartDate: function() {

                //var config = BUPConfig.getCurrentUserConfig();
                var config = BUPConfig.getCurrentUserConfig();

                if (config && config.startedDate)
                    return config.startedDate;

            },
            getRepetitiveActivities: function() {
                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });
                return repetitiveActivities;
            },

            groupActivitiesByDay: function() {
                var currentUser = BUPUser.getCurrentUser();
                if (!currentUser || !currentUser.isLoggedIn) return;
                //console.error('groupActivitiesByDay called');
                //console.error(currentUser);
                //console.time('dailyGraph'); 
                //console.debug(activities);

                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });

                //console.log(repetitiveActivities);

                var UserConfigStartDate = $moment(this.getActivityStartDate().timestamp);

                var graphStart = {
                    year: UserConfigStartDate.get('year'),
                    day: UserConfigStartDate.dayOfYear(),
                    week: UserConfigStartDate.isoWeek()
                };

                //custom date
                // var graphStart = {
                //     year: 2014, 
                //     day: 150, 
                //     week: UserConfigStartDate.isoWeek()
                // };
                var today = $moment().startOf('day');

                var graphToday = {
                    year: today.get('year'),
                    day: today.dayOfYear(),
                    week: today.isoWeek()
                };


                if (debugMode) window.activities = activities;


                //now need to create empty array with from graphStart till graphToday
                if (graphStart.year == graphToday.year) {
                    //great!!! in same year
                    //just create empty array from graphStart.day to graphToday.day
                    window._ua1 = daily = _(graphStart.day).range(graphToday.day + 1).map(function(ind) {
                        var obj = {};
                        obj['count'] = 0;
                        obj['date'] = $moment().dayOfYear(ind).startOf('day').valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('dayIndex')
                        .value();



                } else if (graphStart.year < graphToday.year) {
                    var day_indexes = [];

                    var fromYear = graphStart.year,
                        fromDayofYear = graphStart.day;


                    //for previous years
                    while (fromYear < graphToday.year) {
                        //minimum day of the year is 365 but also check for 366 
                        //If the range is exceeded, it will bubble up to the next years. 
                        //so check if the year value actually increases to make sure we have reached the last day of the year
                        while (fromDayofYear < 365 || $moment().set('year', fromYear).dayOfYear(fromDayofYear).get('year') === fromYear) {
                            day_indexes.push(fromYear + '_' + fromDayofYear);
                            ++fromDayofYear; //increase the "fromDayOfYear" value
                        }
                        fromDayofYear = 1;
                        ++fromYear;

                    }
                    //for current year till today 
                    for (fromDayofYear = 1; fromDayofYear <= (graphStart.day + 1); fromDayofYear++) {
                        day_indexes.push(fromYear + '_' + fromDayofYear);
                    }

                    if (debugMode) window._day_index_range = day_indexes;
                    if (debugMode) console.log(day_indexes);

                    window._ua2 = daily = _(day_indexes).map(function(ind) {
                        //console.log(ind);
                        var info = ind.split('_'),
                            year = info[0],
                            dayOfYear = info[1],
                            theMoment = $moment().year(year).dayOfYear(dayOfYear).startOf('day'),
                            obj = {};
                        obj['count'] = 0;
                        obj['date'] = theMoment.valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('dayIndex')
                        .value();



                    //console.log(daily);


                }



                //now done populating empty dates 


                //now time to add user data activities to them


                _(repetitiveActivities).countBy('date').forEach(function(count, timestampIndex) {
                    //console.log(timestampIndex);
                    var timestamp = $moment(timestampIndex).startOf('day').valueOf();
                    var found = _.findKey(daily, {
                        'date': timestamp
                    });
                    if (found) {
                        daily[found].count = count;
                    }
                    //console.log(found);


                })

                console.timeEnd('dailyGraph');
                return daily;
                //return theMoment.get('year') + '_' + theMoment.dayOfYear();
            },

            groupActivitiesByWeek: function() {




                // console.log("!!!!!!!!!!GETACTIVITIESBYWEEK CALLED!!!!!!!!!");
                // console.log("caller is " + arguments.callee.caller.toString());
                // console.time('weeklyGraph');


                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });



                //custom date
                // var graphStart = {
                //     year: 2014, 
                //     day: 150, 
                //     week: UserConfigStartDate.isoWeek()
                // };



                var today = $moment().startOf('day');

                var graphToday = {
                    year: today.get('year'),
                    day: today.dayOfYear(),
                    week: today.isoWeek()
                };

                try {
                    var UserConfigStartDate = $moment(this.getActivityStartDate().timestamp);

                    var graphStart = {
                        year: UserConfigStartDate.get('year'),
                        day: UserConfigStartDate.dayOfYear(),
                        week: UserConfigStartDate.isoWeek()
                    };
                } catch (e) {
                    console.error('failed to get activity start date');
                    var graphStart = graphToday;
                    //return false;
                }



                if (debugMode) window.activities = activities;


                //now need to create empty array with from graphStart till graphToday
                if (graphStart.year == graphToday.year) {
                    //great!!! in same year
                    //just create empty array from graphStart.day to graphToday.day
                    window._uaw1 = weekly = _(graphStart.week).range(graphToday.week + 1).map(function(ind) {
                        var obj = {};
                        obj['count'] = 0;
                        obj['date'] = $moment().isoWeek(ind).startOf('day').valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('weekIndex')
                        .value();



                } else if (graphStart.year < graphToday.year) {
                    var week_indexes = [];

                    var fromYear = graphStart.year,
                        fromIsoWeek = graphStart.week;


                    //for previous years
                    while (fromYear < graphToday.year) {
                        //minimum week of the year is 52 but also check for more 
                        //If the range is exceeded, it will bubble up to the next years. 
                        //so check if the year value actually increases to make sure we have reached the last week of the year
                        while (fromIsoWeek < 52 || $moment().set('year', fromYear).isoWeek(fromIsoWeek).get('year') === fromYear) {
                            week_indexes.push(fromYear + '_' + fromIsoWeek);
                            ++fromIsoWeek; //increase the "fromDayOfYear" value
                        }
                        fromIsoWeek = 1;
                        ++fromYear;

                    }
                    //for current year till today 
                    for (fromIsoWeek = 1; fromIsoWeek <= (graphStart.week + 1); fromIsoWeek++) {
                        week_indexes.push(fromYear + '_' + fromIsoWeek);
                    }

                    //if (debugMode) window._week_index_range = week_indexes;
                    //if (debugMode) console.log(week_indexes);

                    window._ua2 = weekly = _(week_indexes).map(function(ind) {
                        //console.log(ind);
                        var info = ind.split('_'),
                            year = info[0],
                            week = info[1],
                            theMoment = $moment().year(year).isoWeek(week).startOf('day'),
                            obj = {};
                        obj['count'] = 0;
                        obj['date'] = theMoment.valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('dayIndex')
                        .value();

                    //console.log(weekly);

                    
                }



                //now done populating empty dates 


                //now time to add user data activities to them


                //if (debugMode) window.daily = daily;
                //if (debugMode) console.log(daily);
                //window.weekly = weekly;
                var vals = _(repetitiveActivities).map(function(entry) {
                        entry['week'] = BUPUtility.weekIndex(entry.date);
                        //console.log(entry);

                        return entry;
                    }).countBy('week')
                    .forEach(function(count, weekIndexValue) {
                        if (weekly[weekIndexValue]) {
                            weekly[weekIndexValue].count = count;
                        }
                    })

                //.value();

                //console.log(vals.value());

                // .forEach(function(count, timestampIndex) {

                //     var timestamp = $moment(timestampIndex).startOf('day').valueOf();
                //     console.log(timestamp);
                //     console.log(count);
                //     var found = _.findKey(weekly, {
                //         'date': timestamp
                //     });
                //     if (found) {
                //         weekly[found].count = count;
                //     }
                //     console.log(found);


                // })

                console.timeEnd('weeklyGraph');
                return weekly;
                //return theMoment.get('year') + '_' + theMoment.dayOfYear();
            },


            getPracticeCountInTotal:function(){
                //only repetitive, repetitive activity has taskid, nonrepetitive has homeworkid
                return _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                }).length;
              
            },

            getPracticeCountThisWeek:function(){
                var currentWeekIndex = BUPUtility.weekIndex();
                return _.filter(activities, function(activity) {
                    return activity.week == currentWeekIndex && false !== !!activity.taskid;
                }).length;
            },

            getCurrentUserPracticeCount: function(){
                var response = {
                    total:0,
                    week:0
                };
                var currentWeekIndex = BUPUtility.weekIndex();
                response.total = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                }).length;
                response.week = _.filter(activities, function(activity) {
                    return activity.week == currentWeekIndex && false !== !!activity.taskid;
                }).length;

                return response;
            },


            saveToLocalStorage: function() {
                //???
                // overwriting the activities from the otherusers here
                // 
                //current activities are the activities 
                
                //this will remove the activities of the other users,data of which
                //are in allActivities but not in activities
                //
                
                //window.localStorage['activities'] = JSON.stringify(activities);
                
                //this will maintain the activities of all the users
                window.localStorage['activities'] = JSON.stringify(allActivities);
            },

            /*****************************************************************
             * @id: (int) id of the task
             * @type:  (string) type of the task , either homework or practice
             *****************************************************************
             **/
            getActivityByTaskId: function(taskId) {

                var deferred = $q.defer();

                //first ensure we have all the tasks
                this.getAllTasks().then(function(allTasks) {

                    //console.log('all  tasks (practices and homeworks) available.ensured!');

                    //console.log(allTasks);
                    //console.log('type=' + type);
                    //console.log(allTasks[type]);
                    //get the task type
                    //console.log()
                    var foundTasks = allTasks[type].filter(function(thetask) {
                        //console.log(thetask.id);
                        return thetask != null && parseInt(thetask.id, 10) == id;
                    });
                    //console.log(foundTasks);
                    //make sure the task has been found
                    var requiredTask = foundTasks.length ? foundTasks[0] : {};

                    deferred.resolve(requiredTask);


                });

                return deferred.promise;

            },


                        /*****************************************************************
             * @id: (int) id of the task
             * @type:  (string) type of the task , either homework or practice
             *****************************************************************
             **/
            getUserResponseByTaskId: function(id, type) {

                    console.time('getUserResponseByTaskId'+id+type);
                    console.log(usersFeedbacks);
                    console.log('type=' + type);
                    console.log(usersFeedbacks[type]);
                    var foundItems = usersFeedbacks[type].filter(function(theItem) {
                        return theItem != null && parseInt(theItem.id, 10) == id;
                    });

                    var userResponseForTheTask = false;

                    if (foundItems.length) {
                        userResponseForTheTask = foundItems[0];
                        //userResponseForTheTask = new UserAssignment(foundItems[0]);
                        console.debug('user contributed activity found!')
                    }
                    console.timeEnd('getUserResponseByTaskId'+id+type);
                    return userResponseForTheTask;

            },

            updateUserResponseByTaskId: function(id,type,response) {
                console.log(response);
                // var foundItems = usersFeedbacks[type].filter(function(theItem) {
                //     return theItem != null && parseInt(theItem.id, 10) == id;
                // });
                
                var foundIndex = _.findIndex(usersFeedbacks[type],function(theItem) {
                    return theItem != null && parseInt(theItem.id, 10) == id;
                });

               

                if (foundIndex < 0) {
                    //the file doesn't previously exists
                    //allFiles.push(theFile);
                    console.debug('adding fresh value to type '+type);
                    usersFeedbacks[type].push(new UserAssignment(response));
                    console.log(usersFeedbacks[type][0]);

                } else {
                    //update the same index 
                    console.debug('updating value to index '+foundIndex+' of type '+type);  
                    usersFeedbacks[type][foundIndex] = new UserAssignment(response);
                    console.log(usersFeedbacks[type][foundIndex]);
                    
                }




                // if (foundItems.length) {
                //     //foundItems[0] = response;
                //     foundItems[0] = new UserAssignment(response);
                //     console.log(foundItems[0]);
                //     console.log(usersFeedbacks[type]);
                  
                //     console.debug('existing updated in the user contributed activities');
                // }else {
                //     //usersFeedbacks[type].push(response);
                //     usersFeedbacks[type].push(new UserAssignment(response));

                //     //console.debug('new user contributed activity');
                // }
                //JSON.parse(window.localStorage['usersFeedbacks'])
                window.localStorage['usersFeedbacks'] =  JSON.stringify(usersFeedbacks);

            }

        };

        //service.groupActivitiesByDay();
        //service.groupActivitiesByWeek();

        $rootScope.$on('activitiesInit', service.initActivities);
        //service.initActivities();
        
        window.a = service;

        return service;
    }
])
