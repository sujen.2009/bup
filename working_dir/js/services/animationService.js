SERVICE_MODULE



.factory('film_deep_breath02', [

    function() {


        var lib;

        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes
            var rect; // used to reference frame bounds

            // stage content:
            // 
            //  {src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"sounds/Andas_djupt_ovning_med_intro.mp3"},
                                 // {src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"sounds/Rakna_baklanges_ovning_med_intro.mp3"},
                                 // {src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"sounds/Skona_tanken_ovning_med_intro.mp3"}
            (lib.film_deep_breath02 = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    this.player = playSound("sounds/Andas_djupt_ovning_med_intro.mp3");
                }

                this.pause = function() {
                    console.log('expect the animation and sound to get paused here');
                    this.stop();
                    this.player.pause();
                }

                this.resume = function() {
                    this.play();
                    this.player.resume();
                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(839));

                // Andas Animation 
                this.CIRKEL = new lib.Symbol46();
                this.CIRKEL.setTransform(240, 240);
                this.CIRKEL.alpha = 0;
                this.CIRKEL._off = true;

                this.timeline.addTween(cjs.Tween.get(this.CIRKEL).wait(6).to({
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 57).wait(43).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 12).wait(6).to({
                    scaleX: 0.71,
                    scaleY: 0.71
                }, 89).wait(17).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 44).wait(69).to({
                    scaleX: 0.69,
                    scaleY: 0.69
                }, 53).to({
                    scaleX: 0.49,
                    scaleY: 0.49
                }, 14).wait(45).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 82).wait(42).to({
                    scaleX: 0.63,
                    scaleY: 0.63
                }, 46).to({
                    scaleX: 0.44,
                    scaleY: 0.44
                }, 17).wait(44).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 49).wait(28).to({
                    scaleX: 0.72,
                    scaleY: 0.72
                }, 53).to({
                    scaleX: 0.53,
                    scaleY: 0.53
                }, 9).wait(15));

                // Andas djupt bakgrund
                this.instance = new lib.ID_bakgrund_blue();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance
                    }]
                }).wait(840));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            // symbols:
            (lib.ID_bakgrund_blue = function() {
                this.initialize(img.ID_bakgrund_blue);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_green = function() {
                this.initialize(img.ID_bakgrund_green);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol46 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f().s("#FFFFFF").ss(32, 1, 1).p("ALKrJQEoEoAAGhQAAGikoEoQkoEomiAAQmhAAkokoQkokoAAmiQAAmhEokoQEokoGhAAQGiAAEoEog");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = rect = new cjs.Rectangle(-100.9, -100.9, 202, 202);
            p.frameBounds = [rect];

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});


        return lib.film_deep_breath02;
    }
])


.factory('film_nice_thought', [

    function() {


        var lib;
        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes
            var rect; // used to reference frame bounds

            // stage content:
            (lib.film_nice_thought = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    // playSound("Skona_tanken_ovning_med_intro");
                    this.player = playSound("sounds/Skona_tanken_ovning_med_intro.mp3");

                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(799));

                // Sköna tanken Lager 25
                this.instance = new lib.Symbol45();
                this.instance.setTransform(240, 240);

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(750).to({
                    alpha: 0
                }, 49).wait(1));

                // Sköna tanken Lager 24
                this.instance_1 = new lib.ID_bakgrund_green();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance_1
                    }]
                }).wait(800));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            // symbols:
            (lib.ID_bakgrund_blue = function() {
                this.initialize(img.ID_bakgrund_blue);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_green = function() {
                this.initialize(img.ID_bakgrund_green);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol43 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlZFaQiQiQABjKQgBjJCQiQQCQiQDJABQDKgBCQCQQCQCQgBDJQABDKiQCQQiQCQjKgBQjJABiQiQg");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
            p.frameBounds = [rect];


            (lib.Symbol44 = function(mode, startPosition, loop) {
                this.initialize(mode, startPosition, loop, {});

                // Lager 1
                this.instance = new lib.Symbol43("synched", 0);
                this.instance.alpha = 0;

                this.timeline.addTween(cjs.Tween.get(this.instance).to({
                    alpha: 0.602
                }, 49).wait(115).to({
                    startPosition: 0
                }, 0).to({
                    alpha: 0
                }, 75).wait(1));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            (lib.Symbol45 = function(mode, startPosition, loop) {
                this.initialize(mode, startPosition, loop, {});

                // Lager 22
                this.instance = new lib.Symbol44();
                this.instance.setTransform(148, -38.9);
                this.instance._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(589).to({
                    _off: false
                }, 0).wait(211));

                // Lager 21
                this.instance_1 = new lib.Symbol44();
                this.instance_1.setTransform(47, 121, 1.184, 1.184);
                this.instance_1._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(570).to({
                    _off: false
                }, 0).wait(230));

                // Lager 20
                this.instance_2 = new lib.Symbol44();
                this.instance_2.setTransform(-61.9, 129, 0.571, 0.571);
                this.instance_2._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(546).to({
                    _off: false
                }, 0).wait(254));

                // Lager 19
                this.instance_3 = new lib.Symbol44();
                this.instance_3.setTransform(148, 63);
                this.instance_3._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(519).to({
                    _off: false
                }, 0).wait(281));

                // Lager 18
                this.instance_4 = new lib.Symbol44();
                this.instance_4.setTransform(119, -113.9);
                this.instance_4._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(491).to({
                    _off: false
                }, 0).wait(309));

                // Lager 17
                this.instance_5 = new lib.Symbol44();
                this.instance_5.setTransform(-143.9, -42.9, 1.306, 1.306);
                this.instance_5._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(471).to({
                    _off: false
                }, 0).wait(329));

                // Lager 16
                this.instance_6 = new lib.Symbol44();
                this.instance_6.setTransform(-82.9, -139.9);
                this.instance_6._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(435).to({
                    _off: false
                }, 0).wait(365));

                // Lager 15
                this.instance_7 = new lib.Symbol44();
                this.instance_7.setTransform(124, 0);
                this.instance_7._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(406).to({
                    _off: false
                }, 0).wait(394));

                // Lager 14
                this.instance_8 = new lib.Symbol44();
                this.instance_8.setTransform(-161.9, 75, 0.755, 0.755);
                this.instance_8._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(376).to({
                    _off: false
                }, 0).wait(424));

                // Lager 13
                this.instance_9 = new lib.Symbol44();
                this.instance_9.setTransform(-137.9, -97.9, 0.694, 0.694);
                this.instance_9._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(344).to({
                    _off: false
                }, 0).wait(456));

                // Lager 12
                this.instance_10 = new lib.Symbol44();
                this.instance_10.setTransform(-6.9, -9.9, 1.673, 1.673);
                this.instance_10._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(319).to({
                    _off: false
                }, 0).wait(481));

                // Lager 11
                this.instance_11 = new lib.Symbol44();
                this.instance_11.setTransform(-118.9, 79, 0.612, 0.612);
                this.instance_11._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(293).to({
                    _off: false
                }, 0).wait(507));

                // Lager 10
                this.instance_12 = new lib.Symbol44();
                this.instance_12.setTransform(-13.9, -130.9, 1.347, 1.347);
                this.instance_12._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(261).to({
                    _off: false
                }, 0).wait(539));

                // Lager 9
                this.instance_13 = new lib.Symbol44();
                this.instance_13.setTransform(129, -48.9, 0.796, 0.796);
                this.instance_13._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(224).to({
                    _off: false
                }, 0).wait(576));

                // Lager 8
                this.instance_14 = new lib.Symbol44();
                this.instance_14.setTransform(-103.9, 10, 1.429, 1.429);
                this.instance_14._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(194).to({
                    _off: false
                }, 0).wait(606));

                // Lager 7
                this.instance_15 = new lib.Symbol44();
                this.instance_15.setTransform(-13.9, 110);
                this.instance_15._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(162).to({
                    _off: false
                }, 0).wait(638));

                // Lager 6
                this.instance_16 = new lib.Symbol44();
                this.instance_16.setTransform(68, -101.9, 1.245, 1.245);
                this.instance_16._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(133).to({
                    _off: false
                }, 0).wait(667));

                // Lager 5
                this.instance_17 = new lib.Symbol44();
                this.instance_17.setTransform(68, 0.1, 1.408, 1.408, 0, 0, 0, -7.8, -44.7);
                this.instance_17._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(109).to({
                    _off: false
                }, 0).wait(691));

                // Lager 4
                this.instance_18 = new lib.Symbol44();
                this.instance_18.setTransform(-69.9, 74, 0.735, 0.735);
                this.instance_18._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(79).to({
                    _off: false
                }, 0).wait(721));

                // Lager 3
                this.instance_19 = new lib.Symbol44();
                this.instance_19.setTransform(72, -26.9, 0.755, 0.755);
                this.instance_19._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(57).to({
                    _off: false
                }, 0).wait(743));

                // Lager 2
                this.instance_20 = new lib.Symbol44();
                this.instance_20.setTransform(-82.9, -64.9, 1.531, 1.531);
                this.instance_20._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(31).to({
                    _off: false
                }, 0).wait(769));

                // Lager 1
                this.instance_21 = new lib.Symbol44();
                this.instance_21._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(10).to({
                    _off: false
                }, 0).wait(790));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 0, 0);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-48.9, -48.9, 98, 98), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 207, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 250), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 306, 272), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 295), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 322, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 342, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -196.9, 342, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 367, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 372, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 381, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 376), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});


        return lib.film_nice_thought;
    }
])


.factory('film_countdown02', [

    function() {


        var lib;
        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes

            // stage content:
            (lib.film_countdown02 = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    this.player = playSound("sounds/Rakna_baklanges_ovning_med_intro.mp3");
                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(810));

                // 99
                this.instance = new lib.Symbol42nr5();
                this.instance.setTransform(242.5, 235.4);
                this.instance.alpha = 0.23;
                this.instance._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(317).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 6).wait(469));

                // 98
                this.instance_1 = new lib.Symbol42nr6();
                this.instance_1.setTransform(242.5, 235.4);
                this.instance_1.alpha = 0.23;
                this.instance_1._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(293).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(494));

                // 97
                this.instance_2 = new lib.Symbol42nr7();
                this.instance_2.setTransform(242.5, 235.4);
                this.instance_2.alpha = 0.23;
                this.instance_2._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(269).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(518));

                // 96
                this.instance_3 = new lib.Symbol42nr8();
                this.instance_3.setTransform(242.5, 235.4);
                this.instance_3.alpha = 0.23;
                this.instance_3._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(245).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(542));

                // 95
                this.instance_4 = new lib.Symbolnr9();
                this.instance_4.setTransform(240, 235.4);
                this.instance_4.alpha = 0.23;
                this.instance_4._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(221).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(566));

                // 94
                this.instance_5 = new lib.Symbol42nr4();
                this.instance_5.setTransform(242.5, 235.4);
                this.instance_5.alpha = 0.23;
                this.instance_5._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(342).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(445));

                // 93
                this.instance_6 = new lib.Symbol42nr3();
                this.instance_6.setTransform(242.5, 235.4);
                this.instance_6.alpha = 0.23;
                this.instance_6._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(366).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(423));

                // 92
                this.instance_7 = new lib.Symbol42nr2();
                this.instance_7.setTransform(242.5, 235.4);
                this.instance_7.alpha = 0.23;
                this.instance_7._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(388).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(401));

                // 91
                this.instance_8 = new lib.Symbol42nr1();
                this.instance_8.setTransform(242.5, 235.4);
                this.instance_8.alpha = 0.23;
                this.instance_8._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(410).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 58).wait(324));

                // 90
                this.instance_9 = new lib.Symbol41();
                this.instance_9.setTransform(242.5, 235.4);
                this.instance_9.alpha = 0;
                this.instance_9._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(32).to({
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 45).wait(124).to({
                    scaleX: 1.19,
                    scaleY: 1.19
                }, 5).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(590));

                // 89
                this.instance_10 = new lib.Symbol42nr1();
                this.instance_10.setTransform(242.5, 235.4);
                this.instance_10.alpha = 0.23;
                this.instance_10._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(715).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 1).wait(76));

                // 88
                this.instance_11 = new lib.Symbol42nr2();
                this.instance_11.setTransform(242.5, 235.4);
                this.instance_11.alpha = 0.23;
                this.instance_11._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(693).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(96));

                // 87
                this.instance_12 = new lib.Symbol42nr3();
                this.instance_12.setTransform(242.5, 235.4);
                this.instance_12.alpha = 0.23;
                this.instance_12._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(671).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(118));

                // 86
                this.instance_13 = new lib.Symbol42nr4();
                this.instance_13.setTransform(242.5, 235.4);
                this.instance_13.alpha = 0.23;
                this.instance_13._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(649).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(140));

                // 85
                this.instance_14 = new lib.Symbol42nr5();
                this.instance_14.setTransform(242.5, 235.4);
                this.instance_14.alpha = 0.23;
                this.instance_14._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(627).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(162));

                // 84
                this.instance_15 = new lib.Symbol41();
                this.instance_15.setTransform(242.5, 235.4, 0.757, 0.757);
                this.instance_15.alpha = 0;
                this.instance_15._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(488).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.09,
                    scaleY: 1.09,
                    alpha: 1
                }, 28).to({
                    scaleX: 1.19,
                    scaleY: 1.19
                }, 8).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(272));

                // 83
                this.instance_16 = new lib.Symbol42nr9();
                this.instance_16.setTransform(242.5, 235.4);
                this.instance_16.alpha = 0.23;
                this.instance_16._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(539).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(250));

                // 82
                this.instance_17 = new lib.Symbol42nr8();
                this.instance_17.setTransform(242.5, 235.4);
                this.instance_17.alpha = 0.23;
                this.instance_17._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(561).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(228));

                // 81
                this.instance_18 = new lib.Symbol42nr7();
                this.instance_18.setTransform(242.5, 235.4);
                this.instance_18.alpha = 0.23;
                this.instance_18._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(583).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(206));

                // 80
                this.instance_19 = new lib.Symbol42nr6();
                this.instance_19.setTransform(242.5, 235.4);
                this.instance_19.alpha = 0.23;
                this.instance_19._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(605).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(184));

                // Lager 23
                this.instance_20 = new lib.Symbol45("synched", 0);
                this.instance_20.setTransform(240, 240);
                this.instance_20.alpha = 0;
                this.instance_20._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(6).to({
                    startPosition: 0,
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 57).wait(748));

                // Bakgrund
                this.instance_21 = new lib.ID_bakgrund_orange();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance_21
                    }]
                }).wait(811));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            // symbols:
            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol45 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f().s("#FFFFFF").ss(20, 0, 0, 4).p("ARIwZMgiPAAAMAAAAgzMAiPAAAg");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-109.5, -104.9, 219.2, 210);


            (lib.Symbol42nr9 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjpHIQhPhGgLiHICvgRQAeCCBXAAQCGAAATkiQhRBAhrAAQhxAAhRhVQhQhUAAiUQAAiVBWhiQBXhhCaAAQCpAABdB5QBcB5AAEZQAAIMl1AAQh6AAhPhEgAhnk7QgfAtAABOQAABIAgAwQAgAwBBAAQA6AABFguIAAgLQAAh2glhRQglhRhLAAQgvAAgdAug");
                this.shape.setTransform(-0.1, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


            (lib.Symbol42kopia = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AgDGGQhaiIAAj+QAAj4BYiJQBViKCvAAQCvAABXCJQBZCIgBD6QABD/hbCHQhaCGiqAAQiqAAhYiGgACkkJQgbBdAACsQABCxAbBbQAbBbA+AAQBOAAAUh9QAVh8AAhuQAAiwgbhbQgbhbhAAAQg/AAgcBdgApUH/IAAijICNAAIAAqyIiVAAIAAikIF1AAIAANWICFAAIAACjg");
                this.shape.setTransform(4.9, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-55.7, -49.6, 121.2, 105);


            (lib.Symbol38 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AkKG3QhZhVAAiCQAAizC1hCQiWhVAAiWQAAh3BXhKQBYhKCVAAQCdAABVBJQBUBKAAB0QAACZiTBWQCxBBAAC5QAACHhbBQQhbBQivAAQiwAAhZhVgAhgBcQgnAsAABJQAABIAmArQAnApA7AAQA9AAAngpQAngrAAhIQAAhHgmgtQgngtg+AAQg6AAgnAsgAhUlKQgiAjAABFQAAA5AgAmQAhAmA1AAQA2AAAhgmQAhglAAg9QAAg8gfgmQgggmg5AAQgyAAgiAjg");
                this.shape.setTransform(0.1, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-35.5, -49.6, 71.4, 105);


            (lib.Symbol37 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AkVH9IAAikICBAAQAGiUBOjOQBLjQBfiEIkEAAIAACPIijAAIAAkuIJ7AAIAAB9QhNBchPDmQhPDkAACyICBAAIAACkg");
                this.shape.setTransform(0.6, 3.1);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-31.2, -47.8, 63.7, 101.9);


            (lib.Symbol36 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("Aj4GRQhbh9ABkKQgBkaBnh9QBmh+CuAAQD+AAAgEPIivAQQgeiChZAAQhFAAgnBSQgoBQgCCDQBHhDBrAAQB2AABQBRQBRBQAACaQAACshVBZQhWBYiNAAQi4AAhbh7gAh2BFQADCpAmA+QAjA+BBAAQAxAAAegsQAegsAAhWQABhRgkgpQglgpg0AAQg/AAg/Asg");
                this.shape.setTransform(0.6, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-33.3, -49.6, 68, 105);


            (lib.Symbol35 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjzG2QhVhNgTiEIDBgXQATCSB0AAQBAABAjgtQAjgtAAhRQABi1iDAAQhKAAgwBQIikhRIAeoCII6AAIAADvIiUAAIgPhMIkEAAIgND0QBQg5BbAAQCWAABSBhQBSBfAACLQAACUhYBlQhYBjixAAQiZAAhVhNg");
                this.shape.setTransform(-1.2, 3.7);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36, -47.8, 69.7, 103.2);


            (lib.Symbol34 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("Ag0IGIAAiWIBvAAIAAiDIl0AAQgahOgcg0IG0pwIDIAAIAAJnIBjAAIAACLIhjAAIAACDIBWAAIAACWgAjABiID7AAIAAlhg");
                this.shape.setTransform(-1.5, 2.2);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38.4, -49.6, 73.8, 103.7);


            (lib.Symbol33 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlbDIIDEgQQAECzCDAAQA5AAAmgrQAlgrAAhDQAAhBgkgrQglgshHAAIguABIAAipQBWAAAngnQAngnAAg5QAAgygdgfQgfggguAAQhtAAAACGIi+gOQAFiLBXhJQBXhJCFAAQCUAABUBIQBSBHABB9QgBCiiXBFQC8A8AAC2QAACQhhBRQhiBRiVAAQlGAAgZlEg");
                this.shape.setTransform(-1.2, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36, -49.6, 69.7, 105);


            (lib.Symbol32 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlJIGQAAiGAWhJQAVhJAyhFQAxhFBZhSQByhpAmgxQAkgzAAg/QAAgvgcgeQgcgfgnAAQg9AAgdAzQgdAygFBxIi7gJQAAiTAshLQAthKBLgiQBMgiBWAAQCSAABTBRQBTBRAAB4QAABAgZA3QgaA2glAmQgoAihhBGQiQBogmA+QglA+gHAuIEoAAIAAiEICjAAIgJEog");
                this.shape.setTransform(-1.1, 2.2);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 66.1, 103.7);


            (lib.Symbol31 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjzH9IAAikICNAAIAAqyIiWAAIAAijIF0AAIAANVICFAAIAACkg");
                this.shape.setTransform(0.1, 3.1);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-25.2, -47.8, 50.7, 101.9);


            (lib.Symbolnr9 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol42nr9();

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


            (lib.Symbol42nr8 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol38("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-37.5, -49.6, 71.4, 105);


            (lib.Symbol42nr7 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol37("synched", 0);
                this.instance.setTransform(0.5, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-30.7, -47.8, 63.7, 101.9);


            (lib.Symbol42nr6 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol36("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-35.3, -49.6, 68, 105);


            (lib.Symbol42nr5 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol35("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38, -47.8, 69.7, 103.2);


            (lib.Symbol42nr4 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol34("synched", 0);
                this.instance.setTransform(-3.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-42.4, -49.6, 73.8, 103.7);


            (lib.Symbol42nr3 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol33("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38, -49.6, 69.7, 105);


            (lib.Symbol42nr2 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol32("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36.2, -49.6, 66.1, 103.7);


            (lib.Symbol42nr1 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol31("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-27.2, -47.8, 50.7, 101.9);


            (lib.Symbol41 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol42kopia();
                this.instance.setTransform(-4.2, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-60, -49.6, 121.2, 105);

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});



        return lib.film_countdown02;
    }
])


.service('animationService', ['$rootScope', '$injector',
    function($rootScope, $injector) {
        //var canvas, stage, exportRoot,player;
        var stage, manifest;
        var resizeCanvas = function() {
            // browser viewport size
            var w = window.innerWidth;
            var h = window.innerHeight;

            // stage dimensions
            var ow = 480; // your stage width
            var oh = 480; // your stage height

            var keepAspectRatio = true;

            if (keepAspectRatio) {
                // keep aspect ratio
                var scale = Math.min(w / ow, h / oh);
                stage.scaleX = scale;
                stage.scaleY = scale;

                // adjust canvas size
                stage.canvas.width = ow * scale;
                stage.canvas.height = oh * scale;
            } else {
                // scale to exact fit
                stage.scaleX = w / ow;
                stage.scaleY = h / oh;

                // adjust canvas size
                stage.canvas.width = ow * stage.scaleX;
                stage.canvas.height = oh * stage.scaleY;
            }

            // update the stage
            stage.update()
        }
        var animationFile;

        var animationCompleted = function() {
            $rootScope.$broadcast('audio.ended');
        }
        window.as = this;

        window.addEventListener('resize', resizeCanvas);

        this.init = function(animationPackage, manifest) {
            this.canvas = document.getElementById("canvas");
            this.images = images || {};

            // this.manifest = [
            // 	{src:"images/ID_bakgrund_blue.jpg", id:"ID_bakgrund_blue"},
            // 	{src:"images/ID_bakgrund_green.jpg", id:"ID_bakgrund_green"},
            // 	{src:"images/ID_bakgrund_orange.jpg", id:"ID_bakgrund_orange"},
            // 	{src:"images/stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720.jpg", id:"stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720"},
            // 	{src:"images/under_Background.jpg", id:"under_Background"},
            // 	{src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"Andas_djupt_ovning_med_intro"},
            // 	{src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"Rakna_baklanges_ovning_med_intro"},
            // 	{src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"Skona_tanken_ovning_med_intro"}
            // ];

            this.manifest = manifest;

            animationFile = animationPackage;


            var loader = new createjs.LoadQueue(false);
            loader.installPlugin(createjs.Sound);
            loader.addEventListener("fileload", this.handleFileLoad);
            loader.addEventListener("complete", this.handleComplete);
            loader.loadManifest(this.manifest);
        };

        this.handleFileLoad = function(evt) {
            if (evt.item.type == "image") {
                this.images[evt.item.id] = evt.result;
            }
        }

        this.handleComplete = function() {
            // this.exportRoot = new lib.film_deep_breath02();
            this.exportRoot = new animationFile();

            stage = new createjs.Stage(this.canvas);
            stage.addChild(this.exportRoot);
            stage.update();
            window.st = stage;

            createjs.Ticker.setFPS(12);
            createjs.Ticker.addEventListener("tick", stage);
            resizeCanvas();
        }

        this.playSound = window.playSound = function(id, loop) {

            this.sound_id = id;


                
                console.log(id);
                my_media = new Media("file:///android_asset/www/"+id,
                        // success callback
                         function () { console.log("playAudio():Audio Success"); },
                        // error callback
                         function (err) { console.log("playAudio():Audio Error: " + err); }
                );
                       // Play audio
                my_media.play();
           
               
           

            this.player = createjs.Sound.play(id, createjs.Sound.INTERRUPT_EARLY, 0, 0, loop);
            this.player.addEventListener("complete", animationCompleted);
            return player;
        }

        this.destroy = function() {
            try {
                if(!!my_media){
                    my_media.stop();
                    my_media.release();
                }
                stage.removeAllChildren();
                createjs.Sound.removeAllEventListeners();
                createjs.Sound.removeAllSounds();
            } catch (e) {
                console.error(e);
            }

        }

        this.startAnimation = function(animationType, lib) {
            manifest = [
                                  //{src:"images/ID_bakgrund_blue.jpg", id:"ID_bakgrund_blue"},
                                  //{src:"images/ID_bakgrund_green.jpg", id:"ID_bakgrund_green"},
                                  //pos{src:"images/ID_bakgrund_orange.jpg", id:"ID_bakgrund_orange"},
                                  //{src:"images/stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720.jpg", id:"stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720"},
                                  //{src:"images/under_Background.jpg", id:"under_Background"},
                                  {src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"sounds/Andas_djupt_ovning_med_intro.mp3"},
                                  {src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"sounds/Rakna_baklanges_ovning_med_intro.mp3"},
                                  {src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"sounds/Skona_tanken_ovning_med_intro.mp3"}
                              ];

            var animationpackageLib;
            console.log(animationType);
            switch (animationType) {
                case 3: // takna green






                    // manifest = [];

                    // manifest.push({
                    //     src: "sounds/Skona_tanken_ovning_med_intro.mp3",
                    //     id: "Skona_tanken_ovning_med_intro"
                    // });
                    // manifest.push({
                    //     src: "images/ID_bakgrund_green.jpg",
                    //     id: "ID_bakgrund_green"
                    // });

                    //animationPackage = lib2.film_nice_thought;
                    animationPackage = $injector.get('film_nice_thought');

                    break;

                case 1: //countdown rakna (orange)

                    // manifest = [];

                    // manifest.push({
                    //     src: "images/ID_bakgrund_orange.jpg",
                    //     id: "ID_bakgrund_orange"
                    // });

                     // manifest.push({src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"Rakna_baklanges_ovning_med_intro"});
                   

                    // animationPackage = lib3.film_countdown02;
                   	animationPackage = $injector.get('film_countdown02');
                    //animationPackage = lib3.film_countdown02;

                    break;

                case 2: //andas (blue) //film_deep_breath02
                    //default:
                    console.log("Called");
                    // manifest = [];
                    // manifest.push({
                    //     src: "images/ID_bakgrund_blue.jpg",
                    //     id: "ID_bakgrund_blue"
                    // });
                    // manifest.push({
                    //     src: "sounds/Andas_djupt_ovning_med_intro.mp3",
                    //     id: "Andas_djupt_ovning_med_intro"
                    // });
                    // animationPackage = lib1.film_deep_breath02;
                    animationPackage = $injector.get('film_deep_breath02');

                    break;

            }

            this.init(animationPackage, manifest);

        }
        //
        // window.onresize = function()
        // {
        //      this.onResize();
        // }

    }
]);