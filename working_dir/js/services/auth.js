
SERVICE_MODULE
.service('AuthService', [

    function() {

        var isAuthenticated = false; //false;

        this.setAuthenticated = function(value) {
            isAuthenticated = value;
        };

        this.isAuthenticated = function() {
            //return true;
            return isAuthenticated;
        };

    }
])

.factory('AuthenticationService', ['$rootScope', 'WEBSERVICE_BASE_URI', '$http', 'BUPUtility', 'authService', 'BUPLoader', 'BUPUser', 'BUPConfig', 'BUPWebservice', '$httpBackend','CordovaNetworkAsync',
    function($rootScope, WEBSERVICE_BASE_URI, $http, BUPUtility, authService, BUPLoader, BUPUser, BUPConfig, BUPWebservice, $httpBackend,CordovaNetworkAsync) {


        // var tasks = window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) || {
        //     practices: [],
        //     homeworks: []
        // };

        
        var service = {
            login: function(user,loginOption) {
                CordovaNetworkAsync.isOnline().then(function(isConnected) {
                    if (isConnected) {

                       //force remote login
                        service.doLogin(user,{forceRemoteLogin: true })


                    } else {
                        //normal login in which local login has the first preverence
                        service.doLogin(user,{forceRemoteLogin: false })
                    }

                }, function(err) {
                    console.log(err);
                });

            },
            doLogin: function(user, loginOption) {
                loginOption = loginOption || {
                    forceRemoteLogin: false //parameter to avoid trying to authenticate user locally 
                };

                console.log('authentication started')
                ///api/login
                ///
                console.log(user);

                if (!user.Username && !user.UserPassword) {

                    return false;
                }

                //make sure the user info exists locally
                //ie if the user has previously logged in
                var localUser = BUPUser.getCurrentUser();


                //use cases for local login and remote login
                //Go for local login when
                //1. informations like local user id username token are available
                //2. the username with which user is trying to login is available locally

                //Go for remote login when
                //1. when the username the user is trying to login is not avilable locally

                var tryLocalLogin = false;

                if (loginOption.forceRemoteLogin === false && !!localUser.id && !!localUser.username && !!localUser.tokenKey) {
                    tryLocalLogin = true;
                }

                if (tryLocalLogin === true) {
                    //more conditions to consider remote login 

                    // someone previouslylogged in but trying to login as a different user this time
                    if (String(user.Username).toLowerCase() !== String(localUser.username).toLowerCase()) {
                        tryLocalLogin = false;
                    }
                }


                //if (con)

                if (tryLocalLogin === true) {
                    console.log("loggin in locally");
                    //data available locally!
                    //do not require remote login ie can login without internet Internet

                    //sample localUser Object
                    // var curUser = {
                    //     id: data.UserId,
                    //     name: data.data.FullName,
                    //     username: data.data.UserName,
                    //     password: BUPUtility.encode(user.UserPassword),
                    //     roleId: data.data.RoleId,
                    //     roleName: data.data.RoleName,
                    //     tokenKey: data.TokenKey,
                    //     saveDataInServer: data.data.saveDataInServer,
                    //     isLoggedIn: true
                    // };
                    if (String(user.Username).toLowerCase() !== String(localUser.username).toLowerCase()) {
                        var errorMessage = "Invalid username";
                        //BUPInteraction.toast(errorMessage, 'short');
                        $rootScope.$broadcast('event:auth-login-failed', errorMessage);
                        return false;
                    } else if (user.UserPassword !== BUPUtility.decode(localUser.password)) {
                        var errorMessage = "Invalid password";
                        //BUPInteraction.toast(errorMessage, 'short');
                        $rootScope.$broadcast('event:auth-login-failed', errorMessage);
                        return false;
                    } else {
                        //local login successful


                        localUser.isLoggedIn = true;
                        BUPUser.setCurrentUserInfo(localUser);

                        $rootScope.currentUser = localUser;
                        $rootScope.currentUserConfig = BUPConfig.initUserConfig(localUser.id);

                        $http.defaults.headers.common.Authorization = localUser.tokenKey;

                        authService.loginConfirmed(localUser, function(config) { // Step 2 & 3
                            //config.headers.Authorization = data.user.authorizationToken;
                            config.headers.Authorization = localUser.tokenKey;
                            return config;
                        });


                    }



                } else {
                    console.log("loggin in remotely");

                    var params = BUPUtility.buildQuery(user);
                    $http.get(WEBSERVICE_BASE_URI + '/Login/ValidateUser?' + params, {
                        ignoreAuthModule: true
                    })
                        .success(function(data, status, headers, config) {
                            console.log(data);

                            if (data.status == "ok" && !!data.TokenKey) {
                                //$http.defaults.headers.common.Authorization = data.user.authorizationToken; // Step 1
                                $http.defaults.headers.common.Authorization = data.TokenKey; // Step 1

                                /*
                        Sample data obtained from the webservice:

                        {
                            TokenKey: "<tokenkey>",
                            UserId: "<userid>",
                            data:{
                                UserName:"Patient21",
                                saveDataInServer:'boolean true/false'
                            },
                            status: "ok"
                        }

                        */


                                var curUser = {
                                    id: data.UserId,
                                    name: data.data.FullName,
                                    username: data.data.UserName,
                                    password: BUPUtility.encode(user.UserPassword),
                                    roleId: data.data.RoleId,
                                    roleName: data.data.RoleName,
                                    tokenKey: data.TokenKey,
                                    saveDataInServer: data.data.saveDataInServer,
                                    useMeeting:Boolean(data.data.useMeeting),
                                    isLoggedIn: true
                                };

                                //FullName: "Patient 21"
                                //RoleId: 3
                                //RoleName: "Patient"
                                //UserName: "patient21"
                                //saveDataInServer: false



                                //BUPUser.setCurrentUserInfo(data.user);
                                BUPUser.setCurrentUserInfo(curUser);

                                $rootScope.currentUser = curUser;
                                $rootScope.currentUserConfig = BUPConfig.initUserConfig(curUser.id);


                                //$rootScope.currentUser = data.user;

                                // Need to inform the http-auth-interceptor that
                                // the user has logged in successfully.  To do this, we pass in a function that
                                // will configure the request headers with the authorization token so
                                // previously failed requests(aka with status == 401) will be resent with the
                                // authorization token placed in the header
                                authService.loginConfirmed(curUser, function(config) { // Step 2 & 3
                                    //config.headers.Authorization = data.user.authorizationToken;
                                    config.headers.Authorization = data.TokenKey;

                                    return config;
                                });

                                console.log("here");
                            } else {
                                console.log(data);
                                console.log("invalid user or token key not obtained");
                                $rootScope.$broadcast('event:auth-login-failed', status);
                            }

                        })
                        .error(function(data, status, headers, config) {
                            console.log('login failed?');
                            BUPLoader.hide();
                            console.log(data);
                            console.log(status);
                            console.log(headers);
                            $rootScope.$broadcast('event:auth-login-failed', status);
                        });
                }




                //end of locally authenticate

            },
            logout: function(user) {
                // $http.post('https://logout', {}, {
                //     ignoreAuthModule: true
                // }).
                // finally(function(data) {
                //     delete $http.defaults.headers.common.Authorization;
                //     $rootScope.$broadcast('event:auth-logout-complete');
                // });

                // $http.post('https://logout', {}, {
                //     ignoreAuthModule: true
                // })
                //     .
                // finally(function(data) {
                //     delete $http.defaults.headers.common.Authorization;
                //     $rootScope.$broadcast('event:auth-logout-complete');
                // });

                //delete the authentication token
                delete $http.defaults.headers.common.Authorization;
                $rootScope.$broadcast('event:auth-logout-complete');
                //remove the current user details

                var oldData = BUPUser.getCurrentUser();
                oldData.isLoggedIn = false;
                BUPUser.setCurrentUserInfo(oldData);

                //remove the old tasks
                //BUPWebservice.setTasks({});

            },
            loginCancelled: function() {
                authService.loginCancelled();
            },
            loggedIn: function() {

            },
            invalidateCurrentToken: function() {
                var oldData = BUPUser.getCurrentUser();
                oldData.tokenKey = '';
                BUPUser.setCurrentUserInfo(oldData);
            }
        };

        return service;

    }
])

.run(['$rootScope', '$state', 'AuthenticationService',  'BUPUser',  
    function($rootScope, $state, AuthenticationService, BUPUser ) {

        $rootScope.user = {};

        $rootScope.signIn = function() {
            AuthenticationService.login($rootScope.user);
        };
       
       $rootScope.$on('$stateChangeStart', function(event, curr, prev) {

            if (
                curr.requireLogin !== false 
                && 
                //!AuthService.isAuthenticated()
                 !BUPUser.isLoggedIn()
                ) {
              
                //console.log(BUPUser);
                //console.log(BUPUser.isLoggedIn());
                //console.log(curr.requireLogin);
                //console.log($rootScope.currentUser);

                 //$state.transitionTo('signin');
                 //console.debug('login required to access this page')
                 $rootScope.$broadcast('event:auth-loginRequired');
                 //BUPInteraction.toast('Login Required','short');
                 event.preventDefault();
            }
        });

        $rootScope.$on('event:auth-loginRequired', function(e, rejection) {
            //.log('event fired: event:auth-loginRequired');
            //BUPLoader.hide();
            $rootScope.$broadcast('loading:hide');
            AuthenticationService.invalidateCurrentToken();
            $state.go('signin');
        });


        $rootScope.$on('event:auth-logout-complete', function() {
            //console.log('event fired: event:auth-logout-complete');
            //console.log('time to refresh');
            
           

            /*
            $state.go('signin', {}, {
                //reload: true,
                //inherit: false
            });
            */
        });


        //  $rootScope.$on('old:event:auth-loginConfirmed', function() {
        //     //console.log('event fired: event:auth-loginConfirmed');
        //     //$scope.username = null;
        //     //$scope.password = null;
        //     // $state.go($state.$current, null, {
        //     //     reload: true
        //     // });
        //     $rootScope.loginModal.hide();

        //     window._a = $rootScope.currentUser = BUPUser.getCurrentUser();

        //     //track the earliest logged in date
        //     //console.log('time to init the config');

        //     $rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);
            
           


            
        // });

        

    }
]);
