SERVICE_MODULE

/**
 * CordovaDialog AngularJS service
 * https://gist.github.com/rvanbaalen/9527418
 * com.apache.cordova.dialogs wrapper for AngularJS with fallback for testing without Cordova
 */
.service('CordovaDialog', ['$window', '$q',
    function($window, $q) {
        var notification = navigator.notification || {
            alert: function(message, alertCallback, title, buttonLabel) {
                return alertCallback($window.alert(title + '\n\n' + message + '\n\nButton: ' + buttonLabel));
            },
            confirm: function(message, confirmCallback, title, buttonLabels) {
                buttonLabels = buttonLabels || ['OK', 'Cancel'];

                return $window.confirm(message);
            },
            prompt: function(message, confirmCallback, title, buttonLabels, defaultValue) {
                return $window.prompt(title + '\n\n' + message + '\n\nButtons:\n' + buttonLabels.join('\n'), defaultValue);
            },
            beep: function(times) {
                var def = $q.defer();

                for (var i = 0; i <= times; i++) {
                    notification.alert('Beep ' + (i + 1));

                    if (i === times) {
                        def.resolve(times);
                    }
                }

                return def.promise;
            }
        };

        return {
            alert: function(message, title, buttonName) {
                var def = $q.defer();
                console.info('Alert triggered');
                var cb = function() {
                    console.info('Alert callback triggered');
                    def.resolve();
                };

                notification.alert(message, cb, title, buttonName);

                return def.promise;
            },

            confirm: function(message, title, buttonName) {
                var def = $q.defer();
                var cb = function(buttonIndex) {
                    switch (buttonIndex) {
                        case 1:
                            // First button is the confirm button
                            def.resolve(true);
                            break;
                        default:
                            def.reject(false);
                    }
                };

                var confirmed = notification.confirm(message, cb, title, buttonName);

                if (confirmed) {
                    def.resolve(true);
                } else {
                    def.resolve(false);
                }

                return def.promise;
            },

            prompt: function(message, title, buttonLabels, defaultValue) {
                var def = $q.defer();
                var cb = function(results) {
                    switch (results.buttonIndex) {
                        case 1:
                            // First button is the confirm button
                            def.resolve(results);
                            break;
                        default:
                            def.reject(false);
                    }
                };

                var confirmed = notification.prompt(message, cb, title, buttonLabels, defaultValue);

                if (confirmed && confirmed !== null) {
                    def.resolve(true);
                } else {
                    def.resolve(false);
                }

                return def.promise;
            },

            beep: function(times) {
                return notification.beep(times);
            }
        };
    }
])

;
        /**
         * @todo
         * research more on sounds
         * seems we got more robust libraries try best not to reinvent the wheel
         * try these
         * soundmanager2 ( looks like tries to break cross device incompativilites and bugs)
         * (good news we also have angular directive to this https://github.com/saygoweb/ng-soundmanager2)
         * www.schillmania.com/projects/soundmanager2
         */ 
        /**
         * html5 audio properties
         *
         * currentTime
         * duration
         * muted
         * paused
         * volume
         *
         */

        /*
.factory('BUPAudio', ['$document',
    function($document) {

        //reference http://html5doctor.com/native-audio-in-the-browser
        var audioElement = $document[0].createElement('audio');
        //supportsHtml5Audio = !! audioElement.canPlayType,
        //supportsMp3 = supportsHtml5Audio && "" != audioElement.canPlaytype("audio/mpeg")
        ;




        var seekbar = $document[0].getElementById('seekbar');

        //setup seekbar when the new sound is loaded (duration is changed)
        audioElement.ondurationchange = this.setupSeekbar;

        //when seekbar is updated reflect in the audio
        //seekbar.onchange = this.seekAudio;

        //as the audio goes on playing update the seekbar
        //audioElement.ontimeupdate = this.updateSeekbarPosition;





  
        return {
            audioElement: audioElement,
            setupSeekbar: function() {
                //seekbar.max = audioElement.duration;
            },
            seekAudio: function() {
                //audioElement.currentTime = seekbar.value;
            },
            updateSeekbarPosition: function() {
                console.log(seekbar.value);
                //seekbar.value = audioElement.currentTime;
            },
            load: function(filename) {
                audioElement.src = filename;
                audioElement.load(); //required for 'older' browsers



            },

            play: function(filename) {

                audioElement.play();
            },
            pause: function() {
                console.log("pausing todo");
                audioElement.pause();
                //audioElement.pause();
            }

        };
    }
])
*/
