var DynamicTextClass = function(entry) {
    //the constructor function
    this.code = entry.Code;
    this.modified = entry.ModifyDateTime;
    this.key = entry.OverallTextName;
    this.value = entry.OverallTextDescription;
    return this;
};

SERVICE_MODULE

.value('DynamicTextClass', DynamicTextClass)
/**
 * Dynamic Text Service
 *
 * This service will get the different dynamic text from the server through a webservice
 * and serve the text based on a key
 *
 * This can have some fallback text in case the text has not been downloaded yet
 *
 * Dependency
 *  1. $rootScope.isOnline var to know if there is internet connection or not
 *  2.
 */

.factory('DynamicTextCache', ['$cacheFactory',
    function($cacheFactory) {
        return $cacheFactory('DynamicText');
    }
])


.factory('DynamicTextService', ['$q', '$http', 'DynamicTextCache', 'WEBSERVICE_BASE_URI', 'BUPUtility', 'CordovaNetworkAsync', '$rootScope', 'DynamicTextClass',
    function($q, $http, DynamicTextCache, WEBSERVICE_BASE_URI, BUPUtility, CordovaNetworkAsync, $rootScope, DynamicTextClass) {
        //var entries;    
        var service = {

            getAllFromLocalDB: function() {

                var entries = [];
                try {
                    entries = JSON.parse(window.localStorage['dynamicTexts']);
                } catch (e) {

                }
                return entries;
            },

            saveToLocalDB: function(entriesToSave) {
                window.localStorage['dynamicTexts'] = JSON.stringify(entriesToSave);
            },

            /**
             * [loadToCache description]
             * @param entries optonal if supplied save the same object to cache
             */
            loadToCache: function(entries) {

                var entries = entries || service.getAllFromLocalDB();
                for (var i = 0; i < entries.length; i++) {
                    /**
                     * eg
                     * WELCOME_SCREEN_TEXT_KEY: 'title as seen on the admin'
                     * WELCOME_SCREEN_TEXT: <actual description>
                     */
                    DynamicTextCache.put(entries[i]['code'] + '_KEY', entries[i]['key']);
                    DynamicTextCache.put(entries[i]['code'], entries[i]['value']);

                }
                console.debug('dynamic texts loaded to cache');

            },

            mergeRecentlyUpdatedTexts: function(recentlyUpdated) {
                if (!recentlyUpdated.length) return;

                var allEntries = service.getAllFromLocalDB();
                if (allEntries.length) {
                    angular.forEach(recentlyUpdated, function(freshEntry) {
                        // var index = 
                        var index = _.findIndex(allEntries, function(elem) {
                            return typeof elem['code'] !== "undefined" && elem['code'] === freshEntry['code'];
                        });
                        if (index > -1) {
                            allEntries[index] = freshEntry;
                        } else {
                            allEntries.push(freshEntry)
                        }
                    })
                } else {
                    allEntries = recentlyUpdated;
                }
                service.loadToCache(allEntries);
                service.saveToLocalDB(allEntries);

            },

            getLastModifiedDate: function() {
                var lastModified = 0; //lastResort
                var entries = service.getAllFromLocalDB();
                if (entries.length) {
                    var lastModifiedEntry = _.max(entries, function(entry) {
                        return parseInt(entry.modified, 10);
                    });
                    if (lastModifiedEntry.modified) {
                        lastModified = lastModifiedEntry.modified;
                    }
                }
                return lastModified;

            },

            fetchFromServer: function(all) {
                all = all || false;

                var deferred = $q.defer();
                CordovaNetworkAsync.isOnline()
                    .then(function(isConnected) {
                        if (isConnected) {
                            var lastModifiedDate = (all === false) ? service.getLastModifiedDate() : 0;
                            //@todo calculate lastModifiedDate

                            var webserviceurl = WEBSERVICE_BASE_URI + '/Login/OverallText?logdatetime=' + lastModifiedDate;
                            console.log('dynamic text fetching started');
                            $http.get(webserviceurl).then(function(rawResponse) {
                                if (rawResponse.data && rawResponse.data.status == "ok" && rawResponse.data.data && rawResponse.data.data.length) {
                                    var length = rawResponse.data.data.length;
                                    var entries = [];
                                    for (var i = 0; i < length; i++) {
                                        var entry = new DynamicTextClass(rawResponse.data.data[i]);
                                        entries.push(entry);
                                    }
                                    service.mergeRecentlyUpdatedTexts(entries);
                                    deferred.resolve(entries);
                                    console.log(length + ' updated dynamic texts found');

                                } else {
                                    deferred.reject('no new texts');
                                    console.log(' No new updated dynamic texts found');
                                }

                            });
                        } else {
                            deferred.reject('No internet to pull the dynamic texts');
                            console.log('dynamic texts fetching aborted: no internet');
                        }
                    });


                return deferred.promise;


            },

            getFallbackDynamicText: function(code) {
                var fallbacks = [{
                    "code": "ABOUT_THE_APP",
                    "key": "Om",
                    "value": "<p>Hemuppgiften underlättar din egen träning och repetition när du har en kontakt med behandlare inom Barn- och ungdomspsykiatrin (BUP) i Stockholm.<br> <br>I Hemuppgiften kan du: </p> <ul><li>Träna på & nbsp;nya beteenden </li> <li>Repetera innehåll eller övningar som du gått igenom med din behandlare </li> <li>Bli påmind om när du ska träna eller gå igenom hemuppgifter </li></ul><p><br>För att kunna använda det här verktyget behöver du användarnamn och lösenord som du får från din behandlare. </p>"
                }, {
                    "code": "WELCOME_SCREEN_TEXT",
                    "modified": 635511115519000000,
                    "key": "Welcome",
                    "value": "<h3 style=\"margin: 0px; line-height: 150%; font-family: arial; font-size: 17px;\">\n\t<strong>V&auml;lkommen!</strong></h3>\n<p>\n\tInneh&aring;llet i den h&auml;r appen kommer att ut&ouml;kas efter varje tr&auml;ff. Efter n&aring;gra g&aring;nger kommer du att kunna tr&auml;na p&aring; &rdquo;ilsked&auml;mpare&rdquo; som ska ligga &ouml;verst som tre knappar. Hemuppgifterna baseras p&aring; ditt eget inneh&aring;ll. Inst&auml;llningar f&ouml;r p&aring;minnelser g&ouml;r du p&aring; v&aring;ra m&ouml;ten. Ta kontakt om du har en fr&aring;ga eller om du vill &auml;ndra p&aring; n&aring;got inneh&aring;ll. N&auml;r en ny hemuppgift lagts in beh&ouml;ver du l&auml;sa igenom alla steg och i sista steget intygar du om texten st&auml;mmer eller inte.</p>\n"
                }, {
                    "code": "CORRECT_TEXT",
                    "modified": 634789912742366700,
                    "key": "Agree",
                    "value": "<p>  <strong>Ditt svar: </strong>St&auml;mmer</p> <p>  Nu &auml;r du klar med hemuppgiften, uppgifterna kommer att finnas kvar och du kan l&auml;sa igenom det n&auml;r du vill.</p> <p>  Efter n&auml;sta m&ouml;te kommer en ny hemuppgift.</p> "
                }, {
                    "code": "INCORRECT_TEXT",
                    "modified": 634807473868366700,
                    "key": "Do not agree",
                    "value": "<p>  <strong>Ditt svar: </strong>St&auml;mmer inte</p> <p>  Vid n&auml;sta m&ouml;te kan du &auml;ndra texten med din kontaktperson.</p> <p>  Nu &auml;r du klar med hemuppgiften, uppgifterna kommer att finnas kvar och du kan l&auml;sa igenom det n&auml;r du vill.</p> <p>  Efter n&auml;sta m&ouml;te kommer en ny hemuppgift.</p> "
                }, {
                    "code": "QUESTION_AT_THE_END",
                    "modified": 634807385954766700,
                    "key": "Ask at the end",
                    "value": "<p>  St&auml;mmer beskrivningen med<br />  det som vi gick igenom?</p> "
                }, {
                    "code": "FIRST_REMINDER_TEXT",
                    "modified": 634807388696000000,
                    "key": "first reminder ",
                    "value": "Det vi pratade om sist finns nu i din app. Klar att godkänna."
                }, {
                    "code": "SECOND_REMINDER_TEXT",
                    "modified": 635469797831200000,
                    "key": "second reminder ",
                    "value": "Det vi pratade om sist finns nu i din app. Klar att godkänna.."
                }, {
                    "code": "THIRD_REMINDER_TEXT",
                    "modified": 634807389278966700,
                    "key": "third reminder ",
                    "value": "Det vi pratade om sist finns nu i din app. Klar att godkänna."
                }, {
                    "code": "HOMEWORK_MESSAGE1",
                    "modified": 634836817294600000,
                    "key": "first homework ",
                    "value": "<p>  <strong>G&ouml;r den f&ouml;rsta hemuppgiften!</strong></p> <p>  Det h&auml;r &auml;r den f&ouml;rsta hemuppgiften, l&auml;s igenom och kontrollera att allt st&auml;mmer.</p> <p>  　</p> <p>  　</p> "
                }, {
                    "code": "FEEDBACK_TEXT1",
                    "modified": 634837268705533300,
                    "key": "Feedback text 5 percent ",
                    "value": "<p>  Bra att du har kommit ig&aring;ng!</p> "
                }, {
                    "code": "FEEDBACK_TEXT2",
                    "modified": 634872623291266700,
                    "key": "Feedback text 10 percent ",
                    "value": "<p>  Bra! Forts&auml;tt tr&auml;na!</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE2",
                    "modified": 634858907587766700,
                    "key": "anger Suppressors ",
                    "value": "<p>  <strong>Dags att b&ouml;rja tr&auml;na</strong>!</p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na och n&auml;r du vill ha p&aring;minnelser kom vi &ouml;verens om vid senaste tr&auml;ffen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE3",
                    "modified": 634807475208200000,
                    "key": "Homework message3 ",
                    "value": "<p>  <strong>3 Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om p&aring; v&aring;ra m&ouml;ten. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE4",
                    "modified": 634776632726266600,
                    "key": "Homework message4 ",
                    "value": "<p>  <strong>4 Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HELP_TEXT",
                    "modified": 635511115273300000,
                    "key": "If the app (in the symbol key) ",
                    "value": "<p>\n\ttest new fdfdf</p>\n"
                }, {
                    "code": "REMINDER",
                    "modified": 634775831397200000,
                    "key": "Reminder Text ",
                    "value": "Dags att träna på dina ilskedämpare."
                }, {
                    "code": "HOMEWORK_MESSAGE5",
                    "modified": 634776480000000000,
                    "key": "Homework message5 ",
                    "value": "<p>  <strong>5 Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE6",
                    "modified": 634776638625000000,
                    "key": "Homework message6",
                    "value": "<p>  <strong>6. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE7",
                    "modified": 634776638755799900,
                    "key": "Homework message7",
                    "value": "<p>  <strong>7. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE8",
                    "modified": 634776638854999900,
                    "key": "Homework message8",
                    "value": "<p>  <strong>8. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE9",
                    "modified": 634776638953133300,
                    "key": "Homework message9",
                    "value": "<p>  <strong>9. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE10",
                    "modified": 634776639044700000,
                    "key": "Homework message10",
                    "value": "<p>  <strong>10. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "FEEDBACK_TEXT3",
                    "modified": 634872623630333300,
                    "key": "Feedback text 35 percent ",
                    "value": "<p>  Bra! Nu har du tr&auml;nat en tredjedel!</p> "
                }, {
                    "code": "FEEDBACK_TEXT4",
                    "modified": 634872623985033300,
                    "key": "Feedback text 60 percent ",
                    "value": "<p>  Tv&aring; tredjedelar! Bra jobbat!</p> "
                }, {
                    "code": "THIS_IS_TEST",
                    "modified": 635422906267500000,
                    "key": "this is test",
                    "value": "<p>\n\tthis is test</p>\n"
                }, {
                    "code": "FOURTH",
                    "modified": 635469832776600000,
                    "key": "Fourth",
                    "value": "this is fourth reminder"
                }, {
                    "code": "FEEDBACK_TEXT_70_PERCENT",
                    "modified": 635469833768700000,
                    "key": "Feedback text 70 Percent",
                    "value": "<p>\n\tgood going</p>\n"
                }, {
                    "code": "FEEDBACK_TEXT_100_PERCENT_",
                    "modified": 635469834439600000,
                    "key": "Feedback text 100 percent ",
                    "value": "<p>\n\tBra! Nu har du tr&auml;nat klart! Stapeln &auml;r full!</p>\n"
                }];

                var fallbackEntries = _.filter(fallbacks, function(fallbackEntry) {
                    return fallbackEntry.code == code;
                });
                if (fallbackEntries.length) {
                    return fallbackEntries[0];
                } else {
                    console.log('not found in fallback');
                    return {};
                }
            },

            getTitle: function(code) {
                if (!code.length) {
                    console.error('Invalid dynamic text code');
                    return '';
                }
                var internalCode = code + '_KEY';
                var title = DynamicTextCache.get(internalCode);
                if (title) {
                    console.log('found' + title);
                    return title;
                } else {
                    console.log('dynamic text looking for fallback for CODE :'+code)
                    var fallbackEntry = service.getFallbackDynamicText(code);
                    return fallbackEntry.key;

                }

            },

            getContent: function(code) {
                if (!code.length) {
                    console.error('Invalid dynamic text code');
                    return '';
                }
                var description = DynamicTextCache.get(code);
                if (description) {
                    return description;
                } else {
                    var fallbackEntry = service.getFallbackDynamicText(code);
                    return fallbackEntry.value;
                }

            },

        }
        window.dt = service;
        window.dc = DynamicTextCache;
        return service;
    }
])

.run(['DynamicTextService', '$rootScope', '$ionicPlatform','$ionicModal', 'CordovaNetworkAsync', 'BUPConfig',
    function(DynamicTextService, $rootScope, $ionicPlatform,$ionicModal, CordovaNetworkAsync, BUPConfig) {

        DynamicTextService.loadToCache();

        var checkForUpdate = function() {
            CordovaNetworkAsync.isOnline()
                .then(function(isConnected) {
                    if (isConnected) {
                        DynamicTextService.fetchFromServer();
                    }
                });
        };

        var showWelcomeMessage = function() {
            $ionicModal.fromTemplateUrl('html/partials/modals/welcome.modal.html', {
                scope: $rootScope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                
                $rootScope.welcomeModal = modal;
                $rootScope.welcomeModal.title = DynamicTextService.getTitle('WELCOME_SCREEN_TEXT');
                $rootScope.welcomeModal.content = DynamicTextService.getContent('WELCOME_SCREEN_TEXT');
                $rootScope.welcomeModal.close = function() {
                    $rootScope.welcomeModal.hide();
                    $rootScope.welcomeModal.remove();
                };
                $rootScope.welcomeModal.show();
                console.log($rootScope.welcomeModal)
                $rootScope.currentUserConfig.welcomed = true;
                BUPConfig.updateCurrentUserConfig($rootScope.currentUserConfig);
            });
        };

        $rootScope.$on('app.userready', function() {
            if ($rootScope.currentUserConfig && $rootScope.currentUserConfig.welcomed !== true) {
                showWelcomeMessage();
            }
            
        });

        $ionicPlatform.ready(function() {
            
            checkForUpdate();
            
            //$rootScope.currentUserConfig = BUPConfig.getCurrentUserConfig();
             


            // if (BUPUser.isLoggedIn()) {

            //     if ($rootScope.currentUserConfig && $rootScope.currentUserConfig.welcomed !== true) {
            //         showWelcomeMessage();
            //     }


            // }else {



            // }


            //console.debug(currentUserConfig);
        });

        $ionicPlatform.on('resume', checkForUpdate);


    }
])