SERVICE_MODULE
.factory('BUPFiles', ['$q', '$rootScope', 'BUPWebservice', '$cordovaFile', 'filesystem', 'BUPUtility',
    function($q, $rootScope, BUPWebservice, $cordovaFile, filesystem, BUPUtility) {
        var allFiles = [];
        if (window.localStorage['_files']) {
            try {
                allFiles = JSON.parse(window.localStorage['_files']);
                console.log(JSON.stringify(allFiles));
                //ionic.Platform.
                //$cordovaFile.createDir('SoundTasks',true);

            } catch (e) {

            }
        }

        try{
            
            if(ionic.Platform.isWebView()) {
                ionic.Platform.ready(function(){
                    $cordovaFile.createDir('SoundTasks',true);
                });
            }
            
            
        }catch(e){
            console.error(e);
        }
        var _private = {};


        window.$cordovaFile = $cordovaFile;

        _private.getFilesystem = function() {
            var q = $q.defer();
            // Note: The file system has been prefixed as of Google Chrome 12:
            window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 1024 * 1024, function(filesystem) {
                    window._filesystem = filesystem;
                    q.resolve(filesystem);
                },
                function(err) {
                    q.reject(err);
                });

            return q.promise;
        };




        //$rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask);

        var service = {
            /**
             * returns object with local and remote url
             * Important note: it doesn't check the local file is present or not
             *
             * @param  {[type]} fileUrl [description]
             * @return {[type]}         [description]
             */
            getFileOld: function(fileUrl) {
                //fileUrl = fileUrl.split(' ').join('_');

                var deferred = $q.defer();
                var promises = [];

                //deferred.resolve('http://109.74.12.180/Hemuppgiften2.0/Uploads/Sound/f2c1271d-ad4a-4f78-8e5d-79cf3dbdfd18_Rakna_baklanges_ovning_med_intro.mp3');
                var files = _.filter(allFiles, function(aFile) {
                    return aFile.remote == fileUrl
                });

                console.log('found files matching with ' + fileUrl + ' are');
                console.log(JSON.stringify(files));

                if (files.length) {

                    var theFile = files[files.length - 1];



                    //also check if the file exists
                    try {

                        // var dir = cordova.file.documentsDirectory || cordova.file.externalRootDirectory;
                        // $cordovaFile.checkFile(dir+theFile.local).then(
                        //     function(){
                        //         //file exists locally
                        //        deferred.resolve(theFile.local);
                        //     },
                        //     function(){
                        //         //file doesn't exists locally
                        //        deferred.resolve(theFile.remote);
                        //        //remove this entry
                        //     }
                        // );

                    } catch (e) {

                        // if we can't be sure the file doesn't exist locally then load it from the remote
                        //deferred.resolve(fileUrl);

                    }
                    deferred.resolve(theFile);


                    
                    if (files.length > 1) {


                        allFiles = _.uniq(allFiles,function(a){
                            return a.remote; 
                        });
                        service.saveToLocalStorage();

                        /*
                        console.log('lots of files!!!')
                        var i = 0;
                        for (i; i < files.length; i++) {
                            _.remove(files, files[i]);
                            console.log('removing ' + files[i].local);
                        }
                        */

                    }
                    
                    


                } else {

                    deferred.resolve({
                        local: null,
                        remote: fileUrl
                    });
                }

                return deferred.promise;

            },
            getFile: function(fileUrl){

                var deferred = $q.defer();
                var responseFile = {
                        local: null,
                        remote: fileUrl
                    };
                if(ionic.Platform.isWebView()) {
                    var dir;
                    if(ionic.Platform.isIOS()){
                        dir = cordova.file.documentsDirectory;
                    }else if(ionic.Platform.isAndroid()){
                        dir = cordova.file.externalRootDirectory;
                    }
                    var fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
                    $cordovaFile.checkFile('SoundTasks/'+fileName)
                    .then(function(){
                        responseFile.local = dir +'SoundTasks/'+ fileName;
                        deferred.resolve(responseFile);
                    },function(){
                        deferred.resolve(responseFile);
                    });

                }else{
                    deferred.resolve(responseFile);
                }
                
                return deferred.promise;

            },
            getFileOrig: function(fileUrl){
                var deferred = $q.defer();
                var fileName = fileURI.substring(fileURI.lastIndexOf('/') + 1);

               window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
               fileSystem.root.getDirectory("SoundTasks", {
                       create: true
               }, function(directory) {

                    var directoryReader = directory.createReader();
                    directoryReader.readEntries(function(entries) {
                        var i;
                        for (i=0; i<entries.length; i++) {
                            console.log(entries[i].name);
                        }
                    }, function (error) {
                        alert(error.code);
                    });

                   } );
                }, function(error) {
                   alert("can't even get the file system: " + error.code);
                });

            },
            saveFile: function(fileURI, saveOptions) {
                var deferred = $q.defer();
                if(!ionic.Platform.isWebView()) {
                    deferred.reject('escape saving in browser' );
                    return deferred.promise;
                }

                console.log('trying to save the file ' + fileURI);
                saveOptions = angular.extend({
                    forceFromServer: false,
                    forceSaveAgain: false
                }, saveOptions);


                //tasks 
                //1 download the file
                //2 save the local path with the corresponding url in the locastorage




                
                window.$cordovaFile = $cordovaFile;
                try {



                    console.log(fileURI);
                    window.fileURI = fileURI;


                    var fileName = fileURI.substring(fileURI.lastIndexOf('/') + 1);

                    var dir = cordova.file.documentsDirectory || cordova.file.externalRootDirectory;
                    var filePath = dir +'SoundTasks/'+ fileName;
                    //var filePath = fileName;




                    if (filePath && saveOptions.forceSaveAgain !== true ) {

                        //BUPUtility.UrlExists(filePath).then(function(resp){
                        service.getFile(fileURI).then(function(resp){
                            console.log(resp);
                            if( resp && resp.local){
                                console.log('url exists response');
                                console.log(resp);
                                console.debug("file: " + filePath + " has already been saved before! No need to save again");
                                deferred.reject('File:' + filePath + ' already exists.'); 
                                return deferred.promise;  

                            }else{
                                //file does not exists
                            }
                            
                            
                        },function(){
                            //alert('file:'+filePath+' already')
                            console.log('file doens\'t exist already so need to download it');
                            //cannot read the file (asume the file does not exist already)
                            //deferred.reject('File:' + filePath + ' already exists.'); 
                            //return deferred.promise;  
                        })

                        
                        

                    }

                    


                    // var files = _.filter(allFiles, function(aFile) {
                    //     return aFile.remote == fileURI
                    // });


                    $cordovaFile.downloadFile(fileURI, filePath, true).then(function(result) {
                        console.log('result of file download here:');
                        console.log(result);

                        //console.log('download complete to url: ' + result.toURL());
                        console.log('download complete to url: ' + result.fullPath);
                        //alert(fileURI+'='+filePath);
                        /*
                        $cordovaFile.checkFile(result.toURL()).then(function(result) {
                            //success!
                            console.log('File Success!');

                            deferred.resolve(result.toURL());

                        }, function(err) {
                            console.log(err);
                            deferred.reject('error reading the downloaded file!');
                            console.error('error reading the downloaded file!')
                        })
                        */

                        //deferred.resolve(result.toURL());
                        deferred.resolve(result.fullPath);


                    }, function(err) {
                        deferred.reject('error in downloading file');
                        console.log('error in downloading file');
                        console.log("error code" + err.code);
                        console.error(err);
                    });


                } catch (e) {
                    deferred.reject('some error occured' + e);

                }
                return deferred.promise;



            },
            saveFileOld: function(fileURI, saveOptions) {
                console.log('save file called');
                //if( !isPhoneGap) return;

                console.log('trying to save the file ' + fileURI);
                saveOptions = angular.extend({
                    forceFromServer: false,
                    forceSaveAgain: false
                }, saveOptions);


                //tasks 
                //1 download the file
                //2 save the local path with the corresponding url in the locastorage




                var deferred = $q.defer();
                window.$cordovaFile = $cordovaFile;
                try {



                    console.log(fileURI);
                    window.fileURI = fileURI;


                    //_private.getFilesystem().then(

                    var fileName = fileURI.substring(fileURI.lastIndexOf('/') + 1);

                    //fileName = fileName.split(' ').join('_');

                    //var dir = cordova.file.documentsDirectory;
                    //console.log(JSON.stringify(cordova.file));
                    

                    //console.log(JSON.stringify(allFiles));

                    //var dir = cordova.file.dataDirectory || cordova.file.externalRootDirectory;
                    var dir = cordova.file.documentsDirectory || cordova.file.externalRootDirectory;
                    var filePath = dir + fileName;
                    //var filePath = fileName;




                    if (filePath && saveOptions.forceSaveAgain !== true ) {

                        BUPUtility.UrlExists(filePath).then(function(resp){
                            console.log('url exists response');
                            console.log(resp);
                            console.debug("file: " + filePath + " has already been saved before! No need to save again");
                            deferred.reject('File:' + filePath + ' already exists.'); 
                            return deferred.promise;  

                        },function(){
                            //alert('file:'+filePath+' already')
                            console.log('file doens\'t exist already so need to download it');
                            deferred.reject('File:' + filePath + ' already exists.'); 
                            return deferred.promise;  
                        })

                        
                        

                    }

                    


                    var files = _.filter(allFiles, function(aFile) {
                        return aFile.remote == fileURI
                    });

                    //var localFilePath = fileSystem.root.toURL();

                    $cordovaFile.downloadFile(fileURI, filePath, true).then(function(result) {
                        console.log('result of file download here:');
                        console.log(result);

                        //console.log('download complete to url: ' + result.toURL());
                        console.log('download complete to url: ' + result.fullPath);
                        //alert(fileURI+'='+filePath);
                        /*
                        $cordovaFile.checkFile(result.toURL()).then(function(result) {
                            //success!
                            console.log('File Success!');

                            deferred.resolve(result.toURL());

                        }, function(err) {
                            console.log(err);
                            deferred.reject('error reading the downloaded file!');
                            console.error('error reading the downloaded file!')
                        })
                        */

                        //deferred.resolve(result.toURL());
                        deferred.resolve(result.fullPath);


                    }, function(err) {
                        deferred.reject('error in downloading file');
                        console.log('error in downloading file');
                        console.log("error code" + err.code);
                        console.error(err);
                    });


                } catch (e) {
                    deferred.reject('some error occured' + e);

                }
                return deferred.promise;



            },
            saveToLocalStorage: function() {
                allFiles = _.uniq(allFiles,function(a){
                    return a.remote
                });
                window.localStorage['_files'] = JSON.stringify(allFiles);
            },
            /**
             * This service automatically looks for an audio file in the current practice tasks and
             * saves them locally!
             * @return {[type]} [description]
             */
            audioDaemon: function() {
                console.log('running the audio daemon');
                //console.time('audioDaemon');

                var allTasks = BUPWebservice.getAllTasks({

                    forceFromServer: false,

                    isBackgroundTask: true

                }).then(function(existingTasks) {

                    if (!existingTasks.hasOwnProperty('practices')) {
                        existingTasks.practices = [];
                    }

                    window.existingTasks = existingTasks;

                    var soundFiles = _(existingTasks.practices).filter(function(soundTask) {
                        return soundTask.hasOwnProperty('type') && soundTask.type === "sound";
                    }).pluck('audio_file').valueOf();
                    console.log(soundFiles);
                    window.soundFiles = soundFiles;


                    angular.forEach(soundFiles);

                });

                //console.timeEnd('audioDaemon');

            },
            saveMp3sFromTasks: function(existingTasks) {
                
                if( !isPhoneGap) return;

                if (!existingTasks.hasOwnProperty('practices')) {
                    existingTasks.practices = [];
                }

                if (!existingTasks.practices.length) {

                    console.log('no sound tasks here!');
                    return;
                }

                console.log(JSON.stringify(allFiles));


                console.log('running saveMp3sFromTasks');
                window.existingTasks = existingTasks;

                var soundFiles = _(existingTasks.practices).filter(function(soundTask) {
                    return soundTask.hasOwnProperty('type') && soundTask.type === "sound";
                }).pluck('audio_file').valueOf();
                console.log(soundFiles);
                window.soundFiles = soundFiles;


                angular.forEach(soundFiles, function(soundFile) {


                    //service.saveFile(soundFile).then(function(soundFile) {
                    service.saveFile(soundFile).then(function(localFile) {
                        console.log('going to save the files');
                        var theFile = {
                            remote: encodeURI(soundFile),
                            local: localFile
                        };

                        var oldIndex = _.findIndex(allFiles, theFile);
                        console.log('old index:'+oldIndex);
                        
                        var oldRecord = _.filter(allFiles, function(a){
                            return a.remote == theFile.remote && a.local  == theFile.local;
                        });

                        if(!oldRecord.length) {

                            allFiles.push(theFile);
                            service.saveToLocalStorage();
                        } 

                        /*
                        //check if the localstorage record already has this one
                        if (oldIndex < 0) {
                            //the file doesn't previously exists
                            allFiles.push(theFile);
                        } else {
                            //update the same index   
                            allFiles[oldIndex] = theFile;

                        }

                        console.log(allFiles);
                        */




                        

                    }, function(e) {

                        console.error(e);
                    });
                    //});


                });



                //console.timeEnd('audioDaemon');

            }
        };
        window.serviceFile = service;

        $rootScope.$on('save_audio_locally', function(event, url,options) {
            
            options = options || {};

            if( !isPhoneGap) return;

            console.log('received event save_audio_locally for file:' + url);
            service.saveFile(url,options);


        });

        return service;

    }
]);