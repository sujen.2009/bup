SERVICE_MODULE
/**
 * Service for user UI interaction
 * so that they can be controlled centrally
 *
 * gracefully fallback to other available UI when
 * run on web browser
 *
 * dependencies:
 * include ng-cordova.js with custom build including the dependencies
 *
 * @return {[type]} [description]
 */
.factory('BUPInteraction', ['$timeout', '$cordovaDevice', '$cordovaToast', '$cordovaDialogs', '$cordovaVibration', '$ionicLoading', '$ionicPopup', '$ionicActionSheet',
    function($timeout, $cordovaDevice, $cordovaToast, $cordovaDialogs, $cordovaVibration, $ionicLoading, $ionicPopup, $ionicActionSheet) {

        return {
            toast: function(message, duration, position) {
                if (!!message) {

                    duration = duration || 'short';
                    position = position || 'center';

                    //try {
                    if (window.ionic.Platform.isWebView()) {
                        //make sure the duration is always in string either 'short' or 'long'
                        if (duration === parseInt(duration)) {
                            //oh no value provided in integer
                            duration = duration >= 1000 ? 'long' : 'short';

                        }
                        try {

                            $cordovaToast.show(message, duration, position).then(function(success) {
                                // success
                            }, function(error) {
                                // error
                                console.error(message + 'could not be shown');

                            });

                        } catch (err) {
                            $timeout(function() {
                                //make sure the duration is always in number of milliseconds
                                if (duration === 'short') {
                                    duration = 800;
                                } else if (duration == 'long') {
                                    duration = 1500;
                                } else if (duration !== parseInt(duration)) {
                                    duration = 500; //if other invalid strings sent
                                    console.warn('unsupported duration value provided!');
                                }

                                $ionicLoading.show({
                                    template: message,
                                    noBackdrop: true,
                                    duration: duration
                                });


                            }, 0);
                        }


                    } else {
                        $timeout(function() {
                            //make sure the duration is always in number of milliseconds
                            if (duration === 'short') {
                                duration = 800;
                            } else if (duration == 'long') {
                                duration = 1500;
                            } else if (duration !== parseInt(duration)) {
                                duration = 500; //if other invalid strings sent
                                console.warn('unsupported duration value provided!');
                            }

                            $ionicLoading.show({
                                template: message,
                                noBackdrop: true,
                                duration: duration
                            });


                        }, 0);


                    }


                    //} catch (e) {

                    //alert(message);

                    //}



                }
            },
            /**
             * Uses ionic $ionicActionSheet show method
             * returns a function when called hides the action sheet
             *
             */
            confirm: function(actionSheetOpt) {
                return $ionicActionSheet.show(angular.extend({
                    destructiveText: 'Delete',
                    titleText: 'Are you sure?',
                    cancelText: 'Cancel',
                    cancel: function() {
                        // add cancel code..
                    },
                    destructiveButtonClicked: function() {
                        return true;
                    },
                    buttonClicked: function(index) {
                        return true;
                    }
                }, actionSheetOpt));
            },

            /**
             * Alert
             * @param object option
             *  title: Title of the alert
             *  message: Actual Message of the alert box
             *  label: Ok Text for closing the alert box
             *  callback: the callback function fired after
             *
             * @todo implement dynamic label for ok text
             */
            alert: function(option) {
                option = angular.extend({
                    title: '',
                    message: '',
                    callback: angular.noop
                }, option);

                var popup = $ionicPopup.alert({
                    title: option.title,
                    template: option.message
                });
                popup.then(function(res) {
                    option.callback();
                });

                if(option.autoClose) {
                    $timeout(function(){
                        popup.close();
                    },option.autoClose);
                }
            }

        };
    }
])