SERVICE_MODULE

.factory('BUPLoader', ['$rootScope', '$ionicLoading','$translate',
    function($rootScope, $ionicLoading,$translate) {
        var loading,loadingText = 'Laddar';
        $translate('LABEL_LOADING'). then ( function  (text) {
            loadingText = text;
        });
        return {
            show: function(opt) {
                if (loading) return;

                loading = true;

                opt = opt || {};
                var defaultopt = {
                    template: '<i class="icon ion-loading-c"></i>&nbsp;'+loadingText,
                    noBackdrop:false,
                    delay:250
                };

                if (opt.text) opt.template = '<i class="icon ion-loading-c"></i>&nbsp;' + opt.text;
                
                //if (opt.text) opt.template = '<i class="icon ion-loading-c"></i>&nbsp;' + opt.text;

                var finalOption = angular.extend(defaultopt, opt);
                //console.debug(finalOption);

                //$rootScope.loading = 
                $ionicLoading.show(finalOption);
            },
            
            hide: function() {
                loading = false;
                //$rootScope.loading.hide();
                $ionicLoading.hide();
            }
        }
    }
])

.run(['$rootScope', 'BUPLoader', function ($rootScope,BUPLoader) {
    
        $rootScope.$on('loading:show', function(event,data) {
            data = data || {};
            BUPLoader.show(data);
        })

        $rootScope.$on('loading:hide', function() {
            BUPLoader.hide();
        })

}])