SERVICE_MODULE
/**
 * CordovaNetwork AngularJS service
 * https://gist.github.com/rvanbaalen/9527418
 * com.apache.cordova.network-information wrapper for AngularJS with fallback for testing without Cordova
 */
.factory('CordovaNetwork', ['$rootScope', '$ionicPlatform',
    function($rootScope, $ionicPlatform) {
        // Get Cordova's global Connection object or emulate a smilar one
        var Connection = window.Connection || {
            'ETHERNET': 'ETHERNET',
            'WIFI': 'WIFI',
            'CELL_2G': 'CELL_2G',
            'CELL_3G': 'CELL_3G',
            'CELL_4G': 'CELL_4G',
            'CELL': 'CELL',
            'EDGE': 'EDGE',
            'UNKNOWN': 'unknown'
        };

        // Get Cordova's global navigator.connection object or emulate one
        var networkConnection = navigator.connection || {
            type: 'UNKNOWN'
        };

       
        $ionicPlatform.ready(function() {

            if (window.addEventListener) {
                window.addEventListener("online", function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Online', {});
                }, false);
                window.addEventListener("offline", function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Offline', {});
                }, false);
            } else {
                document.body.ononline = function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Online');
                };
                document.body.onoffline = function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Offline');
                };
            }
        });

        return {
            isOnline: function() {
                var blnReturn = false;

                switch (this.getStatus()) {
                    case Connection.ETHERNET:
                    case Connection.WIFI:
                    case Connection.CELL_2G:
                    case Connection.CELL_3G:
                    case Connection.CELL_4G:
                    case Connection.CELL:
                        blnReturn = true;
                        break;
                    case 'UNKNOWN':
                        //blnReturn = false;
                        if (window.ionic.Platform.isWebView()) {
                            blnReturn = false;
                        } else {
                            blnReturn = navigator.onLine || false;
                        }
                        break;
                }

                return blnReturn;
            },

            getStatus: function() {
                return networkConnection.type;
            }
        };
    }
])



.factory('CordovaNetworkAsync', ['$ionicPlatform', '$q',
    function($ionicPlatform, $q) {
        // Get Cordova's global Connection object or emulate a smilar one
        var Connection = window.Connection || {
            "CELL": "cellular",
            "CELL_2G": "2g",
            "CELL_3G": "3g",
            "CELL_4G": "4g",
            "ETHERNET": "ethernet",
            "NONE": "none",
            "UNKNOWN": "unknown",
            "WIFI": "wifi"
        };

        var asyncGetConnection = function() {
            var q = $q.defer();
            $ionicPlatform.ready(function() {
                if (navigator.connection) {
                    q.resolve(navigator.connection);
                } else {
                    //q.reject('navigator.connection is not defined');
                    q.resolve(
                        {
                            type: 'UNKNOWN'
                        }
                    );
                }
            });
            return q.promise;
        };
        return {
            isOnline: function() {
                return asyncGetConnection().then(function(networkConnection) {
                    var isConnected = false;

                    switch (networkConnection.type) {
                        case Connection.ETHERNET:
                        case Connection.WIFI:
                        case Connection.CELL_2G:
                        case Connection.CELL_3G:
                        case Connection.CELL_4G:
                        case Connection.CELL:
                            isConnected = true;
                            break;
                        case 'UNKNOWN':
                        default:
                            //blnReturn = false;
                            // if (window.ionic.Platform.isWebView()) {
                            //     blnReturn = false;
                            // } else {
                            //     blnReturn = navigator.onLine || false;
                            // }
                            

                            isConnected = navigator.onLine || false;
                            //cannot be relied however!!!
                            
                            // isConnectedSync = function(){

                                
                            //     try{
                            //         var http = new XMLHttpRequest();
                            //         http.open('HEAD', 'http://google.com', true);
                            //         http.send();
                            //         return http.status!=404;

                            //     }catch(e) {

                            //         return false;
                            //     }
                                

                            // }();
                            break;
                    }
                    return isConnected;
                });
            }
        };
}])

.run(['$rootScope', 'CordovaNetworkAsync' ,'CordovaNetwork',function($rootScope,CordovaNetworkAsync,CordovaNetwork){
	
	$rootScope.isOnline = CordovaNetwork.isOnline();

	CordovaNetworkAsync.isOnline()
	.then(function(isConnected) {
        $rootScope.isOnline = isConnected;
    }, function(err) {
    	$rootScope.isOnline = false;
    });

    //update
    
    $rootScope.$on('Cordova.NetworkStatus.Offline', function(event, data) {
        // $rootScope.isOnline = CordovaNetwork.isOnline();
        $rootScope.isOnline = false;

    });
    $rootScope.$on('Cordova.NetworkStatus.Online', function(event, data) {
        // $rootScope.isOnline = CordovaNetwork.isOnline();
        $rootScope.isOnline = true;
    });

}])
