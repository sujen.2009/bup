SERVICE_MODULE
.run(['$rootScope','BUPUser','BUPWebservice','WEBSERVICE_BASE_URI','$http', function ($rootScope,BUPUser,BUPWebservice,WEBSERVICE_BASE_URI,$http) {

    $rootScope.$on('newactivity',function(event,activity){
        var currentUser =  BUPUser.getCurrentUser();

        console.log(currentUser);
        if(!currentUser || !currentUser.roleId || !currentUser.saveDataInServer ){
            console.log('no saing in the server!');
            return;
        }

        //console.log('preparing to save data on the server')
        
        if( activity.homeworkid || activity.type && activity.type=='homework' ) {
            if(!activity.homeworkid) {
                activity.type = 'practice';
            }
            var homeworkType = (activity.homeworkid)?'homeworks':'practices';
            var defaultValues = {}

            //var newActivity = angular.extend(defaultValues, activity);
            console.log(homeworkType)
            BUPWebservice.getTaskById(activity.taskid,homeworkType).then(function(homeworkDetail){
                //console.log(homeworkDetail);
                //console.log(newActivity);
                activity.homeworkData = homeworkDetail;
                console.log(activity);

                $http.post(WEBSERVICE_BASE_URI + '/Login/SaveMeetingData',activity).then(function(rawResponse) {
                    console.debug(rawResponse);
                },function(err){
                    console.err(err);
                });

            })

        }else {

            $http.post(WEBSERVICE_BASE_URI + '/Login/SaveMeetingData',activity).then(function(rawResponse) {
                    console.debug(rawResponse);
                },function(err){
                    console.err(err);
                });

        }

       

        //console.log($rootScope);
      
        console.error(activity);
    });

    $rootScope.$on('homeworkdoneagain',function(event,activity) {
        var currentUser =  BUPUser.getCurrentUser();

        
        if(!currentUser || !currentUser.roleId || !currentUser.saveDataInServer ){
            console.log('no saing in the server!');
            return;
        }
        console.log('meeting id '+ $rootScope.meetingId );
        var defaultValues = {
            date: moment().startOf('day').format('YYYY-MM-DD'),
            userid: Number(BUPUser.getCurrentUser().id),
            meetingId : $rootScope.meetingId
        };
        var newActivity = angular.extend(defaultValues, activity);
        /*
            
            if($rootScope.RoleId && $rootScope.RoleId == 3){
                defaultValues.meetingId = $rootScope.meetingId;
            }
            
            date: $moment().startOf('day').format('YYYY-MM-DD'),
            userid: Number(BUPUser.getCurrentUser().id),
       
         */
    });

}])

.factory('BUPSync', ['$q','BUPWebservice', 'BUPUser', 'BUPFiles', 'BUPInteraction', '$rootScope', 'CordovaNetwork', 'CordovaNetworkAsync','$translate',
    function($q, BUPWebservice, BUPUser, BUPFiles, BUPInteraction, $rootScope, CordovaNetwork, CordovaNetworkAsync,$translate) {



        return {

            /**
             * [manualSync this function is fired when the user clicks sync button in the home navbar]
             * @return {[type]} [description]
             *
             * things to be done here:
             * 1. get the task list (forcefully from the server)
             * 2. update the task list and update the localstorage value
             * 3. abort gracefully when the internet connection is not
             */
            manualSync: function(opt) {

                if (!BUPUser.isLoggedIn()) {
                    console.error("not logged in to sync");
                    return false;
                }
                opt = opt || {};
                var defaultOpt = {
                    silentSync: false
                };
                opt = angular.extend(defaultOpt, opt);

                console.log("silentsync(background)=" + opt.silentSync);


                // if (CordovaNetwork.isOnline()) {
                //     BUPWebservice.getAllTasks({
                //         forceFromServer: true,
                //         isBackgroundTask: opt.silentSync
                //     });
                // } else {
                //     opt.silentSync === false && BUPInteraction.toast('Cannot sync in offline mode!!!');
                //     console.warn("cannot sync in offline mode");


                // }

                CordovaNetworkAsync.isOnline().then(function(isConnected) {
                    if (isConnected) {
                        BUPWebservice.getAllTasks({
                            forceFromServer: true,
                            isBackgroundTask: opt.silentSync
                        });


                    } else {
                        if (opt.silentSync === false) {
                            $translate('INFO_OFFLINE_SYNC_NOT_POSSIBLE'). then ( function  (text) {
                                BUPInteraction.toast(text, 'short');
                            });
                            //BUPInteraction.toast('Cannot sync in offline mode!!!');
                        }
                        console.warn("cannot sync in offline mode");
                    }

                }, function(err) {
                    console.log(err);
                });


                var currentUser = BUPUser.getCurrentUser();
                /**
                 * things to be done in the
                 */
            },

            /**
             * timely sync to check for updates
             *
             * @param  {[type]} opt [description]
             * @return {[type]}     [description]
             */
            regularSync: function(opt) {

            },


            backgroundSync: function(){

                var deferred = $q.defer();
                if (!BUPUser.isAuthenticated()) {
                    deferred.reject('User has not been authenticated to sync the data!');
                }else {
                    CordovaNetworkAsync.isOnline().then(function(isConnected) {
                        if (isConnected) {
                            return BUPWebservice.getAllTasks({
                                forceFromServer: true,
                                isBackgroundTask: true
                            });
                        }else{
                            deferred.reject('User is offline!');
                        }

                    }, function(err) {
                        deferred.reject('User is offline!');
                    });

                }
                return deferred.promise;

            },






            sync: function(opt) {

            }


        };
    }
])

.run(['$rootScope', 'BUPSync', 'CordovaNetworkAsync', 'BUPUser','$ionicPlatform', function ($rootScope,BUPSync,CordovaNetworkAsync,BUPUser, $ionicPlatform) {
    
    var sync = function(withSilence){
        BUPSync.manualSync({
            silentSync: withSilence
        });
    };
    $rootScope.$on('app.userready',function(event,data){
        var silentSync = true;
        if( data && data.manualLogin===true) {
            silentSync = false;
        }
        sync(silentSync);
    });

    //$ionicPlatform.on('resume', checkForUpdate);
    $rootScope.$on('Cordova.NetworkStatus.Online', function(event, data) {
        sync(true);
    });

    $ionicPlatform.on('resume', function(){
        sync(true);
    });


    //http://109.74.12.180/HemuppgiftenDev/webservice/Login/TreatmentDataArray?TokenKey=c54c8c79-d7cc-4cda-b32b-66862e526b26
    
    /*

    if (ionic.Platform.isWebView()) {
        try{

            var Fetcher = window.plugins.backgroundFetch;
            // Your background-fetch handler.
            var fetchCallback = function() {
                
                
                
                console.log('BackgroundFetch initiated');

                BUPSync.backgroundSync().finally(function() {
                     var backgroundExecutionCount = localStorage.getItem('bgExeCount') || 0;
                     ++ backgroundExecutionCount;
                    localStorage.setItem('bgExeCount', backgroundExecutionCount);

                    Fetcher.finish();
                    // Always execute this on both error and success
                });

            }

            Fetcher.configure(fetchCallback);

        }catch(e){
            console.error(e);
        }
        

    }

    */





}])