var UserAssignment = function(homework){
	//the constructor function
	var ua = this;

	//is repetive if it has icon
	ua.isRepetitive = !!homework.icon;

	//id
	ua.id = homework.id;

	ua.homeworkSlides = [];

	//homework
	angular.forEach(homework.homeworkSlides, function(slide) {
		//console.log(slide);
		var userSlide = {
            slidId:slide.slidId
        };
		switch(slide.type) {
			case "normal-text":
			     //console.log(slide.value);
				userSlide.value = slide.value || "";
			break;
			
			case "reorder":
                console.debug(slide.list);

                //do not store all the slided, save only those which has been changed!
                
                var changedList = [];
                _.forEach(slide.list,function(theList){
                    if (theList.added || theList.updated || theList.reordered || theList.trashed) {
                        changedList.push(theList);
                    }
                });

                // userSlide.list = slide.list;
                userSlide.list = changedList;
                
                // if(slide.updated) {
                //     userSlide.updated = slide.updated;
                // }if(slide.reordered) {
                //     userSlide.reordered = slide.reordered;
                // }if(slide.edited) {
                //     userSlide.edited = slide.edited;
                // }if(slide.edited) {
                //     userSlide.edited = slide.edited;
                // }
                //userSlide.list = slide.list;
				//userSlide = slide;
			break;

			case "form":
			case "dynamic-button":
			default:

			break;
		}
		ua.homeworkSlides.push(userSlide);
	});
	//console.log(ua);
	return ua;
};

SERVICE_MODULE

.value('UserAssignment',UserAssignment)

.factory('BUPUser', ['$rootScope', 'authService',
    function($rootScope, authService) {

        //private var
        var user = {};
        var forceReAuthenticate = function() {
            //console.log('re authentication required');
            $rootScope.$broadcast('event:auth-loginRequired');
            //$rootScope.$emit('event:auth-loginRequired');

        };

        

        var service = {
            init:function(){
                if (window.localStorage['user']) {
                    try {
                        user = JSON.parse(window.localStorage['user']);
                        // if(service.isLoggedIn()){
                        //     $rootScope.$broadcast('app.userready');
                        // }
                        //window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) ||
                    } catch (e) {
                        console.error('Could not read the user data from the local storage');
                        forceReAuthenticate();
                    }
                } else {

                    //console.log('user detail not found in the local storage');
                    forceReAuthenticate();

                }
            },
            getCurrentUser: function(authentication) {
                if (user !== null && typeof user == "object" && user.hasOwnProperty('tokenKey') && !!user.tokenKey) {
                    //console.log('current user info is ');
                    //console.log(user);
                    window._u = user;
                    return user;

                } else {

                    console.error('user info not found');
                    return false;
                    //forceReAuthenticate();

                    //$rootScope.$broadcast('event:auth-loginRequired');
                    //authenticateion required

                }
            },
            setCurrentUserInfo: function(userObj) {
                user = userObj;
                //$rootScope.currentUser = userObj;
                //$rootScope.currentUser = curUser;
                //$rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);
                //$rootScope.currentUserConfig = BUPConfig.initUserConfig(userObj.id);

                window.localStorage['user'] = JSON.stringify(userObj);
            },
            isLoggedIn: function() {

                if (user !== null && typeof user == "object" && user.hasOwnProperty('tokenKey') && !!user.tokenKey && !!user.isLoggedIn) {
                    // if ($rootScope.currentUser) {
                    //     return true;
                    // }
                    // return false;
                    return true;

                }

                return false;

            },

            /**
             * if a user has a valid token key but not necessarily logged in=true
             */
            isAuthenticated: function() {

                if (user !== null && typeof user == "object" && user.hasOwnProperty('tokenKey') && !!user.tokenKey ) {
                    // if ($rootScope.currentUser) {
                    //     return true;
                    // }
                    // return false;
                    return true;

                }

                return false;

            },
            locallyAuthenticate: function(checkUser) {


            }

        };

        // if(service.isUs){
        //     // $rootScope.$broadcast('app.userready');
        // }

        return service;
    }
])

//persistent configuration details

.factory('BUPConfig', ['BUPUtility', 'BUPUser', '$moment', '$rootScope',

    function(BUPUtility, BUPUser, $moment, $rootScope) {

        /**
         * note this config is always per user basis. 
         * ie, config of one user never applies to another user
         * stored config is actually an array
         * sample:
            var config = [{
                userId: 11,
                startedDate: "2014-06-15"
            }];
         */


        var config = [];

        var service = {
            /**
             * [readDataFromLocalStorage read the data from  the local storage into the private variable "config"]
             * @return {[type]} [description]
             */
            readDataFromLocalStorage: function() {
                //debugMode && console.log('reading data from the local storage for user config')
                if (window.localStorage['config']) {
                    try {
                        config = JSON.parse(window.localStorage['config']);
                    } catch (e) {}
                } else {
                    //if this object does not exist already then create that object and also save in localstorage

                }
            },

            /**
             * [updateDataToLocalStorage write the content of private variable "config" to the localstorage]
             * @return {[type]} [description]
             */
            updateDataToLocalStorage: function() {
                window.localStorage['config'] = JSON.stringify(config);
            },

            /**
             * BUPConfig.check() i
             * @param  {[int]} userId [id of the user to check]
             * @return {[void]}        doesn't return anything
             * checks if the user related config is set or not.
             * Ensures that must have config eg "userid","startedDate" is there
             * If not it is created and stored
             */
            initUserConfig: function(userId) {
                // var userConfig = _.filter(config, function(configEntry) {
                //     return configEntry.userId && configEntry.userId == userId && configEntry.startedDate;
                // });
                // if (userConfig.length) {
                //     debugMode && console.log('reading previous config');
                //     userConfig = userConfig[0];
                // } 

                if (!userId) return;
                var userConfig = this.get(userId);

                if (userConfig === false) {

                    //debugMode && console.log('setting a new config');
                    var today = $moment().startOf('day');

                    userConfig = {
                        userId: userId,
                        startedDate: {
                            timestamp: $moment(today).valueOf(),
                            dayIndex: BUPUtility.dayIndex(today),
                            weekIndex: BUPUtility.weekIndex(today)
                        }
                    };
                    config.push(userConfig);
                    this.updateDataToLocalStorage();
                }

                return userConfig;

            },

            get: function(userId) {
                if (!userId) return;
                //first check if it exists
                var userConfig = _.filter(config, function(configEntry) {
                    return configEntry.userId && configEntry.userId == userId && configEntry.startedDate;
                });
                if (userConfig.length) {
                    //debugMode && console.log('reading previous config');
                    userConfig = userConfig[0];

                    //make sure the userConfig has all the required values
                    if (!userConfig.startedDate) {
                        userConfig['startedDate'] = {
                            timestamp: $moment(today).valueOf(),
                            dayIndex: BUPUtility.dayIndex(today),
                            weekIndex: BUPUtility.weekIndex(today)
                        };
                    }



                    window._uc = userConfig;
                    return userConfig;
                } else {
                    return false;
                }
                debugMode && console.warn('user config not available!!!');
                return false;
            },
            getCurrentUserConfig: function() {

                var currentUser = BUPUser.getCurrentUser();
                if (currentUser && currentUser.id) {
                    return this.get(currentUser.id);
                }
            },
            
            updateUserConfig: function(userId,newConfig){
                var index = _.findIndex(config, function(userObj) {
                    return typeof userObj['userId'] !== "undefined" && userObj['userId'] === userId;
                });
                if (index > -1) {
                    config[index] = newConfig;
                } else {
                    config.push(newConfig);
                }
                this.updateDataToLocalStorage();
            },
            updateCurrentUserConfig:function(newConfig) {
                
                var currentUser = BUPUser.getCurrentUser();
                if (currentUser && currentUser.id) {
                    this.updateUserConfig(currentUser.id,newConfig);
                }

            },
            set: function(newConfig) {
                config = newConfig;
                this.updateDataToLocalStorage();
            }


        };

        service.readDataFromLocalStorage();

        return service;
    }
])

.run(['BUPUser','$rootScope','BUPConfig', function (BUPUser,$rootScope,BUPConfig) {
    BUPUser.init();
    if(BUPUser.isLoggedIn()) {
        $rootScope.currentUser = BUPUser.getCurrentUser();
        $rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);
        $rootScope.$broadcast('app.userready',{manualLogin:false});
    }
}])