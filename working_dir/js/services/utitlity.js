SERVICE_MODULE
/**
 * Utility Service
 * @return {[type]} [description]
 */

.service('BUPUtility', ['$moment', '$document', '$q','$http',


    function($moment, $document, $q, $http) {
        window.BUPUtility = this;
        /**
         * find out the unique day index
         * @uses moment().dayOfYear() to get the unique day of the year
         * @returns string prefixed with "{currentyear}_" to make sure it is valid for multiple years too
         */
        this.dayIndex = function(date) {
            var theMoment = $moment(date);
            return theMoment.get('year') + '_' + theMoment.dayOfYear();
        };

        /**
         * find out the unique week index
         * @uses moment().isoWeek() to get the unique week number of the year
         * @returns string prefixed with "{currentyear}_" to make sure it is valid for multiple years too
         */
        this.weekIndex = function(date) {
            //if( typeof this.weekIndex.count == 'undefined') this.weekIndex.count = 0;
            //++this.weekIndex.count;
            //console.error("weekIndex called count: "+this.weekIndex.count);
            //console.time('weekIndex');
            var theMoment = $moment(date);
            var res= theMoment.get('year') + '_' + theMoment.isoWeek();
            //console.timeEnd('weekIndex');
            return res;
        };

        /**
         * suitable building query string out of a javascript object
         * eg for making GET Request in webservice
         */

        this.buildQuery = function(obj, prefix) {
            var str = [];
            for (var p in obj) {
                var k = prefix ? prefix + "[" + p + "]" : p,
                    v = obj[p];
                str.push(typeof v == "object" ?
                    serialize(v, k) :
                    encodeURIComponent(k) + "=" + encodeURIComponent(v));
            }
            return str.join("&");
        };


        //https://developer.mozilla.org/en-US/docs/Web/API/window.btoa
        this.encode = function(str) {
            //utf8_to_b64
            return window.btoa(encodeURIComponent(escape(str)));
        };

        this.decode = function(str) {
            //b64_to_utf8
            return unescape(decodeURIComponent(window.atob(str)));
        };

        this.deviceInfo = function() {

            var info = {
                pushID: "",
                model: '', //ionic.Platform.platform()+'_'+ionic.Platform.version(),
                platform: "browser",
                uuid: ""
            }
            info = angular.extend(info, ionic.Platform.device());

            // var device = ionic.Platform.device();
            // info.model = device.model;
            // info.platform = device.platform;
            // info.uuid = device.uuid;

            return info;

        };


        this.UrlExists = function(url) {
            
            window.$http = $http;
            
            var deferred = $q.defer();

            $http.head(url,{cache: false})
            .then(function(response){
                if (response.status == 404) {
                    deferred.reject(response.status);
                    
                }else{
                    deferred.resolve();
                }
            },function(e){
                deferred.reject(e);
            });

            return deferred.promise;



            return $http.head(url,{cache: true})
            .then(function(response){
                if( response.status == 404) {
                   
                    return $q.reject(response);
                    // console.log('file not found');
                    //return $q.reject('404');
                    
                } 
                //console.log(response)
                return response;
                
            },function(e){
                //throw e;
                //console.error(e);
                return $q.reject(e);
                //console.error(e);
            });

            // var http = new XMLHttpRequest();
            // http.open('HEAD', url, false);
            // http.send();
            // return http.status != 404;
        };


        this.findNested = function(obj, key, memo) {
          var self = this;
          _.isArray(memo) || (memo = []);
          _.forOwn(obj, function(val, i) {
            if (i === key) {
              memo.push(val);
            } else {
              self.findNested(val, key, memo);
            }
          });
          return memo;
        };


    }
])

.run(['$http', '$templateCache','$state','$q','BUPUtility',
    function($http, $templateCache,$state,$q,BUPUtility) {
        return;
        /**
         * Template prefetching
         *
         */
        var templates = BUPUtility.findNested($state.get(),'templateUrl'),
            promises = [];

        var modals = ['html/partials/modals/welcome.modal.html','html/partials/modals/welcome.modal.html','html/partials/modals/homework_add_edit_list.html'];
        modals.forEach(function(m){
            templates.push(m);
        }); 

        //html/partials/homework.html
      

        templates.forEach(function (c,i) {
            if ($templateCache.get(c)){
             console.log('template '+ i+ ' already loaded!');
             return; //prevent the prefetching if the template is already in the cache
            }
            promises[i] = $http.get(c).success(function (t) {
              $templateCache.put(c, t);
            });
        });

        $q.all(promises)
        .then(function(resp){
            //all the template loadings has been completed
            console.log('all templates loaded');
            try{
                navigator.splashscreen.hide();
            
            }catch(e){

            }
            //console.timeEnd('templatesPrefetch');

        },function(){
            //if failed then also remove the splash screen
            try{

                navigator.splashscreen.hide();
            
            }catch(e){

            }
            //console.timeEnd('templatesPrefetch');
        });

        

        
    }
])
;