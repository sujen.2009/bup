SERVICE_MODULE
/**
 * A simple example service that returns some data.
 * authService is the service from angular-http-auth module
 *
 */
.factory('tasksCache', function($cacheFactory) {
    return $cacheFactory('tasksCache');
})
.factory('BUPWebservice', ['$q', '$http','$cacheFactory', 'authService', 'WEBSERVICE_BASE_URI', 'BUPUtility', 'BUPUser','BUPActivities', 'BUPLoader', '$rootScope',
    function($q, $http,$cacheFactory ,authService, WEBSERVICE_BASE_URI, BUPUtility, BUPUser,BUPActivities, BUPLoader, $rootScope) {
        // Might use a resource here that returns a JSON array



        // var tasks = {
        //     practices: [],
        //     homeworks: []
        // };
        //read the tasks from the local storage or empty


        // var tasks = window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) || {
        //     practices: [],
        //     homeworks: []
        // };


        var tasks = {
            practices: [],
            homeworks: []
        };



        if (window.localStorage['tasks']) {
            try {
                tasks = JSON.parse(window.localStorage['tasks']);
                //window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) ||
            } catch (e) {

            }
        }

        return {

            // authenticate: function(user) {
            //     BUPLoader.show({
            //         text: 'Authenticating...'
            //     });
            //     return $http.post('/api/login', user).then(function(response) {
            //         BUPLoader.hide();
            //         return response.data;
            //     });

            // },

            getAllTasksOld: function(opt) {
                var defaultOpts = {
                    forceFromServer: false,
                    isBackgroundTask: false
                };
                opt = angular.extend(defaultOpts, opt); //opt || {fromServer:false};
                // return $http.get('/api/practices').then(function(response) {
                //     practices = response.data;
                //     return practices;
                // });
                //if it has already been requested before get the same data
                if (opt.forceFromServer === false && typeof tasks == "object" && tasks != null && tasks.hasOwnProperty("practices") && tasks.hasOwnProperty("homeworks") && (tasks.practices.length || tasks.homeworks.length)) {

                    //controller is configured to expect a promist
                    var deferred = $q.defer();
                    //console.log('the existing all tasks are');
                    //console.log(tasks);
                    //deferred.resolve(practices);
                    deferred.resolve(tasks);

                    //temporary for testing only 
                    //$rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask);

                    //opt.showLoader && BUPLoader.hide();
                    return deferred.promise;

                } else {
                    // !opt.hideLoader && BUPLoader.show({
                    //     delay: 0
                    // });


                    var params = BUPUtility.buildQuery({
                        'TokenKey': '' + BUPUser.getCurrentUser().tokenKey
                    });
                    ///api/practices
                    return $http.get(WEBSERVICE_BASE_URI + '/Login/TreatmentDataArray?' + params).then(function(rawResponse) {
                        
                        var response = rawResponse.data.data[0];

                        //console.log('the backend all task respnse is');
                        //console.log(rawResponse);
                        //console.log(response);

                        //$rootScope.$broadcast('event:auth-loginRequired');

                        //practices = response.data;
                        if (response === "invalid") {

                            $rootScope.$broadcast('event:auth-loginRequired');

                        } else if (response && response.practices && response.homeworks) {

                            // tasks = {
                            //     practices: response.practices,
                            //     homeworks: response.homeworks
                            // }
                            tasks = response;

                            // tasks.practices = response.data.tasks.practices;
                            // tasks.homeworks = response.data.tasks.homeworks;

                            window.localStorage['tasks'] = JSON.stringify(tasks);
                            //!opt.hideLoader && BUPLoader.hide();

                            //if (opt.forceFromServer === true) {
                            //this is the response after request for forcefully get the data from the server
                            //(not the local storage). so time to broadcast this new data
                            //console.log('time to make the braodcast')
                            $rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask);
                            //}
                            return tasks;
                        } else {

                            $rootScope.$broadcast('event:auth-loginRequired');
                        }

                        //return practices;


                    });
                }

            },
            getAllTasks: function(opt) {
                var defaultOpts = {
                    forceFromServer: false,
                    isBackgroundTask: false,
                    cachePreferred:false
                };
                opt = angular.extend(defaultOpts, opt); 

                //if it has already been requested before get the same data
                //if (opt.forceFromServer === false && typeof tasks == "object" && tasks != null && tasks.hasOwnProperty("practices") && tasks.hasOwnProperty("homeworks") && (tasks.practices.length || tasks.homeworks.length)) {

                if (opt.cachePreferred===true) {
                    var deferred = $q.defer();
                    deferred.resolve(tasks);
                    return deferred.promise;
                }

                    //controller is configured to expect a promist
                    var params = BUPUtility.buildQuery({
                        'TokenKey': '' + BUPUser.getCurrentUser().tokenKey
                    });

                    if(opt.forceFromServer) {
                        //delete cache
                        $httpDefaultCache = $cacheFactory.get('$http');
                        $httpDefaultCache.remove(WEBSERVICE_BASE_URI + '/Login/TreatmentDataArray?' + params);
                    }


                    ///api/practices
                    return $http.get(WEBSERVICE_BASE_URI + '/Login/TreatmentDataArray?' + params,{cache:!!!opt.forceFromServer})
                    .then(function(rawResponse) {
                        if( rawResponse.status!=200) { //rawResponse.data != "ok" ||
                            var deferred = $q.defer();
                            deferred.resolve(tasks);
                            return deferred.promise;
                        }
                        //console.log(JSON.stringify(rawResponse));
                        var response = rawResponse.data.data[0];

                        //console.log('the backend all task respnse is');
                        //console.log(rawResponse);
                        //console.log(response);

                        //$rootScope.$broadcast('event:auth-loginRequired');

                        //practices = response.data;
                        if (response === "invalid") {

                            $rootScope.$broadcast('event:auth-loginRequired');

                        } else if (response && response.practices && response.homeworks) {

                            // tasks = {
                            //     practices: response.practices,
                            //     homeworks: response.homeworks
                            // }
                            tasks = response;

                            // tasks.practices = response.data.tasks.practices;
                            // tasks.homeworks = response.data.tasks.homeworks;

                            window.localStorage['tasks'] = JSON.stringify(tasks);
                            //!opt.hideLoader && BUPLoader.hide();

                            //if (opt.forceFromServer === true) {
                            //this is the response after request for forcefully get the data from the server
                            //(not the local storage). so time to broadcast this new data
                            //console.log('time to make the braodcast')
                      
                            $rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask,opt.forceFromServer);
                            //}
                            return tasks;
                        } else {

                            $rootScope.$broadcast('event:auth-loginRequired');
                        }

                        //return practices;


                    },function(err) {
                        var deferred = $q.defer();
                        deferred.resolve(tasks);
                        $rootScope.$broadcast('BUP.tasks_loaded', tasks);
                        return deferred.promise;

                    });
            

            },
            setTasks: function(newTasks) {
                tasks = newTasks;
                window.localStorage['tasks'] = JSON.stringify(newTasks);
                //console.log('now the tasks are');
                //console.log(tasks);
            },

            /*****************************************************************
             * @id: (int) id of the task
             * @type:  (string) type of the task , either homework or practice
             *****************************************************************
             **/
            getTaskById: function(id, type) {

                var deferred = $q.defer();
                console.time('getAllTasks');
                //first ensure we have all the tasks
                this.getAllTasks({cachePreferred:true}).then(function(allTasks) {
                    console.timeEnd('getAllTasks');
                    //console.log('all  tasks (practices and homeworks) available.ensured!');

                    //.log(allTasks);
                    //.log('type=' + type);
                    //console.log(allTasks[type]);
                    //get the task type
                    //console.log()
                    console.time('foundTasks');
                    var foundTasks = allTasks[type].filter(function(thetask) {
                        //console.log(thetask.id);
                        return thetask != null && parseInt(thetask.id, 10) == id;
                    });
                    console.timeEnd('foundTasks');

                    //var userExtendedTask = BUPActivities.getUserResponseByTaskId(id,type);
                    
                    var requiredTask = foundTasks.length ? foundTasks[0] : {};

                    //var mergedTask = angular.extend(requiredTask, userExtendedTask);

                    //var mergedTask = _.merge(requiredTask, userExtendedTask);


                    //console.log(mergedTask);
                    deferred.resolve(requiredTask);
                    //deferred.resolve(mergedTask);


                });

                return deferred.promise;

            },

            getHomeworkById: function(id) {

                return this.getTaskById(id, 'homeworks');

            },

            getPracticeById: function(id) {

                return this.getTaskById(id, 'practices');

            },
            /**
             * Experimental function 
             * @param  {[type]} id     [description]
             * @param  {[type]} type   [description]
             * @param  {[type]} newVal [description]
             * @return {[type]}        [description]
             */
            updateTaskById: function(id,type,newVal) {
                var foundTasks = tasks[type].filter(function(thetask) {
                    //console.log(thetask.id);
                    return thetask != null && parseInt(thetask.id, 10) == id;
                });

                if(foundTasks.length) {
                    //console.log('tasks updated in the localstorage')
                    foundTasks[0] = newVal;

                    window.localStorage['tasks'] = JSON.stringify(tasks);

                } 
            },

            registerCustomGroupUser: function(customGroupUser) {
                var params = BUPUtility.buildQuery(customGroupUser);
                var sampleSuccessReturn = {
                    status: "ok",
                    data: {
                        UserId: 22,
                        UserName: "customuserName",
                        RoleId: 5,
                        RoleName: "Group Patient",
                        saveDataInserver: false,
                        TokenKey: "asampletokenkey"
                    }

                };

                var sampleErrorReturn = {
                    status: "error",
                    data: {
                        message: "Username already taken, please choose another"
                    }
                };

                //sending static data but as a javascript promise similar to $http response

                // var deferred = $q.defer();
                // deferred.resolve(sampleSuccessReturn);
                // return deferred.promise;

                return $http.get(WEBSERVICE_BASE_URI + '/Login/InsertGroupUser?' + params).then(function(rawResponse) {
                    if (rawResponse.status === 200) {
                        var response = rawResponse.data.data;
                        if (response.Status) {
                            response.status = response.Status;
                        }
                        return response;
                        //console.log(rawResponse);
                        //console.log(response);
                        //return sampleSuccessReturn;
                        //return response;
                    } else {

                    }

                });
            }




            //
        }
    }
])