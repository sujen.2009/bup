// document.createElement('ion-tab');
// document.createElement('ion-nav-bar');
// document.createElement('ion-nav-view');
// document.createElement('ion-nav-buttons');
// document.createElement('ion-view');
// document.createElement('ion-content');
// document.createElement('ion-nav-list');
// document.createElement('ion-tabs');
// document.createElement('ion-tab');
// some globals.
var _APP_ = 'BUP',
    _CONTROLLERS_ = _APP_ + '.controllers',
    _DIRECTIVES_ = _APP_ + '.directives',
    _FILTERS_ = _APP_ + '.filters',
    _MODULES_ = _APP_ + '.modules',
    _SERVICES_ = _APP_ + '.services';

// top-level module
var MAINMODULE = angular.module(_APP_, [
    'ionic',

    // Your application's namespaced modules
    // so they won't conflict with other
    // modules. You shouldn't have to touch
    // these unless you want to.             
    _CONTROLLERS_,
    _DIRECTIVES_,
    _FILTERS_,
    _MODULES_,
    _SERVICES_,

    'angular-lodash',
    'angular-momentjs', //additional modules
    'http-auth-interceptor',
    'ngMockE2E',
    'tagged.directives.autogrow'

]);

var debugMode = true;

// Create global modules. You shouldn't have to touch these.
angular.module(_CONTROLLERS_, []);
angular.module(_DIRECTIVES_, []);
angular.module(_FILTERS_, []);
angular.module(_MODULES_, []);
angular.module(_SERVICES_, []);


// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//var BUPAPP = angular.module('_APP_', ['ionic', '_APP_.services', '_APP_.controllers', '_APP_.directives', 'ngMockE2E'])


MAINMODULE
    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            console.log('ionic platform ready');
            //alert(JSON.stringify(window.StatusBar));
            if (window.StatusBar) {
                alert('here');
                // Set the statusbar to use the default style, tweak this to
                // remove the status bar on iOS or change it to use white instead of dark colors.
                //StatusBar.styleDefault();
                //StatusBar.backgroundColorByHexString("#C0C0C0");
                StatusBar.backgroundColorByName("yellow");
                //StatusBar.styleLightContent();
                //StatusBar.styleBlackTranslucent();
            }
        });
    })
    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider

        .state('signin', {
            url: "/sign-in",
            templateUrl: "templates/sign-in.html",
            controller: 'SignInCtrl',
            requireLogin: false

        })

        .state('forgotpassword', {
            url: "/forgot-password",
            templateUrl: "templates/forgot-password.html"
        })

        .state('home', {
            url: "/home",
            controller: 'dashboardCtrl',
            templateUrl: "templates/dashboard.html"
        })

        .state('practice', {
            url: "/practice/:type/:id",
            controller: 'practiceCtrl',
            //templateUrl: "templates/practice.html"
            templateUrl: function($stateParams) {
                switch ($stateParams.type) {
                    case 'sound':
                        return 'templates/practice_sound.html';
                        break;
                    case 'exposure':
                        return "templates/exponera.html";

                        break;
                    case 'homework':
                        return "templates/homework.html";
                        //return "templates/practice_" + $stateParams.type + ".html";
                        break;

                    default:
                        return "templates/practice.html";

                }
            }
        })

        .state('homework', {
            url: "/homework/:id",
            controller: 'homeworkCtrl',
            templateUrl: "templates/homework.html"
        })


        .state('practicemaroon', {
            url: "/practicemaroon",
            controller: 'practiceCtrl',
            templateUrl: "templates/practicemaroon.html"
        })

        .state('practicegreen', {
            url: "/practicegreen",
            controller: 'practiceCtrl',
            templateUrl: "templates/practicegreen.html"
        })

        .state('reorder', {
            url: "/reorder",
            controller: 'homeworkCtrl',
            templateUrl: "templates/reorder.html"
        })

        .state('exponera', {
            url: "/exponera",
            controller: 'homeworkCtrl',
            templateUrl: "templates/exponera.html"
        })

        .state('skattningar', {
            url: "/skattningar",
            controller: 'homeworkCtrl',
            templateUrl: "templates/skattningar.html"
        });


        //$urlRouterProvider.otherwise("/sign-in");
        $urlRouterProvider.otherwise("/home");

    })
    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })



.config(['$httpProvider',
    function($httpProvider) {

        // Remove the default AngularJS X-Request-With header    
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        // Set DO NOT TRACK for all Get requests    
        //$httpProvider.defaults.headers.get['DNT'] = '1';

        //enable http caching on every http request
        $httpProvider.defaults.cache = false;

        // if (typeof currentUser == "object" && currentUser != null && currentUser.hasOwnProperty(authorizationToken) && !! currentUser.authorizationToken) {
        //     console.log('auth token key in localstorage found as:');
        //     console.log(currentUser.authorizationToken);
        //     $http.defaults.headers.common.Authorization = currentUser.authorizationToken;
        // }


    }
])

.run(['$rootScope', '$state', '$http', 'AuthenticationService', 'BUPConfig', 'BUPUser', 'BUPActivities', 'BUPLoader', '$ionicModal', '$ionicPopup',
    function($rootScope, $state, $http, AuthenticationService, BUPConfig, BUPUser, BUPActivities, BUPLoader, $ionicModal, $ionicPopup) {

        // $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
        //     if (!Auth.authorize(toState.data.access)) {
        //         $rootScope.error = "Seems like you tried accessing a route you don't have access to...";
        //         event.preventDefault();

        //         if(fromState.url === '^') {
        //             if(Auth.isLoggedIn()) {
        //                 $state.go('user.home');
        //             } else {
        //                 $rootScope.error = null;
        //                 $state.go('anon.login');
        //             }
        //         }
        //     }
        // });

        // $rootScope.user = {
        //     username: "demo",
        //     password: "demo",
        //     city: ""
        // };

        window._ua = BUPActivities;

        var random_users = [{
            username: "demo",
            password: "demo"
        }, {
            username: "dipesh",
            password: "123456"
        }, {
            username: "maria",
            password: "123456"
        }, {
            username: "avishek",
            password: "123456"
        }, {
            username: "anup",
            password: "123456"
        }],
            rand_index = Math.floor(Math.random() * (random_users.length));

        $rootScope.user = random_users[rand_index];

        $rootScope.signIn = function() {
            AuthenticationService.login($rootScope.user);
        };



        $ionicModal.fromTemplateUrl('templates/login.html', function(modal) {
            $rootScope.loginModal = modal;
            //wait until the login template is completely loaded 
            //so that we don't get error about $rootScope.loginModal is undefined when the user is not logged in

            console.log('>>>>callued get current user');
            $rootScope.currentUser = BUPUser.getCurrentUser();
            $rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);

            //console.log('Searching for previous auth token');
            //var currentUser = BUPUser.getCurrentUser();
            console.log('current rootscope user');
            console.log($rootScope.currentUser);
            if (typeof $rootScope.currentUser == "object" && $rootScope.currentUser != null && $rootScope.currentUser.hasOwnProperty('authorizationToken') && !! $rootScope.currentUser.authorizationToken) {
                console.log('auth token key in localstorage found as:');
                console.log($rootScope.currentUser.authorizationToken);
                $http.defaults.headers.common.Authorization = $rootScope.currentUser.authorizationToken;
            }

        }, {
            scope: $rootScope,
            animation: 'slide-in-up',
            focusFirstInput: true
        });
        //Be sure to cleanup the modal by removing it from the DOM
        // $rootScope.$on('$destroy', function() {
        //     $rootScope.loginModal.remove();
        // });
        // $rootScope.signIn = function() {
        //     AuthenticationService.login($scope.user);
        // };



        $rootScope.$on('event:auth-loginRequired', function(e, rejection) {
            console.log('event fired: event:auth-loginRequired');
            BUPLoader.hide();
            $rootScope.loginModal.show();
            //$state.go('signin');
        });

        $rootScope.$on('event:auth-login-failed', function(e, status) {
            console.log('event fired: auth-login-failed');




            var error = "Login failed.";
            if (status == 401) {
                error = "Invalid Username or Password.";
            }

            $ionicPopup.alert({
                title: 'BUP',
                template: error
            });

            //$scope.message = error;
        });
        $rootScope.$on('event:auth-logout-complete', function() {
            console.log('event fired: event:auth-logout-complete');
            console.log('time to refresh');
            $state.go('signin', {}, {
                reload: true,
                inherit: false
            });
        });

        $rootScope.$on('event:auth-loginConfirmed', function() {
            console.log('event fired: event:auth-loginConfirmed');
            //$scope.username = null;
            //$scope.password = null;
            // $state.go($state.$current, null, {
            //     reload: true
            // });
            $rootScope.loginModal.hide();

            window._a = $rootScope.currentUser = BUPUser.getCurrentUser();

            //track the earliest logged in date
            console.log('time to init the config');

            $rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);

            // 
            // setTimeout(function() {
            //     $rootScope.loginModal.hide();

            // }, 1000);
        });









    }
]);


//https://coderwall.com/p/f6brkg
//https://medium.com/opinionated-angularjs/4e927af3a15f
// .run(function($rootScope, $state, AuthService) {
//     $rootScope.$on('$stateChangeStart', function(event, curr, prev) {

//         if (curr.requireLogin !== false && !AuthService.isAuthenticated()) {
//             $state.transitionTo('signin');
//             event.preventDefault();
//         }

//     });

// });