angular.module(_CONTROLLERS_, [])

.controller('SignInCtrl', function($scope, $rootScope, $state, $http, AuthService, AuthenticationService, BUPWebservice, BUPLoader, $ionicModal, $ionicViewService, $ionicPopup, $ionicLoading) {
    // $scope.user = {
    //     username: "demo",
    //     password: "demo",
    //     city: ""
    // };

    var random_users = [{
        username: "demo",
        password: "demo"
    }, {
        username: "dipesh",
        password: "123456"
    }, {
        username: "maria",
        password: "123456"
    }, {
        username: "avishek",
        password: "123456"
    }, {
        username: "anup",
        password: "123456"
    }],
        rand_index = Math.floor(Math.random() * (random_users.length));

    $scope.user = random_users[rand_index];
    // $ionicModal.fromTemplateUrl('templates/sign-in.html', function(modal) {
    //     $scope.loginModal = modal;
    // }, {
    //     scope: $scope,
    //     animation: 'slide-in-up',
    //     focusFirstInput: true
    // });
    // //Be sure to cleanup the modal by removing it from the DOM
    // $scope.$on('$destroy', function() {
    //     $scope.loginModal.remove();
    // });
    $scope.signIn = function() {
        AuthenticationService.login($scope.user);
    };

    // $rootScope.$on('event:auth-loginRequired', function(e, rejection) {
    //     console.log('event fired: event:auth-loginRequired');
    //     $scope.loginModal.show();
    // });

    $scope.$on('event:auth-loginConfirmed', function() {
        console.log('event fired: event:auth-loginConfirmed');
        $state.go('home');
    });

    $scope.$on('event:auth-login-failed', function(e, status) {
        console.log('event fired: auth-login-failed');




        var error = "Login failed.";
        if (status == 401) {
            error = "Invalid Username or Password.";
        }

        $ionicPopup.alert({
            title: 'BUP',
            template: error
        });

        //$scope.message = error;
    });

    $scope.$on('event:auth-logout-complete', function() {
        console.log('event fired: event:auth-logout-complete');
        console.log('time to refresh');
        $state.go('app.home', {}, {
            reload: true,
            inherit: false
        });
    });

    // $scope.signIn = function() {
    //     AuthService.login($scope.user);
    // };
    $scope.signInOld = function() {

        // $ionicLoading.show({
        //     template: '<i class="icon ion-loading-c"></i>&nbsp;Authenticating...'
        // });

        //_BASE_API_URL + 
        /*
                    var promise = $http.post('/api/login', $scope.user).then(function(response) {
                        console.log(response)
                        if (response.data.status) {
                            $ionicPopup.alert({
                                title: 'Success',
                                template: response.data.message
                            }).then(function(res) {
                                $state.go('home');
                            });
                        } else {
                            $ionicPopup.alert({
                                title: 'Login failed',
                                template: response.data.message
                            });
                        }
                        $ionicLoading.hide();
                        //return response.data;
                    });

                };
                });
        */

        BUPWebservice.authenticate($scope.user).then(function(response) {
            //$ionicLoading.hide();


            if (response.status) {
                //authenticated
                AuthService.setAuthenticated(true);

                $ionicPopup.alert({
                    title: 'Success',
                    template: response.message
                }).then(function(res) {
                    //after clicked OK
                    // $ionicLoading.show({
                    //     template: '<i class="icon ion-loading-c"></i>&nbsp;Loading...'
                    // });
                    //BUPLoader.show();
                    $state.go('home');
                });

                //$scope.on('')

                //success


                BUPWebservice.getAllTasks({
                    hideLoader: true
                }).then(function(response) {
                    console.log(response);
                    //$ionicLoading.hide();
                    //BUPLoader.hide();
                });



            } else {
                $ionicPopup.alert({
                    title: 'Login failed',
                    template: response.message
                });
            }

            //return response.data;
        });

    };

})

.controller('dashboardCtrl', function($scope, $rootScope, $state, $ionicViewService, $ionicModal, $ionicPopup, AuthenticationService, BUPWebservice, BUPUser, BUPActivities) {

    $ionicViewService.clearHistory();

    $ionicModal.fromTemplateUrl('templates/about-the-app-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });
    $scope.openAboutAppModal = function() {
        $scope.modal.show();
    };
    $scope.closeAboutAppModal = function() {
        $scope.modal.hide();
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
        // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });

    $scope.set_icon_bg = function(practice) {
        if (practice.icon) {
            //return { color: "red" }
            return {
                backgroundImage: 'url(' + practice.icon + ')'
            }

        }
    }

    //console.log('am in the dashboard. lets see if we can find the user details here');
    //console.log($rootScope.currentUser);



    $scope.logout = function() {

        $ionicPopup.confirm({
            title: 'Hemuppgiften',
            content: 'Are you sure you want to log out?',
            buttons: [{
                text: 'Cancel',
                type: 'button-red'
            }, {
                text: 'Ok',
                type: 'button-positive',
                onTap: function(res) {
                    $state.go('signin');
                    $ionicViewService.clearHistory();
                    AuthenticationService.logout();
                }

            }, ]
        }).then();
    };


    BUPWebservice.getAllTasks().then(function(response) {
        //console.log(response);
        $scope.practices = response.practices.filter(function(p) {
            return typeof p === 'object' && p != null; // && p.hasOwnProperty('id');
        });
        //console.log($scope.practices);
        $scope.homeworks = response.homeworks;
        //console.log($scope.homeworks);

    });

    //$scope.activities = BUPActivities.getCurrentUserActivities();

    // $scope.dailyActivities = function() {

    //     var data = [{
    //         date: "23/4,",
    //         count: 5
    //     }, {
    //         date: "24/4",
    //         count: 1
    //     }, {
    //         date: "25/4",
    //         count: 1
    //     }, {
    //         date: "26/4",
    //         count: 1
    //     }];

    //     return data;


    //     // return function(list, groupBy) {
    //     //     var filtered = [],
    //     //         byday = [],
    //     //         byweek = [],
    //     //         bymonth = [];
    //     //     var groupday = function(value, index, array) {
    //     //         //console.log('inside groupday filter');
    //     //         //console.log(value);
    //     //         //console.log('value');
    //     //         d = new Date(value['date']);
    //     //         d = Math.floor(d.getTime() / (1000 * 60 * 60 * 24));
    //     //         //console.log(d);
    //     //         byday[d] = byday[d] || [];
    //     //         byday[d].push(value);
    //     //         //console.log(byday);
    //     //     };

    //     //     var groupweek = function(value, index, array) {
    //     //         d = new Date(value['date']);
    //     //         d = Math.floor(d.getTime() / (1000 * 60 * 60 * 24 * 7));
    //     //         byweek[d] = byweek[d] || [];
    //     //         byweek[d].push(value);
    //     //     };

    //     //     var groupmonth = function(value, index, array) {
    //     //         d = new Date(value['date']);
    //     //         d = (d.getFullYear() - 1970) * 12 + d.getMonth();
    //     //         bymonth[d] = bymonth[d] || [];
    //     //         bymonth[d].push(value);
    //     //     };
    //     //     if (list === null) return [];

    //     //     // angular.forEach(list, function(item) {

    //     //     // });

    //     //     console.log('all data');
    //     //     //console.log(list);
    //     //     //console.log(groupBy);




    //     //     switch (groupBy) {
    //     //         case "day":
    //     //             //console.log("before applying the map");
    //     //             list.map(groupday);
    //     //             //console.log("after applying map")
    //     //             console.log(byday);
    //     //             return [];
    //     //             //return byday;
    //     //             break;

    //     //         case "week":
    //     //             list.map(groupweek);
    //     //             return byweek;
    //     //             break;

    //     //         case "month":
    //     //             list.map(groupmonth);
    //     //             break;
    //     //     }
    //     // }


    // };
    $scope.dailyActivities = BUPActivities.dailyActivities();
    $scope.weeklyActivities = BUPActivities.weeklyActivities();

    $scope.practiceRange = {
        max: {
            daily: 0,
            weekly: 0
        },
        min: {
            daily: 0,
            weekly: 0
        }
    };


    //array of counts
    var dailyPracticeCountsArray = Object.keys($scope.dailyActivities).map(function(key) {
        return $scope.dailyActivities[key].count
    }),
        weeklyPracticeCountsArray = Object.keys($scope.weeklyActivities).map(function(key) {
            return $scope.weeklyActivities[key].count
        });

    // $scope.practiceRange.max.daily = Math.max.apply(null, dailyPracticeCountsArray);
    // $scope.practiceRange.min.daily = Math.min.apply(null, dailyPracticeCountsArray);

    // $scope.practiceRange.max.weekly = Math.max.apply(null, weeklyPracticeCountsArray);
    // $scope.practiceRange.min.weekly = Math.min.apply(null, weeklyPracticeCountsArray);


    $scope.maxPracticeNum = {
        daily: Math.max.apply(null, dailyPracticeCountsArray),
        weekly: Math.max.apply(null, weeklyPracticeCountsArray)
    };

    $scope.set_daily_graph_height = function(activity) {
        if (parseInt(activity.count, 10) > -1) {
            console.log('maxdaily=' + $scope.maxPracticeNum.daily);
            console.log(activity.count);
            console.log(parseInt(activity.count, 10) == 0);
            //return { color: "red" }
            return {
                height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.daily * 100) + '%')
            }

        }
    }
    $scope.set_weekly_graph_height = function(activity) {
        console.log('maxweekly=' + $scope.maxPracticeNum.weekly);
        console.log(activity.count);
        console.log(typeof activity.count);
        if (parseInt(activity.count, 10) > -1) {
            //return { color: "red" }
            return {
                height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.weekly * 100) + '%')
            }

        }
    }


    //$scope.activities = $scope.dailyActivities();


})

.controller('homeworkCtrl', function($scope, $state, $stateParams, BUPWebservice, $ionicSlideBoxDelegate) {

    $scope.homeworkSlides = [];
    BUPWebservice.getHomeworkById($stateParams.id).then(function(response) {
        //console.log('following is the get by id response');
        //console.log(response);
        //console.log('<<');
        $scope.homeworkSlides = response.homeworkSlides;
        //$scope.totalSlides = response.length;

        //$scope.practices = response.practices;
        //$scope.homework = response.homeworks;
    });

    $scope.slideIndex = 0;
    $scope.next = function() {
        $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {
        $ionicSlideBoxDelegate.previous();
    };

    // Called each time the slide changes
    $scope.slideChanged = function(index) {
        console.log('slide changed');
        console.log('new index is ' + index);
        $scope.slideIndex = index;
    };



})

.controller('practiceCtrl', function($scope, $state, $stateParams, BUPWebservice, $ionicSlideBoxDelegate) {

    switch ($stateParams.type) {
        case 'sound':

            //<button ng-init="play=1" 
            //ng-class="{'ion-play':play,'ion-pause':!play}" 
            //class="circled" ng-click="play = !play">
            $scope.playing = false;
            $scope.toggle = function() {
                $scope.playing = !$scope.playing;
            };
            //$scope.audio_bgcolor = '#ccc';
            // $scope.set_sound_bg = function() {
            //     if ($scope.audio_bgcolor) {
            //         //return { color: "red" }
            //         return {
            //             backgroundColor: $scope.audio_bgcolor //$scope.practice.audio_bgcolor
            //         }

            //     }
            // }


            BUPWebservice.getPracticeById($stateParams.id).then(function(response) {
                //console.log('following is the get by id response');
                //console.log(response);
                //console.log('<<');
                console.log($stateParams.id);
                $scope.practice = response;
                console.log($scope.practice);
                //$scope.totalSlides = response.length;

                //$scope.audio_bgcolor = $scope.practice.audio_bgcolor;


                $scope.set_sound_bg = function() {
                    if ($scope.practice.audio_bgcolor) {
                        return {
                            backgroundColor: $scope.practice.audio_bgcolor //$scope.practice.audio_bgcolor
                        }

                    }
                }


                // $scope.set_icon_bg = function(practice) {
                //     if (practice.icon) {
                //         //return { color: "red" }
                //         return {
                //             backgroundImage: 'url(' + practice.icon + ')'
                //         }

                //     }
                // }

                //$scope.practices = response.practices;
                //$scope.homework = response.homeworks;
            });

            break;

        case 'exposure':
            break;

        case 'homework':
            $scope.homeworkSlides = [];
            BUPWebservice.getPracticeById($stateParams.id).then(function(response) {
                //console.log('following is the get by id response');
                //console.log(response);
                //console.log('<<');
                $scope.homeworkSlides = response.homeworkSlides;
                //$scope.totalSlides = response.length;

                //$scope.practices = response.practices;
                //$scope.homework = response.homeworks;
            });
            $scope.slideIndex = 0;
            $scope.next = function() {
                $ionicSlideBoxDelegate.next();
            };
            $scope.previous = function() {
                $ionicSlideBoxDelegate.previous();
            };

            // Called each time the slide changes
            $scope.slideChanged = function(index) {
                console.log('slide changed');
                console.log('new index is ' + index);
                $scope.slideIndex = index;
            };
            break;
    }



})



.controller('HomeTabCtrl', function($scope) {
    console.log('HomeTabCtrl');
})

.controller('DashCtrl', function($scope, StoreWebservice) {

})

.controller('StoreListingCtrl', function($scope, StoreWebservice) {
    StoreWebservice.getStores().then(function(response) {
        $scope.stores = response;
        console.log($scope.stores);
        console.log(response);
    });
})

.controller('StoreDetailCtrl', function($scope, $state, $stateParams, StoreWebservice) {

    console.log('getting the  store');

    StoreWebservice.getStore($stateParams.storeID).then(function(response) {
        if (response.length)
            $scope.store = response[0];
    });


    StoreWebservice.getItemsByStore($stateParams.storeID).then(function(response) {
        $scope.items = response;
    });

    $scope.go = function(itemid) {
        $state.go('tab.item-detail', {
            itemID: itemid
        });

    };
})

// .controller('ItemCtrl', function($scope, $stateParams, StoreWebservice) {
//     $scope.item = StoreWebservice.get($stateParams.storeID);
// })

.controller('ItemListingCtrl', function($scope, $stateParams, StoreWebservice) {


    StoreWebservice.getAllItems().then(function(response) {
        $scope.items = response;



    });


})

.controller('ItemDetailCtrl', function($scope, $stateParams, StoreWebservice) {

    console.log('inside item detail controller');
    StoreWebservice.getItemById($stateParams.itemID).then(function(response) {
        if (response.length) {
            $scope.item = response[0];
            StoreWebservice.getStore($scope.item.storeID).then(function(response) {
                if (response.length)
                    $scope.store = response[0];
            });
        }
    });
})




.controller('AboutCtrl', function($scope) {})



//.controller('DashCtrl', function($scope) {

.controller('DashCtrl', function($scope, $state, $timeout, $q, $http, $ionicModal, clientlocation, geolocation, geocoder, eventful, StoreWebservice) {
    //.controller('AppCtrl', function($scope, $timeout, $http, $ionicModal, geocoder, eventful) {
    console.log('inside dash controller')

    $scope.search_form = {
        location: {

        }
    };

    if (!$scope.search_form.distance) {
        $scope.search_form.distance = 20000;
    }


    $scope.maxEventsToLoad = 400;

    //mixpanel.track("Home Page Loaded");


    var StorePromise = StoreWebservice.getStores().then(function(response) {
        $scope.stores = response;
    });


    StoreWebservice.getAllItems().then(function(response) {
        $scope.items = response;
    });



    $scope.searchForEvents = function() {

        //if (!$scope.search_form.location.address) {
        //  return;
        //}

        if (!$scope.search_form.distance) $scope.search_form.distance = 2000; //5;

        $scope.closeSearchModal();


        $scope.showLoader = true;
        $scope.loadingText = "Finding Stores";
        var nearestStores = [];

        angular.forEach($scope.stores, function(aStore) {
            if (aStore.lat && aStore.lng) {

                aStore.latLng = new google.maps.LatLng(aStore.lat, aStore.lng);
                aStore.distance = google.maps.geometry.spherical.computeDistanceBetween($scope.myLatLng, aStore.latLng);

                if (aStore.distance <= $scope.search_form.distance) {
                    nearestStores.push(aStore);
                }
                return nearestStores;
            }

            console.log(aStore.latLng);
            console.log(aStore);
        });

        $scope.$broadcast('storeDataChange', {
            stores: nearestStores
        });


        $scope.showLoader = true;

    }

    $scope.resetZoom = function() {
        $scope.$broadcast('resetZoom');
    }

    $scope.setLoaderStatus = function(val) {
        $scope.showLoader = val;
    }

    $scope.setLoadingText = function(val) {
        $scope.loadingText = val;
    }

    $scope.setInfoPartyContent = function(val) {
        $scope.eventInfo = val;
        $scope.$apply();
    }

    $scope.setUserLocation = function(val) {
        $scope.userLocation = val;
    }

    $scope.selectMe = function(elem) {
        elem.select();
    }

    // Load search $ionicModal
    $ionicModal.fromTemplateUrl('searchModal.html', function(modal) {
        $scope.searchModal = modal;
    }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    $scope.openSearchModal = function() {
        //mixpanel.track("Opened search modal");
        $scope.searchModal.show();
    };
    $scope.closeSearchModal = function() {
        //mixpanel.track("Closed search modal");
        $scope.searchModal.hide();
    };

    // Load event info modal
    $ionicModal.fromTemplateUrl('infoModal.html', function(modal) {
        $scope.infoModal = modal;
    }, {
        // Use our scope for the scope of the modal to keep it simple
        scope: $scope,
        // The animation we want to use for the modal entrance
        animation: 'slide-in-up'
    });

    $scope.openInfoModal = function() {
        //mixpanel.track("Opened info modal");
        $scope.infoModal.show();
    };

    $scope.closeInfoModal = function() {
        //mixpanel.track("Closed info modal");
        $scope.infoModal.hide();
    };



    console.log('inside map controller');
    var markerCluster;
    var markerSpider;
    var infoWindow;

    $scope.storesMap = new google.maps.Map(document.getElementById('map-canvas'), {
        center: new google.maps.LatLng(37.09024, -95.7128910),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_CENTER,
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE]
        }
    });

    $scope.partyMarkers = [];
    $scope.nearestStoreMarkers = [];


    $scope.setLoaderStatus(true);
    $scope.setLoadingText("Getting Location");

    $scope.findNearStores = function() {

        console.log('inside find near stores');
        var nearestStores = [];
        console.log($scope.search_form.distance + ' is the setting distance');

        angular.forEach($scope.stores, function(aStore) {
            if (aStore.lat && aStore.lng) {
                aStore.latLng = new google.maps.LatLng(aStore.lat, aStore.lng);
                aStore.distance = google.maps.geometry.spherical.computeDistanceBetween($scope.myLatLng, aStore.latLng);
                console.log('Distance of ' + aStore.title + ' is ' + aStore.distance);

                if (aStore.distance <= $scope.search_form.distance) {
                    nearestStores.push(aStore);
                }
            }
            //return nearestStores;

        });

        //console.log(aStore.latLng);
        console.log(nearestStores.length + ' stores found nearby')
        console.log(nearestStores);

        if (nearestStores.length) {

            $scope.$broadcast('storeDataChange', {
                stores: nearestStores
            });
            console.log('broadcasted');

        }
        $scope.showLoader = false;
    }

    var myLocationPromise = geolocation.getLocation().then(function(position) {

        $scope.coords = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        var location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        $scope.myLatLng = location;
        $scope.search_form.location.latlng = location;

        $scope.initiateMapElements();

        $scope.storesMap.setCenter(location);
        $scope.setUserLocation(location);

        console.log('client location obtained');
        console.log(location);

        // $scope.setSearchFormLocation(location, function(){
        //   $scope.storesMap.setZoom(11);
        //   //$scope.searchForEvents();  
        //   $scope.displayNearestStores();
        // });  

        //$scope.storesMap.setZoom(11);
        //$scope.displayNearestStores();


    });



    if ($scope.myLatLng && $scope.stores) {

        $scope.findNearStores();


    } else {

        //wait until both stores and uses current location has been obtained and then find the near stores
        $q.all([myLocationPromise, StorePromise]).then(function(data) {
            $scope.findNearStores();
        });


    }



    $scope.displayNearestStores = function() {


        if (!$scope.search_form.location.address) {
            return;
        }

        if (!$scope.search_form.distance) $scope.search_form.distance = 5000; //5;

        // $scope.geocodeSearchLocation(function(){

        //   $scope.closeSearchModal();

        //   $scope.showLoader = true;
        //   $scope.loadingText = "Finding Stores";
        //   if(!$scope.$$phase) {
        //     $scope.$apply();
        //   }


        // });
    };


    // eventful.search($scope.search_form, 1, function(events, page_count){
    //   $scope.$broadcast('eventfulDataChange', {events: events, page_count: page_count});
    // });
    // StoreWebservice.search($scope.search_form, 1, function(events, page_count){
    //   $scope.$broadcast('eventfulDataChange', {events: events, page_count: page_count});
    // });
    // 
    // 
    $scope.$on("resetZoom", function(e, data) {
        if ($scope.storesMap) {
            //mixpanel.track("Reset Zoom");
            $scope.storesMap.setZoom(11);
        }
    });


    // $scope.$on("eventfulDataChange", function(e, data){

    //   var events = data.events  //   var numEventsLoaded = events.length;


    ;


    //   if (!events.length) {
    //     alert("No events found, try different criteria");
    //     //$scope.setLoaderStatus(false);
    //     $scope.openSearchModal();
    //     return;
    //   }

    //   $scope.clearMarkers();
    //   $scope.clearListeners();
    //   $scope.placeUserLocationMarker();

    //   var bounds = $scope.addMarkersToMap(events);

    //   markerSpider.addListener('click', function(marker, event) {
    //     $scope.setInfoPartyContent($scope.setPartyInfo(marker.party));
    //     $scope.openInfoModal();
    //   });

    //   var latlng = $scope.search_form.location.latlng;
    //   $scope.storesMap.panTo(new google.maps.LatLng(latlng[1], latlng[0]));

    //   if ($scope.partyMarkers.length) {
    //     // Getting a $digest already in progress error, so I'm just wrapping it in a timeout
    //     // so it's the last thing on the event queue
    //     $timeout(function(){
    //       $scope.storesMap.fitBounds(bounds);
    //     });
    //   }
    //   //$scope.setLoaderStatus(false);

    //   // Get the rest of the events
    //   for(var i = 2; i <= data.page_count; i++) {

    //     eventful.search($scope.search_form, i, function(events, page_c) {

    //       numEventsLoaded += events.length;

    //       if (numEventsLoaded <= $scope.maxEventsToLoad){
    //         $scope.addMarkersToMap(events);
    //       }

    //     });
    //   }

    // });

    $scope.$on("storeDataChange", function(e, data) {

        console.log('storeData changed event going on')
        var stores = data.stores;
        var numNearStores = stores.length;
        console.log(stores);
        //return;
        if (!stores.length) {
            alert("No stores found nearby, try different criteria");
            //$scope.setLoaderStatus(false);
            $scope.openSearchModal();
            return;
        }

        $scope.clearMarkers();
        $scope.clearListeners();

        $scope.placeUserLocationMarker();

        var bounds = $scope.addStoreMarkersToMap(stores);

        markerSpider.addListener('click', function(marker, event) {
            //#/tab/store/6
            $state.go('tab.store-detail', {
                storeID: marker.store.id
            });
            //$scope.setInfoPartyContent($scope.setStoreInfo(marker.store));
            //$scope.openInfoModal();
        });

        var latlng = $scope.search_form.location.latlng;
        //$scope.storesMap.panTo(new google.maps.LatLng(latlng[1], latlng[0]));
        //$scope.storesMap.panTo(latlng);
        $scope.storesMap.panTo($scope.myLatLng);


        if ($scope.nearestStoreMarkers.length) {
            // Getting a $digest already in progress error, so I'm just wrapping it in a timeout
            // so it's the last thing on the event queue
            $timeout(function() {
                $scope.storesMap.fitBounds(bounds);
            });
        }
        //$scope.setLoaderStatus(false);

        // Get the rest of the events
        // for(var i = 2; i <= data.page_count; i++) {

        //   eventful.search($scope.search_form, i, function(events, page_c) {

        //     numEventsLoaded += events.length;

        //     if (numEventsLoaded <= $scope.maxEventsToLoad){
        //       $scope.addMarkersToMap(events);
        //     }

        //   });
        // }

        $scope.showLoader = false;


    });

    $scope.placeUserLocationMarker = function() {
        if ($scope.userLocation) {
            new google.maps.Marker({
                position: $scope.userLocation,
                icon: new google.maps.MarkerImage('img/client-location.svg', null, null, null, new google.maps.Size(25, 25)),
                title: "Your location",
                map: $scope.storesMap
            });
        }
    }

    $scope.initiateMapElements = function() {
        if (!markerCluster) {
            var mcOptions = {
                gridSize: 50,
                maxZoom: 15,
                styles: [{
                    height: 50,
                    width: 50,
                    textSize: 14,
                    textColor: "#333",
                    url: "img/cluster-icon.png"
                }]
            };
            markerCluster = new MarkerClusterer($scope.storesMap, [], mcOptions);
        }

        if (!infoWindow) {
            infoWindow = new google.maps.InfoWindow();
        }

        if (!markerSpider) {
            var msOptions = {
                keepSpiderfied: true
            }
            markerSpider = new OverlappingMarkerSpiderfier($scope.storesMap, msOptions);
        }
    }

    $scope.clearMarkers = function() {
        /* for (var i = 0; i < $scope.partyMarkers.length; i++) {
      $scope.partyMarkers[i].setMap(null);
    }
    $scope.partyMarkers = [];
    markerCluster.clearMarkers();
    markerSpider.clearMarkers();
}*/

        for (var i = 0; i < $scope.nearestStoreMarkers.length; i++) {
            $scope.nearestStoreMarkers[i].setMap(null);
        }
        $scope.nearestStoreMarkers = [];
        markerCluster.clearMarkers();
        markerSpider.clearMarkers();
    }



    $scope.clearListeners = function() {
        markerSpider.clearListeners("click");
    }

    $scope.setPartyInfo = function(party) {

        var sD = party.date_time.start_date ? new Date(party.date_time.start_date) : undefined;
        var eD = party.date_time.end_date ? new Date(party.date_time.end_date) : undefined;

        var timeString;
        if (party.date_time.all_day) {
            timeString = 'All day';
        } else if (!sD && !eD) {
            timeString = 'No time specified';
        } else if (!eD) {
            timeString = 'Starts at ' + sD.toFormat("H:MI P") + '<br>' + sD.toFormat("MMM D, YYYY");
        } else {
            timeString = sD.toFormat("H:MI P") + ' to ' + eD.toFormat("H:MI P") + '<br>' + sD.toFormat("MMM D, YYYY");
        }

        party.timeString = timeString;
        party.description = (party.description ? party.description : "No description provided");

        return party;
    }

    $scope.setStoreInfo = function(store) {

        return store;
    }

    $scope.addMarkersToMap = function(parties) {

        var bounds = new google.maps.LatLngBounds();

        angular.forEach(parties, function(party) {
            var ll = new google.maps.LatLng(party.location.latlng[1], party.location.latlng[0]);

            var marker = new google.maps.Marker({
                position: ll,
                icon: new google.maps.MarkerImage('img/marker.svg', null, null, null, new google.maps.Size(40, 40)),
                title: party.name
            });

            marker.party = party;
            markerSpider.addMarker(marker);
            $scope.partyMarkers.push(marker);

            bounds.extend(ll);

        });

        markerCluster.addMarkers($scope.partyMarkers);

        return bounds;
    }

    $scope.addStoreMarkersToMap = function(stores) {

        var bounds = new google.maps.LatLngBounds();

        angular.forEach(stores, function(store) {
            //var ll = new google.maps.LatLng(party.location.latlng[1], party.location.latlng[0]);
            var ll = store.latLng;
            var marker = new google.maps.Marker({
                position: ll,
                icon: new google.maps.MarkerImage('img/marker.svg', null, null, null, new google.maps.Size(40, 40)),
                title: store.title
            });
            marker.store = store;
            markerSpider.addMarker(marker);
            //$scope.partyMarkers.push(marker);
            $scope.nearestStoreMarkers.push(marker);

            bounds.extend(ll);
            //$scope.storesMap.fitBounds(bounds);
            // so it's the last thing on the event queue
            $timeout(function() {
                $scope.storesMap.fitBounds(bounds);
            });

        });

        markerCluster.addMarkers($scope.nearestStoreMarkers);

        return bounds;
    }



});