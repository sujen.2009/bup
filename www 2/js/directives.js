angular.module(_DIRECTIVES_, [])

.directive("dynamicBackground", function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var el = element[0],
                attr = el.getAttribute('style');

            el.setAttribute('style', attr);

            // We need to watch for changes in the style in case required data is not yet ready when compiling
            attrs.$observe('style', function() {
                attr = el.getAttribute('style');

                if (attr) {
                    el.setAttribute('style', attr);
                }
            });
        }
    };
})

.directive('selectOnClick', function() {
    // Linker function
    return function(scope, element, attrs) {
        element.bind('click', function() {
            this.select();
        });
    };
})

.directive('openUrl', function() {
    // Linker function
    return function(scope, element, attrs) {

        var target = attrs.target || "_blank";

        element.bind('click', function() {
            window.open(attrs.openUrl, target, 'location=yes');
        });
    };
})

;