  /**
   * Backend functions
   */

  /**
   * utility function to generate a dynamic id (so that diffent unique ids can be assigned to different tasks)
   */

  var dynamic_id = function() {

      if (typeof dynamic_id.id == 'undefined') {
          //first id initialize
          dynamic_id.id = 1;
      }
      return dynamic_id.id++;

  };

  var generate_token = function(user) {
      //generate random number
      var rand = function() {
          return Math.random().toString(36).substr(2); // remove `0.`
      };

      var token = function() {
          return rand() + rand(); // to make it longer
      };

      var generated_token = token();

      //if user object is supplied also set the auth_token in the local storage as well as in the user object
      if (user && user.username) {
          localStorage.setItem('authorizationToken_' + user.username, generated_token);
          user.authorizationToken = generated_token;
      }

      return generated_token; // "bnh5yzdirjinqaorq0ox1tf383nb3xr"
  }
   //local storage key for the user token is auth_{{username}}
  var user_token = function(user) {
      if (user && user.username) {
          return localStorage.getItem('authorizationToken_' + user.username);
      }
      return false;
  }

  var is_valid_token = function(token) {
      if (token) {
          var foundUser = users.filter(function(thisUser) {
              var thisUserToken = user_token(thisUser);
              return thisUserToken !== false && thisUserToken == token;
          });
          if (foundUser.length) return foundUser[0];
      }
      //make sure user has id and authorizationToken
      // if (user && user.id && user.authorizationToken) {
      //     console.log('has the keys');
      //     var foundUser = users.filter(function(thisUser) {

      //         return thisUser.id == user.id && user_token(thisUser) !== false && user_token(thisUser) == user.authorizationToken;
      //     });
      //     console.log(foundUser);
      //     if (foundUser.length) return true;
      // }
      return false;
  }

      function encodeImageUri2(imageUri) {
          var c = document.createElement('canvas');
          var ctx = c.getContext("2d");
          var img = new Image();
          img.onload = function() {
              c.width = this.width;
              c.height = this.height;
              ctx.drawImage(img, 0, 0);
          };
          img.src = imageUri;
          var dataURL = c.toDataURL("image/png");
          return dataURL;
      }

      function encodeImageUri(imageUri) {
          var c = document.createElement('canvas');
          var ctx = c.getContext("2d");
          var img = new Image();
          img.onload = function() {
              c.width = this.width;
              c.height = this.height;
              ctx.drawImage(img, 0, 0);
          };
          img.src = imageUri;
          var dataURL = c.toDataURL("image/jpeg");
          return dataURL;
      }

      function encodeImage(src) {
          var canvas = document.createElement('canvas'),
              ctx = canvas.getContext('2d'),
              img = new Image();
          img.src = src;

          img.onload = function() {
              canvas.width = img.width;
              canvas.height = img.height;
              ctx.drawImage(img, 0, 0, img.width, img.height);
              return canvas.toDataURL();
          }

      }

      /**
       * Converts image URLs to dataURL schema using Javascript only.
       *
       * @param {String} url Location of the image file
       * @param {Function} success Callback function that will handle successful responses. This function should take one parameter
       *                            <code>dataURL</code> which will be a type of <code>String</code>.
       * @param {Function} error Error handler.
       *
       * @example
       * var onSuccess = function(e){
       *     document.body.appendChild(e.image);
       *     alert(e.data);
       * };
       *
       * var onError = function(e){
       *     alert(e.message);
       * };
       *
       * getImageDataURL('myimage.png', onSuccess, onError);
       *
       */
      function getImageDataURL(url, success, error) {
          var data, canvas, ctx;
          var img = new Image();
          img.onload = function() {
              // Create the canvas element.
              canvas = document.createElement('canvas');
              canvas.width = img.width;
              canvas.height = img.height;
              // Get '2d' context and draw the image.
              ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0);
              // Get canvas data URL
              try {
                  data = canvas.toDataURL();
                  success({
                      image: img,
                      data: data
                  });
              } catch (e) {
                  error(e);
              }
          }
          img.setAttribute('crossOrigin', 'anonymous');
          // Load image URL.
          try {
              img.src = url;
          } catch (e) {
              error(e);
          }
      }
      //http://stackoverflow.com/questions/4391575/how-to-find-the-size-of-localstorage
  var localStorageSpace = function() {
      var allStrings = '';
      for (var key in window.localStorage) {
          if (window.localStorage.hasOwnProperty(key)) {
              allStrings += window.localStorage[key];
          }
      }
      return allStrings ? 3 + ((allStrings.length * 16) / (8 * 1024)) + ' KB' : 'Empty (0 KB)';
  };
  /****************************************************************************************************************************************/
  /****************************************************************************************************************************************/

   //anxiety, sound,
   //practices of type sound  

  /**
   * Practice types
   */

   //reusable object homeworkSlides (in both task of type practice and task of type homeworks)
  var homeworkSlides = {
      reorder: [{
          type: "reorder",
          title: "This is your list of things that gives you anxiety",
          list: ["Bath in public swimming pool", "Call a friend", "go on the subway"]
      }, {
          type: "reorder",
          title: "What are the activities that you would like to get involved in?",
          list: ["Going to a park", "go shopping", "Play game in our smartphone"]
      }],
      form: [{
          type: "form",
          title: "Describe your other feelings and other possible bodily reactions"
      }, {
          type: "form",
          title: "Describe one of your happiest moments this year"
      }],
      dynamic_button: [{
          type: "dynamic_button",
          title: "Does the description correspond with what we went through?",
          buttons: [{
              link: "http://twitter.com",
              label: "Correct"
          }, {
              link: "http://facebook.com",
              label: "Not correct"
          }]
      }, {
          type: "dynamic_button",
          title: "Watch this youtube video",
          buttons: [{
              link: "http://youtube.com",
              label: "Watch"
          }]
      }]
  };

  var icons = {

  }

  var tasks = {

      //practices can be of diffent types
      practices: {
          sound: [{
              id: dynamic_id(),
              title: "Breath deeply",
              type: 'sound',
              //icon: 'http://dev.websearchpro.net/BUP/www/images/icon_breath@2x.png',
              icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACjCAMAAAAtp3V4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkxQTFRFLDRg////////LDRg////LDRg////LDRgLDRg////LDRg////LDRgLDRgLDRgLDRgLDRg////LDRgLDRg////////LDRg////////////////////////LDRg////LDRgLDRgLDRg////////LDRg////LDRg////LDRgLDRg////////////////////////iIylLDRg////LDRgbnSS////////LDRgLDRg////////LDRg////////////////////////////////////////////////LDRgLDRgLDRg////LDRgLDRg////xcfTLDRgtbjI////LDRgLDRgLDRgLDRg////////////////////LDRg////LDRgLDRg////////LDRg////LDRg////LDRgQEuGQk2HRE+JR1GKSVOMS1aNTViPT1V6UFqQUVh8UlyRVF6TVl2AWWKWWmSXW2SXXWeZXmSFX2maYmubY2ycZWuLZm+eaHGga3OhbHWibXWib3ekcXqldX2oeICqeoKrfYSsfoatf4aug4ihg4uxhIiiho2yh46ziIyliY2mjJO2jpOqj5a4mJ6+mqC/nJ+0nKLAoabDoafEo6jFparGp6u9qKu+qq/Jq6/ArLHKrbDBr7LDsLXNsbTEs7jPtrnIu77MvL/UwMLQwMTXwsbZxcjaycvWyczdzM7YztDg0NPh0tXj1tfg19nl2dvn293o3d/q4uPs5OXq5Obu5ufs5ujv6erx6+zy7e707/D19PT4/f39////28c3qAAAAGZ0Uk5TAAACAwMGBgkMDA8PEhghJDA2OTw8RUhIS05RVFpgYGNscnN3eHl7gIGEhIWGh4iJio2NkJCQlpmcnKKlp6utrq+wsrm8vsHExsnMzc/S1tfY2Nnb3uHk5efo6uvw8PP29vf5+vz9w0jslAAABj5JREFUeNrt3fl/1EQUAPAJitYLTw4vtIooKt5a632igjfegChgYglaU4wosbK0qUbUKCai8Qheq1GjBhXwQtH9x5zJ9tjuzraZzEz2rez7Ibvbz6fpt0lm8pKZl0VKTRwyb/Fty56qgIi1y5acO/fgWh2aeDu/d1UFWDx2xSkU6onXVkDGVbPrqRdWwMbZk6gzbqz+dO8HWwef1kBE3+DQh79WVdfUUGfckv7ou00asNj8Qwq7eYJ6Hfm8b4sGMIb/Ht+uhHo++bTrGQ1kDPxCdGemVDQvlfZpQGP9HuI7ARHq9fjdn/0a2Bggx0Avoc4n6K0a4BghwpMwtRe/fquBjgQTL1HQEYS8CTb1FUxcVW1UuzTgsTdtWBfg5XvQqR9h5FloCV4OQae+ipE3oRV42Q+dugkj70dr8bIPOrUfI1cg0gFAl2obSBfQoXaoHWqH2qECpuqW44ZhlFQjCkPPtQx4VMMJxoyTIw4cExDV9OjMca5fAkE13Kmdo1rPbDXVCpKsEdqtpFphwhKR3SoqIzTFWq2g6n6SJ0KjcKoTJznDLZaqh0n+KJsFUq044YnYLozqJrzhF0T1E/4o6wVQ9XIiIsqGdGoGaUhi+gPWlEydUhoHtVmfYTl+JMSaizqFtExN9nByyG/NQ20qjT0jR+oV6fKofs40xI64+oEcVHp/GjsZfrUJ1pdELVH/mpdty+j0/9ORQjVivqzOpB7opgwqrasMdN7TXJamxUp1BORzNu0AEk6l7X726w+TshZLNDUQIaVay4Kplhgp1eqIpUaCpNja2DHrIqm2gCuk/OtiojZs1IDjNofHullZqDbzPpsyyoyblYUasncwTE0rEkY1+a7iMuQ9tiiqL3T30479UBQ1Ftb6m3bThhhqKWfyznL0O2KovvCN2vjvl8VQY9FHKu1o1UVQTcHNn55T2iKoTo68PcMFDMMGyEwNmLrrzMGw2szUmDlrz5W16PxUQ+g5Ndd6s1Lre2tNVESZe9asVJflFMhx/eoJp3rCqE7mbZCVGua4HZInDxBPtYRRjcyNoOVUTThVVgfQVtSofahhh3pgHwDCm1XcPp1V+54CIJ9Y2yhdcdonCWyj1Lr+0rIkqVUJuGBpo8vAgi6uywfYLYs2uhFU31QB316TctPSlnPTspSI36ySbgVLuMFuS7rB3jAmJn7YYuoROwaqIXIskJICTXcK5Bpi4zu7NoyDiRtia5xgA3fgsnE4mCcX9GUOB1MGxj1xrV/sIHvjPss9dcGSPHVB6oSQaVf0v51mI2jyUqmIyUvUKWEed4uSMSWMPtEuZOlfqfUZZQkT7TSdNn0xzn7eKnD6Iq0XyD6DUfcKnBTadKqtmwFrx4VOtW06gXlabNMJzJo0qtZ0Wrjf/Jgz3Jiz5EL0ZPuIWv1nOs1/I3N6JqOEIXRLEx26ablB3LoShkyFIXEYhtNXOUWyC0M4q8LylQa1togpLKKIqUk6wBZsyQNPwZ0ZcUFjxqtIvjLGgGfns9aHchaHlvJWMsbsYzTcJbdeLmlQfMktOWLZu62wJYXMaVrYLuXhKdaXvEXFUUn1X5aeK/I4nmoh9AER5WmcfEMdgh+7Yft0buQ73E8JkfDcFct2/bGkCqdXgetYQoY4Ok+z6VA71A61jahP4uV66FQDIx9FD+PlAHTqSxh5N7oVL4ehU1/DyBvQeXj5PnRqiJFnoLl4uRs69XeMPBYd9gR+GYQtHcbERxSkXI5fE9jU3Zi4GFNPJj3rCGTpG0Q4mzyC+x78Zv8LcKWb/8HA+9KnhV+UPn5/A1RpP2lTlctS6qx3yfs9QM8Dz6dfGrDtmJSq3Pk5+bT/dYjSt8jer3z8AKpSu9Vv0m8QSMCdtUZ+SmFfq+eMUrtWql9Vv5ph36fbhwaBxPD2L/6qqr5UVx81SlUWqurbYL+F499tqnopGqPOXK6qGz+DKf3kWVV9qGucqhy3RlXVl3f8Ac35244XMWzdHDRBVU5V09j4zs4ffwYS3+9887mqqhvVUpWLVbBxJZpMPagHqvTqmXVURVmwDqR0kYImqGNx/HJ40AfnjPNqqejQhSthQR9f1IXoVIQO774LDvTeBUfW2uqoOGad1nP70tWtRa5ZekfP6UfXwZT/AK1wy4NGLGTJAAAAAElFTkSuQmCC',
              audio_background: 'http://dev.websearchpro.net/BUP/www/images/audio_background_yellow@2x.png',
              audio_bgcolor: '#7eaf47',
              audio_duration: 1,
              practice_duration: 5,
              audio_filename: "This file name is dynamic"
          }, {
              id: dynamic_id(),
              title: "Andas djupt",
              type: 'sound',
              icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACjCAMAAAAtp3V4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkxQTFRFLDRg////////LDRg////LDRg////LDRgLDRg////LDRg////LDRgLDRgLDRgLDRgLDRg////LDRgLDRg////////LDRg////////////////////////LDRg////LDRgLDRgLDRg////////LDRg////LDRg////LDRgLDRg////////////////////////iIylLDRg////LDRgbnSS////////LDRgLDRg////////LDRg////////////////////////////////////////////////LDRgLDRgLDRg////LDRgLDRg////xcfTLDRgtbjI////LDRgLDRgLDRgLDRg////////////////////LDRg////LDRgLDRg////////LDRg////LDRg////LDRgQEuGQk2HRE+JR1GKSVOMS1aNTViPT1V6UFqQUVh8UlyRVF6TVl2AWWKWWmSXW2SXXWeZXmSFX2maYmubY2ycZWuLZm+eaHGga3OhbHWibXWib3ekcXqldX2oeICqeoKrfYSsfoatf4aug4ihg4uxhIiiho2yh46ziIyliY2mjJO2jpOqj5a4mJ6+mqC/nJ+0nKLAoabDoafEo6jFparGp6u9qKu+qq/Jq6/ArLHKrbDBr7LDsLXNsbTEs7jPtrnIu77MvL/UwMLQwMTXwsbZxcjaycvWyczdzM7YztDg0NPh0tXj1tfg19nl2dvn293o3d/q4uPs5OXq5Obu5ufs5ujv6erx6+zy7e707/D19PT4/f39////28c3qAAAAGZ0Uk5TAAACAwMGBgkMDA8PEhghJDA2OTw8RUhIS05RVFpgYGNscnN3eHl7gIGEhIWGh4iJio2NkJCQlpmcnKKlp6utrq+wsrm8vsHExsnMzc/S1tfY2Nnb3uHk5efo6uvw8PP29vf5+vz9w0jslAAABj5JREFUeNrt3fl/1EQUAPAJitYLTw4vtIooKt5a632igjfegChgYglaU4wosbK0qUbUKCai8Qheq1GjBhXwQtH9x5zJ9tjuzraZzEz2rez7Ibvbz6fpt0lm8pKZl0VKTRwyb/Fty56qgIi1y5acO/fgWh2aeDu/d1UFWDx2xSkU6onXVkDGVbPrqRdWwMbZk6gzbqz+dO8HWwef1kBE3+DQh79WVdfUUGfckv7ou00asNj8Qwq7eYJ6Hfm8b4sGMIb/Ht+uhHo++bTrGQ1kDPxCdGemVDQvlfZpQGP9HuI7ARHq9fjdn/0a2Bggx0Avoc4n6K0a4BghwpMwtRe/fquBjgQTL1HQEYS8CTb1FUxcVW1UuzTgsTdtWBfg5XvQqR9h5FloCV4OQae+ipE3oRV42Q+dugkj70dr8bIPOrUfI1cg0gFAl2obSBfQoXaoHWqH2qECpuqW44ZhlFQjCkPPtQx4VMMJxoyTIw4cExDV9OjMca5fAkE13Kmdo1rPbDXVCpKsEdqtpFphwhKR3SoqIzTFWq2g6n6SJ0KjcKoTJznDLZaqh0n+KJsFUq044YnYLozqJrzhF0T1E/4o6wVQ9XIiIsqGdGoGaUhi+gPWlEydUhoHtVmfYTl+JMSaizqFtExN9nByyG/NQ20qjT0jR+oV6fKofs40xI64+oEcVHp/GjsZfrUJ1pdELVH/mpdty+j0/9ORQjVivqzOpB7opgwqrasMdN7TXJamxUp1BORzNu0AEk6l7X726w+TshZLNDUQIaVay4Kplhgp1eqIpUaCpNja2DHrIqm2gCuk/OtiojZs1IDjNofHullZqDbzPpsyyoyblYUasncwTE0rEkY1+a7iMuQ9tiiqL3T30479UBQ1Ftb6m3bThhhqKWfyznL0O2KovvCN2vjvl8VQY9FHKu1o1UVQTcHNn55T2iKoTo68PcMFDMMGyEwNmLrrzMGw2szUmDlrz5W16PxUQ+g5Ndd6s1Lre2tNVESZe9asVJflFMhx/eoJp3rCqE7mbZCVGua4HZInDxBPtYRRjcyNoOVUTThVVgfQVtSofahhh3pgHwDCm1XcPp1V+54CIJ9Y2yhdcdonCWyj1Lr+0rIkqVUJuGBpo8vAgi6uywfYLYs2uhFU31QB316TctPSlnPTspSI36ySbgVLuMFuS7rB3jAmJn7YYuoROwaqIXIskJICTXcK5Bpi4zu7NoyDiRtia5xgA3fgsnE4mCcX9GUOB1MGxj1xrV/sIHvjPss9dcGSPHVB6oSQaVf0v51mI2jyUqmIyUvUKWEed4uSMSWMPtEuZOlfqfUZZQkT7TSdNn0xzn7eKnD6Iq0XyD6DUfcKnBTadKqtmwFrx4VOtW06gXlabNMJzJo0qtZ0Wrjf/Jgz3Jiz5EL0ZPuIWv1nOs1/I3N6JqOEIXRLEx26ablB3LoShkyFIXEYhtNXOUWyC0M4q8LylQa1togpLKKIqUk6wBZsyQNPwZ0ZcUFjxqtIvjLGgGfns9aHchaHlvJWMsbsYzTcJbdeLmlQfMktOWLZu62wJYXMaVrYLuXhKdaXvEXFUUn1X5aeK/I4nmoh9AER5WmcfEMdgh+7Yft0buQ73E8JkfDcFct2/bGkCqdXgetYQoY4Ok+z6VA71A61jahP4uV66FQDIx9FD+PlAHTqSxh5N7oVL4ehU1/DyBvQeXj5PnRqiJFnoLl4uRs69XeMPBYd9gR+GYQtHcbERxSkXI5fE9jU3Zi4GFNPJj3rCGTpG0Q4mzyC+x78Zv8LcKWb/8HA+9KnhV+UPn5/A1RpP2lTlctS6qx3yfs9QM8Dz6dfGrDtmJSq3Pk5+bT/dYjSt8jer3z8AKpSu9Vv0m8QSMCdtUZ+SmFfq+eMUrtWql9Vv5ph36fbhwaBxPD2L/6qqr5UVx81SlUWqurbYL+F499tqnopGqPOXK6qGz+DKf3kWVV9qGucqhy3RlXVl3f8Ac35244XMWzdHDRBVU5V09j4zs4ffwYS3+9887mqqhvVUpWLVbBxJZpMPagHqvTqmXVURVmwDqR0kYImqGNx/HJ40AfnjPNqqejQhSthQR9f1IXoVIQO774LDvTeBUfW2uqoOGad1nP70tWtRa5ZekfP6UfXwZT/AK1wy4NGLGTJAAAAAElFTkSuQmCC', //encodeImageUri2('http://dev.websearchpro.net/BUP/www/images/icon_breath@2x.png'),
              audio_background: 'http://dev.websearchpro.net/BUP/www/images/audio_background_yellow@2x.png',
              audio_bgcolor: '#7eaf47',
              audio_duration: 1,
              practice_duration: 5,
              audio_filename: "This file name is dynamic"
          }],
          exposure: [{
              id: dynamic_id(),
              title: "Count",
              //icon: 'http://dev.websearchpro.net/BUP/www/images/icon_check_green@2x.png',
              icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACjCAMAAAAtp3V4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAlVQTFRFAUEx////////AUEx////AUEx////AUExAUEx////AUEx////AUExAUExAUExAUExAUEx////AUExAUEx////////AUEx////////////////////////AUEx////AUExAUExAUEx////////AUEx////AUEx////AUExAUEx////////////////////////cJSLAUEx////AUExUX1y////////AUExAUEx////////AUEx////////////////////////////////////////////////AUExAUExAUEx////AUExAUEx////ucvGAUExpr23////AUExAUExAUExAUEx////////////////////AUEx////AUExAUEx////////AUEx////AUEx////AUExAVpEBFxGB15ICmBLC2BLEGRPE2ZRFWdTFmhTGWpWHGtYH21aIm9cJXFeKHNhKXRhK2BTK3VjLmNVLndlM3ppNGdaNHtpN31sOn9uPIBvPW5hPYFwQINyQ4V0RnRpRod3TIt7T4x9UI1+Uo5/VZCCWpSFW5SGXpaIZJqNZ5yPao+Ga5CHbaCTcJSLcZSLd6abeJqReaicfKqegK2iga2jhK+lh6aeh7GnirOpjbWrkLeulLqxlbCplrGqlruymrStnLWvnrexnsC4obmzp724q8nBrcK9rsvEs8bCt9DKvdTPvs/KwNbRwdHNw9jTydzXzN7azdrXz+Dc0uLe2Obi2+jl3ubk3urn4enn4ezp5O7r5+/t6vHw8PX0/P39////HdibbQAAAGZ0Uk5TAAACAwMGBgkMDA8PEhghJDA2OTw8RUhIS05RVFpgYGNscnN3eHl7gIGEhIWGh4iJio2NkJCQlpmcnKKlp6utrq+wsrm8vsHExsnMzc/S1tfY2Nnb3uHk5efo6uvw8PP29vf5+vz9w0jslAAABkpJREFUeNrt3WmX1EQUBuAqFMUNNzY3dBRRVNwVcV9RwR13QBQwEYIZidIqzWiEMdjaGrA1aitGW0XLHVGRRUXA93f5oWGmGTLpqsqtnvKcfj/NmcPpfk5IVzq37s0w3pLDJk2/fc7TsCKL58w6b+KhrTo2+OPkmQtgWR6/8tQM6knXwcpcPX4o9SJYm3MOoI66qfnb7R+v6XvGtSJL+9Z+sqOpuraFOupWAMCPK13LsupnAMAtg9TrAWDXq66F6d89cFwZ5/wCANjyrGtlVvwOAGdxzhlnkwBgy1LX0izbBgDjGGec3QDgr17X2qzYDWAm44xPBoA1rsVZBwAnc8ZnAvjetTqbAVzK2VEAsNJu6isAFjQ/VFtcy7MdwDh2IYAPbad+CuBsNgvAWtupbwC4mc0D0Gs7dSWAB9hiAEttp/YCmMcAwHapuxzAgi61S+1Su9Qu1WZqUK5Uk4HElbBkI9UPq3WRkbQWBTZRg2zmADcu20H1o1S0TaMajDi1FAvJJOGIUkuJUEgajhg1UIIKIURaHhGqFwuNJEHnqeWG0Eulw1SvJrRTDzpJDRqiQBph56iRKJjY6xA1FoVT9zpB9RJBkDQwT/XqgiSNwDSVSqpk1aLSSVWsWtREEKbhGaTGgjSy64AGNRLEqZmiBoI8kRmql9JTRWCEWjMgFalngFoWRlKlp3oNM1RRIqfGhqQipaYGwlgiYirFZSrO/kbe/qKlRC1RSIe7e6iQUhMS6TDWtodVhVoikg5jrRBSYypptjWlo/p00mxrSEaNCKWZ1hoZNaWUZlp9ImpAK816wYiIWiWWZnxK60TUunGpEB4J1e+ANH8NkKaGHZBm/DsdarUD0vyTVZpa74A0/2SVpnZEmnszIEvNW1UbFSpp7soqSy3nVp1CImnu7aAstZJfHwtppCIhoFbbVPJCEmnuEiBLTdrVHEMKqRDmqC3V0ZBCao56QB03JJAaow6pOIfFpaaoB9XGw8JSU9SDy81hUamxEyDHqik197Ea1qorNbhYDWPVlhqkZlv1pRTUWH4rJ6zqS+tGv65IbDvJV5Aovq6EBbbIFGpdMQG1pL+dp1KVq5i+Ycm1KtUPKW5Y8itWOVa1SqdPQY31tnTVpCnJzXWktf2sWD2ukVDbFQIzrap17oimvJaqW5Ur8j4Nte37HmRVlqZERcv2G8FDrOq7HFWqAntDzaqxHxNQUSVqgS1WDWmdbNtCZjNgwKqzxxXRbbEl8lYdabudSxWqVOG67unuG8aU28GppFVvh9OnpMrtB9T1pO0OqmLrgonGJdmDqkgtm5O27wlSbLNJTEklmhgVqcb6bCR67VRbwipmpAl9S1jRrdbh/vt9E9TARP+a1IiAelNoSC+NXTNU+mY7ybZgDapHfLrKNobr9FoTd9vKtrBrdbCTfrSkp270RhgIrfLzQZqDIWRWhUkm3XEbIqvKzJX2EBPFOtBQmmTTn7fyCveyK87cFRm4K9gkqDjEVmyMMSiwwCqPBhYcDvW0W5oS3+0s1XVLWge2EWm8VfFB5kh92ap67ohQXa+iho19vfchGbr3KtKnQUMXSvcog1Bqla1Hnv5b0D0gwo/aXL/SarHnWZA+dsML42G4aS3yi746+cNMvFJUTVoqG2kSV0o+xSt3n2bTpXapXer/iPoUgGW2U58D8Bh7BMAK26kvAbiH3Qag33bqmwBuZOcD+Mh26mcAzmQTAWy1nboTwPHsiCcB9Nkt7QfwKGf8CgCb7aZuBTCdM34KAKyzWfo2AIznjLN7Aex5wV7pqr0A7meccXYxAGxfbqu0dycAXM4442zsBwCwzdLrwPM7AGDDcYwzzvhd3wDAnrdslL6zFwC+eJA1qT3OTwCAzdZdtdb9BgD4wTl3H3XMfOe75p9m2PX5+rV9lqR//Vf/NFXfOguP2UflUx3nPWv/Cse/GxznMrafOnqu46z+2k7ply86zsNjBqj8hEWO47y+8W/bnH9ufM1xnCUT2CCVn+Y4juM4q9/f9OsfluSXTe++3FT1sFYqv8SxNlexA6mHzLBVes3oIVTOpyyxUjqNs0Hq/pw41z7oQxMGeK1UdvjU+XZBn5g2hmVTGTuy5257oPdNObrVNoTKGBt7+ow7Zi8cWeSi2XfOOOPYITD+H1TgyTHKJB3JAAAAAElFTkSuQmCC',
              type: 'exposure',
              label_ten: "maximum",
              label_zero: "minimum",
              label_go_to_graph: "",
              feedback: false
          }, {
              id: dynamic_id(),
              title: "Räkna",
              //icon: 'http://dev.websearchpro.net/BUP/www/images/icon_check_green@2x.png',
              icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACjCAMAAAAtp3V4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAlVQTFRFAUEx////////AUEx////AUEx////AUExAUEx////AUEx////AUExAUExAUExAUExAUEx////AUExAUEx////////AUEx////////////////////////AUEx////AUExAUExAUEx////////AUEx////AUEx////AUExAUEx////////////////////////cJSLAUEx////AUExUX1y////////AUExAUEx////////AUEx////////////////////////////////////////////////AUExAUExAUEx////AUExAUEx////ucvGAUExpr23////AUExAUExAUExAUEx////////////////////AUEx////AUExAUEx////////AUEx////AUEx////AUExAVpEBFxGB15ICmBLC2BLEGRPE2ZRFWdTFmhTGWpWHGtYH21aIm9cJXFeKHNhKXRhK2BTK3VjLmNVLndlM3ppNGdaNHtpN31sOn9uPIBvPW5hPYFwQINyQ4V0RnRpRod3TIt7T4x9UI1+Uo5/VZCCWpSFW5SGXpaIZJqNZ5yPao+Ga5CHbaCTcJSLcZSLd6abeJqReaicfKqegK2iga2jhK+lh6aeh7GnirOpjbWrkLeulLqxlbCplrGqlruymrStnLWvnrexnsC4obmzp724q8nBrcK9rsvEs8bCt9DKvdTPvs/KwNbRwdHNw9jTydzXzN7azdrXz+Dc0uLe2Obi2+jl3ubk3urn4enn4ezp5O7r5+/t6vHw8PX0/P39////HdibbQAAAGZ0Uk5TAAACAwMGBgkMDA8PEhghJDA2OTw8RUhIS05RVFpgYGNscnN3eHl7gIGEhIWGh4iJio2NkJCQlpmcnKKlp6utrq+wsrm8vsHExsnMzc/S1tfY2Nnb3uHk5efo6uvw8PP29vf5+vz9w0jslAAABkpJREFUeNrt3WmX1EQUBuAqFMUNNzY3dBRRVNwVcV9RwR13QBQwEYIZidIqzWiEMdjaGrA1aitGW0XLHVGRRUXA93f5oWGmGTLpqsqtnvKcfj/NmcPpfk5IVzq37s0w3pLDJk2/fc7TsCKL58w6b+KhrTo2+OPkmQtgWR6/8tQM6knXwcpcPX4o9SJYm3MOoI66qfnb7R+v6XvGtSJL+9Z+sqOpuraFOupWAMCPK13LsupnAMAtg9TrAWDXq66F6d89cFwZ5/wCANjyrGtlVvwOAGdxzhlnkwBgy1LX0izbBgDjGGec3QDgr17X2qzYDWAm44xPBoA1rsVZBwAnc8ZnAvjetTqbAVzK2VEAsNJu6isAFjQ/VFtcy7MdwDh2IYAPbad+CuBsNgvAWtupbwC4mc0D0Gs7dSWAB9hiAEttp/YCmMcAwHapuxzAgi61S+1Su9Qu1WZqUK5Uk4HElbBkI9UPq3WRkbQWBTZRg2zmADcu20H1o1S0TaMajDi1FAvJJOGIUkuJUEgajhg1UIIKIURaHhGqFwuNJEHnqeWG0Eulw1SvJrRTDzpJDRqiQBph56iRKJjY6xA1FoVT9zpB9RJBkDQwT/XqgiSNwDSVSqpk1aLSSVWsWtREEKbhGaTGgjSy64AGNRLEqZmiBoI8kRmql9JTRWCEWjMgFalngFoWRlKlp3oNM1RRIqfGhqQipaYGwlgiYirFZSrO/kbe/qKlRC1RSIe7e6iQUhMS6TDWtodVhVoikg5jrRBSYypptjWlo/p00mxrSEaNCKWZ1hoZNaWUZlp9ImpAK816wYiIWiWWZnxK60TUunGpEB4J1e+ANH8NkKaGHZBm/DsdarUD0vyTVZpa74A0/2SVpnZEmnszIEvNW1UbFSpp7soqSy3nVp1CImnu7aAstZJfHwtppCIhoFbbVPJCEmnuEiBLTdrVHEMKqRDmqC3V0ZBCao56QB03JJAaow6pOIfFpaaoB9XGw8JSU9SDy81hUamxEyDHqik197Ea1qorNbhYDWPVlhqkZlv1pRTUWH4rJ6zqS+tGv65IbDvJV5Aovq6EBbbIFGpdMQG1pL+dp1KVq5i+Ycm1KtUPKW5Y8itWOVa1SqdPQY31tnTVpCnJzXWktf2sWD2ukVDbFQIzrap17oimvJaqW5Ur8j4Nte37HmRVlqZERcv2G8FDrOq7HFWqAntDzaqxHxNQUSVqgS1WDWmdbNtCZjNgwKqzxxXRbbEl8lYdabudSxWqVOG67unuG8aU28GppFVvh9OnpMrtB9T1pO0OqmLrgonGJdmDqkgtm5O27wlSbLNJTEklmhgVqcb6bCR67VRbwipmpAl9S1jRrdbh/vt9E9TARP+a1IiAelNoSC+NXTNU+mY7ybZgDapHfLrKNobr9FoTd9vKtrBrdbCTfrSkp270RhgIrfLzQZqDIWRWhUkm3XEbIqvKzJX2EBPFOtBQmmTTn7fyCveyK87cFRm4K9gkqDjEVmyMMSiwwCqPBhYcDvW0W5oS3+0s1XVLWge2EWm8VfFB5kh92ap67ohQXa+iho19vfchGbr3KtKnQUMXSvcog1Bqla1Hnv5b0D0gwo/aXL/SarHnWZA+dsML42G4aS3yi746+cNMvFJUTVoqG2kSV0o+xSt3n2bTpXapXer/iPoUgGW2U58D8Bh7BMAK26kvAbiH3Qag33bqmwBuZOcD+Mh26mcAzmQTAWy1nboTwPHsiCcB9Nkt7QfwKGf8CgCb7aZuBTCdM34KAKyzWfo2AIznjLN7Aex5wV7pqr0A7meccXYxAGxfbqu0dycAXM4442zsBwCwzdLrwPM7AGDDcYwzzvhd3wDAnrdslL6zFwC+eJA1qT3OTwCAzdZdtdb9BgD4wTl3H3XMfOe75p9m2PX5+rV9lqR//Vf/NFXfOguP2UflUx3nPWv/Cse/GxznMrafOnqu46z+2k7ply86zsNjBqj8hEWO47y+8W/bnH9ufM1xnCUT2CCVn+Y4juM4q9/f9OsfluSXTe++3FT1sFYqv8SxNlexA6mHzLBVes3oIVTOpyyxUjqNs0Hq/pw41z7oQxMGeK1UdvjU+XZBn5g2hmVTGTuy5257oPdNObrVNoTKGBt7+ow7Zi8cWeSi2XfOOOPYITD+H1TgyTHKJB3JAAAAAElFTkSuQmCC',
              type: 'exposure',
              label_ten: "maximum",
              label_zero: "minimum",
              label_go_to_graph: "",
              feedback: true
          }],
          homework: [{
              id: dynamic_id(),
              type: "homework",
              title: "homework",
              //icon: "http://dev.websearchpro.net/BUP/www/images/icon_homework_yellow@2x.png",
              icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACjCAMAAAAtp3V4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkxQTFRFq3sC////////q3sC////q3sC////q3sCq3sC////q3sC////q3sCq3sCq3sCq3sCq3sC////q3sCq3sC////////q3sC////////////////////////q3sC////q3sCq3sCq3sC////////q3sC////q3sC////q3sCq3sC////////////////////////0LRwq3sC////q3sCxaRR////////q3sCq3sC////////q3sC////////////////////////////////////////////////q3sCq3sCq3sC////q3sCq3sC////6Nu6q3sC4tGn////q3sCq3sCq3sCq3sC////////////////////q3sC////q3sCq3sC////////q3sC////q3sC////q3sCuZEsupIvvJU1v5o+wp9GzrFqzrJr0LRw0LVx0rl418GI3MiW3MmX3sub3syd382f4M6i4tGo5NWu5ti06KUB6KYE6acH6agK6akM6aoQ6qsT6qwW6q0X6q0Z6q8c6t2/67Ei67Ej67Il69/B7LMo7LQr7LUu7bc07bg37bk57bk67bo97rtA7rxD7r1E779M78FP78FQ78JS7+XN8MNV8MVb8MZe8chl8cln8s1x889589B89NOB9NSE9NSG9NWH9O7e9deN9diQ9diR9e/h9tqW9tyd9+Co9+Gr+OKu+OSz+ea3+ei9+enA+urD+uzJ+u3M++7P++/S+/HY/PLb/PPe/PTh/fXk/ffn/fjq/vrw/v38////wLHhVAAAAGZ0Uk5TAAACAwMGBgkMDA8PEhghJDA2OTw8RUhIS05RVFpgYGNscnN3eHl7gIGEhIWGh4iJio2NkJCQlpmcnKKlp6utrq+wsrm8vsHExsnMzc/S1tfY2Nnb3uHk5efo6uvw8PP29vf5+vz9w0jslAAACIpJREFUeNrtneef1EQYx7Moig0rxYaiiKJiV8ReUcGOHRAFjKJYUKwRghfR0XPOCBeMOi4xOpZooisooKLo/WPOTNqk7ebYTW5e5HmR3bvP3ez3Js/8njKzt1KLs0Nmzr9tyVNjQtjqJYvOnXEwTyfFT2ctXDEmmD12xSk5qCdeOyakXTUtjXrhmLB2dgJ10o3+d/d+vXXkbUUI2ziy7Zs/fKprONRJt7Bv/TKsCGbwVwZ2c4x6Hf163yeKgDb6TzSvFPV8+tWudxQhbcvvlO5MhirNZKQbFUFt0x7Kd4JEUa8nz/4aUoS1LdQHFlLUWRR6qyKwbaeEJxHUheTxZ0Vo20kQL2lJR1DkYbFRPyaIK/xFtUsR3PayhXUBuX4lOuq3BPIsaRG5bhMd9VMCeZO0jFyHREcdJpD3S6vJdaPoqEMEcplEBUB0UmUzlYAGtUFtUBvUBrVBbVAb1Aa1QW1QG9RcAxAhjHEnMBdjG5lQG/drQp2N44UDORhbyIDqYFA1ox0hZgy3jZK8mo5iwox5GMG+UWHx+OEUW3qPWdEMy+30NKdfVKtTxmy9mNN0OuUM9omKQ+9ESIcQAkVRyQM0kZW8nx7K9QQj5TwObhMnj434ro0HiGoXDKJC0+burJWGVRH313g20rVCR3YHhIq6OqJpRzhtNR/Us3utPlwHKoUyQnf0Yp/VQ1DP0nsLRF2oVHjD5Wf5E6uGDoiNUlpWIypxhADWobcaBFOKYcnYMGhUja1YRMILzBsW+DPpAcXoAkoFxCCD+HJSDWo6ZpHYmo6IZqBb/oOZDVh2SmNdtQpUkB9i2ok1A2J1crTkyssPWHAQqGo8i/6stjH2bz51AgtHr+xZ3Guo4bRZCc5Yz9ggpu8DNrbVZKQhZowf1eykULPpgRnOlGuqKVaLX2/hGiuOAbx3jR8V9UTlA4CHQliVgtlpYeglrjWgUjaTza2rc78YuoTqg/YOAvWgUlfAnPpzqCxiFSQyVaJqBiIryzeL6CJIwDqRg8aoeiYvYGWAFQxCcixQCaqazbEdK34t+vNpVPLMiX8AIjurV6AKVK2g1Ij8EJhpB1AtlCNX1aOSWYFR4Ud1MZqjhLAml1WQX0ecxHP0MCiTCGsadS0rFaLgJRyjEDVMWz3bBBOrAOGtdWEuagDqtoEQYqUi5gphkORR/fza1pWJ0FUNwpwJMlw/+0stKzu35GKjqFWiIuzyPQZMMhf+x8n84axYdTBIZA0Ic1AutipBBfndlejeak6QFMWoBpezkizQ6ZEEDm5WHZrBkXyaRRtugm1DVQrECmg5bQs3DHkEz9PqWFZaHH28hDtmdFW34+hW1EmrXgE0I0hGuTIqhWr4f4/b7trZqkesgkmz1DxU1jbpeL1ktbYkUGM6H7YrOFQNl20FDBiVSCoKTE+Jq4o4UYtQWVHopKq7oEBn5bVWEaqTad+2jRiYTqCd1VW+wgamlS7Qq1EAUJADRlKF7HS0Ai7W4hrM69SmqwijsDVqcF1RKqxK9ySQk1WPFhB6OEhF0Sq/vZ/TB06jQjtqsBbvT9SgAKoeCGtcQiVR/QqRyCoUQaz8Jnq0gHhUX608CyiC6GrQl8CZ4tqkM+4itW5dhX5t7XX86jq5w8amL12xmom2S5A4kAUZl+iwElStx8YYmcEcVDtuZul5W3WVVKxq0ZaVGWZ6MO2ralSRAuTUWVyTuKpy4RHFk5QU1oyuBk0t6rY2KR00rn8NaltWwAhiENe1TKNGewRds8BaFCAQVtfIRQ1AHVMTQ6x8YQ3jPY/qZ4e9ZbUCVBBsjeZ2LT2YXlY4o1aBo0OYqtEHi9p2kkJFVJG7q3pBcW1ryS2WhGC5OXsBVSWBrhVtnqo4KAP4/mq8kQnys8CqimuHltaI7a9wO+zpJcMXLMGkgbbL19ZtNgiJWh23luJagUbkEold+LSuRpvFJLjpYMIUgERLt3txHfYsHRNMvFhBX1ij2iSBynKr3rJam64GXUsz2whit94qsX1dW74aaH07hcpOE+FSp5tqRGW91Jzi2i15IKBOVKKiQS3IFde2pSoiohYX1w1qg1ojKqBn63wzYPfToGVRg+M2tLkEBobqejnFaruoWd4TFRgIdznS2Bdq8TlOK+cAXTdUTe9yGLZSVL+8A+VQVb3MKdYqUTM7qLmooPQp1mpR2da1WozKZ9YTj8ptXqVQx8dZC2q08Zs4EVT+vteL6sNySaB1IGPUhEp32gLUuFMlKmrHo7ccRqfrREb1BaGP3x0/KuxMjHnjR1UMXNp6e6VXdigbHADqeAza3SVX72/4wb6JSSsUJmyo/Q4+6Pdb5cJ6bW0AQw/+rWEwc3LcGMzAVbyLzeTV1IKDGraSN9xpdifv6I2IqDSTwh62+19KNaBWYQ1qg9qgNqgNaoPaoDaoDWqDKj7qk+S6SXTUdwnko9LD5LpFdNSPCOTd0q3kOio66mcE8gbpPHK1RUf9nkCeIc0g192io/5JII+VDnuCPIyITTpKEB9pSa3LyeNOsVF3E8T5BPVkqqzbRSZFlHAa/Rfc95An+z8QlxT+SwDvY/8t/CL27/c3i0o6RNfU2GUMdepL9PkeQePA++xDA9Ydw1ClO1+jX+3/QkTSL+ndH3v5gZaPOlt+i32CwE7hotb23xjYm/I5AeqU5fIb/kcz7Pthx7YRQWx0x09/+1SvyyuPClClubL8grCfwvHfOlm+tBWiTl4qy2tfFZP0ladl+aEpEap03CpZlp9b/6NonN+tf5aArZneilGlU2Vma1/c8OHngth7G55/xqea3eJRpYtlYe3KVhL1oAWikl49OYUqteasEZJ0XvQxPBGqJB2/VDzQB6dHeDyqdOjc5WKBPj5vipSPKkmHz75LHNB75xzJs6VQiU09bcHti1dOLOSqxXcsOP3oFFjrf3rOCZmQLTnJAAAAAElFTkSuQmCC',
              homeworkSlides: [homeworkSlides.reorder[0], homeworkSlides.form[0], homeworkSlides.dynamic_button[0]]
          }, {
              id: dynamic_id(),
              type: "homework",
              title: "homework",
              icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACjCAMAAAAtp3V4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAkxQTFRFq3sC////////q3sC////q3sC////q3sCq3sC////q3sC////q3sCq3sCq3sCq3sCq3sC////q3sCq3sC////////q3sC////////////////////////q3sC////q3sCq3sCq3sC////////q3sC////q3sC////q3sCq3sC////////////////////////0LRwq3sC////q3sCxaRR////////q3sCq3sC////////q3sC////////////////////////////////////////////////q3sCq3sCq3sC////q3sCq3sC////6Nu6q3sC4tGn////q3sCq3sCq3sCq3sC////////////////////q3sC////q3sCq3sC////////q3sC////q3sC////q3sCuZEsupIvvJU1v5o+wp9GzrFqzrJr0LRw0LVx0rl418GI3MiW3MmX3sub3syd382f4M6i4tGo5NWu5ti06KUB6KYE6acH6agK6akM6aoQ6qsT6qwW6q0X6q0Z6q8c6t2/67Ei67Ej67Il69/B7LMo7LQr7LUu7bc07bg37bk57bk67bo97rtA7rxD7r1E779M78FP78FQ78JS7+XN8MNV8MVb8MZe8chl8cln8s1x889589B89NOB9NSE9NSG9NWH9O7e9deN9diQ9diR9e/h9tqW9tyd9+Co9+Gr+OKu+OSz+ea3+ei9+enA+urD+uzJ+u3M++7P++/S+/HY/PLb/PPe/PTh/fXk/ffn/fjq/vrw/v38////wLHhVAAAAGZ0Uk5TAAACAwMGBgkMDA8PEhghJDA2OTw8RUhIS05RVFpgYGNscnN3eHl7gIGEhIWGh4iJio2NkJCQlpmcnKKlp6utrq+wsrm8vsHExsnMzc/S1tfY2Nnb3uHk5efo6uvw8PP29vf5+vz9w0jslAAACIpJREFUeNrtneef1EQYx7Moig0rxYaiiKJiV8ReUcGOHRAFjKJYUKwRghfR0XPOCBeMOi4xOpZooisooKLo/WPOTNqk7ebYTW5e5HmR3bvP3ez3Js/8njKzt1KLs0Nmzr9tyVNjQtjqJYvOnXEwTyfFT2ctXDEmmD12xSk5qCdeOyakXTUtjXrhmLB2dgJ10o3+d/d+vXXkbUUI2ziy7Zs/fKprONRJt7Bv/TKsCGbwVwZ2c4x6Hf163yeKgDb6TzSvFPV8+tWudxQhbcvvlO5MhirNZKQbFUFt0x7Kd4JEUa8nz/4aUoS1LdQHFlLUWRR6qyKwbaeEJxHUheTxZ0Vo20kQL2lJR1DkYbFRPyaIK/xFtUsR3PayhXUBuX4lOuq3BPIsaRG5bhMd9VMCeZO0jFyHREcdJpD3S6vJdaPoqEMEcplEBUB0UmUzlYAGtUFtUBvUBrVBbVAb1Aa1QW1QG9RcAxAhjHEnMBdjG5lQG/drQp2N44UDORhbyIDqYFA1ox0hZgy3jZK8mo5iwox5GMG+UWHx+OEUW3qPWdEMy+30NKdfVKtTxmy9mNN0OuUM9omKQ+9ESIcQAkVRyQM0kZW8nx7K9QQj5TwObhMnj434ro0HiGoXDKJC0+burJWGVRH313g20rVCR3YHhIq6OqJpRzhtNR/Us3utPlwHKoUyQnf0Yp/VQ1DP0nsLRF2oVHjD5Wf5E6uGDoiNUlpWIypxhADWobcaBFOKYcnYMGhUja1YRMILzBsW+DPpAcXoAkoFxCCD+HJSDWo6ZpHYmo6IZqBb/oOZDVh2SmNdtQpUkB9i2ok1A2J1crTkyssPWHAQqGo8i/6stjH2bz51AgtHr+xZ3Guo4bRZCc5Yz9ggpu8DNrbVZKQhZowf1eykULPpgRnOlGuqKVaLX2/hGiuOAbx3jR8V9UTlA4CHQliVgtlpYeglrjWgUjaTza2rc78YuoTqg/YOAvWgUlfAnPpzqCxiFSQyVaJqBiIryzeL6CJIwDqRg8aoeiYvYGWAFQxCcixQCaqazbEdK34t+vNpVPLMiX8AIjurV6AKVK2g1Ij8EJhpB1AtlCNX1aOSWYFR4Ud1MZqjhLAml1WQX0ecxHP0MCiTCGsadS0rFaLgJRyjEDVMWz3bBBOrAOGtdWEuagDqtoEQYqUi5gphkORR/fza1pWJ0FUNwpwJMlw/+0stKzu35GKjqFWiIuzyPQZMMhf+x8n84axYdTBIZA0Ic1AutipBBfndlejeak6QFMWoBpezkizQ6ZEEDm5WHZrBkXyaRRtugm1DVQrECmg5bQs3DHkEz9PqWFZaHH28hDtmdFW34+hW1EmrXgE0I0hGuTIqhWr4f4/b7trZqkesgkmz1DxU1jbpeL1ktbYkUGM6H7YrOFQNl20FDBiVSCoKTE+Jq4o4UYtQWVHopKq7oEBn5bVWEaqTad+2jRiYTqCd1VW+wgamlS7Qq1EAUJADRlKF7HS0Ai7W4hrM69SmqwijsDVqcF1RKqxK9ySQk1WPFhB6OEhF0Sq/vZ/TB06jQjtqsBbvT9SgAKoeCGtcQiVR/QqRyCoUQaz8Jnq0gHhUX608CyiC6GrQl8CZ4tqkM+4itW5dhX5t7XX86jq5w8amL12xmom2S5A4kAUZl+iwElStx8YYmcEcVDtuZul5W3WVVKxq0ZaVGWZ6MO2ralSRAuTUWVyTuKpy4RHFk5QU1oyuBk0t6rY2KR00rn8NaltWwAhiENe1TKNGewRds8BaFCAQVtfIRQ1AHVMTQ6x8YQ3jPY/qZ4e9ZbUCVBBsjeZ2LT2YXlY4o1aBo0OYqtEHi9p2kkJFVJG7q3pBcW1ryS2WhGC5OXsBVSWBrhVtnqo4KAP4/mq8kQnys8CqimuHltaI7a9wO+zpJcMXLMGkgbbL19ZtNgiJWh23luJagUbkEold+LSuRpvFJLjpYMIUgERLt3txHfYsHRNMvFhBX1ij2iSBynKr3rJam64GXUsz2whit94qsX1dW74aaH07hcpOE+FSp5tqRGW91Jzi2i15IKBOVKKiQS3IFde2pSoiohYX1w1qg1ojKqBn63wzYPfToGVRg+M2tLkEBobqejnFaruoWd4TFRgIdznS2Bdq8TlOK+cAXTdUTe9yGLZSVL+8A+VQVb3MKdYqUTM7qLmooPQp1mpR2da1WozKZ9YTj8ptXqVQx8dZC2q08Zs4EVT+vteL6sNySaB1IGPUhEp32gLUuFMlKmrHo7ccRqfrREb1BaGP3x0/KuxMjHnjR1UMXNp6e6VXdigbHADqeAza3SVX72/4wb6JSSsUJmyo/Q4+6Pdb5cJ6bW0AQw/+rWEwc3LcGMzAVbyLzeTV1IKDGraSN9xpdifv6I2IqDSTwh62+19KNaBWYQ1qg9qgNqgNaoPaoDaoDWqDKj7qk+S6SXTUdwnko9LD5LpFdNSPCOTd0q3kOio66mcE8gbpPHK1RUf9nkCeIc0g192io/5JII+VDnuCPIyITTpKEB9pSa3LyeNOsVF3E8T5BPVkqqzbRSZFlHAa/Rfc95An+z8QlxT+SwDvY/8t/CL27/c3i0o6RNfU2GUMdepL9PkeQePA++xDA9Ydw1ClO1+jX+3/QkTSL+ndH3v5gZaPOlt+i32CwE7hotb23xjYm/I5AeqU5fIb/kcz7Pthx7YRQWx0x09/+1SvyyuPClClubL8grCfwvHfOlm+tBWiTl4qy2tfFZP0ladl+aEpEap03CpZlp9b/6NonN+tf5aArZneilGlU2Vma1/c8OHngth7G55/xqea3eJRpYtlYe3KVhL1oAWikl49OYUqteasEZJ0XvQxPBGqJB2/VDzQB6dHeDyqdOjc5WKBPj5vipSPKkmHz75LHNB75xzJs6VQiU09bcHti1dOLOSqxXcsOP3oFFjrf3rOCZmQLTnJAAAAAElFTkSuQmCC',
              //icon: "http://dev.websearchpro.net/BUP/www/images/icon_homework_yellow@2x.png",
              homeworkSlides: [homeworkSlides.reorder[1], homeworkSlides.form[1], homeworkSlides.dynamic_button[1]]
          }]

      },
      homeworks: [{
          id: dynamic_id(),
          title: "Anger calm dowm",
          homeworkSlides: [homeworkSlides.reorder[1], homeworkSlides.form[1], homeworkSlides.dynamic_button[1]]
      }, {
          id: dynamic_id(),
          title: "Trigger signals",
          homeworkSlides: [homeworkSlides.form[0], homeworkSlides.reorder[1], homeworkSlides.dynamic_button[0]]
      }, {
          id: dynamic_id(),
          title: "Trigger behaviour consequence",
          homeworkSlides: [homeworkSlides.reorder[0], homeworkSlides.reorder[1], homeworkSlides.form[1], homeworkSlides.dynamic_button[1]]
      }, {
          id: dynamic_id(),
          title: "First assignment",
          homeworkSlides: [homeworkSlides.form[0], homeworkSlides.reorder[0], homeworkSlides.form[1], homeworkSlides.dynamic_button[1]]
      }]
  };

  /**
   * Users of this app
   * @type {Array}
   */
  var users = [{
      id: dynamic_id(),
      username: "dipesh",
      name: "Dipesh KC",
      password: "123456",
      createdDate: "2014",
      saveDataInServer: false,
      tasks: {
          practices: [tasks.practices.sound[0], tasks.practices.exposure[0], tasks.practices.homework[0]],
          homeworks: [tasks.homeworks[0], tasks.homeworks[1], tasks.homeworks[2]]
      }
  }, {
      id: dynamic_id(),
      username: "anup",
      name: "Anup Maharjan",
      password: "123456",
      saveDataInServer: true,
      tasks: {
          practices: [tasks.practices.exposure[0], tasks.practices.sound[1], , tasks.practices.homework[1]],
          homeworks: [tasks.homeworks[3], tasks.homeworks[1], tasks.homeworks[0]]
      }
  }, {
      id: dynamic_id(),
      username: "maria",
      name: "Maria",
      password: "123456",
      saveDataInServer: true,
      tasks: {
          practices: [tasks.practices.sound[0], tasks.practices.exposure[0], tasks.practices.homework[0]],
          homeworks: [tasks.homeworks[2], tasks.homeworks[1], tasks.homeworks[0]]
      }
  }, {
      id: dynamic_id(),
      username: "avishek",
      name: "Avishek",
      password: "123456",
      saveDataInServer: true,
      tasks: {
          practices: [tasks.practices.sound[0], tasks.practices.exposure[0], tasks.practices.homework[0]],
          homeworks: [tasks.homeworks[0], tasks.homeworks[1], tasks.homeworks[3]]
      }
  }, {
      id: dynamic_id(),
      username: "demo",
      name: "Test user",
      password: "demo",
      saveDataInServer: true,
      tasks: {
          practices: [tasks.practices.sound[0], tasks.practices.exposure[0], tasks.practices.homework[0]],
          homeworks: [tasks.homeworks[0], tasks.homeworks[1], tasks.homeworks[2], tasks.homeworks[3]]
      }
  }];






   //var practice_tasks = [practice_sound1, practice_exposure1, practice_homework1, practice_exposure2];
   //var homework_tasks = [homework_assignment1, homework_assignment2, homework_assignment3, homework_assignment4];

  MAINMODULE
      .constant('Config', {
          useMocks: true,
          fakeDelay: 0
      })

   //configure the delay in the server response 
  .config(function($provide, Config) {
      if (!Config.useMocks) return;
      $provide.decorator('$httpBackend', function($delegate) {
          var proxy = function(method, url, data, callback, headers) {
              var interceptor = function() {
                  var _this = this,
                      _arguments = arguments;
                  setTimeout(function() {
                      callback.apply(_this, _arguments);
                  }, Config.fakeDelay);
              };
              return $delegate.call(this, method, url, data, interceptor, headers);
          };
          for (var key in $delegate) {
              proxy[key] = $delegate[key];
          }
          return proxy;
      });
  })

  .run(function($httpBackend, Config) {
      if (!Config.useMocks) return;

      // returns the current list of phones
      //$httpBackend.whenGET('/tasks').respond(phones);

      //fake login
      $httpBackend.whenPOST('/api/login').respond(function(method, url, data, headers) {
          data = angular.fromJson(data);
          // console.log(method)
          // console.log(url)
          // console.log(data)
          // console.log(headers)

          var foundUser = users.filter(function(theUser) {

              return theUser.username == data.username && theUser.password == data.password;
          });

          var response = {
              status: false,
              message: "Incorrect username or password"
          };

          if (foundUser.length == 1) {
              var current_user = foundUser[0];
              var new_token = generate_token(current_user); //this function also stores the token_key in the local storage
              current_user.authorizationToken = new_token;
              //response.authorizationToken = new_token;
              response.user = angular.copy(current_user);
              delete response.user.tasks;
              response.status = true;
              response.message = "Welcome " + foundUser[0].name;
              return [200, response, {}];
          }

          //console.log('server response here');
          //console.log(response);


          //send the data after 3 seconds
          return [401, response, {}];

      });

      $httpBackend.whenGET('/api/practices').respond(function(method, url, data, headers) {
          //console.log(headers);
          if (!headers.Authorization || !is_valid_token(headers.Authorization)) {
              console.log('un-authorized request');
              return [401];
          }
          //console.log('current user is');
          var current_user = is_valid_token(headers.Authorization);
          //console.log(current_user);
          var response = {
              status: true,
              tasks: current_user.tasks
              // tasks: {
              //     practices: practice_tasks,
              //     homeworks: homework_tasks
              // }


          };
          return [200, response, {}];
      });

      //allow the templates to be normally loaded with original httprequest
      $httpBackend.whenGET(/templates\//).passThrough();

  });