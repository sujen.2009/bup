angular.module(_SERVICES_, [])
/**
 * Utility Service
 * @return {[type]} [description]
 */
.service('BUPUtility', ['$moment',

    function($moment) {
        /**
         * find out the unique day index
         * @uses moment().dayOfYear() to get the unique day of the year
         * @returns string prefixed with "{currentyear}_" to make sure it is valid for multiple years too
         */
        this.dayIndex = function(date) {
            var theMoment = $moment(date);
            return theMoment.get('year') + '_' + theMoment.dayOfYear();
        };

        /**
         * find out the unique week index
         * @uses moment().isoWeek() to get the unique week number of the year
         * @returns string prefixed with "{currentyear}_" to make sure it is valid for multiple years too
         */
        this.weekIndex = function(date) {
            var theMoment = $moment(date);
            return theMoment.get('year') + '_' + theMoment.isoWeek();

        };
    }
])
//persistent configuration details

.factory('BUPConfig', ['BUPUtility', '$moment',

    function(BUPUtility, $moment) {

        /**
         * note this config is always per user basis. 
         * ie, config of one user never applies to another user
         * stored config is actually an array
         * sample:
            var config = [{
                userId: 11,
                startedDate: "2014-06-15"
            }];
         */


        var config = [];

        var service = {
            /**
             * [readDataFromLocalStorage read the data from  the local storage into the private variable "config"]
             * @return {[type]} [description]
             */
            readDataFromLocalStorage: function() {
                debugMode && console.log('reading data from the local storage for user config')
                if (window.localStorage['config']) {
                    try {
                        config = JSON.parse(window.localStorage['config']);
                    } catch (e) {}
                } else {
                    //if this object does not exist already then create that object and also save in localstorage

                }
            },

            /**
             * [updateDataToLocalStorage write the content of private variable "config" to the localstorage]
             * @return {[type]} [description]
             */
            updateDataToLocalStorage: function() {
                window.localStorage['config'] = JSON.stringify(config);
            },

            /**
             * BUPConfig.check() i
             * @param  {[int]} userId [id of the user to check]
             * @return {[void]}        doesn't return anything
             * checks if the user related config is set or not.
             * Ensures that must have config eg "userid","startedDate" is there
             * If not it is created and stored
             */
            initUserConfig: function(userId) {
                // var userConfig = _.filter(config, function(configEntry) {
                //     return configEntry.userId && configEntry.userId == userId && configEntry.startedDate;
                // });
                // if (userConfig.length) {
                //     debugMode && console.log('reading previous config');
                //     userConfig = userConfig[0];
                // } 
                var userConfig = this.get(userId);

                if (userConfig === false) {

                    debugMode && console.log('setting a new config');
                    var today = $moment().startOf('day');

                    userConfig = {
                        userId: userId,
                        startedDate: {
                            date: $moment(today).valueOf(),
                            dayIndex: BUPUtility.dayIndex(today),
                            weekIndex: BUPUtility.weekIndex(today)
                        }
                    };
                    config.push(userConfig);
                    this.updateDataToLocalStorage();
                }

                return userConfig;

            },

            get: function(userId) {
                //first check if it exists
                var userConfig = _.filter(config, function(configEntry) {
                    return configEntry.userId && configEntry.userId == userId && configEntry.startedDate;
                });
                if (userConfig.length) {
                    debugMode && console.log('reading previous config');
                    userConfig = userConfig[0];
                    window._uc = userConfig;
                    return userConfig;
                }
                debugMode && console.warn('user config not available!!!');
                return false;
            },
            set: function(newConfig) {
                config = newConfig;
                this.updateDataToLocalStorage();
            }


        };

        service.readDataFromLocalStorage();

        return service;
    }
])

.factory('BUPLoader', ['$rootScope', '$ionicLoading',
    function($rootScope, $ionicLoading) {

        var loading;
        return {
            show: function(opt) {
                if (loading) return;
                loading = true;
                opt = opt || {};
                var defaultopt = {
                    template: '<i class="icon ion-loading-c"></i>&nbsp;Loading...'
                };

                if (opt.text) opt.template = '<i class="icon ion-loading-c"></i>&nbsp;' + opt.text;

                //$rootScope.loading = 
                $ionicLoading.show(angular.extend(defaultopt, opt));
            },
            hide: function() {
                loading = false;
                //$rootScope.loading.hide();
                $ionicLoading.hide();
            }
        }
    }
])

.service('AuthService', function() {

    var isAuthenticated = false; //false;

    this.setAuthenticated = function(value) {
        isAuthenticated = value;
    };

    this.isAuthenticated = function() {
        //return true;
        return isAuthenticated;
    };

})

.factory('AuthenticationService', function($rootScope, $http, authService, BUPUser, BUPWebservice, $httpBackend) {


    // var tasks = window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) || {
    //     practices: [],
    //     homeworks: []
    // };


    return {
        login: function(user) {
            console.log('authentication started')
            $http.post('/api/login', user, {
                ignoreAuthModule: true
            })
                .success(function(data, status, headers, config) {
                    // console.log('authentication success');
                    // console.log(data);
                    // console.log(status);
                    // console.log(headers);
                    // console.log(config);
                    console.log(data);
                    $http.defaults.headers.common.Authorization = data.user.authorizationToken; // Step 1


                    BUPUser.setCurrentUserInfo(data.user);
                    $rootScope.currentUser = data.user;

                    // Need to inform the http-auth-interceptor that
                    // the user has logged in successfully.  To do this, we pass in a function that
                    // will configure the request headers with the authorization token so
                    // previously failed requests(aka with status == 401) will be resent with the
                    // authorization token placed in the header
                    authService.loginConfirmed(data, function(config) { // Step 2 & 3
                        config.headers.Authorization = data.user.authorizationToken;

                        return config;
                    });
                })
                .error(function(data, status, headers, config) {
                    console.log('login failed?');
                    console.log(data);
                    console.log(status);
                    console.log(headers);
                    $rootScope.$broadcast('event:auth-login-failed', status);
                });
        },
        logout: function(user) {
            // $http.post('https://logout', {}, {
            //     ignoreAuthModule: true
            // }).
            // finally(function(data) {
            //     delete $http.defaults.headers.common.Authorization;
            //     $rootScope.$broadcast('event:auth-logout-complete');
            // });

            // $http.post('https://logout', {}, {
            //     ignoreAuthModule: true
            // })
            //     .
            // finally(function(data) {
            //     delete $http.defaults.headers.common.Authorization;
            //     $rootScope.$broadcast('event:auth-logout-complete');
            // });

            //delete the authentication token
            delete $http.defaults.headers.common.Authorization;
            $rootScope.$broadcast('event:auth-logout-complete');
            //remove the current user details
            BUPUser.setCurrentUserInfo({});

            //remove the old tasks
            BUPWebservice.setTasks({});
        },
        loginCancelled: function() {
            authService.loginCancelled();
        }
    };

})

.factory('BUPUser', ['$rootScope', 'authService',
    function($rootScope, authService) {

        //private var
        var user = {};
        var forceReAuthenticate = function() {
            console.log('re authentication required');
            $rootScope.$broadcast('event:auth-loginRequired');
            //$rootScope.$emit('event:auth-loginRequired');

        };

        if (window.localStorage['user']) {
            try {
                user = JSON.parse(window.localStorage['user']);
                //window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) ||
            } catch (e) {
                console.log('Could not read the user data from the local storage');
                forceReAuthenticate();
            }
        } else {

            console.log('user detail not found in the local storage');
            forceReAuthenticate();

        }

        var service = {
            getCurrentUser: function() {
                if (user !== null && typeof user == "object" && user.hasOwnProperty('authorizationToken') && !! user.authorizationToken) {
                    //console.log('current user info is ');
                    //console.log(user);
                    window._u = user;
                    return user;

                } else {

                    console.log('user info not found');
                    forceReAuthenticate();

                    //$rootScope.$broadcast('event:auth-loginRequired');
                    //authenticateion required

                }
            },
            setCurrentUserInfo: function(userObj) {
                user = userObj;
                window.localStorage['user'] = JSON.stringify(userObj);
            }

        };

        return service;
    }
])
/**
 * A simple example service that returns some data.
 * authService is the service from angular-http-auth module
 *
 */
.factory('BUPWebservice', ['$q', '$http', 'authService', 'BUPLoader', '$rootScope',
    function($q, $http, authService, BUPLoader, $rootScope) {
        // Might use a resource here that returns a JSON array



        // var tasks = {
        //     practices: [],
        //     homeworks: []
        // };
        //read the tasks from the local storage or empty


        // var tasks = window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) || {
        //     practices: [],
        //     homeworks: []
        // };


        var tasks = {
            practices: [],
            homeworks: []
        };



        if (window.localStorage['tasks']) {
            try {
                tasks = JSON.parse(window.localStorage['tasks']);
                //window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) ||
            } catch (e) {

            }
        }

        return {

            // authenticate: function(user) {
            //     BUPLoader.show({
            //         text: 'Authenticating...'
            //     });
            //     return $http.post('/api/login', user).then(function(response) {
            //         BUPLoader.hide();
            //         return response.data;
            //     });

            // },

            getAllTasks: function(opt) {
                opt = opt || {};
                // return $http.get('/api/practices').then(function(response) {
                //     practices = response.data;
                //     return practices;
                // });
                //if it has already been requested before get the same data
                if (typeof tasks == "object" && tasks != null && tasks.hasOwnProperty("practices") && tasks.hasOwnProperty("homeworks") && (tasks.practices.length || tasks.homeworks.length)) {

                    //controller is configured to expect a promist
                    var deferred = $q.defer();
                    //console.log('the existing all tasks are');
                    //console.log(tasks);
                    //deferred.resolve(practices);
                    deferred.resolve(tasks);

                    //opt.showLoader && BUPLoader.hide();
                    return deferred.promise;

                } else {
                    !opt.hideLoader && BUPLoader.show({
                        delay: 0
                    });
                    return $http.get('/api/practices').then(function(response) {
                        //console.log('the backend all task respnse is');
                        //console.log(response);;
                        //practices = response.data;
                        tasks = {
                            practices: response.data.tasks.practices,
                            homeworks: response.data.tasks.homeworks
                        }
                        // tasks.practices = response.data.tasks.practices;
                        // tasks.homeworks = response.data.tasks.homeworks;

                        window.localStorage['tasks'] = JSON.stringify(tasks);
                        !opt.hideLoader && BUPLoader.hide();

                        //return practices;
                        return tasks;

                    });
                }

            },

            setTasks: function(newTasks) {
                tasks = newTasks;
                window.localStorage['tasks'] = JSON.stringify(newTasks);
                console.log('now the tasks are');
                console.log(tasks);
            },

            /*****************************************************************
             * @id: (int) id of the task
             * @type:  (string) type of the task , either homework or practice
             *****************************************************************
             **/
            getTaskById: function(id, type) {

                var deferred = $q.defer();

                //first ensure we have all the tasks
                this.getAllTasks().then(function(allTasks) {

                    //console.log('all  tasks (practices and homeworks) available.ensured!');

                    console.log(allTasks);
                    console.log('type=' + type);
                    console.log(allTasks[type]);
                    //get the task type
                    //console.log()
                    var foundTasks = allTasks[type].filter(function(thetask) {
                        //console.log(thetask.id);
                        return thetask != null && parseInt(thetask.id, 10) == id;
                    });
                    console.log(foundTasks);
                    //make sure the task has been found
                    var requiredTask = foundTasks.length ? foundTasks[0] : {};

                    deferred.resolve(requiredTask);


                });

                return deferred.promise;

            },

            getHomeworkById: function(id) {

                return this.getTaskById(id, 'homeworks');

            },

            getPracticeById: function(id) {

                return this.getTaskById(id, 'practices');

            },


            getStores: function() {



                if (stores.length) {

                    //controller is configured to expect a promist
                    var deferred = $q.defer();
                    deferred.resolve(stores);
                    return deferred.promise;

                } else {

                    // return a http promise
                    // /?json=store/getAll
                    // /?json=get_posts&post_type=store
                    return $http.get(_BASE_API_URL + '/?json=store/getAll').then(function(response) {

                        console.log(response);

                        if (response.data.status == "ok") {
                            stores = response.data.posts;
                            return response.data.posts;
                        }

                    });
                }

            },

            getStore: function(id) {

                var deferred = $q.defer();
                var items = this.getStores().then(function(allStores) {
                    console.log('received all stores');
                    console.log(allStores);

                    var store = allStores.filter(function(theStore) {
                        console.log(theStore.id);
                        return parseInt(theStore.id, 10) == id;
                    })

                    console.log(store);
                    console.log('above is the determined store')

                    deferred.resolve(store);
                });

                return deferred.promise;



            },

            getAllItems: function() {
                if (items.length) {
                    console.log('all items already loaded');
                    //controller is configured to expect a promist
                    var deferred = $q.defer();
                    deferred.resolve(items);
                    return deferred.promise;

                } else {
                    console.log('all items being  loaded');

                    ///?json=get_posts&post_type=item
                    return $http.get(_BASE_API_URL + '/?json=item/getAll').then(function(response) {
                        console.log(response);
                        if (response.data.status == "ok") {
                            items = response.data.posts;
                            return response.data.posts;
                        }
                    });
                }

            },
            getItemsByStore: function(id) {

                var deferred = $q.defer();
                var items = this.getAllItems().then(function(allItems) {
                    console.log('received all items');
                    console.log(allItems);

                    var storeItems = allItems.filter(function(theItem) {
                        console.log(theItem.storeID);
                        return parseInt(theItem.storeID, 10) == id;
                    })

                    deferred.resolve(storeItems);
                });

                return deferred.promise;

            },
            getItemById: function(id) {
                console.log('inside service getitemby id' + id);
                var deferred = $q.defer();
                var items = this.getAllItems().then(function(allItems) {
                    console.log('received all items');
                    console.log(allItems);

                    var item = allItems.filter(function(theItem) {
                        console.log(theItem.id);
                        return parseInt(theItem.id, 10) == id;
                    })

                    console.log(item);
                    console.log('above is the determined store')

                    deferred.resolve(item);
                });

                return deferred.promise;

            },
            //


            search: function(search_form, page_number, callback) {

                var eventfulOptions = {
                    app_key: "NdNx6C2Fp4pgxRgG",
                    location: search_form.location.latlng[0] + ", " + search_form.location.latlng[1],
                    within: search_form.distance,
                    page_size: 50,
                    page_number: page_number,
                    date: "This Week",
                    mature: "safe"
                };

                EVDB.API.call("/events/search", eventfulOptions, function(data) {

                    var eventFulEvents = [];

                    if (data.events) {
                        for (var i = 0; i < data.events.event.length; i++) {

                            var event = data.events.event[i];

                            event = formatEventfulEvent(event);

                            if (event) eventFulEvents.push(event);
                        };
                    }

                    callback(eventFulEvents, data.page_count);
                });

            }


            //
        }
    }
])

.factory('BUPActivities', ['BUPConfig', 'BUPUser', 'BUPLoader', '$rootScope', '$filter', 'authService', 'BUPWebservice',
        function(BUPConfig, BUPUser, BUPLoader, $rootScope, $filter, authService, BUPWebservice) {

            var activities = [{
                date: 1403560123006,
                taskid: 1,
                userid: 1
            }, {
                date: 1403560123406,
                taskid: 1,
                userid: 1
            }, {
                date: 1403560123706,
                taskid: 3,
                userid: 1
            }, {
                date: 1403560123306,
                taskid: 3,
                userid: 1
            }, {
                date: 1403560123706,
                taskid: 5,
                userid: 1
            }, {
                date: 1403560123006,
                taskid: 1,
                userid: 1
            }, {
                date: 1403560123406,
                taskid: 1,
                userid: 1
            }, {
                date: 1403560163706,
                taskid: 3,
                userid: 1
            }, {
                date: 1405560124106,
                taskid: 3,
                userid: 1
            }, {
                date: 1405560123797,
                taskid: 5,
                userid: 1
            }, {
                date: 1405560123042,
                taskid: 1,
                userid: 1
            }, {
                date: 1404560123468,
                taskid: 1,
                userid: 1
            }, {
                date: 1404560123723,
                taskid: 3,
                userid: 1
            }, {
                date: 1401560123356,
                taskid: 3,
                userid: 1
            }, {
                date: 1401560123236,
                taskid: 5,
                userid: 1
            }, {
                date: 1401560123786,
                taskid: 1,
                userid: 1
            }, {
                date: 1401560123346,
                taskid: 1,
                userid: 1
            }, {
                date: 1401560123886,
                taskid: 3,
                userid: 11
            }, {
                date: 1401560123306,
                taskid: 3,
                userid: 11
            }, {
                date: 1401560123706,
                taskid: 5,
                userid: 11
            }, {
                date: 1401560123000,
                taskid: 1,
                userid: 11
            }, {
                date: 1401560123401,
                taskid: 1,
                userid: 11
            }, {
                date: 1401560123746,
                taskid: 3,
                userid: 11
            }, {
                date: 1401560123506,
                taskid: 3,
                userid: 11
            }, {
                date: 1403560128706,
                taskid: 5,
                userid: 11
            }];

            // var activities = [{
            //     date: 1403560128706,
            //     taskid: 5,
            //     userid: 11
            // }];
            var daily = [{
                date: "23/4,",
                count: 0
            }, {
                date: "24/4",
                count: 0
            }, {
                date: "25/4",
                count: 0
            }, {
                date: "26/4",
                count: 0
            }];
            var weekly = [{
                date: "23/4- 28/4",
                count: 5
            }, {
                date: "18/4-23/4",
                count: 1
            }, {
                date: "11/4-18/4",
                count: 0
            }];
            /**
             * @todo
             * Questions: is  the saved activities only related to the repetitive practices???
             * what abou the homework list added???
             */

            if (window.localStorage['activities']) {
                try {
                    activities = JSON.parse(window.localStorage['activities']);
                } catch (e) {

                }
            }




            //http://www.kidsil.net/2013/09/filtering-with-angularjs/
            //https://docs.angularjs.org/tutorial/step_03
            //http://stackoverflow.com/questions/20714531/angularjs-daterange-filter
            //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
            //http://stackoverflow.com/questions/847185/convert-a-unix-timestamp-to-time-in-javascript
            //http://jsfiddle.net/c6BfQ/3/

            var groupEvents = function(eventsArray) {

                this.dates = eventsArray;

                this.arranged = {};

                this.monthKey = function(month) {
                    return 'month' + month;
                };
                this.dayKey = function(month) {
                    return 'day' + month;
                };
                this.getYear = function(year) {
                    this.arranged[year] = this.arranged[year] || {}
                    return this.arranged[year]
                };
                this.getMonth = function(month, year) {
                    var y = this.getYear(year);
                    var k = this.monthKey(month);
                    y[k] = y[k] || [];
                    return y[k]
                };
                this.getDate = function(day, year) {
                    var y = this.getYear(year);
                    var k = this.dayKey(day);
                    y[k] = y[k] || [];
                    return y[k]
                };
                this.addToMonth = function(info, month, year) {
                    var y = this.getMonth(month, year);
                    y.push(info);
                };
                this.addToDay = function(info, day, year) {
                    var y = this.getDate(day, year);
                    y.push(info);
                };
                this.breakdownDate = function(date) {
                    return {
                        month: date.getMonth(),
                        day: date.getDate(),
                        year: date.getFullYear()
                    };
                }
                /** grab a date, break it up into day, month year
      and then categorize it */
                this.arrange = function() {
                    if (!this.arranged.length) {
                        var ii = 0;
                        var nn = this.dates.length;
                        for (; ii < nn; ++ii) {
                            var el = this.dates[ii];
                            var date = new Date(el.date * 1000);
                            var parsed = this.breakdownDate(date);
                            this.addToMonth(el, parsed.month, parsed.year);
                            this.addToDay(el, parsed.month, parsed.year);
                        }
                    }
                    return this.arranged;
                };
                return this;
            };
            var service = {


                getAll: function(opt) {
                    opt = opt || {};
                    if (typeof activities == "object" && activities != null && activities.hasOwnProperty("length")) { //make sure it is a valid array
                        //controller is configured to expect a promist
                        var deferred = $q.defer();
                        deferred.resolve(activities);
                        !opt.hideLoader && BUPLoader.show({
                            delay: 0
                        });
                        return deferred.promise;

                    } else {
                        return []; //return the empty array
                    }
                },

                getCurrentUserActivities: function() {
                    var user = BUPUser.getCurrentUser();
                    return activities;
                    var cua = activities.filter(function(activity) {
                        return activity.userid == user.id;
                    });
                    return cua;
                },
                dailyActivities: function() {
                    /**
                     * @todo
                     */
                    //max count
                    ////count 
                    ///set height relatively
                    return daily;
                },
                weeklyActivities: function() {
                    return weekly;
                },
                addActivity: function(newActivity) {

                    //validation
                    activities.push(newActivity);
                    this.saveToLocalStorage();

                },
                getActivityStartDate: function() {
                    //get the started Date of using the app
                    //the graph should start from this date
                    var user = BUPUser.getCurrentUser();
                    var userConfig = BUPConfig.initUserConfig(user.id);

                    return userConfig.startedDate;
                },
                groupActivitiesByDayOld: function() {
                    //first a temporary
                    console.time('groupingtime');
                    var _dailyActivities = {};

                    activities.map(function(activity, index) {
                        var d = new Date(activity['date']);

                        //var d = new Date();
                        var index = new Date(d.getFullYear(), d.getMonth(), d.getDay()).getTime();
                        //var index = d.getFullYear() + '_' + d.getMonth() + '_' + d.getDay();
                        //
                        if (!_dailyActivities[index]) {
                            _dailyActivities[index] = [];
                            //_dailyActivities[index]

                        }
                        _dailyActivities[index].push(activity);

                    });
                    //sort by index


                    daily = []; //empty the array before inserting
                    console.log(_dailyActivities);
                    angular.forEach(_dailyActivities, function(activity, key) {
                        daily.push({
                            date: key, //date.getDay() + '/' + date.getMonth(), //$filter('date', activity.date, 'dd/MM/yyyy'), //"23/4,",
                            count: activity.length
                        });
                    })
                    console.timeEnd('groupingtime');

                    console.log('final data looks like');
                    console.log(daily);
                    window._d = daily;
                    /**
                     * @todo
                     * achieve this in one iteration
                     */


                },

                groupActivitiesByDay: function() {
                    //var graphStart = this.getActivityStartDate();
                    //theMoment.dayOfYear();
                    var graphStart {
                        year: 2014,
                        day: 1,
                        week: 1
                    };

                    var today = $moment().startOf('day');
                    var graphToday = {

                        year: 2014, //today.get('year'),
                        day: 114, //today.get('day'),
                        week: 10 //toady.get('week')
                    };

                    var totalDailyActivities = [];

                    //now need to create empty array with from graphStart till graphToday
                    if (graphStart.year == graphToday.year) {
                        //great in same year
                        //just create empty array from graphStart.day to graphToday.day
                        totalDailyActivities = _.range(graphStart.day, graphToday.day).map(function(ind) {
                            //return graphToday.year+'_'+ind;
                            var realIndex = graphStart.year + '_' + ind,
                                //create an empty object in which the index is the realIndex contains empty array
                                ret = {};
                            ret[ri] = [];
                            return ret;

                        })
                    } else if (graphStart.year < graphToday.year) {
                        var thisYear = graphToday.year;

                        do {

                            --thisYear;

                        } while (thisYear)


                    } else {

                        console.warn('the impossible!!!');
                    }


                    return theMoment.get('year') + '_' + theMoment.dayOfYear();






                    userConfig = {
                        userId: userId,
                        startedDate: {
                            timestamp: $moment(today).valueOf(),
                            dayIndex: BUPUtility.dayIndex(today),
                            weekIndex: BUPUtility.weekIndex(today)
                        }
                    };


                    var userActivities = _.chain(activities).groupBy(activities, function(activityEntry) {
                            return BUPUtility.dayIndex(activityEntry.date);
                        }

                        activities.map(function(activity, index) {
                            var d = new Date(activity['date']);

                            //var d = new Date();
                            var index = new Date(d.getFullYear(), d.getMonth(), d.getDay()).getTime();
                            //var index = d.getFullYear() + '_' + d.getMonth() + '_' + d.getDay();
                            //
                            if (!_dailyActivities[index]) {
                                _dailyActivities[index] = [];
                                //_dailyActivities[index]

                            }
                            _dailyActivities[index].push(activity);

                        });

                    }

                    groupActivitiesByWeek: function() {
                        var times = Object.keys(daily).map(function(k) {
                            return daily[k].date
                        });

                        var today = new Date(),
                            min = Math.min.apply(null, times),
                            max = new Date(today.getFullYear(), today.getMonth(), today.getMinutes()).getTime();

                        //max = Math.max.apply(null, times) //maybe today should be the max day???

                        //date diff between today and the minimum date
                        var one_day = 1000 * 60 * 60 * 24;
                        var max_days_diff = (Math.round((max - min) / one_day));
                        var total_weeks = Math.ceil(max_days_diff / 7); //total number of weeks since the start

                        //todo validation make sure there are no math errors like NaN or infinite (eg total weeks = 0 etc , just started first day etc)

                        //group each days in the corresponding week
                        //algorithm:
                        //step1: iterate over each day
                        //step2: calculate the days difference from today
                        //step3: From the day difference group into the corresponding weejk

                        var calculate_day_diff = function(timestamp) {
                            //max is todays date (do not calculate it everytime)
                            return (Math.round((max - parseInt(timestamp)) / one_day));
                        }

                        var _weeklyActivities = {};

                        //from week 0 to week maxweek create empty weekly activites

                        var i = 0;
                        for (; i <= total_weeks; i++) {
                            _weeklyActivities[i] = []; //
                        }

                        // {

                        // }

                        daily.map(function(activity) {
                            //var d = new Date(activity['date']);

                            var day_diff = calculate_day_diff(activity.date);

                            var weekNumber = Math.floor(day_diff / 7); //index based on week number

                            //keep two separate dates: one for start of week and another for end of week



                            //var d = new Date();
                            //var index = new Date(d.getFullYear(), d.getMonth(), d.getDay()).getTime();
                            //var index = d.getFullYear() + '_' + d.getMonth() + '_' + d.getDay();
                            //
                            if (!_weeklyActivities[weekNumber]) {
                                _weeklyActivities[weekNumber] = [];
                                //_dailyActivities[index]

                            }
                            _weeklyActivities[weekNumber].push(activity);

                        });
                        weekly = [];
                        angular.forEach(_weeklyActivities, function(activity, key) {
                            //calculate the date difference
                            //calculate week from now, eg 0=> till 7 days back from now, 1 = 14-7 days past from now etc

                            //here key is the week number
                            //0 represents current week
                            //make sure the array is already sorted by key in the descending order
                            weekly.push({
                                date: key, //date.getDay() + '/' + date.getMonth(), //$filter('date', activity.date, 'dd/MM/yyyy'), //"23/4,",
                                count: activity.length
                            });

                        })
                        window._wkly = weekly;


                    },

                    saveToLocalStorage: function() {
                        window.localStorage['activities'] = JSON.stringify(activities);
                    },

                    /*****************************************************************
                     * @id: (int) id of the task
                     * @type:  (string) type of the task , either homework or practice
                     *****************************************************************
                     **/
                    getActivityByTaskId: function(taskId) {

                        var deferred = $q.defer();

                        //first ensure we have all the tasks
                        this.getAllTasks().then(function(allTasks) {

                            //console.log('all  tasks (practices and homeworks) available.ensured!');

                            console.log(allTasks);
                            console.log('type=' + type);
                            console.log(allTasks[type]);
                            //get the task type
                            //console.log()
                            var foundTasks = allTasks[type].filter(function(thetask) {
                                //console.log(thetask.id);
                                return thetask != null && parseInt(thetask.id, 10) == id;
                            });
                            console.log(foundTasks);
                            //make sure the task has been found
                            var requiredTask = foundTasks.length ? foundTasks[0] : {};

                            deferred.resolve(requiredTask);


                        });

                        return deferred.promise;

                    }

                };

                service.groupActivitiesByDay();
                service.groupActivitiesByWeek();

                return service;
            }
            ])