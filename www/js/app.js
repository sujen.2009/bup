var _APP_ = 'BUP',
    _CONTROLLERS_ = _APP_ + '.controllers',
    _DIRECTIVES_ = _APP_ + '.directives',
    _FILTERS_ = _APP_ + '.filters',
    _MODULES_ = _APP_ + '.modules',
    _SERVICES_ = _APP_ + '.services',
    _CONFIG_ = _APP_ + '.config',
    _ANIMATION_ = _APP_ + '.animations';

// top-level module
var MAIN_MODULE = angular.module(_APP_, [
    'ionic',
    //'ngRoute',
    _CONFIG_,
    _CONTROLLERS_,
    _DIRECTIVES_,
    _FILTERS_,
    _MODULES_,
    _SERVICES_,
    _ANIMATION_,
    'ngCordova',
    'angular-lodash',
    'angular-momentjs', //additional modules
    'btford.phonegap.ready',
    'mike360.phonegap.filesystem',
    'http-auth-interceptor',
    //'ngMockE2E',
    'pascalprecht.translate',//translate module
    'tagged.directives.autogrow',

    'templates'

    //'ngMaterial'
    //'nouislider'

]);





// Create global modules.



var CONTROLLER_MODULE = angular.module(_CONTROLLERS_, []),
    SERVICE_MODULE = angular.module(_SERVICES_, []),
    DIRECTIVE_MODULE = angular.module(_DIRECTIVES_, []),
    FILTERS_MODULE = angular.module(_FILTERS_, []),
    MODULES_MODULE = angular.module(_MODULES_, []),
    CONFIG_MODULE = angular.module(_CONFIG_, []),
    ANIMATION_MODULE = angular.module(_ANIMATION_, []);



CONFIG_MODULE
    .constant('APP_NAME','Hemuppgiften')
    .constant('APP_VERSION',_APP_VERSION)
    .constant('WEBSERVICE_BASE_URI',_BASE_API_URL);
;
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

//var BUPAPP = angular.module('_APP_', ['ionic', '_APP_.services', '_APP_.controllers', '_APP_.directives', 'ngMockE2E'])


MAIN_MODULE

  
    .run(['$ionicPlatform',function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }

            try{
                cordova.plugins.Keyboard.disableScroll(false);
            }catch(e){

            }
        });
    }])



.config(['$httpProvider',
    function($httpProvider) {

        // Remove the default AngularJS X-Request-With header    
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        //http://stackoverflow.com/questions/12111936/angularjs-performs-an-options-http-request-for-a-cross-origin-resource/15306568#15306568
        //Reset headers to avoid OPTIONS request (aka preflight)
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};

        // Set DO NOT TRACK for all Get requests    
        //$httpProvider.defaults.headers.get['DNT'] = '1';

        //enable http caching on every http request
        
        //$httpProvider.defaults.cache = false;

        // if (typeof currentUser == "object" && currentUser != null && currentUser.hasOwnProperty(authorizationToken) && !! currentUser.authorizationToken) {
        //     console.log('auth token key in localstorage found as:');
        //     console.log(currentUser.authorizationToken);
        //     $http.defaults.headers.common.Authorization = currentUser.authorizationToken;
        // }


        $httpProvider.interceptors.push(function($rootScope) {
            return {
                request: function(config) {
                    $rootScope.$broadcast('loading:show',{noBackdrop:true});
                    return config
                },
                response: function(response) {
                    $rootScope.$broadcast('loading:hide');
                    return response
                },
                responseError: function(rejection) {
                    $rootScope.$broadcast('loading:hide');
                    return rejection;
                }
            }
        })

    }
])

.run(['$rootScope', '$state','$stateParams','$timeout' ,'$http','$document','$window', 'AuthenticationService', 'BUPConfig', 'BUPUser','BUPFiles' ,'BUPActivities', 'BUPLoader', 'BUPInteraction', 'BUPSync','CordovaNetworkAsync' ,'CordovaNetwork', '$ionicModal', '$ionicPopup',
    function($rootScope, $state,$stateParams,$timeout, $http,$document,$window ,AuthenticationService, BUPConfig, BUPUser,BUPFiles, BUPActivities, BUPLoader, BUPInteraction, BUPSync,CordovaNetworkAsync, CordovaNetwork, $ionicModal, $ionicPopup) {

       
        // $rootScope.$stateParams = $stateParams;
        $rootScope.goBack  = function(){
            window.history.back();
        }
        
        



        $ionicModal.fromTemplateUrl('html/partials/modals/login.html', function(modal) {
            $rootScope.loginModal = modal;
            //wait until the login template is completely loaded 
            //so that we don't get error about $rootScope.loginModal is undefined when the user is not logged in

            //console.log('>>>>callued get current user');
            $rootScope.currentUser = BUPUser.getCurrentUser();
            if ($rootScope.currentUser && $rootScope.currentUser.id) {
                $rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);

            }

            //console.log('Searching for previous auth token');
            //var currentUser = BUPUser.getCurrentUser();
            //console.log('current rootscope user');
            
            //console.log($rootScope.currentUser);

            if (typeof $rootScope.currentUser == "object" && $rootScope.currentUser != null && $rootScope.currentUser.hasOwnProperty('authorizationToken') && !!$rootScope.currentUser.authorizationToken) {
                //console.log('auth token key in localstorage found as:');
                //console.log($rootScope.currentUser.authorizationToken);
                $http.defaults.headers.common.Authorization = $rootScope.currentUser.authorizationToken;
            }

        }, {
            scope: $rootScope,
            animation: 'slide-in-up',
            focusFirstInput: true
        });
        //Be sure to cleanup the modal by removing it from the DOM
        // $rootScope.$on('$destroy', function() {
        //     $rootScope.loginModal.remove();
        // });
        // $rootScope.signIn = function() {
        //     AuthenticationService.login($scope.user);
        // };



        



       

        



        // if (BUPUser.isLoggedIn()) {

            
        //     //$rootScope.currentUser = BUPUser.getCurrentUser();
           
        //     //console.debug($rootScope.currentUser);  

        //     //$rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);

             
            
        //     CordovaNetworkAsync.isOnline().then(function(isConnected) {
        //         //console.log(isConnected)
        //         if (isConnected) {
        //             //console.log('is online!')
        //             BUPSync.manualSync({
        //                 silentSync: true
        //             });

        //             console.log("syncing in background");


        //         } else {
        //             console.debug('YOU ARE OFFLINE');
        //         }

        //     }, function(err) {
        //         console.log(err);
        //         console.debug('YOU ARE OFFLINE');
        //     });

        //     $rootScope.$on('Cordova.NetworkStatus.Offline', function(event, data) {
        //         //BUPInteraction.toast('you have gone offline');
        //         $rootScope.isOnline = CordovaNetwork.isOnline();
 
        //         console.log("offline");

        //     });
        //     $rootScope.$on('Cordova.NetworkStatus.Online', function(event, data) {
        //         BUPSync.manualSync({
        //             silentSync: true
        //         });
     
        //         //BUPInteraction.toast('you are now online!');
          
        //         $rootScope.isOnline = CordovaNetwork.isOnline();
        //     });


            

        // }



        $rootScope.fixIonicModalDisabledBug = function() {
           /**
             * a bug in current version of ionic in which
             * after a modal open (probably with combination of ionic loading)
             * body has a class "loading-active" which disables the entire screen and thus
             * freezed the app
             * @return {[type]} [description]
             */
            //console.log('modal shown'); 
            window.$document = $document;

            setTimeout(function(){
                //if( $document[0].body.classList.contains('loading-active')) {
                    $document[0].body.classList.remove('loading-active');
                //}
            },100);
        }



        $rootScope.$on('BUP.tasks_updated', function(event, freshTasks, isBackgroundTask,forceFromServer) {

            if ( forceFromServer ) {

                BUPFiles.saveMp3sFromTasks(freshTasks);                
            }



            //$scope.categories = newCategoryData
        });

        $rootScope.$emit('activitiesInit');


          angular.element($window).bind('native.keyboardshow', function(){
             $rootScope.$broadcast('native.keyboardshown');
             console.log('keyboard shown');
          });


          angular.element($window).bind('native.keyboardhide', function(){
             $rootScope.$broadcast('native.keyboardhidden');
             console.log('keyboard hidden');
          });





    }
]);
