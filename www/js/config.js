/*
 * config/router.js
 *
 * Defines the routes for the application.
 *
 */
angular.module(_APP_).config([
    '$stateProvider', '$urlRouterProvider','$ionicConfigProvider',
    function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {


        $stateProvider

        .state('signin', {
            url: "/sign-in",
            templateUrl: "html/partials/sign-in.html",
            controller: 'SignInCtrl',
            requireLogin: false

        })


        .state('home', {
            url: "/home",
            controller: 'dashboardCtrl',
            templateUrl: "html/partials/dashboard.html"
        })

        /**
         * exposure/exponera type practice
         * has rating
         *
         */
        .state('exposure', {
            url: "/exposure/:id",
            abstract: true,
            template: '<ui-view/>',


        })

        .state('exposure.intro', {
            url: "/intro",
            controller: 'exponeraCtrl',
            data: {
                step: 1
            },
            templateUrl: "html/partials/exposure.intro.html"
        })

        .state('exposure.rating', {
            url: "/rating",
            controller: 'exponeraCtrl',
            data: {
                step: 2
            },
            templateUrl: "html/partials/exposure.rating.html",
    
        })


        .state('exposure.graph', {
            url: "/graph",
            controller: 'exponeraCtrl',
            data: {
                step: 3
            },
            templateUrl: "html/partials/exposure.graph.html"
        })

   

        /**
         * combined homework
         * for both practice type homeworks and homework
         */

        .state('homework', {
            url: "/homework/:id/repetitive/:isRepetitive",
            controller: 'homeworkCtrl',
            templateUrl: "html/partials/homework.html"
        })

        .state('sound', {
            url: "/sound/:id",
            controller: 'soundCtrl',
            templateUrl: "html/partials/practice_sound.html"
        });

        $urlRouterProvider.otherwise("/home");


        $ionicConfigProvider.prefetchTemplates(false);

    }
]);
/*
 * config/sanitizer.js
 *
 * Defines the regex for link sanitation.
 *
 */
angular.module(_APP_).config([
  '$compileProvider',
  function($compileProvider) {

    // sanitize white list for angular/phonegap
    var sanitation = new RegExp('^\s*(https?|ftp|mailto|file|tel|comgooglemaps|sms):');
    $compileProvider.aHrefSanitizationWhitelist(sanitation);
  }
]);
