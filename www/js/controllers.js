CONTROLLER_MODULE
    .controller('dashboardCtrl', [
        '$scope', '$rootScope', '$state','$translate' ,'$timeout', '$ionicViewService', '$ionicTabsDelegate', '$ionicScrollDelegate', '$ionicModal', '$ionicPopup', 'AuthenticationService', 'BUPWebservice', 'BUPUser', 'BUPActivities', 'BUPSync', 'BUPInteraction','DynamicTextService',
        function($scope, $rootScope, $state,$translate, $timeout, $ionicViewService, $ionicTabsDelegate, $ionicScrollDelegate, $ionicModal, $ionicPopup, AuthenticationService, BUPWebservice, BUPUser, BUPActivities, BUPSync, BUPInteraction,DynamicTextService) {


            //$ionicScrollDelegate.forgetScrollPosition();
            window.sh = $ionicScrollDelegate;
            window.s = $scope;
            $scope.scrollHandle = $ionicScrollDelegate;//.$getByHandle('dashboardScroll');
            // $scope.scrollHandle = $ionicScrollDelegate.$getByHandle('dashboardScroll');
            window.sH = $scope.scrollHandle;

            $timeout(function() {

                $scope.scrollHandle.forgetScrollPosition();

            }, 1000);





            //console.log($rootScope.currentUser);
            //$ionicViewService.clearHistory();
            $scope.$on('modal.shown', function() {
                /**
                 * a bug in current version of ionic in which
                 * after a modal open (probably with combination of ionic loading)
                 * body has a class "loading-active" which disables the entire screen and thus
                 * freezed the app
                 * @return {[type]} [description]
                 */

                //function defined in the rootscope
                $scope.fixIonicModalDisabledBug();
            });
            $ionicModal.fromTemplateUrl('html/partials/modals/about-the-app-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.title = DynamicTextService.getTitle('ABOUT_THE_APP');
                $scope.modal.content = DynamicTextService.getContent('ABOUT_THE_APP');
                //console.log($scope.modal)
            });
            $scope.openAboutAppModal = function() {
                $scope.modal.show();
            };
            $scope.closeAboutAppModal = function() {
                $scope.modal.hide();
            };
            //Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function() {
                $scope.modal.remove();
            });
            // Execute action on hide modal
            $scope.$on('modal.hide', function() {
                // Execute action
            });
            // Execute action on remove modal
            $scope.$on('modal.removed', function() {
                // Execute action
            });


            $scope.taskGroups = function() {
                var practices = $scope.practices;


                if(practices && practices.length && practices.length ===4) {
                    return _.groupBy(practices, function(element, index){
                      return Math.floor(index/2);
                    });

                }else {
                    return [practices];
                }

                //var practices = angular.copy(tpractices);
                window.p = practices;
                //var practices = practices || $scope.practices;
                if(!practices ||  !practices.length || practices.length !=4) return [practices];
                var chunkSize = practices.length;
                switch(practices.length) {
                    case 4:
                    chunkSize = 2;
                    break;
                }
                var groups = [];
                for (var i=0; i<practices.length; i+=chunkSize)
                    // groups.push(practices.slice(i,i+chunkSize));
                    groups.push(angular.copy(practices.slice(i,i+chunkSize)) ) ;
                return groups;


                // groups = _.groupBy(practices, function(element, index){
                //   return Math.floor(index/chunks);
                // });
                //console.log(groups);
                return groups;

                //return [practices];
                
                
            }
            $scope.linkToPractice = function(practice) {
                switch (practice.type) {
                    case 'sound':
                        // $state.go('practice', {
                        //     'type': 'sound',
                        //     'id': practice.id
                        // });
                        // 
                        $state.go('sound', {
                            id: practice.id,
                        });
                        break;

                    case 'exposure':
                        //$state.go('practice',{type:'exposure',id:practice.id});
                        $state.go('exposure.intro', {
                            'id': practice.id,
                            //'step':1
                        });

                        break;

                    case 'homework':
                        // $state.go('practice', {
                        //     type: 'homework',
                        //     id: practice.id
                        // });
                        $state.go('homework', {
                            //type: 'homework',
                            id: practice.id,
                            isRepetitive: 'yes'
                        });
                        break;

                }
            };

            $scope.set_icon_bg = function(practice) {
                if (practice.icon) {
                    //return { color: "red" }
                    return {
                        backgroundImage: 'url(' + practice.icon + ')'
                    }

                }
            }
            /**
             * to know whether the practice is untouched or not
             * @param  {[type]}  practice [description]
             * @return {Boolean}          [description]
             */
            $scope.isVirgin = function(practice) {
                return 0 === BUPActivities.getActivitiesCountByTaskId(practice.id);
            }

            $scope.isHomeworkDone = function(homework) {
                //console.log('checking homework'+homework.id)
                return BUPActivities.isHomeworkDone(homework.id);
            }

            //console.log('am in the dashboard. lets see if we can find the user details here');
            //console.log($rootScope.currentUser);


            /*
            $scope.logout = function() {

                $ionicPopup.confirm({
                    title: 'Hemuppgiften',
                    content: 'Are you sure you want to log out?',
                    buttons: [{
                        text: 'Cancel',
                        type: 'button-red'
                    }, {
                        text: 'Ok',
                        type: 'button-positive',
                        onTap: function(res) {
                            $state.go('signin');
                            $ionicViewService.clearHistory();
                            AuthenticationService.logout();
                        }

                    }, ]
                }).then();
            };
            */

            $scope.logout = function() {

                $translate(['LABEL_CONFIRM_LOGOUT', 'LABEL_LOG_ME_OUT', 'LABEL_CANCEL'])
                .then(function (translations) {
                    BUPInteraction.confirm({
                        destructiveText: translations.LABEL_LOG_ME_OUT,
                        titleText: translations.LABEL_CONFIRM_LOGOUT,//'Are you sure you want to log out?',
                        cancelText: translations.LABEL_CANCEL,//'Cancel',
                        destructiveButtonClicked: function() {
                            $state.go('signin');
                            $ionicViewService.clearHistory();
                            AuthenticationService.logout();
                             //console.log('logged out!');
                            $timeout(function () {
                                //$state.reload();
                                //console.warn('refreshing!')
                                $state.transitionTo('signin', {}, { reload: true });
                            }, 100);
                            return true;
                        }
                    });

                });


            }

            $scope.sync = function() {
                BUPSync.manualSync();
            };

            //$scope.practicesBeingLoaded = true;

            // BUPWebservice.getAllTasks().then(function(response) {
            //     //console.log(response);
            //     $scope.practices = angular.copy(response.practices.filter(function(p) {
            //         return typeof p === 'object' && p != null; // && p.hasOwnProperty('id');
            //     }));
  
            //     $scope.homeworks = response.homeworks;
            //     $scope.practicesBeingLoaded = false;

            //     $scope.usePeriodsGraphs = response.usePeriodsGraphs;
            //     if (response.RoleId && response.RoleId == 3) {
            //         $rootScope.meetingId = response.meeting_id;  
            //         $scope.totalPracticesToBeMade = response.total_practice;
            //     }
            //     $rootScope.$emit('activitiesInit');

            // });

            //@todo intelligent syncing
            //keep all the user added information
            //
            
            var loadTasksAndHomeworks = function(freshTasks,isBackgroundTask,obtainedFromServer) {
                
                if( obtainedFromServer ) {
                    
                }
                if (!angular.equals(freshTasks.practices.filter(function(p) {
                    return typeof p === 'object' && p != null; // && p.hasOwnProperty('id');
                }), $scope.practices)) {
                    $scope.practices = angular.copy(freshTasks.practices);
                    //isBackgroundTask === false && BUPInteraction.toast('practices updated');
                }

                if (!angular.equals(freshTasks.homeworks, $scope.homeworks)) {
                    $scope.homeworks = freshTasks.homeworks;
                    //isBackgroundTask === false && BUPInteraction.toast('homeworks updated');
                }


                $rootScope.RoleId = freshTasks.RoleId;
                if (freshTasks.RoleId && freshTasks.RoleId == 3) {
                    $rootScope.meetingId = freshTasks.meeting_id;  
                    $scope.totalPracticesToBeMade = freshTasks.total_practice;
                }

                $scope.usePeriodsGraphs = freshTasks.usePeriodsGraphs;

                $scope.showHomeworkOneByOne = freshTasks.showHomeworkOneByOne || false;

                $rootScope.$emit('activitiesInit');
                $scope.scrollHandle.resize();
                $scope.$broadcast('scroll.refreshComplete');
                $scope.$broadcast('scroll.update');
                $scope.$broadcast('scroll.resize');

            }

            BUPWebservice.getAllTasks({cachePreferred:true}).then(function(response) {
                //immediately get data from cache if any    
                loadTasksAndHomeworks(response, false,false);

                //also not if user is online the data is being fetched in the background from server
                //on the event "app.userready" 
            });

            $scope.$on('BUP.tasks_updated', function(event, freshTasks, isBackgroundTask,obtainedFromServer) {
                loadTasksAndHomeworks(freshTasks, isBackgroundTask,obtainedFromServer);
            });

            $scope.$on('BUP.tasks_loaded', function(event, freshTasks) {
                //obtained when no internet from the cache
                loadTasksAndHomeworks(freshTasks, false,false);
            });







            $scope.updateGraph = function() {
                //if group user (data saved locally only)
                //if ($rootScope.currentUser.saveDataInServer === true) {
                
                //console.debug($rootScope.currentUser);
                if ($rootScope.currentUser && $rootScope.currentUser.useMeeting) {
                    //$scope.totalPracticesToBeMade
                    var practiceActivities = BUPActivities.getRepetitiveActivities();
                    var practicePercent = 0;
                    if (practiceActivities.length && practiceActivities.length >= $scope.totalPracticesToBeMade ) {
                        practicePercent = 100;
                    }else {
                        practicePercent = (practiceActivities.length/$scope.totalPracticesToBeMade)*100;
                    }
                    //$scope.practiceLevels = _.range(1, $scope.totalPracticesToBeMade, 1);
                    $scope.setMeetingGraphStyle = function() {
                        
                        // $scope.totalPracticesToBeMade;

                        // return {
                        //         height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.weekly * 100) + '%')
                        // }
                        // 
                        return {
                            width: practicePercent+'%'
                        };
                    };
                }
                else {
                //if ($rootScope.currentUser.saveDataInServer === false) {
                    
                    $scope.practiceRange = {
                        max: {
                            daily: 0,
                            weekly: 0
                        },
                        min: {
                            daily: 0,
                            weekly: 0
                        }
                    };

                    $scope.maxPracticeNum = {
                        daily: 0,
                        weekly: 0
                    };

                    var graphTabsHandle = $ionicTabsDelegate.$getByHandle('graphTabs');

                    $scope.dailyGraphTabSelected = function() {
                        $timeout(function() {

                            $scope.currentGraphScrollHandle = $ionicScrollDelegate.$getByHandle('dailyGraphScroll');

                            if (1 || typeof $scope.dailyGraphTabSelected.executionCount == "undefined") {
                                //this block is executed exactly once
                                $scope.dailyGraphTabSelected.executionCount = 0;
                                
                                //BUPActivities.groupActivitiesByDay();
                                //$scope.dailyActivities = BUPActivities.dailyActivities();
                                $scope.dailyActivities = BUPActivities.groupActivitiesByDay();

                                $scope.dailyActivitiesCount = _.size($scope.dailyActivities);
                                //console.log($scope.dailyActivities);




                                var dailyPracticeCountsArray = Object.keys($scope.dailyActivities).map(function(key) {
                                    return $scope.dailyActivities[key].count
                                });

                                $scope.maxPracticeNum.daily = Math.max.apply(null, dailyPracticeCountsArray);
                                if($scope.maxPracticeNum.daily<5) $scope.maxPracticeNum.daily = 5;

                                //know if it is the first time click or switched later
                                if ($scope.dailyActivitiesCount > 6) {
                                    $scope.currentGraphScrollHandle.scrollBottom();
                                } else {
                                    //items less than 6, add empty boxes

                                    $scope.futureDailyActivities = BUPActivities.futureDailyActivities(7 - $scope.dailyActivitiesCount);
                                    //console.log($scope.futureDailyActivities );

                                }

                                $scope.currentGraphScrollHandle.rememberScrollPosition('dailyScrollRememberId');
                                //at first go to last ie current day

                            } else {
                                //this block is executed everyting daily tab is selected
                                //when switched across daily and weekly remember the last position
                                $scope.currentGraphScrollHandle.rememberScrollPosition('dailyScrollRememberId');
                                $scope.currentGraphScrollHandle.scrollToRememberedPosition();
                                $scope.dailyGraphTabSelected.executionCount++;
                            }

                        }, 0);

                    };




                    /**
                     * function that run when the
                     * @return {[type]} [description]
                     */
                    $scope.weeklyGraphTabSelected = function() {
                        //$scope.weeklyGraphTabSelected = ionic.debounce(function() {

                        $timeout(function() {

                            $scope.currentGraphScrollHandle = $ionicScrollDelegate.$getByHandle('weeklyGraphScroll');
                            if (typeof $scope.weeklyGraphTabSelected.executionCount == "undefined") {
                                $scope.weeklyGraphTabSelected.executionCount = 0;

                                BUPActivities.groupActivitiesByWeek();
                                $scope.weeklyActivities = BUPActivities.weeklyActivities();
                                $scope.weeklyActivitiesCount = _.size($scope.weeklyActivities);

                                var weeklyPracticeCountsArray = Object.keys($scope.weeklyActivities).map(function(key) {
                                    return $scope.weeklyActivities[key].count
                                });

                                $scope.maxPracticeNum.weekly = Math.max.apply(null, weeklyPracticeCountsArray);
                                if($scope.maxPracticeNum.weekly<5) $scope.maxPracticeNum.weekly = 5;


                                //on first time click to weekly tab go to last (current week)
                                if ($scope.weeklyActivitiesCount > 4) {
                                    $scope.currentGraphScrollHandle.scrollBottom();
                                } else {

                                    $scope.futureWeeklyActivities = BUPActivities.futureWeeklyActivities(4 - $scope.weeklyActivitiesCount);
                                }

                                $scope.currentGraphScrollHandle.rememberScrollPosition('weeklyScrollRememberId');


                            } else {

                                //when switched across daily and weekly remember the last position
                                $scope.currentGraphScrollHandle.rememberScrollPosition('weeklyScrollRememberId');
                                $scope.currentGraphScrollHandle.scrollToRememberedPosition();
                                $scope.weeklyGraphTabSelected.executionCount++;
                            }


                        }, 0);

                    };
                    ////}, 100);

                    $scope.$on("$destroy", function() {
                        //forget the scroll position once user navigates to other pages
                        $scope.currentGraphScrollHandle.forgetScrollPosition();
                    });


                    ionic.DomUtil.ready(function() {

                        var tabsNode = angular.element(document.querySelector('#fixedgraph'));
                        //var tabWidth = ionic.DomUtil.getTextBounds(tabsNode[0]);
                        $scope.tabsWidth = tabsNode[0].offsetWidth;
                        $scope.dailyGraphTabSelected();

                    });

                    /**
                     * to scroll the graph to previous view
                     * @uses $scope.tabsWidth which is precalculated once
                     * shifts left by $scope.tabsWidth value
                     * @return void
                     */
                    $scope.graphScrollLeft = function() {

                        $scope.currentGraphScrollHandle.scrollBy(-$scope.tabsWidth, 0, true);

                    }

                    /**
                     * to show/hide the graphScrollLeft button/link based on the current
                     * scroll position of the scroller
                     *
                     * called with throttle function to rate limit the execution of this function
                     * not used debounce here since we want to the graph scroll fn to execute every 300 millisecond durting
                     * the scrolling movement
                     *
                     * with time 300 millisecond minimum time before next execution will be 300 milisecond
                     *
                     * updates the $scope var hideScrollLeft
                     *
                     * @return void
                     */
                    $scope.graphScrolled = ionic.throttle(function() {
                        //
                        //
                        var currentPos = $scope.currentGraphScrollHandle.getScrollPosition();

                        var currentlyAtBeginningOfGraph = currentPos.left < 10;
                        //this is to prevent update of scope.hideScrollLeft every time
                        if (currentlyAtBeginningOfGraph !== $scope.hideScrollLeft) {
                            $scope.hideScrollLeft = currentlyAtBeginningOfGraph;
                        }
                    }, 300);


                    /**
                     * set the maximum height of the bar graph (daily) based on the maximum value
                     * @param [object] {height:'Inpx or %'}
                     */
                    $scope.set_daily_graph_height = function(activity) {
                        if (parseInt(activity.count, 10) > -1) {
                            return {
                                height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.daily * 100) + '%')

                            }

                        }
                    }

                    /**
                     * set the maximum height of the bar graph (weekly) based on the maximum value
                     * @param [object] {height:'Inpx or %'}
                     */
                    $scope.set_weekly_graph_height = function(activity) {
                        if (parseInt(activity.count, 10) > -1) {
                            return {
                                height: parseInt(activity.count, 10) === 0 ? '6px' : ((activity.count / $scope.maxPracticeNum.weekly * 100) + '%')
                            }
                        }
                    }


                }
            };

            $scope.$on('event:auth-loginConfirmed', function(event, data) {
                $scope.updateGraph();
            });
            //$scope.updateGraph();

            //$scope.$watch('totalPracticesToBeMade', $scope.updateGraph , true);
            //$scope.$watch('dailyActivities', $scope.updateGraph , true);

            $scope.$on('graphInit',function(event,data) {
                //console.debug('graphInit fired');
                $scope.updateGraph();

            });

            //$scope.activities = $scope.dailyActivities();


        }
    ]);
CONTROLLER_MODULE
/**
 * Repetitive practice of type exposure
 * may or may not have the anxiety rating
 * may or may not have the feeback
 */
.controller('exponeraCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$timeout', 'BUPInteraction', 'BUPActivities', 'BUPWebservice',
    function($rootScope, $scope, $state, $stateParams, $timeout, BUPInteraction, BUPActivities, BUPWebservice) {
        /**
         * step to determine which substep of expousre is the current page
         * intro is step 1
         * rating is step 2
         * graph is step 3
         */
        $scope.step = $state.current.data.step;
        $scope.practice = {};

        //user contributed values
        $scope.userResponse = {};

        //the default rating values for the user;
        $scope.rating = {};
        $scope.rating.value = null;
        $scope.feedBackShowDuration = 900;


        BUPWebservice.getPracticeById($stateParams.id).then(function(response) {
            $scope.practice = response;
            //mocking test data
            //$scope.practice.anxiety_question = 'what is your problem?';
            //$scope.audio_bgcolor = $scope.practice.audio_bgcolor;

            $scope.practice.has_anxiety_rating = $scope.practice.has_anxiety_rating || false;
            //update the userResponse id same as the practice id
            $scope.userResponse.id = $scope.practice.id;
            $scope.userResponse.type = 'practices';

            $scope.userResponse.rating = $scope.practice.rating || {};

            /*
                if (false === $scope.practice.has_anxiety_rating) {
                    //if there is no rating mark this task as done immediately after this page is opened

                    BUPActivities.addActivity({
                        taskid: Number($stateParams.id)
                    });

                }
            */

        });

        //make this method available to go to slider
        $scope.goToSlider = function() {
            if ($scope.practice.has_anxiety_rating !== false) {
                $state.transitionTo('exposure.rating', {
                    'id': $stateParams.id,
                    'step': 2
                });
            } else {

                //

                BUPActivities.addActivity({
                    taskid: Number($stateParams.id)
                });

                // BUPInteraction.toast($scope.feedbackMessage(), $scope.feedBackShowDuration);
                // $timeout(function() {
                //     //make user properly seee (feel) the feedback message
                //     $state.go('home');
                // }, $scope.feedBackShowDuration);

                BUPInteraction.alert({
                    title: "Feedback",
                    message:$scope.feedbackMessage(),
                    callback:function() {
                        $state.go('home');
                    }
                });


            }
        };



        $scope.feedbackMessage = function() {
            var message = $scope.practice.general_feedback || "Bra jobbat";

            //make sure the rating value is integer so the comparing would give the correct result!
            $scope.rating.value = parseInt($scope.rating.value, 10);

            $scope.practice.feedback_option = $scope.practice.feedback_option || {
                compare: [2, 5, 8, 10],
                message: ["<=2 feedback message 1", "> 2 & <=5feedback message 2", " > 5 & <=8 feedback message 3", "> 8 feedbackmessage 4"]
            };

            //if feed back is enabled and feedback options are correctly configured 
            //value to compare against with logic <= (less than or equal to)
            //minimum have one value so that we have two range at minimum
            //condtional feedback message
            //if number of items in compare is n then there should be (n+1) messages

            if (!!$scope.practice.feedback && $scope.practice.feedback_option.compare instanceof Array && $scope.practice.feedback_option.message instanceof Array && ($scope.practice.feedback_option.compare.length === $scope.practice.feedback_option.message.length)) {
                var i, compareLength = $scope.practice.feedback_option.compare.length;


                /*
            //compare with first item
            if ($scope.rating.value <= $scope.practice.feedback_option.compare[0]) {
                //console.log('first case');
                message = $scope.practice.feedback_option.message[0];
            } else if ($scope.rating.value > $scope.practice.feedback_option.compare[compareLength - 1]) {
                //console.log('last case');

                message = $scope.practice.feedback_option.message[compareLength];

            } else {
                for (i = 1; i < compareLength; i++) {
                    if ($scope.rating.value > $scope.practice.feedback_option.compare[i - 1] &&
                        $scope.rating.value <= $scope.practice.feedback_option.compare[i]
                    ) {
                        // console.log("\n" + 'i = ' + i);
                        // console.log("actual rating value=" + $scope.rating.value);
                        // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                        // console.log("next=" + $scope.practice.feedback_option.compare[i]);

                        message = $scope.practice.feedback_option.message[i];
                        break;
                    }
                    // console.log('???i=' + i);
                    // console.log("actual rating value=" + $scope.rating.value);
                    // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                    // console.log("next=" + $scope.practice.feedback_option.compare[i]);


                }

            }
            */
                if ($scope.rating.value <= $scope.practice.feedback_option.compare[0]) {
                    //console.log('first case');
                    message = $scope.practice.feedback_option.message[0];
                } else {
                    for (i = 1; i < compareLength; i++) {
                        if ($scope.rating.value > $scope.practice.feedback_option.compare[i - 1] &&
                            $scope.rating.value <= $scope.practice.feedback_option.compare[i]
                        ) {
                            // console.log("\n" + 'i = ' + i);
                            // console.log("actual rating value=" + $scope.rating.value);
                            // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                            // console.log("next=" + $scope.practice.feedback_option.compare[i]);

                            message = $scope.practice.feedback_option.message[i];
                            break;
                        }
                        // console.log('???i=' + i);
                        // console.log("actual rating value=" + $scope.rating.value);
                        // console.log("cur=" + $scope.practice.feedback_option.compare[i - 1]);
                        // console.log("next=" + $scope.practice.feedback_option.compare[i]);


                    }
                }

            }
            return message;
        }
        //save the rating in step 2
        $scope.save = function() {
            console.log($scope.rating.value);
            if (undefined == $scope.rating.value) {

                BUPInteraction.toast('Ange ett värde!', 'short'); //please set a value

                return false;
            } else {

                /*
                    BUPInteraction.toast($scope.feedbackMessage(), $scope.feedBackShowDuration);
                    
                    $timeout(function() {
                        //make user properly seee (feel) the feedback message
                        $state.go('home');
                    }, $scope.feedBackShowDuration);

                */
               $scope.$on('newactivity',function(){
                    BUPInteraction.alert({
                        title: "Feedback",
                        message:$scope.feedbackMessage(),
                        callback:function() {
                            $state.go('home');
                        }
                    });
                }) 

                BUPActivities.addActivity({
                    taskid: Number($stateParams.id),
                    rating: $scope.rating.value
                });
                
                //$rootScope.$broadcast('newactivity', newActivity,activities);  
                
                
                    
                  

            }



        }

        $scope.hide_rating_graph = false;
        if ($scope.practice.has_anxiety_rating !== false) {
            $scope.activities = BUPActivities.getExposureRatingActivities($stateParams.id);
            if ($scope.activities.length === 0) {
                $scope.hide_rating_graph = true;
            }

        }

        if ($scope.step === 3) {

            console.log("in graph step");

            $scope.today = moment().startOf('day').format('YYYY-MM-DD');





            console.log("activities for the current graph only");
            console.log($scope.activities);
            //$scope.startedDate = _.min($scope.activities, 'date');
            $scope.numberOfEstimates = _.size($scope.activities);
            if ($scope.numberOfEstimates > 0) {

                $scope.startedDate = _.min($scope.activities, function(act) {
                    return moment(act.date).valueOf();
                }).date;

                $scope.totalDays = 1 + moment().startOf('day').diff($scope.startedDate, 'days');


            } else {
                $scope.startedDate = '-';
                $scope.totalDays = '-';
            }
            console.log($scope.activities);
        }


    }
])
.controller('ratingTest', ['$scope', function ($scope) {
    
}])
;

CONTROLLER_MODULE
.controller('homeworkCtrl', [
    '$scope', '$rootScope', '$state', '$stateParams','$translate', '$ionicModal', 'BUPWebservice', 'BUPInteraction', 'BUPActivities', '$ionicSlideBoxDelegate','$ionicScrollDelegate','$sce','$timeout',
    function($scope, $rootScope, $state, $stateParams,$translate, $ionicModal, BUPWebservice, BUPInteraction, BUPActivities, $ionicSlideBoxDelegate,$ionicScrollDelegate,$sce,$timeout) {
        $scope.practice = {};
        $scope.practice.homeworkSlides = [];

        var isRepetitiveType = $stateParams.isRepetitive == 'yes';
        //console.log("is repetitive = " + isRepetitiveType);

        /*
            if (isRepetitiveType) {
                BUPWebservice.getPracticeById($stateParams.id).then(function(response) {
                    $scope.homeworkSlides = response.homeworkSlides;`~~
                    $scope.totalNumberOfSlides = $scope.homeworkSlides.length;
                    if( $scope.totalNumberOfSlides === 1) {

                        $scope.$broadcast('save_homework_activity'); 

                    }
                });
            } else {
                BUPWebservice.getHomeworkById($stateParams.id).then(function(response) {
                  $scope.homeworkSlides = response.homeworkSlides;
                  $scope.totalNumberOfSlides = $scope.homeworkSlides.length;
                  if( $scope.totalNumberOfSlides === 1) {

                        $scope.$broadcast('save_homework_activity'); 

                    }
                });

            }
        */
        window.sb = $ionicSlideBoxDelegate;
        //var to hold the promise
        var homeworkSlidesReceived,practiceType;
        
        if (isRepetitiveType) {

            homeworkSlidesReceived = BUPWebservice.getPracticeById($stateParams.id);
            practiceType =  'practices';

        } else {

            homeworkSlidesReceived = BUPWebservice.getHomeworkById($stateParams.id);
            practiceType =  'homeworks';

        }

         
        // $scope.$watch('practice', function(newVal,oldVal){
        //     console.debug('inside watch');      
        //     console.log(newVal);
        //     var type;
        //     practices = isRepetitiveType? 'practices':'homeworks';
        //     //BUPWebservice.updateTaskById($stateParams.id,practices,newVal);
        //     BUPActivities.updateUserResponseByTaskId($stateParams.id,practices,newVal);

        // }, true);



       $scope.$watch('practice',  ionic.throttle(function(newVal,oldVal){
        console.debug('inside watch');      
        
            //var type;
         
            //console.log(newVal);
            if( newVal !== oldVal) {
                //console.log('updating user response');
                BUPActivities.updateUserResponseByTaskId($stateParams.id,practiceType,newVal);
            }
            
      
            //BUPWebservice.updateTaskById($stateParams.id,practices,newVal);
        },5000), true);


        $scope.canSave = false;
        /**
         * The purpose of this merging is
         * 1. show the latest update form the server as possible
         * 2. retain the user changes, eg form value, list order, new list, list edits etc
         * 3. 
         * @param  {[type]} server [description]
         * @param  {[type]} local  [description]
         * @return {[type]}        [description]
         */
        var intelligentlyMerge = function(server,local){
            // var ultimate = _.clone(server);
           // console.log(server);
            var ultimate = server;
            //console.log('local list');
            //console.log(local);
            
            //make backward compatible
            //earlier locally saved hoemwork slides didn't have slidId
            //so consider only those slides which have sildeId
            if (local.homeworkSlides && local.homeworkSlides.length ) {
                local.homeworkSlides = _.filter(local.homeworkSlides,function(theSlide){
                    return theSlide.slidId;
                });
            }

            //if there is no data in local (no data from the user side) no need to merge
            if (local.homeworkSlides && local.homeworkSlides.length ) {
                
                //Index the user homework slide content by the user so that they can be 
                //easily referenced by id
                var userSlides = _.indexBy(local.homeworkSlides,'slidId');
                //console.log(userSlides);

                _.forEach(ultimate.homeworkSlides,function(slide,homeworkSlideIndex){
                    var slideId = slide.slidId;
                    switch(slide.type) {
                        //Form type
                        //bring the from value
                        case 'normal-text':
                            
                            //console.log('normal-text');
                            if (userSlides[slideId] && userSlides[slideId].value) {
                                slide.value = userSlides[slideId].value;
                                //console.log('user content updated');
                            }

                        break;

                        /**
                         * data structure 
                         * {
                         *     homeworkSlides:[
                         *     {
                         *         slidId: <id>,
                         *         list:[
                         *             {
                         *                 id: <id>
                         *                 list: <list label>
                         *                 reordered:true,//if the list has been updated
                         *                 added:true,//if the list was added later on by the user
                         *                 updated:true //if the user updated the list
                         *             }
                         *         ]
                         *     }
                         *     
                         *     ]
                         * }
                         *
                         *
                         * Challenges:
                         * 1. delete the list that has been deleted from the server but they might be present in the local
                               delete those list
                         * 2.Updated list reordered as in the server but  maintain the list order if list reordered by the user
                         * 3.Do not show the list if the user has deleted it even if the admin makes some changes to it
                         * 4.Show the updated  text if the text has been updated by the user
                         *
                         * Solution Approach:
                         * 1. start from the list taken from the local list so that the users order is maintained
                         * 2. by comparing with local and remote list determine
                         *    i. which list are newly added in the server
                         *    ii. which list have been deleted from the server
                         * 3. for each list in the local
                         *     if the list item has been removed from the server remove it
                         *     if the local list has been marked as added or updated ( user added that list or updated that list) then user the same version
                         *     otherwise user the list label from the admin so that update made by the admin would be refledted
                         * 4  for each new list added in the server also add it     
                         *        
                         */
                        case "reorder":
                            var userList = _.indexBy(userSlides[slideId].list,'id');
                            var serverList = _.indexBy(slide.list,'id');
                            var finalList = [];
                            //get the array of ids of list from the server
                            var serverListIds = _.map(_.keys(serverList),function(a){return parseInt(a)});
                            var localListIds = _.map(_.keys(userList),function(a){return parseInt(a)});

                            var serverRemovedList = _.difference(localListIds,serverListIds);
                            var serverAddedList = _.difference(serverListIds,localListIds);

                            //constraint always added newly added list from the server at the end
                            
                            

                            //get started with local user list
                            _.forEach(userSlides[slideId].list,function(item,key){

                                if (_.indexOf(serverRemovedList,item.id) < 0) {//not removed from the server
                                    var aList = item;
                                    aList.id = item.id;
                                    //if the user has not made any changes to the list or if it is not the one created by the user itself
                                    //let the list be updated if admin has changged in the server
                                    if ( !(item.added===true || item.updated===true ) ) {
                                        aList.list = serverList[item.id].list;
                                    }
                                    finalList.push(aList);

                                } else if( item.added) {
                                    finalList.push(item);
                                }

                                
                            });



                            //new items from the server that are not present in the local
                            _.forEach( serverAddedList, function(listId){
                                //add in the list
                                var aList = serverList[listId];
                                finalList.push(aList);
                            });

                            slide.list = finalList;

                            // Super Awesome!

         
                        break;
                    }
                });    
            }
            

            return ultimate;

        };

        homeworkSlidesReceived.then(function(response) {

            
            //console.log('fresh task from the server');
            //console.log(response);

            var userExtendedTask = BUPActivities.getUserResponseByTaskId($stateParams.id,practiceType);
            //console.log('pure user data');

            //console.log(userExtendedTask);
            
            // $scope.practice = response;
            $scope.practice = intelligentlyMerge(response,userExtendedTask);

            //console.log('merged user data');
            
           // console.log($scope.practice);
            

            //var deletedHomeworkSlideIds = _.filter()
            //$scope.practice.homeworkSlides[$scope.slideIndex].list.splice(index, 1);





            //$scope.homeworkSlides = response.homeworkSlides;

            //$scope.totalNumberOfSlides = $scope.homeworkSlides.length;
            $scope.totalNumberOfSlides = $scope.practice.homeworkSlides.length;

            //$scope.homeworkSlideScroll = $ionicScrollDelegate.$getByHandle('homeworkSlideScroll');
            //$scope.homeworkSlideScroll = $ionicScrollDelegate.$getByHandle('homeworkSlideScroll');
            //window.hs = $scope.homeworkSlideScroll;
            //$scope.homeworkSlideScroll.forgetScrollPosition();
            //$scope.homeworkSlideScroll.resize();
            if ($scope.totalNumberOfSlides === 1) {
                $scope.canSave = true;
                // $scope.$broadcast('save_homework_activity', {
                //     'isRepetitiveType': isRepetitiveType
                // });

            }

                        $ionicSlideBoxDelegate.update();
        });

        $ionicModal.fromTemplateUrl('html/partials/modals/homework_add_edit_list.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }, {

            focusFirstInput: true

        }).then(function(modal) {
            $scope.modal = modal;
            //console.log("modal loaded");
        });

        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        $scope.data = {
            showDelete:false,
            showReorder:true,
            showEdit: false

        };
        //data.showDelete=false;data.showReorder=true; data.showEdit=true;



        $scope.taskAlreadyCompleted = false; //$scope.totalNumberOfSlides > 0;
        

        $scope.slideIndex = 0;
        $scope.next = function() {
            $ionicSlideBoxDelegate.next();
        };
        $scope.previous = function() {
            $ionicSlideBoxDelegate.previous();
        };
        $scope.currSlide = 0;//$ionicSlideBoxDelegate.currentIndex();
        //console.debug('current slide is'+$scope.currSlide);



        // Called each time the slide changes
        $scope.slideChanged = function(index) {
            $scope.slideIndex = index;
            $scope.currSlide = $ionicSlideBoxDelegate.currentIndex();
            //console.debug('slide changed new current slide is'+$scope.currSlide);
            //$scope.$broadcast('scroll.resize');
            //$scope.$broadcast('scroll.scrollTop');
            //$scope.homeworkSlideScroll.resize();
            //console.log('slide changed');
            if ($scope.taskAlreadyCompleted === false && (Number(index) === Number($scope.totalNumberOfSlides - 1))) {
                $scope.canSave = true;
                // $scope.$broadcast('save_homework_activity', {
                //     'isRepetitiveType': isRepetitiveType
                // });
                // $scope.taskAlreadyCompleted = true;
            }

            $timeout( function() {
              $ionicScrollDelegate.resize();
              $ionicScrollDelegate.scrollTop(false);
              $ionicSlideBoxDelegate.update();
            }, 50);
            

            //updateTask on the slide Change
        };

        $scope.$on('save_homework_activity', function(event, info) {
            
            if (info.isRepetitiveType === true) {
                //only keep save the activities if it is a repetitive one
                BUPActivities.addActivity({
                    taskid: Number($stateParams.id),
                    type: 'homework'
                    //date: moment().startOf('day').add('day',-7).format('YYYY-MM-DD'),//
                });


            } else {
                //if not repetitive task we do not need to keep track of every practices
                //but we need to keep track if the homework has been done once or not
                //alert('non repetitive type')
                //first get its previous activity count
                //if this has not been practiced previously then add the activity
                //alert(BUPActivities.getActivitiesCountByTaskId($stateParams.id))

                if (!BUPActivities.isHomeworkDone($stateParams.id)) {
                    BUPActivities.addActivity({
                        homeworkid: Number($stateParams.id),
                        //type: 'homework',
                        //repetitive: false
                        //date: moment().startOf('day').add('day',-7).format('YYYY-MM-DD'),//
                    });
                }else{

                    $rootScope.$broadcast('homeworkdoneagain', {
                        homeworkid: Number($stateParams.id)
                    });

                }

            }
        })
        $scope.showSaveButton = function() {
            return $scope.canSave===true;// = true;
        };

        $scope.save = function(){
            $scope.$broadcast('save_homework_activity', {
                'isRepetitiveType': isRepetitiveType
            });
            $scope.taskAlreadyCompleted = true;
            $scope.canSave=false;
            
        };
        $scope.theReorderItem = {
            list: "",
        };
        $scope.updateTheList = function() {
            if (!$scope.theReorderItem.list.length) {
                $translate('INFO_LIST_CANNOT_BE_EMPTY'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });
            }

            if ($scope.theReorderItem.task == 'add') {
                $scope.practice.homeworkSlides[$scope.slideIndex].list.push({
                    list: $scope.theReorderItem.list,
                    added:true,
                    reordered:true,
                    id: (new Date).getTime()
                });
                //when added the newly added list appears at the bottom,but since the order is not saved 
                //next time while loading the just added list appears on the top
                //so instead mark all the list as reordered so that the order would be maintained
                _.map($scope.practice.homeworkSlides[$scope.slideIndex].list,function(a){
                    a.reordered=true;
                    return a;
                });

                $translate('LABEL_LIST_INSERT_SUCCESS'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                });

                $scope.$broadcast('scroll.resize');


            } else if ($scope.theReorderItem.task == 'edit') {
                $scope.practice.homeworkSlides[$scope.slideIndex].list[$scope.theReorderItem.index].list = $scope.theReorderItem.list;
                $scope.practice.homeworkSlides[$scope.slideIndex].list[$scope.theReorderItem.index].updated = true;
                //BUPInteraction.toast('Updated!');
                $translate('LABEL_LIST_UPDATE_SUCCESS'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                });
            }

            $scope.closeListModal();

        }

        $scope.deleteTheList = function(index) {
            // BUPInteraction.confirm({
            //     destructiveButtonClicked: function() {
            //         console.log($scope.practice.homeworkSlides[$scope.slideIndex].list[index]);
            //         $scope.practice.homeworkSlides[$scope.slideIndex].list.splice(index, 1);
            //         BUPInteraction.toast('Deleted!');
            //         return true;
            //     }
            // });

            $translate(['LABEL_CONFIRM_LIST_DELETE', 'LABEL_LIST_DELETE', 'LABEL_CANCEL','LABEL_LIST_DELETE_SUCCESS'])
            .then(function (translations) {
                BUPInteraction.confirm({
                    destructiveText: translations.LABEL_LIST_DELETE,
                    titleText: translations.LABEL_CONFIRM_LIST_DELETE,//'Are you sure you want to log out?',
                    cancelText: translations.LABEL_CANCEL,//'Cancel',
                    destructiveButtonClicked: function() {
                        //$scope.practice.homeworkSlides[$scope.slideIndex].list.splice(index, 1);
                        $scope.practice.homeworkSlides[$scope.slideIndex].list[index].trashed = true;
                        BUPInteraction.toast(translations.LABEL_LIST_DELETE_SUCCESS);//'Deleted!'
                        return true;
                    }
                });

            });

        };



        $scope.reorderList = function(item, fromIndex, toIndex) {
            //Move the item in the array
            //console.log(item);
            //console.log($scope.practice.homeworkSlides[$scope.slideIndex].list);
            $scope.practice.homeworkSlides[$scope.slideIndex].list.splice(fromIndex, 1); //remove from
            $scope.practice.homeworkSlides[$scope.slideIndex].list.splice(toIndex, 0, item); //add item to
            //console.log("reorder from " + fromIndex + 'to ' + toIndex);
            //keep the flat reordered
            
            $scope.practice.homeworkSlides[$scope.slideIndex].list[fromIndex].reordered = true;
            $scope.practice.homeworkSlides[$scope.slideIndex].list[toIndex].reordered = true;
        };




        $scope.updateListModal = function(index) {

            angular.copy($scope.practice.homeworkSlides[$scope.slideIndex].list[index], $scope.theReorderItem);

            $scope.theReorderItem.task = "edit";

            //var oldList = $scope.theReorderItem.list;

            $scope.theReorderItem.oldList = $scope.theReorderItem.list;
            $scope.theReorderItem.index = index;

            //angular.copy($scope.master, $scope.user);
            $scope.$broadcast('addOrUpdateList'); //to focus on the textarea

            $scope.modal.show();
        };

        $scope.openListModal = function() {
            $scope.theReorderItem = {
                list: "",
                oldList: "",
                task: "add"
            };
            $scope.$broadcast('addOrUpdateList'); //to focus on the textarea

            $scope.modal.show();
        };

        $scope.closeListModal = function() {
            $scope.modal.hide();
        };


        /**
         * dynamic button action
         */
        $scope.button_action = function(dynamicButton) {
            console.log(dynamicButton);
            if (dynamicButton.link) {


                if (dynamicButton.contentbuttontypeid==1) {

                    window.open(encodeURI(dynamicButton.link), '_system');

                }else if (dynamicButton.contentbuttontypeid == 2) {
                  
                    BUPInteraction.alert({message:dynamicButton.link});
                
                }

                //window.open(encodeURI($attrs.browseTo), '_system');
                
                //window.open(encodeURI(dynamicButton.link), '_blank','location=yes');
            }
        };

        $scope.$on('modal.shown', function() {
            /**
             * a bug in current version of ionic in which
             * after a modal open (probably with combination of ionic loading)
             * body has a class "loading-active" which disables the entire screen and thus
             * freezed the app
             * @return {[type]} [description]
             */

            //function defined in the rootscope
            $scope.fixIonicModalDisabledBug();
        });

        $scope.htmlSafe = function(html){
            //return html;
            return $sce.trustAsHtml(html);
        }

        $scope.$on('native.keyboardshown',function(event){
            //disable the slide feature
            $ionicSlideBoxDelegate.enableSlide(false);
        });
        $scope.$on('native.keyboardhidden',function(event){
            //enbale the slide feature
            $ionicSlideBoxDelegate.enableSlide(true);

        });

        



    }
]);

CONTROLLER_MODULE
.controller('SignInCtrl', [
    '$scope',
    '$rootScope', '$document', '$state', '$http','$translate', 'BUPUser', 'AuthService', 'AuthenticationService', 'BUPWebservice', 'BUPInteraction', 'BUPLoader', 'BUPSync', '$ionicModal', '$ionicPlatform', '$ionicViewService', '$ionicPopup', '$ionicLoading', 'BUPUtility',
    function($scope, $rootScope, $document, $state, $http,$translate, BUPUser, AuthService, AuthenticationService, BUPWebservice, BUPInteraction, BUPLoader, BUPSync, $ionicModal, $ionicPlatform, $ionicViewService, $ionicPopup, $ionicLoading, BUPUtility) {

        // var random_users = [{
        //     Username: "patient21",
        //     UserPassword: "123456"
        // }];
        // rand_index = Math.floor(Math.random() * (random_users.length));

        //$scope.user = random_users[rand_index];


        //if a user is alreay logged in then don't waste his/her time    
        if (BUPUser.isLoggedIn()) {
            $state.go('home');
            //BUPInteraction.toast('already logged in', 'short');
        }
        console.log($scope.currentUser);
        $scope.user = {};
        $scope.user = {
            Username: String($scope.currentUser ? $scope.currentUser.username : ""),
            UserPassword: ""
        };
        console.log(BUPUser);

        $ionicPlatform.ready().then(function() {

            $scope.user = angular.extend($scope.user, BUPUtility.deviceInfo());
            console.debug($scope.user);

        });


        $scope.signIn = function() {

            if (!$scope.user.Username) {
                $translate('INFO_USERNAME_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });


            } else if (!$scope.user.UserPassword) {
                $translate('INFO_PASSWORD_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });
            } else {

                AuthenticationService.login($scope.user);
            }
        };

        $scope.$on('event:auth-loginConfirmed', function(event, data) {
            //console.debug(data);
            
            // BUPSync.manualSync({
            //     silentSync: true
            // });

            console.debug('event fired: event:auth-loginConfirmed');

            $translate('INFO_LOGIN_SUCCESS'). then ( function  (text) {
                BUPInteraction.toast(text, 'short');
            });

            try {

                cordova.plugins.Keyboard.close();

            } catch (err) {
                
                //console.error(err);

            }
            /**
             * A decisional point in the app
             * Direction the user according to the role
             * For Individual user and custom group user redirect to the home page
             * ie
             * for roleId in (3 or 5 ) (individual or custom group user)
             *
             * But if roleId == 4 ( general group user)
             * then
             *     redirect to a user setting page
             *     where they must choose a custom username and password
             *     Then they they are logged out and should login as roleId 4
             *
             */

            if (data.roleId !== 4) {

                if ($scope.settingModal) {
                    //$scope.settingModal.isShown()
                    $scope.settingModal.hide();
                    $scope.settingModal.remove();

                }
                $rootScope.$emit('activitiesInit');
                $state.go('home');
                $rootScope.$broadcast('app.userready',{manualLogin:true});

            } else {
                /**
                 * @todo create a ionic popup modal
                 * for choosing custom username and password
                 */
                //$scope.generalGroupUserAction();

                //consider that the user is NOT logged in since 
                //the user is never going to use the app with this username
                var user = BUPUser.getCurrentUser();
                user.isLoggedIn = false;
                BUPUser.setCurrentUserInfo(user);
                $rootScope.currentUser = user;


                $scope.customGroupUser = {
                    GroupUserName: data.username,
                    UserId: data.id,
                    Username: "",
                    UserPassword: ""
                };
                $ionicPlatform.ready().then(function() {

                    $scope.customGroupUser = angular.extend($scope.customGroupUser, BUPUtility.deviceInfo());
                    console.debug($scope.customGroupUser);

                });
                console.log("generalGroupUserAction");
                $ionicModal.fromTemplateUrl('html/partials/modals/setting.html', function(modal) {
                    $scope.settingModal = modal;
                    $scope.settingModal.show();

                    $scope.$on('$destroy', function() {
                        $scope.settingModal.remove();
                    });
                    // Execute action on hide modal
                    $scope.$on('modal.hide', function() {
                        $scope.settingModal.remove();
                    });


                }, {
                    scope: $scope,
                    backdropClickToClose: false,
                    hardwareBackButtonClose: false,
                    animation: 'slide-in-up',
                    focusFirstInput: false,
                });
            }

        });



        $scope.$on('event:auth-login-failed', function(e, status) {

            console.log('event fired: auth-login-failed');
            console.log(status);
            $translate('INFO_LOGIN_FAILED'). then ( function  (text) {
                BUPInteraction.toast(text, 'short');
                return false;
            });

        });

        $scope.$on('modal.shown', function() {
            console.log('modal shown');
        });
        $scope.register = function(user) {

            if (!$scope.customGroupUser.Username) {
                //BUPInteraction.toast('Please choose a cutom username', 'short');
                //return false;
                $translate('INFO_USERNAME_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });


            } else if (!$scope.customGroupUser.UserPassword) {
                //BUPInteraction.toast('Please choose a cutom password', 'short');
                //return false;
                $translate('INFO_PASSWORD_MISSING'). then ( function  (text) {
                    BUPInteraction.toast(text, 'short');
                    return false;
                });
            } else {

                //$scope.customGroupUser = angular.extend($scope.customGroupUser, BUPUtility.deviceInfo());


                BUPWebservice.registerCustomGroupUser($scope.customGroupUser).then(function(response) {
                    if (response.status === "Success") {
                        //alert('welcome');
                        console.log(response);

                        AuthenticationService.login($scope.customGroupUser);

                        $scope.settingModal.hide();
                    } else {

                        BUPInteraction.toast(response.Message, 'short');

                    }

                });
            }
        };

        // $scope.$on('event:auth-logout-complete', function() {
        //     console.log('event fired: event:auth-logout-complete');
        //     console.log('time to refresh');
        //     $state.go('app.home', {}, {
        //         reload: true,
        //         inherit: false
        //     });
        // });

        // $scope.signIn = function() {
        //     AuthService.login($scope.user);
        // };

        $scope.$on('modal.shown', function() {
            /**
             * a bug in current version of ionic in which
             * after a modal open (probably with combination of ionic loading)
             * body has a class "loading-active" which disables the entire screen and thus
             * freezed the app
             * @return {[type]} [description]
             */

            //function defined in the rootscope
            $scope.fixIonicModalDisabledBug();
        });

    }
]);
CONTROLLER_MODULE
    .run(['$rootScope','$stateParams','BUPActivities',function($rootScope,$stateParams,BUPActivities) {
               var finishedAudioCallbackUnregister = $rootScope.$on('audio.ended', function(event, data) {
                //alert('audio finished');
                //finishedAudioCallbackUnregister();
                BUPActivities.addActivity({
                    taskid: Number($stateParams.id),
                    //date: moment().startOf('day').add('day',-7).format('YYYY-MM-DD'),//
                });
            });

            $rootScope.$on('audio.restarted',function(event,data) {
                finishedAudioCallbackUnregister = $rootScope.$on('audio.ended', function(event, data) {
                    //alert('audio finished');
                    finishedAudioCallbackUnregister();
                    BUPActivities.addActivity({
                        taskid: Number($stateParams.id),
                        //date: moment().startOf('day').add('day',-7).format('YYYY-MM-DD'),//
                    });

                });
            });
    }])

.factory('film_deep_breath02', [
    function() {


        var lib;

        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes
            var rect; // used to reference frame bounds

            // stage content:
            (lib.film_deep_breath02 = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    this.player = playSound("Andas_djupt_ovning_med_intro");
                }

                this.pause = function() {
                    console.log('expect the animation and sound to get paused here');
                    this.stop();
                    this.player.pause();
                }

                this.resume = function() {
                    this.play();
                    this.player.resume();
                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(839));

                // Andas Animation 
                this.CIRKEL = new lib.Symbol46();
                this.CIRKEL.setTransform(240, 240);
                this.CIRKEL.alpha = 0;
                this.CIRKEL._off = true;

                this.timeline.addTween(cjs.Tween.get(this.CIRKEL).wait(6).to({
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 57).wait(43).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 12).wait(6).to({
                    scaleX: 0.71,
                    scaleY: 0.71
                }, 89).wait(17).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 44).wait(69).to({
                    scaleX: 0.69,
                    scaleY: 0.69
                }, 53).to({
                    scaleX: 0.49,
                    scaleY: 0.49
                }, 14).wait(45).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 82).wait(42).to({
                    scaleX: 0.63,
                    scaleY: 0.63
                }, 46).to({
                    scaleX: 0.44,
                    scaleY: 0.44
                }, 17).wait(44).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 49).wait(28).to({
                    scaleX: 0.72,
                    scaleY: 0.72
                }, 53).to({
                    scaleX: 0.53,
                    scaleY: 0.53
                }, 9).wait(15));

                // Andas djupt bakgrund
                this.instance = new lib.ID_bakgrund_blue();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance
                    }]
                }).wait(840));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            // symbols:
            (lib.ID_bakgrund_blue = function() {
                this.initialize(img.ID_bakgrund_blue);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_green = function() {
                this.initialize(img.ID_bakgrund_green);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol46 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f().s("#FFFFFF").ss(32, 1, 1).p("ALKrJQEoEoAAGhQAAGikoEoQkoEomiAAQmhAAkokoQkokoAAmiQAAmhEokoQEokoGhAAQGiAAEoEog");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = rect = new cjs.Rectangle(-100.9, -100.9, 202, 202);
            p.frameBounds = [rect];

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});


        return lib.film_deep_breath02;
    }
])


.factory('film_nice_thought', [

    function() {


        var lib;
        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes
            var rect; // used to reference frame bounds

            // stage content:
            (lib.film_nice_thought = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    // playSound("Skona_tanken_ovning_med_intro");
                    this.player = playSound("Skona_tanken_ovning_med_intro");

                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(799));

                // Sköna tanken Lager 25
                this.instance = new lib.Symbol45();
                this.instance.setTransform(240, 240);

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(750).to({
                    alpha: 0
                }, 49).wait(1));

                // Sköna tanken Lager 24
                this.instance_1 = new lib.ID_bakgrund_green();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance_1
                    }]
                }).wait(800));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            // symbols:
            (lib.ID_bakgrund_blue = function() {
                this.initialize(img.ID_bakgrund_blue);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_green = function() {
                this.initialize(img.ID_bakgrund_green);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol43 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlZFaQiQiQABjKQgBjJCQiQQCQiQDJABQDKgBCQCQQCQCQgBDJQABDKiQCQQiQCQjKgBQjJABiQiQg");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
            p.frameBounds = [rect];


            (lib.Symbol44 = function(mode, startPosition, loop) {
                this.initialize(mode, startPosition, loop, {});

                // Lager 1
                this.instance = new lib.Symbol43("synched", 0);
                this.instance.alpha = 0;

                this.timeline.addTween(cjs.Tween.get(this.instance).to({
                    alpha: 0.602
                }, 49).wait(115).to({
                    startPosition: 0
                }, 0).to({
                    alpha: 0
                }, 75).wait(1));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            (lib.Symbol45 = function(mode, startPosition, loop) {
                this.initialize(mode, startPosition, loop, {});

                // Lager 22
                this.instance = new lib.Symbol44();
                this.instance.setTransform(148, -38.9);
                this.instance._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(589).to({
                    _off: false
                }, 0).wait(211));

                // Lager 21
                this.instance_1 = new lib.Symbol44();
                this.instance_1.setTransform(47, 121, 1.184, 1.184);
                this.instance_1._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(570).to({
                    _off: false
                }, 0).wait(230));

                // Lager 20
                this.instance_2 = new lib.Symbol44();
                this.instance_2.setTransform(-61.9, 129, 0.571, 0.571);
                this.instance_2._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(546).to({
                    _off: false
                }, 0).wait(254));

                // Lager 19
                this.instance_3 = new lib.Symbol44();
                this.instance_3.setTransform(148, 63);
                this.instance_3._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(519).to({
                    _off: false
                }, 0).wait(281));

                // Lager 18
                this.instance_4 = new lib.Symbol44();
                this.instance_4.setTransform(119, -113.9);
                this.instance_4._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(491).to({
                    _off: false
                }, 0).wait(309));

                // Lager 17
                this.instance_5 = new lib.Symbol44();
                this.instance_5.setTransform(-143.9, -42.9, 1.306, 1.306);
                this.instance_5._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(471).to({
                    _off: false
                }, 0).wait(329));

                // Lager 16
                this.instance_6 = new lib.Symbol44();
                this.instance_6.setTransform(-82.9, -139.9);
                this.instance_6._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(435).to({
                    _off: false
                }, 0).wait(365));

                // Lager 15
                this.instance_7 = new lib.Symbol44();
                this.instance_7.setTransform(124, 0);
                this.instance_7._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(406).to({
                    _off: false
                }, 0).wait(394));

                // Lager 14
                this.instance_8 = new lib.Symbol44();
                this.instance_8.setTransform(-161.9, 75, 0.755, 0.755);
                this.instance_8._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(376).to({
                    _off: false
                }, 0).wait(424));

                // Lager 13
                this.instance_9 = new lib.Symbol44();
                this.instance_9.setTransform(-137.9, -97.9, 0.694, 0.694);
                this.instance_9._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(344).to({
                    _off: false
                }, 0).wait(456));

                // Lager 12
                this.instance_10 = new lib.Symbol44();
                this.instance_10.setTransform(-6.9, -9.9, 1.673, 1.673);
                this.instance_10._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(319).to({
                    _off: false
                }, 0).wait(481));

                // Lager 11
                this.instance_11 = new lib.Symbol44();
                this.instance_11.setTransform(-118.9, 79, 0.612, 0.612);
                this.instance_11._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(293).to({
                    _off: false
                }, 0).wait(507));

                // Lager 10
                this.instance_12 = new lib.Symbol44();
                this.instance_12.setTransform(-13.9, -130.9, 1.347, 1.347);
                this.instance_12._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(261).to({
                    _off: false
                }, 0).wait(539));

                // Lager 9
                this.instance_13 = new lib.Symbol44();
                this.instance_13.setTransform(129, -48.9, 0.796, 0.796);
                this.instance_13._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(224).to({
                    _off: false
                }, 0).wait(576));

                // Lager 8
                this.instance_14 = new lib.Symbol44();
                this.instance_14.setTransform(-103.9, 10, 1.429, 1.429);
                this.instance_14._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(194).to({
                    _off: false
                }, 0).wait(606));

                // Lager 7
                this.instance_15 = new lib.Symbol44();
                this.instance_15.setTransform(-13.9, 110);
                this.instance_15._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(162).to({
                    _off: false
                }, 0).wait(638));

                // Lager 6
                this.instance_16 = new lib.Symbol44();
                this.instance_16.setTransform(68, -101.9, 1.245, 1.245);
                this.instance_16._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(133).to({
                    _off: false
                }, 0).wait(667));

                // Lager 5
                this.instance_17 = new lib.Symbol44();
                this.instance_17.setTransform(68, 0.1, 1.408, 1.408, 0, 0, 0, -7.8, -44.7);
                this.instance_17._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(109).to({
                    _off: false
                }, 0).wait(691));

                // Lager 4
                this.instance_18 = new lib.Symbol44();
                this.instance_18.setTransform(-69.9, 74, 0.735, 0.735);
                this.instance_18._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(79).to({
                    _off: false
                }, 0).wait(721));

                // Lager 3
                this.instance_19 = new lib.Symbol44();
                this.instance_19.setTransform(72, -26.9, 0.755, 0.755);
                this.instance_19._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(57).to({
                    _off: false
                }, 0).wait(743));

                // Lager 2
                this.instance_20 = new lib.Symbol44();
                this.instance_20.setTransform(-82.9, -64.9, 1.531, 1.531);
                this.instance_20._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(31).to({
                    _off: false
                }, 0).wait(769));

                // Lager 1
                this.instance_21 = new lib.Symbol44();
                this.instance_21._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(10).to({
                    _off: false
                }, 0).wait(790));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 0, 0);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-48.9, -48.9, 98, 98), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 207, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 250), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 306, 272), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 295), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 322, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 342, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -196.9, 342, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 367, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 372, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 381, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 376), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});


        return lib.film_nice_thought;
    }
])


.factory('film_countdown02', [

    function() {


        var lib,images,createjs;
        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes

            // stage content:
            (lib.film_countdown02 = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    this.player = playSound("Rakna_baklanges_ovning_med_intro");
                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(810));

                // 99
                this.instance = new lib.Symbol42nr5();
                this.instance.setTransform(242.5, 235.4);
                this.instance.alpha = 0.23;
                this.instance._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(317).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 6).wait(469));

                // 98
                this.instance_1 = new lib.Symbol42nr6();
                this.instance_1.setTransform(242.5, 235.4);
                this.instance_1.alpha = 0.23;
                this.instance_1._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(293).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(494));

                // 97
                this.instance_2 = new lib.Symbol42nr7();
                this.instance_2.setTransform(242.5, 235.4);
                this.instance_2.alpha = 0.23;
                this.instance_2._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(269).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(518));

                // 96
                this.instance_3 = new lib.Symbol42nr8();
                this.instance_3.setTransform(242.5, 235.4);
                this.instance_3.alpha = 0.23;
                this.instance_3._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(245).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(542));

                // 95
                this.instance_4 = new lib.Symbolnr9();
                this.instance_4.setTransform(240, 235.4);
                this.instance_4.alpha = 0.23;
                this.instance_4._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(221).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(566));

                // 94
                this.instance_5 = new lib.Symbol42nr4();
                this.instance_5.setTransform(242.5, 235.4);
                this.instance_5.alpha = 0.23;
                this.instance_5._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(342).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(445));

                // 93
                this.instance_6 = new lib.Symbol42nr3();
                this.instance_6.setTransform(242.5, 235.4);
                this.instance_6.alpha = 0.23;
                this.instance_6._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(366).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(423));

                // 92
                this.instance_7 = new lib.Symbol42nr2();
                this.instance_7.setTransform(242.5, 235.4);
                this.instance_7.alpha = 0.23;
                this.instance_7._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(388).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(401));

                // 91
                this.instance_8 = new lib.Symbol42nr1();
                this.instance_8.setTransform(242.5, 235.4);
                this.instance_8.alpha = 0.23;
                this.instance_8._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(410).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 58).wait(324));

                // 90
                this.instance_9 = new lib.Symbol41();
                this.instance_9.setTransform(242.5, 235.4);
                this.instance_9.alpha = 0;
                this.instance_9._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(32).to({
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 45).wait(124).to({
                    scaleX: 1.19,
                    scaleY: 1.19
                }, 5).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(590));

                // 89
                this.instance_10 = new lib.Symbol42nr1();
                this.instance_10.setTransform(242.5, 235.4);
                this.instance_10.alpha = 0.23;
                this.instance_10._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(715).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 1).wait(76));

                // 88
                this.instance_11 = new lib.Symbol42nr2();
                this.instance_11.setTransform(242.5, 235.4);
                this.instance_11.alpha = 0.23;
                this.instance_11._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(693).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(96));

                // 87
                this.instance_12 = new lib.Symbol42nr3();
                this.instance_12.setTransform(242.5, 235.4);
                this.instance_12.alpha = 0.23;
                this.instance_12._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(671).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(118));

                // 86
                this.instance_13 = new lib.Symbol42nr4();
                this.instance_13.setTransform(242.5, 235.4);
                this.instance_13.alpha = 0.23;
                this.instance_13._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(649).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(140));

                // 85
                this.instance_14 = new lib.Symbol42nr5();
                this.instance_14.setTransform(242.5, 235.4);
                this.instance_14.alpha = 0.23;
                this.instance_14._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(627).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(162));

                // 84
                this.instance_15 = new lib.Symbol41();
                this.instance_15.setTransform(242.5, 235.4, 0.757, 0.757);
                this.instance_15.alpha = 0;
                this.instance_15._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(488).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.09,
                    scaleY: 1.09,
                    alpha: 1
                }, 28).to({
                    scaleX: 1.19,
                    scaleY: 1.19
                }, 8).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(272));

                // 83
                this.instance_16 = new lib.Symbol42nr9();
                this.instance_16.setTransform(242.5, 235.4);
                this.instance_16.alpha = 0.23;
                this.instance_16._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(539).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(250));

                // 82
                this.instance_17 = new lib.Symbol42nr8();
                this.instance_17.setTransform(242.5, 235.4);
                this.instance_17.alpha = 0.23;
                this.instance_17._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(561).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(228));

                // 81
                this.instance_18 = new lib.Symbol42nr7();
                this.instance_18.setTransform(242.5, 235.4);
                this.instance_18.alpha = 0.23;
                this.instance_18._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(583).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(206));

                // 80
                this.instance_19 = new lib.Symbol42nr6();
                this.instance_19.setTransform(242.5, 235.4);
                this.instance_19.alpha = 0.23;
                this.instance_19._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(605).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(184));

                // Lager 23
                this.instance_20 = new lib.Symbol45("synched", 0);
                this.instance_20.setTransform(240, 240);
                this.instance_20.alpha = 0;
                this.instance_20._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(6).to({
                    startPosition: 0,
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 57).wait(748));

                // Bakgrund
                this.instance_21 = new lib.ID_bakgrund_orange();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance_21
                    }]
                }).wait(811));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            // symbols:
            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol45 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f().s("#FFFFFF").ss(20, 0, 0, 4).p("ARIwZMgiPAAAMAAAAgzMAiPAAAg");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-109.5, -104.9, 219.2, 210);


            (lib.Symbol42nr9 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjpHIQhPhGgLiHICvgRQAeCCBXAAQCGAAATkiQhRBAhrAAQhxAAhRhVQhQhUAAiUQAAiVBWhiQBXhhCaAAQCpAABdB5QBcB5AAEZQAAIMl1AAQh6AAhPhEgAhnk7QgfAtAABOQAABIAgAwQAgAwBBAAQA6AABFguIAAgLQAAh2glhRQglhRhLAAQgvAAgdAug");
                this.shape.setTransform(-0.1, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


            (lib.Symbol42kopia = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AgDGGQhaiIAAj+QAAj4BYiJQBViKCvAAQCvAABXCJQBZCIgBD6QABD/hbCHQhaCGiqAAQiqAAhYiGgACkkJQgbBdAACsQABCxAbBbQAbBbA+AAQBOAAAUh9QAVh8AAhuQAAiwgbhbQgbhbhAAAQg/AAgcBdgApUH/IAAijICNAAIAAqyIiVAAIAAikIF1AAIAANWICFAAIAACjg");
                this.shape.setTransform(4.9, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-55.7, -49.6, 121.2, 105);


            (lib.Symbol38 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AkKG3QhZhVAAiCQAAizC1hCQiWhVAAiWQAAh3BXhKQBYhKCVAAQCdAABVBJQBUBKAAB0QAACZiTBWQCxBBAAC5QAACHhbBQQhbBQivAAQiwAAhZhVgAhgBcQgnAsAABJQAABIAmArQAnApA7AAQA9AAAngpQAngrAAhIQAAhHgmgtQgngtg+AAQg6AAgnAsgAhUlKQgiAjAABFQAAA5AgAmQAhAmA1AAQA2AAAhgmQAhglAAg9QAAg8gfgmQgggmg5AAQgyAAgiAjg");
                this.shape.setTransform(0.1, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-35.5, -49.6, 71.4, 105);


            (lib.Symbol37 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AkVH9IAAikICBAAQAGiUBOjOQBLjQBfiEIkEAAIAACPIijAAIAAkuIJ7AAIAAB9QhNBchPDmQhPDkAACyICBAAIAACkg");
                this.shape.setTransform(0.6, 3.1);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-31.2, -47.8, 63.7, 101.9);


            (lib.Symbol36 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("Aj4GRQhbh9ABkKQgBkaBnh9QBmh+CuAAQD+AAAgEPIivAQQgeiChZAAQhFAAgnBSQgoBQgCCDQBHhDBrAAQB2AABQBRQBRBQAACaQAACshVBZQhWBYiNAAQi4AAhbh7gAh2BFQADCpAmA+QAjA+BBAAQAxAAAegsQAegsAAhWQABhRgkgpQglgpg0AAQg/AAg/Asg");
                this.shape.setTransform(0.6, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-33.3, -49.6, 68, 105);


            (lib.Symbol35 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjzG2QhVhNgTiEIDBgXQATCSB0AAQBAABAjgtQAjgtAAhRQABi1iDAAQhKAAgwBQIikhRIAeoCII6AAIAADvIiUAAIgPhMIkEAAIgND0QBQg5BbAAQCWAABSBhQBSBfAACLQAACUhYBlQhYBjixAAQiZAAhVhNg");
                this.shape.setTransform(-1.2, 3.7);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36, -47.8, 69.7, 103.2);


            (lib.Symbol34 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("Ag0IGIAAiWIBvAAIAAiDIl0AAQgahOgcg0IG0pwIDIAAIAAJnIBjAAIAACLIhjAAIAACDIBWAAIAACWgAjABiID7AAIAAlhg");
                this.shape.setTransform(-1.5, 2.2);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38.4, -49.6, 73.8, 103.7);


            (lib.Symbol33 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlbDIIDEgQQAECzCDAAQA5AAAmgrQAlgrAAhDQAAhBgkgrQglgshHAAIguABIAAipQBWAAAngnQAngnAAg5QAAgygdgfQgfggguAAQhtAAAACGIi+gOQAFiLBXhJQBXhJCFAAQCUAABUBIQBSBHABB9QgBCiiXBFQC8A8AAC2QAACQhhBRQhiBRiVAAQlGAAgZlEg");
                this.shape.setTransform(-1.2, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36, -49.6, 69.7, 105);


            (lib.Symbol32 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlJIGQAAiGAWhJQAVhJAyhFQAxhFBZhSQByhpAmgxQAkgzAAg/QAAgvgcgeQgcgfgnAAQg9AAgdAzQgdAygFBxIi7gJQAAiTAshLQAthKBLgiQBMgiBWAAQCSAABTBRQBTBRAAB4QAABAgZA3QgaA2glAmQgoAihhBGQiQBogmA+QglA+gHAuIEoAAIAAiEICjAAIgJEog");
                this.shape.setTransform(-1.1, 2.2);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 66.1, 103.7);


            (lib.Symbol31 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjzH9IAAikICNAAIAAqyIiWAAIAAijIF0AAIAANVICFAAIAACkg");
                this.shape.setTransform(0.1, 3.1);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-25.2, -47.8, 50.7, 101.9);


            (lib.Symbolnr9 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol42nr9();

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


            (lib.Symbol42nr8 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol38("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-37.5, -49.6, 71.4, 105);


            (lib.Symbol42nr7 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol37("synched", 0);
                this.instance.setTransform(0.5, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-30.7, -47.8, 63.7, 101.9);


            (lib.Symbol42nr6 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol36("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-35.3, -49.6, 68, 105);


            (lib.Symbol42nr5 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol35("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38, -47.8, 69.7, 103.2);


            (lib.Symbol42nr4 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol34("synched", 0);
                this.instance.setTransform(-3.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-42.4, -49.6, 73.8, 103.7);


            (lib.Symbol42nr3 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol33("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38, -49.6, 69.7, 105);


            (lib.Symbol42nr2 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol32("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36.2, -49.6, 66.1, 103.7);


            (lib.Symbol42nr1 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol31("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-27.2, -47.8, 50.7, 101.9);


            (lib.Symbol41 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol42kopia();
                this.instance.setTransform(-4.2, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-60, -49.6, 121.2, 105);

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});



        return lib.film_countdown02;
    }
])


.service('animationService', ['$rootScope', '$injector',
    function($rootScope, $injector) {
        //var canvas, stage, exportRoot,player;
        var stage, manifest;
        var resizeCanvas = function() {
            // browser viewport size
            var w = window.innerWidth;
            var h = window.innerHeight;

            // stage dimensions
            var ow = 480; // your stage width
            var oh = 480; // your stage height

            var keepAspectRatio = true;

            if (keepAspectRatio) {
                // keep aspect ratio
                var scale = Math.min(w / ow, h / oh);
                stage.scaleX = scale;
                stage.scaleY = scale;

                // adjust canvas size
                stage.canvas.width = ow * scale;
                stage.canvas.height = oh * scale;
            } else {
                // scale to exact fit
                stage.scaleX = w / ow;
                stage.scaleY = h / oh;

                // adjust canvas size
                stage.canvas.width = ow * stage.scaleX;
                stage.canvas.height = oh * stage.scaleY;
            }

            // update the stage
            stage.update()
        }
        var animationFile;

        var animationCompleted = function() {
            $rootScope.$broadcast('audio.ended');
        }
        window.as = this;

        window.addEventListener('resize', resizeCanvas);

        this.init = function(animationPackage, manifest) {
            this.canvas = document.getElementById("canvas");
            //this.images = images || {};

            // this.manifest = [
            //  {src:"images/ID_bakgrund_blue.jpg", id:"ID_bakgrund_blue"},
            //  {src:"images/ID_bakgrund_green.jpg", id:"ID_bakgrund_green"},
            //  {src:"images/ID_bakgrund_orange.jpg", id:"ID_bakgrund_orange"},
            //  {src:"images/stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720.jpg", id:"stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720"},
            //  {src:"images/under_Background.jpg", id:"under_Background"},
            //  {src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"Andas_djupt_ovning_med_intro"},
            //  {src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"Rakna_baklanges_ovning_med_intro"},
            //  {src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"Skona_tanken_ovning_med_intro"}
            // ];

            this.manifest = manifest;

            animationFile = animationPackage;


            var loader = new createjs.LoadQueue(false);
            loader.installPlugin(createjs.Sound);
            loader.addEventListener("fileload", this.handleFileLoad);
            loader.addEventListener("complete", this.handleComplete);
            loader.loadManifest(this.manifest);
        };

        this.handleFileLoad = function(evt) {
            if (evt.item.type == "image") {
                this.images[evt.item.id] = evt.result;
            }
        }

        this.handleComplete = function() {
            // this.exportRoot = new lib.film_deep_breath02();
            this.exportRoot = new animationFile();

            stage = new createjs.Stage(this.canvas);
            stage.addChild(this.exportRoot);
            stage.update();
            window.st = stage;

            createjs.Ticker.setFPS(12);
            createjs.Ticker.addEventListener("tick", stage);
            resizeCanvas();
        }

        this.playSound = window.playSound = function(id, loop) {

            this.sound_id = id;

            this.player = createjs.Sound.play(id, createjs.Sound.INTERRUPT_EARLY, 0, 0, loop);
            this.player.addEventListener("complete", animationCompleted);
            return player;
        }

        this.destroy = function() {
            try {
                stage.removeAllChildren();
                createjs.Sound.removeAllEventListeners();
                createjs.Sound.removeAllSounds();
            } catch (e) {
                console.error(e);
            }

        }

        this.startAnimation = function(animationType, lib) {
            manifest = [
                                  //{src:"images/ID_bakgrund_blue.jpg", id:"ID_bakgrund_blue"},
                                  //{src:"images/ID_bakgrund_green.jpg", id:"ID_bakgrund_green"},
                                  //pos{src:"images/ID_bakgrund_orange.jpg", id:"ID_bakgrund_orange"},
                                  //{src:"images/stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720.jpg", id:"stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720"},
                                  //{src:"images/under_Background.jpg", id:"under_Background"},
                                  {src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"Andas_djupt_ovning_med_intro"},
                                  {src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"Rakna_baklanges_ovning_med_intro"},
                                  {src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"Skona_tanken_ovning_med_intro"}
                              ];

            var animationpackageLib;
            console.log(animationType);
            switch (animationType) {
                case 3: // takna green






                    // manifest = [];

                    // manifest.push({
                    //     src: "sounds/Skona_tanken_ovning_med_intro.mp3",
                    //     id: "Skona_tanken_ovning_med_intro"
                    // });
                    // manifest.push({
                    //     src: "images/ID_bakgrund_green.jpg",
                    //     id: "ID_bakgrund_green"
                    // });

                    //animationPackage = lib2.film_nice_thought;
                    animationPackage = $injector.get('film_nice_thought');

                    break;

                case 1: //countdown rakna (orange)

                    // manifest = [];

                    // manifest.push({
                    //     src: "images/ID_bakgrund_orange.jpg",
                    //     id: "ID_bakgrund_orange"
                    // });

                     // manifest.push({src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"Rakna_baklanges_ovning_med_intro"});
                   

                    // animationPackage = lib3.film_countdown02;
                    animationPackage = $injector.get('film_countdown02');
                    //animationPackage = lib3.film_countdown02;

                    break;

                case 2: //andas (blue) //film_deep_breath02
                    //default:
                    console.log("Called");
                    // manifest = [];
                    // manifest.push({
                    //     src: "images/ID_bakgrund_blue.jpg",
                    //     id: "ID_bakgrund_blue"
                    // });
                    // manifest.push({
                    //     src: "sounds/Andas_djupt_ovning_med_intro.mp3",
                    //     id: "Andas_djupt_ovning_med_intro"
                    // });
                    // animationPackage = lib1.film_deep_breath02;
                    animationPackage = $injector.get('film_deep_breath02');

                    break;

            }

            this.init(animationPackage, manifest);

        }
        //
        // window.onresize = function()
        // {
        //      this.onResize();
        // }

    }
])
.controller('soundCtrl', ['$rootScope', '$scope', '$stateParams', 'BUPWebservice', 'BUPInteraction', 'BUPFiles','BUPUtility','$injector', 
    function($rootScope, $scope, $stateParams, BUPWebservice, BUPInteraction,  BUPFiles,BUPUtility,$injector  ) {

        $scope.playing = false;
        


            $scope.set_sound_bg = function() {
                if ($scope.practice.audio_bgcolor) {
                    return {
                        backgroundColor: $scope.practice.audio_bgcolor //$scope.practice.audio_bgcolor
                    }

                }
            };


        /**
         * Do not let the phone sleep for the time being 
         */
        if (ionic.Platform.isWebView()) {
            
            try{
                window.plugins.insomnia.keepAwake();
            }catch(e){
                console.error(e)
            }    


            $scope.$on('$destroy',function(){
                console.log('scope being destroyed!');
                try{
                    window.plugins.insomnia.allowSleepAgain()
                }catch(e){
                    console.error(e)
                }            
            });



        }



        $scope.practice = {};
        $scope.practice.loading = true;

        $scope.existingAnimations=[''];

        var animationService;

        BUPWebservice.getPracticeById($stateParams.id).then(function(response) {

          
            $scope.practice = response;

            

            if (response && response.hasOwnProperty('isAnimation') && !!response.isAnimation) {
                //animation
                   $scope.animationEnabled = true;
                   animationService = $injector.get('animationService');

                    animationService.startAnimation(response.animationType,window.lib);
                        
                

            }else {

                $scope.animationEnabled = false;


                //sound
                
                //

           

                 ///*
    
                var audioData = {};
                audioData.file =  encodeURI($scope.practice.audio_file);
                audioData.loop = $scope.practice.repeat_automatic;

                console.log(audioData);
      
                var tryLoadingFromServer = function(file){
                        file.remote = encodeURI(file.remote);
                        console.log('<><><><><><><><><><><><><><><><><><><><>');

                        BUPUtility.UrlExists(file.remote).then(function(data){
                            console.log(data);
                            //console.log(file);
                            audioData.file = file.remote;
                            console.log('local file for :'+audioData.file+' not found locally trying to load it from remote!');
                            //also signal that we need to store this file locally
                            $rootScope.$broadcast('audio.set', audioData);
                            $rootScope.$broadcast('save_audio_locally',audioData.file,{forceSaveAgain:true} );

                            console.log('<><><><><><><><><><><><><><><><><><><><>');
                        },function(e){
                            BUPInteraction.alert({message:'Sound file is either not present or not supported!'});
                            console.error(e);
                            console.log('<><><><><><><><><><><><><><><><><><><><>');
                        });

                };
                
                BUPFiles.getFile($scope.practice.audio_file).then(function(file){
                    console.log(file);
                    if (file.local ) {
                        
                        audioData.file = file.local;
                        $rootScope.$broadcast('audio.set', audioData);

                    } else {
                        

                        if($rootScope.isOnline){
                            audioData.file = file.remote;
                            console.log('local file for :'+audioData.file+' not found locally trying to load it from remote!');
                            //also signal that we need to store this file locally
                            $rootScope.$broadcast('audio.set', audioData);
                            $rootScope.$broadcast('save_audio_locally',audioData.file,{forceSaveAgain:true} );
                        }else{
                             BUPInteraction.alert({message:'No Internet!'});
                        }

                    }


                });


            //*/
           

            }
            
            //window.as = animationService;
            //console.log(animationService);


            

        });


        $scope.$on('$destroy', function() {
            

             
            try{

                if($scope.animationEnabled && animationService) {

                    animationService.destroy();
                    
                }

                // $scope.audio.pause();
                // $scope.audio.src = null;
         
                
            }catch(e){
                console.error(e);
            }

            //remove the instance when user goes out of the current scope d x
        });




    }
]);
