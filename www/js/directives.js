var BUPAudioAdapter = function(){
};


DIRECTIVE_MODULE
//BUPAudio
.directive('bupAudio', ['$rootScope', '$interval',
    function($rootScope, $interval) {


        //var $seekbar = 
        var timeoutId;
        //var audioElement = $document[0].createElement('audio');
        return {
            restrict: 'E',
            replace: false,
            //transculde: true,
            scope: {},
            //link: link,
            controller: function($scope, $element) {

                console.log($scope);
                //console.log(audioElement);
                //console.log('up');
                $scope.audio = new Audio();
                //$scope.seekbar = angular.element(seekbar);
                //$scope.currentNum = 0;
                $scope.playpause = function() {
                    var a = $scope.audio.paused ? $scope.audio.play() : $scope.audio.pause();
                };

                $scope.toggle = function() {

                    $scope.playing = !$scope.playing;
                    $scope.playing ? $scope.audio.play() : $scope.audio.pause();

                };

                $scope.playAudio = function() {
                    //BUPAudio.play($scope.practice.audio_file);
                    //BUPAudio.play();
                    $scope.audio.play();
                    $scope.playing = true;
                    //audioElement.play();

                };

                $scope.pauseAudio = function() {
                    // BUPAudio.pause();
                    $scope.audio.pause();
                    $scope.playing = false;
                    //audioElement.pause();

                };


                //$scope.stopAudio = function() {};



                //listen for audio-element events
                // $scope.audio.addEventListener('play', function() {
                //     $rootScope.$broadcast('audio.play', this);
                // });
                // $scope.audio.addEventListener('pause', function() {
                //     $rootScope.$broadcast('audio.pause', this);
                // });
                $scope.played = 0;
                /*
                $scope.audio.addEventListener('timeupdate', function() {
                    $rootScope.$broadcast('audio.time', this);
                    console.log('timeupdate fired');
                    $scope.played = $scope.audio.currentTime;
                    window.clearTimeout($scope.checkLoading);

                    if( $scope.audio.ended === true) {
                        $scope.audioEnded();
                    }
                    
                    //console.log($scope.audio.currentTime);
                });
                */

             //improved
                $scope.onTimeUpdate = ionic.throttle(function() {

                    $rootScope.$broadcast('audio.time', this);
                    //console.log('timeupdate fired');
                    
                    

                    $scope.played = $scope.audio.currentTime;
                    if( $scope.audio.ended ) {
                       
                        $scope.audioEnded();
                    }

                    window.clearTimeout($scope.checkLoading);

                    
                    
                },500);

             

                $scope.audio.addEventListener('timeupdate', $scope.onTimeUpdate);
               

                $scope.seekbarChanged = function(a){
                    //console.log(a);
                    $scope.audio.currentTime = $scope.played;

                    //console.log('seekbar changed');
                };

               
               
               $scope.audioEnded = _.debounce(function() {
                    $rootScope.$broadcast('audio.ended', this);
                    console.log('audio.ended fired');
                    $scope.playing = false;
                    $scope.played = 0;
                    //if( !$scope.audio.loop)
                      //  $scope.audio.removeEventListener('ended', $scope.audioEnded);
                    if ($scope.loop) {
                        $scope.audio.currentTime = 0;
                        $scope.playAudio();
                    }  
                   
                    //@todo
                    //if has option restart replay the audio
                },5000);


                $scope.audio.addEventListener('ended', $scope.audioEnded);




                //console.log('here audio.set');

                $scope.checkLoading = null;
                $scope.loadingStarted = function() {
                    $scope.loading = true;
                    //console.log('loadstart fired');

                    $scope.audio.removeEventListener('ended', $scope.audioEnded);

                    $scope.checkLoading = setTimeout(function() {
                        //$scope.audio.play();
                        //$scope.audio.play();
                        $scope.playAudio();
                    }, 50);

                }
                $scope.audio.addEventListener('loadstart', $scope.loadingStarted);

                

                $scope.audio.addEventListener('loadeddata', function() {
                    //console.log('loadeddata fired');
                    $scope.loading = false;

                });

                $scope.audio.addEventListener('canplay', function() {
                    //console.log('can play');
                    $scope.loading = false;

                });




                var unregisterAudioSetListener = $rootScope.$on('audio.set', function(event, obj) {
                    //console.log('audio.set called');
                    var playing = !$scope.audio.paused;
                    //console.log(playing);
                    //console.log(obj)
                    //$scope.loading = false;
                    $scope.audio.src = obj.file;
                    $scope.loop = Boolean(parseInt(obj.loop,10));

                    //$scope.audio.loop = obj.loop;

                    unregisterAudioSetListener();

              
                });





                timeoutId = $interval(function() {
                    //if ( !! !$scope.$$phase) {
                    //$digest or $apply
                    //console.log("being applied");
                    //$scope.$apply();
                    //$scope.$$phase || $scope.$apply();
                    // }

                }, 0);

                $scope.$on('$destroy', function() {

                    $scope.audio.pause();
                    $interval.cancel(timeoutId);
                    delete $scope.audio;
                    $scope.audio =  {};
                    // $scope.audio.src = null;

                })

                //update display of things - makes scrub work
                // setInterval(function() {
                //     //console.log('scope applied')
                //     $scope.$apply();
                // }, 500);
            },
            //$scope.audio.currentTime
            template: '<div class="audiocontrol">\
                            <button ng-class="{\'ion-play\':!playing,\'ion-pause\':playing,\'audio-loading ion-loading-c\':loading}" class="circled" ng-click="toggle()" > </button>\
                            <div id="audio_timeline">\
                                <div class="playingtime">{{played | number:2 | audiotime}}</div>\
                                <div class="range2">\
                                    <input ng-change="seekbarChanged();" ng-model="played"  type="range" precision="float" step="any" min="0" max="{{audio.duration}}"  id="seekbar">\
                                </div>\
                                <div class="totaltime">{{audio.duration | number:2 | audiotime}}</div>\
                            </div>\
                        </div>'
            //templateUrl: 'templates/audio-player.html'
            // link: function($scope, $element, attrs) {
            //     scope.timeElapsed

            // }
        };
    }
]);

        // function ctrl($scope, $element) {
        //     console.log('here');
        //     $scope.audio = new Audio();
        //     $scope.currentNum = 0;
        //     $scope.playpause = function() {
        //         var a = $scope.audio.paused ? $scope.audio.play() : $scope.audio.pause();
        //     };

        //     //listen for audio-element events
        //     $scope.audio.addEventListener('play', function() {
        //         $rootScope.$broadcast('audio.play', this);
        //     });
        //     $scope.audio.addEventListener('pause', function() {
        //         $rootScope.$broadcast('audio.pause', this);
        //     });
        //     $scope.audio.addEventListener('timeupdate', function() {
        //         $rootScope.$broadcast('audio.time', this);
        //     });
        //     $scope.audio.addEventListener('ended', function() {
        //         $rootScope.$broadcast('audio.ended', this);
        //         //goto next practice???
        //         //$scope.next();
        //     });

        //     $rootScope.$on('audio.set', function(obj) {
        //         console.log('audio.set called');
        //         console.log(obj);
        //         var playing = !$scope.audio.paused;
        //         $scope.audio.src = obj.file;
        //         var a = playing ? $scope.audio.play() : $scope.audio.pause();
        //         $scope.info = obj.info;
        //         //set other audio related inforation from source to the audio object
        //     });

        //     //update display of things - makes scrub work
        //     // setInterval(function() {
        //     //     //console.log('scope applied')
        //     //     $scope.$apply();
        //     // }, 500);
        // }

DIRECTIVE_MODULE

/**
 * directive to make autofocus work in the modal box
 */


.directive('focusOn', function() {
    return function(scope, elem, attr) {
        scope.$on(attr.focusOn, function(e) {
            //elem[0].focus();
            //elem[0].select();
        });
    };
})

.directive('dipeshSlider', [function () {
  return {
    restrict: 'A',
    scope: {
      rating: '='
    },
    require: '?ngModel',
    link: function (scope, $element, $attrs) {
        
     var sliderDimensions = {};
     var trackContainer =  $element;
     var min,max,step;

     function refreshSliderDimensions() {
        sliderDimensions = trackContainer[0].getBoundingClientRect();
     }
     refreshSliderDimensions();
     console.log(sliderDimensions);
     console.log("<- slider dimensions");

      var hammertime = new Hammer($element[0], {
        recognizers: [
          [Hammer.Pan, { direction: Hammer.DIRECTION_VERTICAL }]
        ]
      });
      hammertime.on('hammer.input', onInput);
      hammertime.on('panstart', onPanStart);
      hammertime.on('pan', onPan);
      //hammertime.on('panend', onPanEnd);


          /**
           * Slide listeners
           */
          var isSliding = false;
          var rating;
          var isDiscrete = angular.isDefined($attrs.mdDiscrete);
          function updateSlider(y) {
            y = y-30;
            

            var point = Math.round( (sliderDimensions.bottom-y)/20);
            if (point < 0){
              rating = '0';
            }else if (point>10) {
              rating = 10
            }else{
              rating = point;
            }
            if(rating==0){
              rating = '0';
            }

            //console.log(''+(y+30)+', '+y+', '+sliderDimensions.bottom+', '+point+', '+rating);

            scope.$apply(function() {
              scope.rating = rating;
            });
            //console.log($attrs.whenSlidePan);
          }
          function onInput(ev) {
            if (!isSliding && ev.eventType === Hammer.INPUT_START &&
                !$element[0].hasAttribute('disabled')) {

              isSliding = true;

              $element.addClass('active');
              $element[0].focus();
              //refreshSliderDimensions();

              onPan(ev);

              ev.srcEvent.stopPropagation();

            } else if (isSliding && ev.eventType === Hammer.INPUT_END) {

              if ( isSliding && isDiscrete ) onPanEnd(ev);
              isSliding = false;

              $element.removeClass('panning active');
            }
          }
          function onPanStart() {
            if (!isSliding) return;
            $element.addClass('panning');
          }
          function onPan(ev) {
            if (!isSliding) return;

            // While panning discrete, update only the
            // visual positioning but not the model value.

            if ( isDiscrete ) {
              //adjustThumbPosition( ev.center.x );
              console.log('discrete')
              //console.log(ev.center.y);
              updateSlider(ev.center.y);
            }
            else{
              //doSlide( ev.center.x );
              //console.log(ev.center.y);
              updateSlider(ev.center.y);
            }              

            ev.preventDefault();
            ev.srcEvent.stopPropagation();
          }

          function onPanEnd(ev) {
            console.log('onpanend');
            if ( isDiscrete && !$element[0].hasAttribute('disabled') ) {
              // Convert exact to closest discrete value.
              // Slide animate the thumb... and then update the model value.

              var exactVal = percentToValue( positionToPercent( ev.center.x ));
              var closestVal = minMaxValidator( stepValidator(exactVal) );

              setSliderPercent( valueToPercent(closestVal));
              $$rAF(function(){
                setModelValue( closestVal );
              });

              ev.preventDefault();
              ev.srcEvent.stopPropagation();
            }
          }



    }
  };
}])

.directive('slider', function () {
  return {
    restrict: 'A',
    scope: {
      start: '@',
      step: '@',
      end: '@',
      callback: '@',
      margin: '@',
      orientation: '=',
      ngModel: '=',
      ngFrom: '=',
      ngTo: '='
    },
    link: function (scope, element, attrs) {
      var callback, fromParsed, parsedValue, slider, toParsed;
      slider = $(element);
      callback = scope.callback ? scope.callback : 'slide';
      if (scope.ngFrom != null && scope.ngTo != null) {
        fromParsed = null;
        toParsed = null;
        slider.noUiSlider({
          start: [
            scope.ngFrom || scope.start,
            scope.ngTo || scope.end
          ],
          step: parseFloat(scope.step || 1),
          orientation: scope.orientation || 'horizontal',
          margin: parseFloat(scope.margin || 0),
          range: {
            min: [parseFloat(scope.start)],
            max: [parseFloat(scope.end)]
          }
        });
        slider.on(callback, function () {
          var from, to, _ref;
          _ref = slider.val(), from = _ref[0], to = _ref[1];
          fromParsed = parseFloat(from);
          toParsed = parseFloat(to);
          return scope.$apply(function () {
            scope.ngFrom = fromParsed;
            return scope.ngTo = toParsed;
          });
        });
        scope.$watch('ngFrom', function (newVal, oldVal) {
          if (newVal !== fromParsed) {
            return slider.val([
              newVal,
              null
            ]);
          }
        });
        return scope.$watch('ngTo', function (newVal, oldVal) {
          if (newVal !== toParsed) {
            return slider.val([
              null,
              newVal
            ]);
          }
        });
      } else {
        parsedValue = null;
        slider.noUiSlider({
          start: [scope.ngModel || scope.start],
          step: parseFloat(scope.step || 1),
          orientation: scope.orientation || 'vertical',
          direction: 'rtl',
          connect: 'lower',
          range: {
            min: [parseFloat(scope.start)],
            max: [parseFloat(scope.end)]
          }
        });
        slider.on(callback, function () {
          parsedValue = parseFloat(slider.val());
          return scope.$apply(function () {
            return scope.ngModel = parsedValue;
          });
        });
        return scope.$watch('ngModel', function (newVal, oldVal) {
          if (newVal !== parsedValue) {
            return slider.val(newVal);
          }
        });
      }
    }
  };
})
.directive('sglide', [function () {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            //alert('linked');
            jQuery(document).ready(function(){
                //(iElement).sGlide({
                jQuery('#anxietyLevel').sGlide({
                'startAt': 0,
                'flat': true,
                'width': 218,
                'unit': 'px',
                'snap': {
                    'points': 11,
                    'markers': true,
                    'hard': true,
                    'onlyOnDrop':false
                },
                'vertical': true,
                'showKnob': false
            });

            });
            
        }
    };
}])

    .directive("dynamicBackground", function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var el = element[0],
                    attr = el.getAttribute('style');

                el.setAttribute('style', attr);

                // We need to watch for changes in the style in case required data is not yet ready when compiling
                attrs.$observe('style', function() {
                    attr = el.getAttribute('style');

                    if (attr) {
                        el.setAttribute('style', attr);
                    }
                });
            }
        };
    })

.directive('selectOnClick', function() {
    // Linker function
    return function(scope, element, attrs) {
        element.bind('click', function() {
            this.select();
        });
    };
})

.directive('openUrl', function() {
    // Linker function
    return function(scope, element, attrs) {

        var target = attrs.target || "_blank";

        element.bind('click', function() {
            window.open(attrs.openUrl, target, 'location=yes');
        });
    };
})
// .directive('ionNetwork', function($interval) {
//     return {
//         restrict: 'A',
//         scope: {
//             interval: '@?ionNetwork'
//         },
//         link: function(scope, element) {
//             if (window.cordova) {
//                 var allowedNetworkStates = [Connection.WIFI, Connection.CELL_4G, Connection.CELL_3G, Connection.CELL_2G];
//                 var disabledTags = ['input', 'button', 'textarea', 'select'];
//                 var tag = element[0].tagName.toLowerCase();
//                 scope.interval = parseInt(scope.interval) || 500;

//                 function checkNetworkState() {
//                     if (allowedNetworkStates.indexOf(navigator.connection.type) === -1) {
//                         if (disabledTags.indexOf(tag) !== -1) {
//                             element[0].disabled = true;
//                         }
//                         element.removeClass('online');
//                         element.addClass('offline');
//                     } else {
//                         if (disabledTags.indexOf(tag) !== -1) {
//                             element[0].disabled = false;
//                         }
//                         element.removeClass('offline');
//                         element.addClass('online');
//                     }
//                 }

//                 checkNetworkState();
//                 stop = $interval(checkNetworkState, scope.interval);

//                 scope.$on('$destroy', function() {
//                     $interval.cancel(stop);
//                 });
//             }
//         }
//     };
// })

.directive('kcNetwork', function($rootScope, $interval, CordovaNetwork) {
    return {
        restrict: 'A',
        scope: {
            interval: '@?kcNetwork'
        },
        link: function(scope, element) {
            return;
            //if (window.cordova) {
            var disabledTags = ['input', 'button', 'textarea', 'select'];
            var tag = element[0].tagName.toLowerCase();
            scope.interval = parseInt(scope.interval) || 500;

            var whenOnline = function() {
                if (disabledTags.indexOf(tag) !== -1) {
                    element[0].disabled = false;
                }
                element.removeClass('offline');
                element.addClass('online');
            }

            var whenOffline = function() {
                if (disabledTags.indexOf(tag) !== -1) {
                    element[0].disabled = true;
                }
                element.removeClass('online');
                element.addClass('offline');
            }

            function checkNetworkState() {

                // if (CordovaNetwork.isOnline() === false) {
                //     whenOffline();
                // } else {
                //     whenOnline();
                // }
                if ($rootScope.isOnline) {
                    whenOnline();
                } else {
                    whenOffline();
                }
            }

            checkNetworkState();
            stop = $interval(checkNetworkState, scope.interval);

            scope.$on('$destroy', function() {
                $interval.cancel(stop);
            });

            $rootScope.$on('Cordova.NetworkStatus.Offline', function() {
                whenOffline();
            });
            $rootScope.$on('Cordova.NetworkStatus.Online', function() {
                whenOnline();
            });
            //}
        }
    };
})

DIRECTIVE_MODULE

.directive('ratingSlider', SliderDirective);

/**
 * @ngdoc directive
 * @name mdSlider
 * @module material.components.slider
 * @restrict E
 * @description
 * The `<md-slider>` component allows the user to choose from a range of
 * values.
 *
 * It has two modes: 'normal' mode, where the user slides between a wide range
 * of values, and 'discrete' mode, where the user slides between only a few
 * select values.
 *
 * To enable discrete mode, add the `md-discrete` attribute to a slider,
 * and use the `step` attribute to change the distance between
 * values the user is allowed to pick.
 *
 * @usage
 * <h4>Normal Mode</h4>
 * <hljs lang="html">
 * <md-slider ng-model="myValue" min="5" max="500">
 * </md-slider>
 * </hljs>
 * <h4>Discrete Mode</h4>
 * <hljs lang="html">
 * <md-slider md-discrete ng-model="myDiscreteValue" step="10" min="10" max="130">
 * </md-slider>
 * </hljs>
 *
 * @param {boolean=} mdDiscrete Whether to enable discrete mode.
 * @param {number=} step The distance between values the user is allowed to pick. Default 1.
 * @param {number=} min The minimum value the user is allowed to pick. Default 0.
 * @param {number=} max The maximum value the user is allowed to pick. Default 100.
 */
function SliderDirective($mdTheming) {
  return {
    scope: {},
    require: ['?ngModel', 'ratingSlider'],
    controller: SliderController,
    template:
      '<div class="md-track-container">' +
        '<div class="md-track"></div>' +
        '<div class="md-track md-track-fill"></div>' +
        '<div class="md-track-ticks"></div>' +
      '</div>' +
      '<div class="md-thumb-container">' +
        '<div class="md-thumb"></div>' +
        '<div class="md-focus-thumb"></div>' +
        '<div class="md-focus-ring"></div>' +
        '<div class="md-sign">' +
          '<span class="md-thumb-text"></span>' +
        '</div>' +
        '<div class="md-disabled-thumb"></div>' +
      '</div>',
    link: postLink
  };

  function postLink(scope, element, attr, ctrls) {
    //$mdTheming(element);
    var ngModelCtrl = ctrls[0] || {
      // Mock ngModelController if it doesn't exist to give us
      // the minimum functionality needed
      $setViewValue: function(val) {
        this.$viewValue = val;
        this.$viewChangeListeners.forEach(function(cb) { cb(); });
      },
      $parsers: [],
      $formatters: [],
      $viewChangeListeners: []
    };

    var sliderCtrl = ctrls[1];
    sliderCtrl.init(ngModelCtrl);
  }
}

/**
 * We use a controller for all the logic so that we can expose a few
 * things to unit tests
 */
function SliderController($scope, $element, $attrs, $$rAF, $window, $mdAria, $mdUtil, $mdConstant) {

  this.init = function init(ngModelCtrl) {
    var thumb = angular.element($element[0].querySelector('.md-thumb'));
    var thumbText = angular.element($element[0].querySelector('.md-thumb-text'));
    var thumbContainer = thumb.parent();
    var trackContainer = angular.element($element[0].querySelector('.md-track-container'));
    var activeTrack = angular.element($element[0].querySelector('.md-track-fill'));
    var tickContainer = angular.element($element[0].querySelector('.md-track-ticks'));
    var throttledRefreshDimensions = $mdUtil.throttle(refreshSliderDimensions, 5000);

    // Default values, overridable by $attrss
    $attrs.min ? $attrs.$observe('min', updateMin) : updateMin(0);
    $attrs.max ? $attrs.$observe('max', updateMax) : updateMax(100);
    $attrs.step ? $attrs.$observe('step', updateStep) : updateStep(1);

    // We have to manually stop the $watch on ngDisabled because it exists
    // on the parent $scope, and won't be automatically destroyed when
    // the component is destroyed.
    var stopDisabledWatch = angular.noop;
    if ($attrs.ngDisabled) {
      stopDisabledWatch = $scope.$parent.$watch($attrs.ngDisabled, updateAriaDisabled);
    } else {
      updateAriaDisabled(!!$attrs.disabled);
    }

    $mdAria.expect($element, 'aria-label');

    $element.attr('tabIndex', 0);
    $element.attr('role', 'slider');
    $element.on('keydown', keydownListener);

    var hammertime = new Hammer($element[0], {
      recognizers: [
        [Hammer.Pan, { direction: Hammer.DIRECTION_HORIZONTAL }]
      ]
    });
    hammertime.on('hammer.input', onInput);
    hammertime.on('panstart', onPanStart);
    hammertime.on('pan', onPan);
    hammertime.on('panend', onPanEnd);

    // On resize, recalculate the slider's dimensions and re-render
    function updateAll() {
      refreshSliderDimensions();
      ngModelRender();
      redrawTicks();
    }
    setTimeout(updateAll);

    var debouncedUpdateAll = $$rAF.debounce(updateAll);
    angular.element($window).on('resize', debouncedUpdateAll);

    $scope.$on('$destroy', function() {
      angular.element($window).off('resize', debouncedUpdateAll);
      hammertime.destroy();
      stopDisabledWatch();
    });

    ngModelCtrl.$render = ngModelRender;
    ngModelCtrl.$viewChangeListeners.push(ngModelRender);
    ngModelCtrl.$formatters.push(minMaxValidator);
    ngModelCtrl.$formatters.push(stepValidator);

    /**
     * Attributes
     */
    var min;
    var max;
    var step;
    function updateMin(value) {
      min = parseFloat(value);
      $element.attr('aria-valuemin', value);
    }
    function updateMax(value) {
      max = parseFloat(value);
      $element.attr('aria-valuemax', value);
    }
    function updateStep(value) {
      step = parseFloat(value);
      redrawTicks();
    }
    function updateAriaDisabled(isDisabled) {
      $element.attr('aria-disabled', !!isDisabled);
    }

    // Draw the ticks with canvas.
    // The alternative to drawing ticks with canvas is to draw one $element for each tick,
    // which could quickly become a performance bottleneck.
    var tickCanvas, tickCtx;
    function redrawTicks() {
      if (!angular.isDefined($attrs.mdDiscrete)) return;

      var numSteps = Math.floor( (max - min) / step );
      if (!tickCanvas) {
        tickCanvas = angular.element('<canvas style="position:absolute;">');
        tickCtx = tickCanvas[0].getContext('2d');
        tickCtx.fillStyle = 'black';
        tickContainer.append(tickCanvas);
      }
      var dimensions = getSliderDimensions();
      tickCanvas[0].width = dimensions.width;
      tickCanvas[0].height = dimensions.height;

      var distance;
      for (var i = 0; i <= numSteps; i++) {
        distance = Math.floor(dimensions.width * (i / numSteps));
        tickCtx.fillRect(distance - 1, 0, 2, dimensions.height);
      }
    }


    /**
     * Refreshing Dimensions
     */
    var sliderDimensions = {};
    refreshSliderDimensions();
    function refreshSliderDimensions() {
      sliderDimensions = trackContainer[0].getBoundingClientRect();
    }
    function getSliderDimensions() {
      throttledRefreshDimensions();
      return sliderDimensions;
    }

    /**
     * left/right arrow listener
     */
    function keydownListener(ev) {
      if($element[0].hasAttribute('disabled')) {
        return;
      }

      var changeAmount;
      if (ev.keyCode === $mdConstant.KEY_CODE.LEFT_ARROW) {
        changeAmount = -step;
      } else if (ev.keyCode === $mdConstant.KEY_CODE.RIGHT_ARROW) {
        changeAmount = step;
      }
      if (changeAmount) {
        if (ev.metaKey || ev.ctrlKey || ev.altKey) {
          changeAmount *= 4;
        }
        ev.preventDefault();
        ev.stopPropagation();
        $scope.$evalAsync(function() {
          setModelValue(ngModelCtrl.$viewValue + changeAmount);
        });
      }
    }

    /**
     * ngModel setters and validators
     */
    function setModelValue(value) {
      ngModelCtrl.$setViewValue( minMaxValidator(stepValidator(value)) );
    }
    function ngModelRender() {

      if (isNaN(ngModelCtrl.$viewValue)) {
        ngModelCtrl.$viewValue = ngModelCtrl.$modelValue;
      }

      var percent = (ngModelCtrl.$viewValue - min) / (max - min);
      $scope.modelValue = ngModelCtrl.$viewValue;
      $element.attr('aria-valuenow', ngModelCtrl.$viewValue);
      setSliderPercent(percent);
    }

    function minMaxValidator(value) {
      if (angular.isNumber(value)) {
        return Math.max(min, Math.min(max, value));
      }
    }
    function stepValidator(value) {
      if (angular.isNumber(value)) {
        return Math.round(value / step) * step;
      }
    }

    /**
     * @param percent 0-1
     */
    function setSliderPercent(percent) {
      activeTrack.css('width', (percent * 100) + '%');
      thumbContainer.css(
        $mdConstant.CSS.TRANSFORM,
        'translate3d(' + getSliderDimensions().width * percent + 'px,0,0)'
      );
      $element.toggleClass('md-min', percent === 0);
    }


    /**
     * Slide listeners
     */
    var isSliding = false;
    var isDiscrete = angular.isDefined($attrs.mdDiscrete);

    function onInput(ev) {
      if (!isSliding && ev.eventType === Hammer.INPUT_START &&
          !$element[0].hasAttribute('disabled')) {

        isSliding = true;

        $element.addClass('active');
        $element[0].focus();
        refreshSliderDimensions();

        onPan(ev);

        ev.srcEvent.stopPropagation();

      } else if (isSliding && ev.eventType === Hammer.INPUT_END) {

        if ( isSliding && isDiscrete ) onPanEnd(ev);
        isSliding = false;

        $element.removeClass('panning active');
      }
    }
    function onPanStart() {
      if (!isSliding) return;
      $element.addClass('panning');
    }
    function onPan(ev) {
      if (!isSliding) return;

      // While panning discrete, update only the
      // visual positioning but not the model value.

      if ( isDiscrete ) adjustThumbPosition( ev.center.x );
      else              doSlide( ev.center.x );

      ev.preventDefault();
      ev.srcEvent.stopPropagation();
    }

    function onPanEnd(ev) {
      if ( isDiscrete && !$element[0].hasAttribute('disabled') ) {
        // Convert exact to closest discrete value.
        // Slide animate the thumb... and then update the model value.

        var exactVal = percentToValue( positionToPercent( ev.center.x ));
        var closestVal = minMaxValidator( stepValidator(exactVal) );

        setSliderPercent( valueToPercent(closestVal));
        $$rAF(function(){
          setModelValue( closestVal );
        });

        ev.preventDefault();
        ev.srcEvent.stopPropagation();
      }
    }

    /**
     * Expose for testing
     */
    this._onInput = onInput;
    this._onPanStart = onPanStart;
    this._onPan = onPan;

    /**
     * Slide the UI by changing the model value
     * @param x
     */
    function doSlide( x ) {
      $scope.$evalAsync( function() {
        setModelValue( percentToValue( positionToPercent(x) ));
      });
    }

    /**
     * Slide the UI without changing the model (while dragging/panning)
     * @param x
     */
    function adjustThumbPosition( x ) {
      var exactVal = percentToValue( positionToPercent( x ));
      var closestVal = minMaxValidator( stepValidator(exactVal) );
      setSliderPercent( positionToPercent(x) );
      thumbText.text( closestVal );
    }

    /**
     * Convert horizontal position on slider to percentage value of offset from beginning...
     * @param x
     * @returns {number}
     */
    function positionToPercent( x ) {
      return Math.max(0, Math.min(1, (x - sliderDimensions.left) / (sliderDimensions.width)));
    }

    /**
     * Convert percentage offset on slide to equivalent model value
     * @param percent
     * @returns {*}
     */
    function percentToValue( percent ) {
      return (min + percent * (max - min));
    }

    function valueToPercent( val ) {
      return (val - min)/(max - min);
    }

  };
}

//angular.module('ui.slider', [])
DIRECTIVE_MODULE

.value('uiSliderConfig',{})
.directive('uiSlider', ['uiSliderConfig', '$timeout', function(uiSliderConfig, $timeout) {
    uiSliderConfig = uiSliderConfig || {};
    return {
        require: 'ngModel',
        compile: function () {
            return function (scope, elm, attrs, ngModel) {

                function parseNumber(n, decimals) {
                    return (decimals) ? parseFloat(n) : parseInt(n);
                };

                var options = angular.extend(scope.$eval(attrs.uiSlider) || {}, uiSliderConfig);
                // Object holding range values
                var prevRangeValues = {
                    min: null,
                    max: null
                };

                // convenience properties
                var properties = ['min', 'max', 'step'];
                var useDecimals = (!angular.isUndefined(attrs.useDecimals)) ? true : false;

                var init = function() {
                    // When ngModel is assigned an array of values then range is expected to be true.
                    // Warn user and change range to true else an error occurs when trying to drag handle
                    if (angular.isArray(ngModel.$viewValue) && options.range !== true) {
                        console.warn('Change your range option of ui-slider. When assigning ngModel an array of values then the range option should be set to true.');
                        options.range = true;
                    }

                    // Ensure the convenience properties are passed as options if they're defined
                    // This avoids init ordering issues where the slider's initial state (eg handle
                    // position) is calculated using widget defaults
                    // Note the properties take precedence over any duplicates in options
                    angular.forEach(properties, function(property) {
                        if (angular.isDefined(attrs[property])) {
                            options[property] = parseNumber(attrs[property], useDecimals);
                        }
                    });

                    $(elm).slider(options);
                    init = angular.noop;
                };

                // Find out if decimals are to be used for slider
                angular.forEach(properties, function(property) {
                    // support {{}} and watch for updates
                    attrs.$observe(property, function(newVal) {
                        if (!!newVal) {
                            init();
                            $(elm).slider('option', property, parseNumber(newVal, useDecimals));
                            ngModel.$render();
                        }
                    });
                });
                attrs.$observe('disabled', function(newVal) {
                    init();
                    $(elm).slider('option', 'disabled', !!newVal);
                });

                // Watch ui-slider (byVal) for changes and update
                scope.$watch(attrs.uiSlider, function(newVal) {
                    init();
                    if(newVal != undefined) {
                      $(elm).slider('option', newVal);
                    }
                }, true);

                // Late-bind to prevent compiler clobbering
                $timeout(init, 0, true);

                // Update model value from slider
                $(elm).bind('slide', function(event, ui) {
                    ngModel.$setViewValue(ui.values || ui.value);
                    scope.$apply();
                });

                // Update slider from model value
                ngModel.$render = function() {
                    init();
                    var method = options.range === true ? 'values' : 'value';
                    
                    if (!options.range && isNaN(ngModel.$viewValue) && !(ngModel.$viewValue instanceof Array)) {
                        ngModel.$viewValue = 0;
                    }
                    else if (options.range && !angular.isDefined(ngModel.$viewValue)) {
                            ngModel.$viewValue = [0,0];
                    }

                    // Do some sanity check of range values
                    if (options.range === true) {
                        
                        // Check outer bounds for min and max values
                        if (angular.isDefined(options.min) && options.min > ngModel.$viewValue[0]) {
                            ngModel.$viewValue[0] = options.min;
                        }
                        if (angular.isDefined(options.max) && options.max < ngModel.$viewValue[1]) {
                            ngModel.$viewValue[1] = options.max;
                        }

                        // Check min and max range values
                        if (ngModel.$viewValue[0] > ngModel.$viewValue[1]) {
                            // Min value should be less to equal to max value
                            if (prevRangeValues.min >= ngModel.$viewValue[1])
                                ngModel.$viewValue[0] = prevRangeValues.min;
                            // Max value should be less to equal to min value
                            if (prevRangeValues.max <= ngModel.$viewValue[0])
                                ngModel.$viewValue[1] = prevRangeValues.max;
                        }

                        // Store values for later user
                        prevRangeValues.min = ngModel.$viewValue[0];
                        prevRangeValues.max = ngModel.$viewValue[1];

                    }
                    $(elm).slider(method, ngModel.$viewValue);
                };

                scope.$watch(attrs.ngModel, function() {
                    if (options.range === true) {
                        ngModel.$render();
                    }
                }, true);

                function destroy() {
                    $(elm).slider('destroy');
                }
                elm.bind('$destroy', destroy);
            };
        }
    };
}]);