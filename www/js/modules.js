'use strict';

angular.module('mike360.phonegap.filesystem',
  ['btford.phonegap.ready'])
  .factory('filesystem', function ($rootScope, $window, phonegapReady) {
    return {
      requestFileSystem: phonegapReady(function(onSuccess, onError){
        var requestFileSystem = $window.requestFileSystem || $window.webkitRequestFileSystem;
        //$window.requestFileSystem(
        requestFileSystem(
          LocalFileSystem.PERSISTENT, 0,
          function () {
            var that = this,
              args = arguments;

            if (onSuccess) {
              $rootScope.$apply(function() {
                onSuccess.apply(that, args);
              });
            }
          },
          function () {
            var that = this,
              args = arguments;

            if (onError) {
              $rootScope.$apply(function () {
                onError.apply(that, args);
              });
            }
          }
        );
      })
    };
  });
/*
 * angular-phonegap-ready v0.0.1
 * (c) 2013 Brian Ford http://briantford.com
 * License: MIT
 */

'use strict';

angular.module('btford.phonegap.ready', []).
  factory('phonegapReady', function ($rootScope) {
    return function (fn) {
      var queue = [];

      var impl = function () {
        queue.push(Array.prototype.slice.call(arguments));
      };

      document.addEventListener('deviceready', function () {
        queue.forEach(function (args) {
          fn.apply(this, args);
        });
        impl = fn;
      }, false);
      
      return function () {
        return impl.apply(this, arguments);
      };
    };
  });
/*
var lib1,lib2,lib3, images, createjs;

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes
    var rect; // used to reference frame bounds

    // stage content:
    (lib.film_deep_breath02 = function(mode, startPosition, loop) {
        if (loop == null) {
            loop = false;
        }
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.player = playSound("Andas_djupt_ovning_med_intro");
        }

        this.pause = function() {
            console.log('expect the animation and sound to get paused here');
            this.stop();
            this.player.pause();
        }

        this.resume = function() {
            this.play();
            this.player.resume();
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(839));

        // Andas Animation 
        this.CIRKEL = new lib.Symbol46();
        this.CIRKEL.setTransform(240, 240);
        this.CIRKEL.alpha = 0;
        this.CIRKEL._off = true;

        this.timeline.addTween(cjs.Tween.get(this.CIRKEL).wait(6).to({
            _off: false
        }, 0).to({
            alpha: 1
        }, 57).wait(43).to({
            scaleX: 1.62,
            scaleY: 1.62
        }, 12).wait(6).to({
            scaleX: 0.71,
            scaleY: 0.71
        }, 89).wait(17).to({
            scaleX: 1.62,
            scaleY: 1.62
        }, 44).wait(69).to({
            scaleX: 0.69,
            scaleY: 0.69
        }, 53).to({
            scaleX: 0.49,
            scaleY: 0.49
        }, 14).wait(45).to({
            scaleX: 1.62,
            scaleY: 1.62
        }, 82).wait(42).to({
            scaleX: 0.63,
            scaleY: 0.63
        }, 46).to({
            scaleX: 0.44,
            scaleY: 0.44
        }, 17).wait(44).to({
            scaleX: 1.62,
            scaleY: 1.62
        }, 49).wait(28).to({
            scaleX: 0.72,
            scaleY: 0.72
        }, 53).to({
            scaleX: 0.53,
            scaleY: 0.53
        }, 9).wait(15));

        // Andas djupt bakgrund
        this.instance = new lib.ID_bakgrund_blue();

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.instance
            }]
        }).wait(840));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
    p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


    // symbols:
    (lib.ID_bakgrund_blue = function() {
        this.initialize(img.ID_bakgrund_blue);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.ID_bakgrund_green = function() {
        this.initialize(img.ID_bakgrund_green);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.ID_bakgrund_orange = function() {
        this.initialize(img.ID_bakgrund_orange);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
        this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


    (lib.under_Background = function() {
        this.initialize(img.under_Background);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.Symbol46 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#FFFFFF").ss(32, 1, 1).p("ALKrJQEoEoAAGhQAAGikoEoQkoEomiAAQmhAAkokoQkokoAAmiQAAmhEokoQEokoGhAAQGiAAEoEog");

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = rect = new cjs.Rectangle(-100.9, -100.9, 202, 202);
    p.frameBounds = [rect];

})(lib1 = lib1 || {}, images = images || {}, createjs = createjs || {});


(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes
    var rect; // used to reference frame bounds

    // stage content:
    (lib.film_nice_thought = function(mode, startPosition, loop) {
        if (loop == null) {
            loop = false;
        }
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            // playSound("Skona_tanken_ovning_med_intro");
            this.player = playSound("Skona_tanken_ovning_med_intro");

        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(799));

        // Sköna tanken Lager 25
        this.instance = new lib.Symbol45();
        this.instance.setTransform(240, 240);

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(750).to({
            alpha: 0
        }, 49).wait(1));

        // Sköna tanken Lager 24
        this.instance_1 = new lib.ID_bakgrund_green();

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.instance_1
            }]
        }).wait(800));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
    p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


    // symbols:
    (lib.ID_bakgrund_blue = function() {
        this.initialize(img.ID_bakgrund_blue);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.ID_bakgrund_green = function() {
        this.initialize(img.ID_bakgrund_green);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.ID_bakgrund_orange = function() {
        this.initialize(img.ID_bakgrund_orange);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
        this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


    (lib.under_Background = function() {
        this.initialize(img.under_Background);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.Symbol43 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AlZFaQiQiQABjKQgBjJCQiQQCQiQDJABQDKgBCQCQQCQCQgBDJQABDKiQCQQiQCQjKgBQjJABiQiQg");

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
    p.frameBounds = [rect];


    (lib.Symbol44 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Lager 1
        this.instance = new lib.Symbol43("synched", 0);
        this.instance.alpha = 0;

        this.timeline.addTween(cjs.Tween.get(this.instance).to({
            alpha: 0.602
        }, 49).wait(115).to({
            startPosition: 0
        }, 0).to({
            alpha: 0
        }, 75).wait(1));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
    p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


    (lib.Symbol45 = function(mode, startPosition, loop) {
        this.initialize(mode, startPosition, loop, {});

        // Lager 22
        this.instance = new lib.Symbol44();
        this.instance.setTransform(148, -38.9);
        this.instance._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(589).to({
            _off: false
        }, 0).wait(211));

        // Lager 21
        this.instance_1 = new lib.Symbol44();
        this.instance_1.setTransform(47, 121, 1.184, 1.184);
        this.instance_1._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(570).to({
            _off: false
        }, 0).wait(230));

        // Lager 20
        this.instance_2 = new lib.Symbol44();
        this.instance_2.setTransform(-61.9, 129, 0.571, 0.571);
        this.instance_2._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(546).to({
            _off: false
        }, 0).wait(254));

        // Lager 19
        this.instance_3 = new lib.Symbol44();
        this.instance_3.setTransform(148, 63);
        this.instance_3._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(519).to({
            _off: false
        }, 0).wait(281));

        // Lager 18
        this.instance_4 = new lib.Symbol44();
        this.instance_4.setTransform(119, -113.9);
        this.instance_4._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(491).to({
            _off: false
        }, 0).wait(309));

        // Lager 17
        this.instance_5 = new lib.Symbol44();
        this.instance_5.setTransform(-143.9, -42.9, 1.306, 1.306);
        this.instance_5._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(471).to({
            _off: false
        }, 0).wait(329));

        // Lager 16
        this.instance_6 = new lib.Symbol44();
        this.instance_6.setTransform(-82.9, -139.9);
        this.instance_6._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(435).to({
            _off: false
        }, 0).wait(365));

        // Lager 15
        this.instance_7 = new lib.Symbol44();
        this.instance_7.setTransform(124, 0);
        this.instance_7._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(406).to({
            _off: false
        }, 0).wait(394));

        // Lager 14
        this.instance_8 = new lib.Symbol44();
        this.instance_8.setTransform(-161.9, 75, 0.755, 0.755);
        this.instance_8._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(376).to({
            _off: false
        }, 0).wait(424));

        // Lager 13
        this.instance_9 = new lib.Symbol44();
        this.instance_9.setTransform(-137.9, -97.9, 0.694, 0.694);
        this.instance_9._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(344).to({
            _off: false
        }, 0).wait(456));

        // Lager 12
        this.instance_10 = new lib.Symbol44();
        this.instance_10.setTransform(-6.9, -9.9, 1.673, 1.673);
        this.instance_10._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(319).to({
            _off: false
        }, 0).wait(481));

        // Lager 11
        this.instance_11 = new lib.Symbol44();
        this.instance_11.setTransform(-118.9, 79, 0.612, 0.612);
        this.instance_11._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(293).to({
            _off: false
        }, 0).wait(507));

        // Lager 10
        this.instance_12 = new lib.Symbol44();
        this.instance_12.setTransform(-13.9, -130.9, 1.347, 1.347);
        this.instance_12._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(261).to({
            _off: false
        }, 0).wait(539));

        // Lager 9
        this.instance_13 = new lib.Symbol44();
        this.instance_13.setTransform(129, -48.9, 0.796, 0.796);
        this.instance_13._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(224).to({
            _off: false
        }, 0).wait(576));

        // Lager 8
        this.instance_14 = new lib.Symbol44();
        this.instance_14.setTransform(-103.9, 10, 1.429, 1.429);
        this.instance_14._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(194).to({
            _off: false
        }, 0).wait(606));

        // Lager 7
        this.instance_15 = new lib.Symbol44();
        this.instance_15.setTransform(-13.9, 110);
        this.instance_15._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(162).to({
            _off: false
        }, 0).wait(638));

        // Lager 6
        this.instance_16 = new lib.Symbol44();
        this.instance_16.setTransform(68, -101.9, 1.245, 1.245);
        this.instance_16._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(133).to({
            _off: false
        }, 0).wait(667));

        // Lager 5
        this.instance_17 = new lib.Symbol44();
        this.instance_17.setTransform(68, 0.1, 1.408, 1.408, 0, 0, 0, -7.8, -44.7);
        this.instance_17._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(109).to({
            _off: false
        }, 0).wait(691));

        // Lager 4
        this.instance_18 = new lib.Symbol44();
        this.instance_18.setTransform(-69.9, 74, 0.735, 0.735);
        this.instance_18._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(79).to({
            _off: false
        }, 0).wait(721));

        // Lager 3
        this.instance_19 = new lib.Symbol44();
        this.instance_19.setTransform(72, -26.9, 0.755, 0.755);
        this.instance_19._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(57).to({
            _off: false
        }, 0).wait(743));

        // Lager 2
        this.instance_20 = new lib.Symbol44();
        this.instance_20.setTransform(-82.9, -64.9, 1.531, 1.531);
        this.instance_20._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(31).to({
            _off: false
        }, 0).wait(769));

        // Lager 1
        this.instance_21 = new lib.Symbol44();
        this.instance_21._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(10).to({
            _off: false
        }, 0).wait(790));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = rect = new cjs.Rectangle(0, 0, 0, 0);
    p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-48.9, -48.9, 98, 98), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 207, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 250), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 306, 272), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 295), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 322, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 342, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -196.9, 342, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 367, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 372, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 381, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 376), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];

})(lib2 = lib2 || {}, images = images || {}, createjs = createjs || {});

(function(lib, img, cjs) {

    var p; // shortcut to reference prototypes

    // stage content:
    (lib.film_countdown02 = function(mode, startPosition, loop) {
        if (loop == null) {
            loop = false;
        }
        this.initialize(mode, startPosition, loop, {});

        // timeline functions:
        this.frame_0 = function() {
            this.player = playSound("Rakna_baklanges_ovning_med_intro");
        }

        // actions tween:
        this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(810));

        // 99
        this.instance = new lib.Symbol42nr5();
        this.instance.setTransform(242.5, 235.4);
        this.instance.alpha = 0.23;
        this.instance._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance).wait(317).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 6).wait(469));

        // 98
        this.instance_1 = new lib.Symbol42nr6();
        this.instance_1.setTransform(242.5, 235.4);
        this.instance_1.alpha = 0.23;
        this.instance_1._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(293).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 5).wait(494));

        // 97
        this.instance_2 = new lib.Symbol42nr7();
        this.instance_2.setTransform(242.5, 235.4);
        this.instance_2.alpha = 0.23;
        this.instance_2._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(269).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 5).wait(518));

        // 96
        this.instance_3 = new lib.Symbol42nr8();
        this.instance_3.setTransform(242.5, 235.4);
        this.instance_3.alpha = 0.23;
        this.instance_3._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(245).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 5).wait(542));

        // 95
        this.instance_4 = new lib.Symbolnr9();
        this.instance_4.setTransform(240, 235.4);
        this.instance_4.alpha = 0.23;
        this.instance_4._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(221).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 5).wait(566));

        // 94
        this.instance_5 = new lib.Symbol42nr4();
        this.instance_5.setTransform(242.5, 235.4);
        this.instance_5.alpha = 0.23;
        this.instance_5._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(342).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 5).wait(445));

        // 93
        this.instance_6 = new lib.Symbol42nr3();
        this.instance_6.setTransform(242.5, 235.4);
        this.instance_6.alpha = 0.23;
        this.instance_6._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(366).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(423));

        // 92
        this.instance_7 = new lib.Symbol42nr2();
        this.instance_7.setTransform(242.5, 235.4);
        this.instance_7.alpha = 0.23;
        this.instance_7._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(388).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(401));

        // 91
        this.instance_8 = new lib.Symbol42nr1();
        this.instance_8.setTransform(242.5, 235.4);
        this.instance_8.alpha = 0.23;
        this.instance_8._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(410).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 58).wait(324));

        // 90
        this.instance_9 = new lib.Symbol41();
        this.instance_9.setTransform(242.5, 235.4);
        this.instance_9.alpha = 0;
        this.instance_9._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(32).to({
            _off: false
        }, 0).to({
            alpha: 1
        }, 45).wait(124).to({
            scaleX: 1.19,
            scaleY: 1.19
        }, 5).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(590));

        // 89
        this.instance_10 = new lib.Symbol42nr1();
        this.instance_10.setTransform(242.5, 235.4);
        this.instance_10.alpha = 0.23;
        this.instance_10._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(715).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 1).wait(76));

        // 88
        this.instance_11 = new lib.Symbol42nr2();
        this.instance_11.setTransform(242.5, 235.4);
        this.instance_11.alpha = 0.23;
        this.instance_11._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(693).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(96));

        // 87
        this.instance_12 = new lib.Symbol42nr3();
        this.instance_12.setTransform(242.5, 235.4);
        this.instance_12.alpha = 0.23;
        this.instance_12._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(671).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(118));

        // 86
        this.instance_13 = new lib.Symbol42nr4();
        this.instance_13.setTransform(242.5, 235.4);
        this.instance_13.alpha = 0.23;
        this.instance_13._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(649).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(140));

        // 85
        this.instance_14 = new lib.Symbol42nr5();
        this.instance_14.setTransform(242.5, 235.4);
        this.instance_14.alpha = 0.23;
        this.instance_14._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(627).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(162));

        // 84
        this.instance_15 = new lib.Symbol41();
        this.instance_15.setTransform(242.5, 235.4, 0.757, 0.757);
        this.instance_15.alpha = 0;
        this.instance_15._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(488).to({
            _off: false
        }, 0).to({
            scaleX: 1.09,
            scaleY: 1.09,
            alpha: 1
        }, 28).to({
            scaleX: 1.19,
            scaleY: 1.19
        }, 8).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(272));

        // 83
        this.instance_16 = new lib.Symbol42nr9();
        this.instance_16.setTransform(242.5, 235.4);
        this.instance_16.alpha = 0.23;
        this.instance_16._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(539).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(250));

        // 82
        this.instance_17 = new lib.Symbol42nr8();
        this.instance_17.setTransform(242.5, 235.4);
        this.instance_17.alpha = 0.23;
        this.instance_17._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(561).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(228));

        // 81
        this.instance_18 = new lib.Symbol42nr7();
        this.instance_18.setTransform(242.5, 235.4);
        this.instance_18.alpha = 0.23;
        this.instance_18._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(583).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(206));

        // 80
        this.instance_19 = new lib.Symbol42nr6();
        this.instance_19.setTransform(242.5, 235.4);
        this.instance_19.alpha = 0.23;
        this.instance_19._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(605).to({
            _off: false
        }, 0).to({
            scaleX: 1.19,
            scaleY: 1.19,
            alpha: 1
        }, 7).to({
            scaleX: 1.03,
            scaleY: 1.03,
            alpha: 0.75
        }, 5).to({
            scaleX: 0.81,
            scaleY: 0.81,
            alpha: 0
        }, 7).to({
            _off: true
        }, 3).wait(184));

        // Lager 23
        this.instance_20 = new lib.Symbol45("synched", 0);
        this.instance_20.setTransform(240, 240);
        this.instance_20.alpha = 0;
        this.instance_20._off = true;

        this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(6).to({
            startPosition: 0,
            _off: false
        }, 0).to({
            alpha: 1
        }, 57).wait(748));

        // Bakgrund
        this.instance_21 = new lib.ID_bakgrund_orange();

        this.timeline.addTween(cjs.Tween.get({}).to({
            state: [{
                t: this.instance_21
            }]
        }).wait(811));

    }).prototype = p = new cjs.MovieClip();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    // symbols:
    (lib.ID_bakgrund_orange = function() {
        this.initialize(img.ID_bakgrund_orange);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
        this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


    (lib.under_Background = function() {
        this.initialize(img.under_Background);
    }).prototype = p = new cjs.Bitmap();
    p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


    (lib.Symbol45 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f().s("#FFFFFF").ss(20, 0, 0, 4).p("ARIwZMgiPAAAMAAAAgzMAiPAAAg");

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-109.5, -104.9, 219.2, 210);


    (lib.Symbol42nr9 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AjpHIQhPhGgLiHICvgRQAeCCBXAAQCGAAATkiQhRBAhrAAQhxAAhRhVQhQhUAAiUQAAiVBWhiQBXhhCaAAQCpAABdB5QBcB5AAEZQAAIMl1AAQh6AAhPhEgAhnk7QgfAtAABOQAABIAgAwQAgAwBBAAQA6AABFguIAAgLQAAh2glhRQglhRhLAAQgvAAgdAug");
        this.shape.setTransform(-0.1, 2.8);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


    (lib.Symbol42kopia = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AgDGGQhaiIAAj+QAAj4BYiJQBViKCvAAQCvAABXCJQBZCIgBD6QABD/hbCHQhaCGiqAAQiqAAhYiGgACkkJQgbBdAACsQABCxAbBbQAbBbA+AAQBOAAAUh9QAVh8AAhuQAAiwgbhbQgbhbhAAAQg/AAgcBdgApUH/IAAijICNAAIAAqyIiVAAIAAikIF1AAIAANWICFAAIAACjg");
        this.shape.setTransform(4.9, 2.8);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-55.7, -49.6, 121.2, 105);


    (lib.Symbol38 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AkKG3QhZhVAAiCQAAizC1hCQiWhVAAiWQAAh3BXhKQBYhKCVAAQCdAABVBJQBUBKAAB0QAACZiTBWQCxBBAAC5QAACHhbBQQhbBQivAAQiwAAhZhVgAhgBcQgnAsAABJQAABIAmArQAnApA7AAQA9AAAngpQAngrAAhIQAAhHgmgtQgngtg+AAQg6AAgnAsgAhUlKQgiAjAABFQAAA5AgAmQAhAmA1AAQA2AAAhgmQAhglAAg9QAAg8gfgmQgggmg5AAQgyAAgiAjg");
        this.shape.setTransform(0.1, 2.8);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-35.5, -49.6, 71.4, 105);


    (lib.Symbol37 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AkVH9IAAikICBAAQAGiUBOjOQBLjQBfiEIkEAAIAACPIijAAIAAkuIJ7AAIAAB9QhNBchPDmQhPDkAACyICBAAIAACkg");
        this.shape.setTransform(0.6, 3.1);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-31.2, -47.8, 63.7, 101.9);


    (lib.Symbol36 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("Aj4GRQhbh9ABkKQgBkaBnh9QBmh+CuAAQD+AAAgEPIivAQQgeiChZAAQhFAAgnBSQgoBQgCCDQBHhDBrAAQB2AABQBRQBRBQAACaQAACshVBZQhWBYiNAAQi4AAhbh7gAh2BFQADCpAmA+QAjA+BBAAQAxAAAegsQAegsAAhWQABhRgkgpQglgpg0AAQg/AAg/Asg");
        this.shape.setTransform(0.6, 2.8);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-33.3, -49.6, 68, 105);


    (lib.Symbol35 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AjzG2QhVhNgTiEIDBgXQATCSB0AAQBAABAjgtQAjgtAAhRQABi1iDAAQhKAAgwBQIikhRIAeoCII6AAIAADvIiUAAIgPhMIkEAAIgND0QBQg5BbAAQCWAABSBhQBSBfAACLQAACUhYBlQhYBjixAAQiZAAhVhNg");
        this.shape.setTransform(-1.2, 3.7);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-36, -47.8, 69.7, 103.2);


    (lib.Symbol34 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("Ag0IGIAAiWIBvAAIAAiDIl0AAQgahOgcg0IG0pwIDIAAIAAJnIBjAAIAACLIhjAAIAACDIBWAAIAACWgAjABiID7AAIAAlhg");
        this.shape.setTransform(-1.5, 2.2);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-38.4, -49.6, 73.8, 103.7);


    (lib.Symbol33 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AlbDIIDEgQQAECzCDAAQA5AAAmgrQAlgrAAhDQAAhBgkgrQglgshHAAIguABIAAipQBWAAAngnQAngnAAg5QAAgygdgfQgfggguAAQhtAAAACGIi+gOQAFiLBXhJQBXhJCFAAQCUAABUBIQBSBHABB9QgBCiiXBFQC8A8AAC2QAACQhhBRQhiBRiVAAQlGAAgZlEg");
        this.shape.setTransform(-1.2, 2.8);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-36, -49.6, 69.7, 105);


    (lib.Symbol32 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AlJIGQAAiGAWhJQAVhJAyhFQAxhFBZhSQByhpAmgxQAkgzAAg/QAAgvgcgeQgcgfgnAAQg9AAgdAzQgdAygFBxIi7gJQAAiTAshLQAthKBLgiQBMgiBWAAQCSAABTBRQBTBRAAB4QAABAgZA3QgaA2glAmQgoAihhBGQiQBogmA+QglA+gHAuIEoAAIAAiEICjAAIgJEog");
        this.shape.setTransform(-1.1, 2.2);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 66.1, 103.7);


    (lib.Symbol31 = function() {
        this.initialize();

        // Lager 1
        this.shape = new cjs.Shape();
        this.shape.graphics.f("#FFFFFF").s().p("AjzH9IAAikICNAAIAAqyIiWAAIAAijIF0AAIAANVICFAAIAACkg");
        this.shape.setTransform(0.1, 3.1);

        this.addChild(this.shape);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-25.2, -47.8, 50.7, 101.9);


    (lib.Symbolnr9 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol42nr9();

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


    (lib.Symbol42nr8 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol38("synched", 0);
        this.instance.setTransform(-1.9, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-37.5, -49.6, 71.4, 105);


    (lib.Symbol42nr7 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol37("synched", 0);
        this.instance.setTransform(0.5, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-30.7, -47.8, 63.7, 101.9);


    (lib.Symbol42nr6 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol36("synched", 0);
        this.instance.setTransform(-1.9, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-35.3, -49.6, 68, 105);


    (lib.Symbol42nr5 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol35("synched", 0);
        this.instance.setTransform(-1.9, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-38, -47.8, 69.7, 103.2);


    (lib.Symbol42nr4 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol34("synched", 0);
        this.instance.setTransform(-3.9, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-42.4, -49.6, 73.8, 103.7);


    (lib.Symbol42nr3 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol33("synched", 0);
        this.instance.setTransform(-1.9, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-38, -49.6, 69.7, 105);


    (lib.Symbol42nr2 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol32("synched", 0);
        this.instance.setTransform(-1.9, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-36.2, -49.6, 66.1, 103.7);


    (lib.Symbol42nr1 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol31("synched", 0);
        this.instance.setTransform(-1.9, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-27.2, -47.8, 50.7, 101.9);


    (lib.Symbol41 = function() {
        this.initialize();

        // Lager 1
        this.instance = new lib.Symbol42kopia();
        this.instance.setTransform(-4.2, 0);

        this.addChild(this.instance);
    }).prototype = p = new cjs.Container();
    p.nominalBounds = new cjs.Rectangle(-60, -49.6, 121.2, 105);

})(lib3 = lib3 || {}, images = images || {}, createjs = createjs || {});


*/
/**
 * Module to do sessionTimeout after certain time of inactivity
 *
 */
MODULES_MODULE

    .run(['$rootScope','AuthenticationService','$state',
            function($rootScope,AuthenticationService,$state) {
            	
            	var timeoutDuration = 30*60*1000;//30 minutes
           
            	
            	//var timeoutDuration = 6*1000;//6 seconds
            	var inactivityTimer = null;


            	var inactivityExecution = function() {
            		//console.error('inactive executed!');
            		$rootScope.$broadcast('inactivity_timeout');
            		AuthenticationService.logout();
            		$state.transitionTo('signin', {}, { reload: true });
            		clearInterval(inactivityTimer);
            	};

            	var resetTimer = function() {
            		//console.error('timer reset!');
            		clearInterval(inactivityTimer);
            		inactivityTimer = setInterval(function() {
                        inactivityExecution();
                    }, timeoutDuration);
            	};

            	//ionic.EventController.on('touchstart',resetTimer);
            	resetTimer();
            	ionic.EventController.on('touchend',resetTimer);
            	ionic.EventController.on('touchend',resetTimer);
            	ionic.EventController.on('mousemove',resetTimer);
            	ionic.EventController.on('keypress',resetTimer);
            	//ionic.EventController.on('load',resetTimer);
    
            }
      ]);
/*!
 * ngCordova
 * Copyright 2014 Drifty Co. http://drifty.com/
 * See LICENSE in this repository for license information
 */
(function() {

angular.module('ngCordova.plugins', [
    //'ngCordova.plugins.deviceMotion',
    //'ngCordova.plugins.camera',
    //'ngCordova.plugins.geolocation',
    //'ngCordova.plugins.deviceOrientation',
    'ngCordova.plugins.dialogs',
    'ngCordova.plugins.vibration',
    'ngCordova.plugins.network',
    'ngCordova.plugins.device',
    //'ngCordova.plugins.barcodeScanner',
    'ngCordova.plugins.splashscreen',
    //'ngCordova.plugins.keyboard',
    //'ngCordova.plugins.contacts',
    'ngCordova.plugins.statusbar',
    'ngCordova.plugins.file',
    //'ngCordova.plugins.socialSharing',
    //'ngCordova.plugins.globalization',
    //'ngCordova.plugins.sqlite',
    //'ngCordova.plugins.ga',
    //'ngCordova.plugins.push',
    //'ngCordova.plugins.spinnerDialog',
    //'ngCordova.plugins.pinDialog',
    'ngCordova.plugins.localNotification',
    'ngCordova.plugins.toast',
    //'ngCordova.plugins.capture',
    //'ngCordova.plugins.appAvailability',
    //'ngCordova.plugins.prefs'
]);
angular.module('ngCordova', [
    'ngCordova.plugins'
]);



angular.module('ngCordova.plugins.dialogs', [])

.factory('$cordovaDialogs', [

    function() {

        return {
            alert: function(message, callback, title, buttonName) {
                return navigator.notification.alert.apply(navigator.notification, arguments);
            },

            confirm: function(message, callback, title, buttonName) {
                return navigator.notification.confirm.apply(navigator.notification, arguments);
            },

            prompt: function(message, promptCallback, title, buttonLabels, defaultText) {
                return navigator.notification.prompt.apply(navigator.notification, arguments);
            },

            beep: function(times) {
                return navigator.notification.beep(times);
            }
        }
    }
]);
angular.module('ngCordova.plugins.toast', [])

.factory('$cordovaToast', ['$q',
    function($q) {

        return {
            showShortTop: function(message) {
                var q = $q.defer();
                window.plugins.toast.showShortTop(message, function(response) {
                    q.resolve(response);
                }, function(error) {
                    q.reject(error)
                })
                return q.promise;
            },

            showShortCenter: function(message) {
                var q = $q.defer();
                window.plugins.toast.showShortCenter(message, function(response) {
                    q.resolve(response);
                }, function(error) {
                    q.reject(error)
                })
                return q.promise;
            },

            showShortBottom: function(message) {
                var q = $q.defer();
                window.plugins.toast.showShortBottom(message, function(response) {
                    q.resolve(response);
                }, function(error) {
                    q.reject(error)
                })
                return q.promise;
            },

            showLongTop: function(message) {
                var q = $q.defer();
                window.plugins.toast.showLongTop(message, function(response) {
                    q.resolve(response);
                }, function(error) {
                    q.reject(error)
                })
                return q.promise;
            },

            showLongCenter: function(message) {
                var q = $q.defer();
                window.plugins.toast.showLongCenter(message, function(response) {
                    q.resolve(response);
                }, function(error) {
                    q.reject(error)
                })
                return q.promise;
            },

            showLongBottom: function(message) {
                var q = $q.defer();
                window.plugins.toast.showLongBottom(message, function(response) {
                    q.resolve(response);
                }, function(error) {
                    q.reject(error)
                })
                return q.promise;
            },


            show: function(message, duration, position) {
                var q = $q.defer();
                window.plugins.toast.show(message, duration, position, function(response) {
                    q.resolve(response);
                }, function(error) {
                    q.reject(error)
                })
                return q.promise;
            }
        }

    }
]);
angular.module('ngCordova.plugins.vibration', [])

.factory('$cordovaVibration', [

    function() {

        return {
            vibrate: function(times) {
                return navigator.notification.vibrate(times);
            },
            vibrateWithPattern: function(pattern, repeat) {
                return navigator.notification.vibrateWithPattern(pattern, repeat);
            },
            cancelVibration: function() {
                return navigator.notification.cancelVibration();
            }
        }
    }
]);
// TODO: writeFile needs work, doesn't function
// TODO: add support for readFile -> readAsData
// TODO: add support for readFile -> readAsBinaryString
// TODO: add support for readFile -> readAsArrayBuffer
// TODO: add functionality to define storage size in the getFilesystem() -> requestFileSystem() method
// TODO: add documentation for FileError types
// TODO: add abort() option to downloadFile and uploadFile methods.
// TODO: add support for downloadFile and uploadFile options. (or detailed documentation) -> for fileKey, fileName, mimeType, headers
// TODO: add support for onprogress property


angular.module('ngCordova.plugins.file', [])

//Filesystem (checkDir, createDir, checkFile, creatFile, removeFile, writeFile, readFile)
.factory('$cordovaFile', ['$q',
    function($q) {

        return {
            checkDir: function(dir) {
                var q = $q.defer();

                getFilesystem().then(
                    function(filesystem) {
                        filesystem.root.getDirectory(dir, {
                                create: false
                            },
                            //Dir exists
                            function() {
                                q.resolve();
                            },
                            //Dir doesn't exist
                            function() {
                                q.reject();
                            }
                        );
                    }
                );

                return q.promise;
            },

            createDir: function(dir, replaceBOOL) {
                getFilesystem().then(
                    function(filesystem) {
                        filesystem.root.getDirectory(dir, {
                            create: true,
                            exclusive: replaceBOOL
                        });
                    }
                );
            },

            checkFile: function(filePath) {
                var q = $q.defer();

                // Backward compatibility for previous function checkFile(dir, file)
                if (arguments.length == 2) {
                    filePath = '/' + filePath + '/' + arguments[1];
                }

                getFilesystem().then(
                    function(filesystem) {
                        filesystem.root.getFile(filePath, {
                                create: false
                            },
                            // File exists
                            function() {
                                q.resolve();
                            },
                            // File doesn't exist
                            function() {
                                q.reject();
                            }
                        );
                    }
                );

                return q.promise;
            },

            createFile: function(filePath, replaceBOOL) {
                // Backward compatibility for previous function createFile(dir, file, replaceBOOL)
                if (arguments.length == 3) {
                    filePath = '/' + filePath + '/' + arguments[1];
                    replaceBOOL = arguments[2];
                }

                getFilesystem().then(
                    function(filesystem) {
                        filesystem.root.getFile(filePath, {
                                create: true,
                                exclusive: replaceBOOL
                            },
                            function(success) {

                            },
                            function(err) {

                            });
                    }
                );
            },

            removeFile: function(filePath) {
                var q = $q.defer();

                // Backward compatibility for previous function removeFile(dir, file)
                if (arguments.length == 2) {
                    filePath = '/' + filePath + '/' + arguments[1];
                }

                getFilesystem().then(
                    function(filesystem) {
                        filesystem.root.getFile(filePath, {
                            create: false
                        }, function(fileEntry) {
                            fileEntry.remove(function() {
                                q.resolve();
                            });
                        });
                    }
                );

                return q.promise;
            },

            writeFile: function(filePath) {
                var q = $q.defer();

                // Backward compatibility for previous function writeFile(dir, file)
                if (arguments.length == 2) {
                    filePath = '/' + filePath + '/' + arguments[1];
                }

                getFilesystem().then(
                    function(filesystem) {
                        filesystem.root.getFile(filePath, {
                                create: false
                            },
                            function(fileEntry) {
                                fileEntry.createWriter(
                                    function(fileWriter) {
                                        q.resolve(fileWriter);
                                    },
                                    function(error) {
                                        q.reject(error);
                                    });
                            }
                        );
                    }
                );

                return q.promise;
            },

            readFile: function(filePath) {
                var q = $q.defer();

                // Backward compatibility for previous function readFile(dir, file)
                if (arguments.length == 2) {
                    filePath = '/' + filePath + '/' + arguments[1];
                }

                getFilesystem().then(
                    function(filesystem) {

                        filesystem.root.getFile(filePath, {
                                create: false
                            },
                            // success
                            function(fileEntry) {
                                fileEntry.file(function(file) {
                                    var reader = new FileReader();
                                    reader.onloadend = function() {
                                        q.resolve(this.result);
                                    };

                                    reader.readAsText(file);
                                });
                            },
                            // error
                            function(error) {
                                q.reject(error);
                            });
                    }
                );

                return q.promise;
            },

            readFileMetadata: function(filePath) {
                var q = $q.defer();

                getFilesystem().then(
                    function(filesystem) {
                        filesystem.root.getFile(filePath, {
                                create: false
                            },
                            // success
                            function(fileEntry) {
                                fileEntry.file(function(file) {
                                    q.resolve(file);
                                });
                            },
                            // error
                            function(error) {
                                q.reject(error);
                            });
                    }
                );

                return q.promise;
            },

            downloadFile: function(source, filePath, trustAllHosts, options) {
                var q = $q.defer();
                var fileTransfer = new FileTransfer();
                var uri = encodeURI(source);

                fileTransfer.onprogress = function(progressEvent) {
                    q.notify(progressEvent);
                };

                fileTransfer.download(
                    uri,
                    filePath,
                    function(entry) {
                        q.resolve(entry);
                    },
                    function(error) {
                        q.reject(error);
                    },
                    trustAllHosts, options);

                return q.promise;
            },

            uploadFile: function(server, filePath, options) {
                var q = $q.defer();
                var fileTransfer = new FileTransfer();
                var uri = encodeURI(server);

                fileTransfer.onprogress = function(progressEvent) {
                    q.notify(progressEvent);
                };

                fileTransfer.upload(
                    filePath,
                    uri,
                    function(result) {
                        q.resolve(result);
                    },
                    function(error) {
                        q.reject(error);
                    },
                    options)

                return q.promise
            }

        };

        function getFilesystem() {
            var q = $q.defer();
            var fileQuota = 10*1024 * 1024;/*10 MB*/
             // Note: The file system has been prefixed as of Google Chrome 12:
            window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
            
            window.requestFileSystem(LocalFileSystem.PERSISTENT,fileQuota , function(filesystem) {
                    q.resolve(filesystem);
                },
                function(err) {
                    var msg = '';

                      switch (e.code) {
                        case FileError.QUOTA_EXCEEDED_ERR:
                          msg = 'QUOTA_EXCEEDED_ERR';
                          break;
                        case FileError.NOT_FOUND_ERR:
                          msg = 'NOT_FOUND_ERR';
                          break;
                        case FileError.SECURITY_ERR:
                          msg = 'SECURITY_ERR';
                          break;
                        case FileError.INVALID_MODIFICATION_ERR:
                          msg = 'INVALID_MODIFICATION_ERR';
                          break;
                        case FileError.INVALID_STATE_ERR:
                          msg = 'INVALID_STATE_ERR';
                          break;
                        default:
                          msg = 'Unknown Error';
                          break;
                      };

                      console.log('Error: ' + msg);
                      
                    q.reject(msg);
                });

            return q.promise;
        }
    }
]);
angular.module('ngCordova.plugins.network', [])

.factory('$cordovaNetwork', [

    function() {

        return {

            getNetwork: function() {
                return navigator.connection.type;
            },

            isOnline: function() {
                var networkState = navigator.connection.type;
                return networkState !== Connection.UNKNOWN && networkState !== Connection.NONE;
            },

            isOffline: function() {
                var networkState = navigator.connection.type;
                return networkState === Connection.UNKNOWN || networkState === Connection.NONE;
            }
        }
    }
])


// angular.module('ngCordova.plugins.push', [])

// .factory('$cordovaPush', ['$q',
//     function($q) {
//         return {
//             register: function(config) {
//                 var q = $q.defer();
//                 window.plugins.pushNotification.register(
//                     function(result) {
//                         q.resolve(result);
//                     },
//                     function(error) {
//                         q.reject(error);
//                     },
//                     config);

//                 return q.promise;
//             },

//             unregister: function(options) {
//                 var q = $q.defer();
//                 window.plugins.pushNotification.unregister(
//                     function(result) {
//                         q.resolve(result);
//                     },
//                     function(error) {
//                         q.reject(error);
//                     },
//                     options);

//                 return q.promise;
//             },

//             // iOS only
//             setBadgeNumber: function(number) {
//                 var q = $q.defer();
//                 window.plugins.pushNotification.setApplicationIconBadgeNumber(
//                     function(result) {
//                         q.resolve(result);
//                     },
//                     function(error) {
//                         q.reject(error);
//                     },
//                     number);
//                 return q.promise;
//             }
//         };
//     }
// ]);
angular.module('ngCordova.plugins.statusbar', [])

.factory('$cordovaStatusbar', [

    function() {

        return {
            overlaysWebView: function(bool) {
                return StatusBar.overlaysWebView(true);
            },

            // styles: Default, LightContent, BlackTranslucent, BlackOpaque
            style: function(style) {
                switch (style) {
                    case 0: // Default
                        return StatusBar.styleDefault();
                        break;

                    case 1: // LightContent
                        return StatusBar.styleLightContent();
                        break;

                    case 2: // BlackTranslucent
                        return StatusBar.styleBlackTranslucent();
                        break;

                    case 3: // BlackOpaque
                        return StatusBar.styleBlackOpaque();
                        break;

                    default: // Default
                        return StatusBar.styleDefault();
                }
            },


            // supported names: black, darkGray, lightGray, white, gray, red, green, blue, cyan, yellow, magenta, orange, purple, brown
            styleColor: function(color) {
                return StatusBar.backgroundColorByName(color);
            },

            styleHex: function(colorHex) {
                return StatusBar.backgroundColorByHexString(colorHex);
            },

            hide: function() {
                return StatusBar.hide();
            },

            show: function() {
                return StatusBar.show()
            },

            isVisible: function() {
                return StatusBar.isVisible();
            }
        }
    }
]);
angular.module('ngCordova.plugins.device', [])

.factory('$cordovaDevice', [

    function() {

        return {
            getDevice: function() {
                return device;
            },

            getCordova: function() {
                return device.cordova;
            },

            getModel: function() {
                return device.model;
            },

            // Waraning: device.name is deprecated as of version 2.3.0. Use device.model instead.
            getName: function() {
                return device.name;
            },

            getPlatform: function() {
                return device.platform;
            },

            getUUID: function() {
                return device.uuid;
            },

            getVersion: function() {
                return device.version;
            }
        }
    }
]);

angular.module('ngCordova.plugins.splashscreen', [])

.factory('$cordovaSplashscreen', [

function() {

    return {
        hide: function() {
            return navigator.splashscreen.hide();
        },

        show: function() {
            return navigator.splashscreen.show();
        }
    };

}
]);


// install   :  cordova plugin add de.appplant.cordova.plugin.local-notification
// link      :  https://github.com/katzer/cordova-plugin-local-notifications/

angular.module('ngCordova.plugins.localNotification', [])

  .factory('$cordovaLocalNotification', ['$q', '$window', function ($q, $window) {

    return {
      add: function (options, scope) {
        var q = $q.defer();
        $window.plugin.notification.local.add(
          options,
          function (result) {
            q.resolve(result);
          },
          scope);
        return q.promise;
      },

      cancel: function (id, scope) {
        var q = $q.defer();
        $window.plugin.notification.local.cancel(
          id, function (result) {
            q.resolve(result);
          }, scope);

        return q.promise;
      },

      cancelAll: function (scope) {
        var q = $q.defer();

        $window.plugin.notification.local.cancelAll(
          function (result) {
            q.resolve(result);
          }, scope);

        return q.promise;
      },

      isScheduled: function (id, scope) {
        var q = $q.defer();

        $window.plugin.notification.local.isScheduled(
          id,
          function (result) {
            q.resolve(result);
          }, scope);

        return q.promise;
      },

      getScheduledIds: function (scope) {
        var q = $q.defer();

        $window.plugin.notification.local.getScheduledIds(
          function (result) {
            q.resolve(result);
          }, scope);

        return q.promise;
      },

      isTriggered: function (id, scope) {
        var q = $q.defer();

        $window.plugin.notification.local.isTriggered(
          id, function (result) {
            q.resolve(result);
          }, scope);

        return q.promise;
      },

      getTriggeredIds: function (scope) {
        var q = $q.defer();

        $window.plugin.notification.local.getTriggeredIds(
          function (result) {
            q.resolve(result);
          }, scope);

        return q.promise;
      },

      getDefaults: function () {
        return $window.plugin.notification.local.getDefaults();
      },

      setDefaults: function (Object) {
        $window.plugin.notification.local.setDefaults(Object);
      },

      onadd: function () {
        return $window.plugin.notification.local.onadd;
      },

      ontrigger: function () {
        return $window.plugin.notification.local.ontrigger;
      },

      onclick: function () {
        return $window.plugin.notification.local.onclick;
      },

      oncancel: function () {
        return $window.plugin.notification.local.oncancel;
      }
    };
  }]);

})();
var ReminderClass = function(entry) {
    //the constructor function

    if (entry.id) this.id = "reminder_"+String(entry.id);
    //if (entry.Code) this.code = entry.Code;
    //
    if (entry.OverallTextName)  this.title = he.decode((entry.OverallTextName||'').replace(/(<([^>]+)>)/ig,"") );
    if (entry.OverallTextDescription)    this.message = he.decode((entry.OverallTextDescription||'').replace(/(<([^>]+)>)/ig,"") );

    
    if (entry.days)    this.days = entry.days;

    return this;
};

var FeedbackClass = function(entry) {
    //the constructor function

    if (entry.id) this.id = "feedback_"+String(entry.id);
    //if (entry.Code) this.code = entry.Code;
    if (entry.overallTextName)  this.title = he.decode((entry.overallTextName||'').replace(/(<([^>]+)>)/ig,"") );
    if (entry.overallTextDescription)    this.message = he.decode((entry.overallTextDescription||'').replace(/(<([^>]+)>)/ig,"") );
    if (entry.practiceCount)    this.practiceCount = entry.practiceCount;
    if (entry.type) {
    	switch(entry.type) {
    		case 'intotal':
    		this.type = 'total';
    		break;

    		case 'thisweek':
    		this.type = 'week';
    		break;
    	}
    }    

    return this;
};

MODULES_MODULE
.factory('BUPReminders', ['$rootScope','$cordovaLocalNotification','$moment','BUPInteraction',function ($rootScope, $cordovaLocalNotification, $moment, BUPInteraction) {
	var service  =  {

		getAll: function() {
			var reminders =[];
			var data = false;
			try{

				var data = JSON.parse(window.localStorage['tasks']);

			}catch(e){
				
			}

			if(data && data.reminders && data.reminders.length  ) {
				for (var i=0; i<data.reminders.length; i++) {
					var reminder = new ReminderClass(data.reminders[i]);
                    reminders.push(reminder);
				}
			}

			return reminders;

		},
		prepare: function() {
			if ( ionic.Platform.isWebView()){
				 window.plugin.notification.local.hasPermission(function(granted) {

	                if (granted) {

	                    //$('#NoRightsWarning').hide();       // Hide the warning message
	                    console.log('Push Notification is disabled');

	                } else {
	                	
	                	window.plugin.notification.local.promptForPermission();
	                	console.log('Push Notification is disabled');

	                    // Alas, no permission granted (yet)
	                    /*
	                    if (appData.getUser().getHasBeenAsked4Permission()) {

	                        // The user has been asked for permission already but refused... :-(
	                        $('#NoRightsWarning').show();       // Show the warning message

	                    } else {

	                        window.plugin.notification.local.promptForPermission();
	                        appData.getUser().setHasBeenAsked4Permission();

	                        appData.checkPermissions.call(appData);    // Recursive call to check the answer given by the user

	                    }
	                    */
	                }
	            });

				window.plugin.notification.local.ontrigger = function (id, state, json) {
					console.log(state);
					console.log(json);
					try{
						var message = JSON.parse(json);
						if (message && message.type=='reminder' && message.message) {
							console.log('notification '+id+' has been triggered');
							BUPInteraction.alert({
								title: message.title,
								message: message.message
							});
						}
					}catch(e){
						console.error(e);
					}

					//alert('notification '+id+' has been triggered');


				};


			}
		},
		cancelAllPreviousReminders: function() {
			try{
	        	console.log('push notification');
	        	$cordovaLocalNotification.cancelAll($rootScope);
	        }catch(e) {
	        	console.error(e);
	        }
		},
		reSchedule: function() {
			console.log('rescheduling the notifications');
			//cancel all previously scheduled reminders
			if (ionic.Platform.isWebView()){
				//cancel all previous scheduled reminders and schedule it again
				service.cancelAllPreviousReminders();

				//read the latest reminder settings
				var reminders = service.getAll();


				console.debug(reminders);
				var allReminders = [];
				_.forEach(reminders,function(reminder){
					if (reminder.days && reminder.message) {

						var date = $moment().add(reminder.days,'d').toDate();//seconds
						// s = seconds
						// d = days
						// m = minutes                  
						var data = {
							//user: $rootScope.currentUser,
							id: reminder.id,
							title:reminder.title,
							message:reminder.message,
							type: 'reminder'
						}
						var notificationSetting = {
							id:      reminder.id,
						    title:   reminder.title,
						    message: reminder.message,
						    json:       JSON.stringify(data),
						    date:  date
						}
						allReminders.push(notificationSetting);
						console.debug(notificationSetting);
						window.ars = allReminders;
						try{
							
							$cordovaLocalNotification.add(notificationSetting,$rootScope);

						}catch(e){
							
							console.error(e);

						}

						//return;

						

					}
				});

				window.ars = allReminders;
			}
			

		}


	};
	window.rem = service;
	return service;
}])

.factory('BUPFeedbacks', ['$rootScope','$cordovaLocalNotification','$moment','BUPInteraction','BUPActivities','$timeout',function ($rootScope, $cordovaLocalNotification, $moment, BUPInteraction, BUPActivities, $timeout) {
	var service  =  {

		getAll: function() {
			var feedbacks =[];
			var data = false;
			try{

				var data = JSON.parse(window.localStorage['tasks']);

			}catch(e){
				
			}

			if(data && data.feedbacks && data.feedbacks.length  ) {
				for (var i=0; i<data.feedbacks.length; i++) {
					var feedback = new FeedbackClass(data.feedbacks[i]);
                    feedbacks.push(feedback);
				}
			}

			return feedbacks;

		},
		giveFeedback: function(feedbackObject) {
			console.log('executing feedback message:');
			console.log(feedbackObject);
			if (feedbackObject &&  feedbackObject.message) {
				console.log('notification '+feedbackObject.id+' has been triggered');
				$timeout(function(){
					BUPInteraction.alert({
						title: feedbackObject.title,
						message: feedbackObject.message,
						//autoClose: 3000
					});
					
				},2000);
				
			}
			if (ionic.Platform.isWebView() && feedbackObject.message){
				console.log(feedbackObject);
				var date = $moment().add(1,'s');//seconds
				// s = seconds
				// d = days
				// m = minutes                  
				var data = {
					id: date.valueOf(),
					title:feedbackObject.title,
					message:feedbackObject.message
				}
				var notificationSetting = {
					id:      date.valueOf(),
				    title:   feedbackObject.title,
				    message: feedbackObject.message,
				    autoCancel:false,
				    //date:  date.toDate(),
				    json: JSON.stringify(data)
				};

				window.ns = notificationSetting;
				console.debug(notificationSetting);
				
				try{
					
					$cordovaLocalNotification.add(notificationSetting,$rootScope);

				}catch(e){
					
					console.error(e);

				}

				//return;

					

			
				
			}
			

		},
		checkFeedbackCondition: function(newPracticeActivity) {
			console.log('checking the feedback setting for the user');
			var feedbackSetting = service.getAll();
			window.fs = feedbackSetting;	
			//sort by practiceCount in descending order and group by type
			//var groupedSettings = _(feedbackSetting).sortBy('practiceCount').reverse().groupBy('type').valueOf();
			var groupedSettings = _(feedbackSetting).groupBy('type').valueOf();

			console.debug(groupedSettings);
			console.log('currentPracticeActivity');
			console.log(newPracticeActivity);
			/*
			var groupedSettings = _.groupBy(feedbackSetting,'type');
			*/
			// var reading = {
			// 	totalMatchIndex : -1,
			// 	weekMatchIndex: -1
			// };
			var reading = BUPActivities.getCurrentUserPracticeCount();
			reading.totalMatchIndex = -1;
			reading.weekMatchIndex = -1;4

			
			//type = total
			if (groupedSettings.total && groupedSettings.total.length) {
				//the setting should be in descending order of practiceCount;
				console.log('checking total');
				console.time('getPracticeCountInTotal');
				//reading.total = BUPActivities.getPracticeCountInTotal();
				console.timeEnd('getPracticeCountInTotal');
				
				if (reading.total > 0) {
					reading.totalMatchIndex = _.findIndex(groupedSettings.total,function(feedbackOption) {
						return feedbackOption.practiceCount == reading.total;
					});	
				}

				


				
				//calculate the total number of practices made by the current User in total from the beginning
			}

			if (groupedSettings.week && groupedSettings.week.length) {
				//the setting should be in descending order of practiceCount;
				console.time('getPracticeCountThisWeek');
				//reading.week = BUPActivities.getPracticeCountThisWeek();
				console.timeEnd('getPracticeCountThisWeek');

				//calculate the  number of practices made by the current User in this week
				
				
				if (reading.week > 0) {
					reading.weekMatchIndex = _.findIndex(groupedSettings.week,function(feedbackOption) {
						return feedbackOption.practiceCount == reading.week;
					});	
				}

				
			}
			
			console.debug(reading);

			if (reading.totalMatchIndex > -1) {
				//time to give the feedback!
				console.log('practiceCount '+reading.total+ ' matched to index:'+reading.totalMatchIndex);
				console.log(groupedSettings.total[reading.totalMatchIndex]);
				service.giveFeedback(groupedSettings.total[reading.totalMatchIndex]);
			}
			if (reading.weekMatchIndex > -1) {
				//time to give the feedback!
				console.log('practiceCount '+reading.week+ ' matched to index:'+reading.weekMatchIndex);
				console.log(groupedSettings.week[reading.weekMatchIndex]);
				service.giveFeedback(groupedSettings.week[reading.weekMatchIndex]);
			}

				
			

			//console.log(feedbackSetting);


		} 


	};
	window.rem = service;
	return service;
}])


.run(['$rootScope','BUPUser','BUPConfig','$moment','BUPReminders','BUPFeedbacks',function($rootScope,BUPUser,BUPConfig,$moment, BUPReminders, BUPFeedbacks) {
     
    var giveFeedbackIfRequired = function(event,newEvent) {
    	var isOfTypeHomework = newEvent.hasOwnProperty('homeworkid');
     	if(isOfTypeHomework) return;
     	console.log(newEvent);
     	BUPFeedbacks.checkFeedbackCondition(newEvent);
    };

    $rootScope.$on('newactivity',giveFeedbackIfRequired);


 	var updateAppLastPracticed = _.debounce(function(){
        if(BUPUser.isLoggedIn()){
            $rootScope.currentUserConfig.lastUsed = $moment().valueOf();
            BUPConfig.updateCurrentUserConfig($rootScope.currentUserConfig);
            console.debug($rootScope.currentUserConfig);
            console.debug('last practiced date logged');
            BUPReminders.reSchedule();
        }
	}, 10000);//60000

    //update when a user started using the app as logged in user
    //$rootScope.$on('app.userready',updateAppLastUsed);

    //update when a user has made a practice
    $rootScope.$on('newactivity',updateAppLastPracticed);
    $rootScope.$on('homeworkdoneagain',updateAppLastPracticed);

    BUPReminders.prepare();


}]);


(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['angular'], factory);
  } else {
    // Browser globals
    root.taggedAutogrow = factory(root.angular);
  }
}(this, function (angular) {
  "use strict";

  /*
   * Adapted from: http://code.google.com/p/gaequery/source/browse/trunk/src/static/scripts/jquery.autogrow-textarea.js
   *
   * Works nicely with the following styles:
   * textarea {
   *  resize: none;
   *  transition: 0.05s;
   *  -moz-transition: 0.05s;
   *  -webkit-transition: 0.05s;
   *  -o-transition: 0.05s;
   * }
   *
   * Usage: <textarea ng-model="myModel" tagged-autogrow></textarea>
   */
  var module = angular.module('tagged.directives.autogrow', []);
  module.directive('taggedAutogrow', ['$window', '$document', '$timeout', function($window, $document, $timeout) {
    var $shadow = angular.element('<div></div>').css({
      position: 'absolute',
      top: '-10000px',
      left: '-10000px',
      whiteSpace: 'pre-wrap'
    });
    angular.element($document[0].body).append($shadow);

    return {
      require: 'ngModel',
      link: function(scope, element, attr) {
        $timeout(function() {
          var minHeight = element[0].offsetHeight,
            styles = $window.getComputedStyle(element[0]),
            stylesToDuplicate = [
              'width', 'boxSizing', 'paddingTop', 'paddingRight',
              'paddingBottom', 'paddingLeft', 'borderTopWidth',
              'borderRightWidth', 'borderBottomWidth', 'borderLeftWidth',
              'lineHeight'
            ];

          scope.$watch(attr.ngModel, function() {
            var newStyles = {},
              // additional text to add to the shadow element
              padding = ' oooooo',
              val,
              height;

            angular.forEach(stylesToDuplicate, function(style) {
              newStyles[style] = styles[style];
            });

            $shadow.css(newStyles);
            val = element.val().replace(/\n$/g, "\n.") + padding;
            $shadow.text(val);
            height = Math.max($shadow[0].offsetHeight, minHeight);
            element.css('height', height + 'px');
          });
        });
      }
    };
  }]);

  return module;
}));

MAIN_MODULE
.config(function($translateProvider) {
  $translateProvider

  /************************
   * English translation
   ***********************/
  .translations('en', {
		
	///global translations:
	LOADING_TEXT:'Laddar',


	//^^^^^^^^^[ Login screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
	HEADLINE_LOGIN:'Login',
	LABEL_BUTTON_LOGIN: 'Login',
	PLACEHOLDER_USERNAME:'Username',
	PLACEHOLDER_PASSWORD:'Password',
    INFO_LOGIN_FAILED:'Login failed',
    INFO_LOGIN_SUCCESS:'Login success',
    INFO_USERNAME_MISSING:'Please provide the username',
    INFO_PASSWORD_MISSING:'Please enter your password',
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

    //^^^^^^^^^[ Dashboard screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
    HEADLINE_DASHBOARD: 'Start',
    HEADLINE_ABOUT_APP_MODAL: 'About Hemuppgiften',
    LABEL_CONFIRM_LOGOUT:'',
    LABEL_BUTTON_LOGOUT:'',
    HEADLINE_FEEDBACK: 'there, This is my awesome app!',
    HEADLINE_ABOUT: 'About Hemuppgiften',
    LABEL_DELETE:'',
    LABEL_CANCEL:'',
    LOADING:'Loading',
    INFO_HOMEWORKS_UPDATED:'',
    INFO_PRACTICES_UPDATED:'',
    LABEL_LIST_UPDATED:'',

  })

  /**********************
   * Swedish translation
   ***********************/

  .translations('sv', {
	
  	///global translations:
	LOADING_TEXT:'Laddar',

	
	//^^^^^^^^^[ Login screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
	HEADLINE_LOGIN:'Logga in',
	LABEL_BUTTON_LOGIN: 'Logga in',
	PLACEHOLDER_USERNAME:'Användarnamn',
	PLACEHOLDER_PASSWORD:'Lösenord',
    INFO_LOGIN_FAILED:'Inloggningen misslyckades',
    INFO_LOGIN_SUCCESS:'Lyckad inloggning',
    INFO_USERNAME_MISSING:'Var vänlig ange användarnamn',
    INFO_PASSWORD_MISSING:'Du måste fylla i lösenord',
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    
    //CUSTOM LOGIN SETTING
    HEADLINE_CUSTOM_LOGIN:'Välj inställningar',
    INFO_USERNAME_ALREADY_TAKEN:'Användarnamnet finns redan, var vänlig välj ett nytt',


	//^^^^^^^^^[ Login screen ]^^^^^^^^^^^^^^^^^^^^^^^^^
    LABEL_GRAPH_PREVIOUS:'Tidigare',
    LABEL_GRAPH_DAILY:'Dagar',
    LABEL_GRAPH_WEEKLY:'Veckor',
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	

    //LOGOUT
    HEADLINE_DASHBOARD: 'Start',
    HEADLINE_ABOUT_APP_MODAL: 'Om hemuppgiften',
   

    LABEL_BUTTON_LOGOUT:'Logga ut',
    LABEL_CONFIRM_LOGOUT:'Är du säker på att du vill logga ut? ',
    LABEL_LOG_ME_OUT:'Ja, logga ut mig!',

    LABEL_CANCEL:'Avbryt',//common for all confirms

    LABEL_LIST_DELETE:'Radera',
    LABEL_CONFIRM_LIST_DELETE:'Är du säker på att du vill radera? ',
    LABEL_LIST_DELETE_SUCCESS:'Raderad!',//@todo
    LABEL_LIST_UPDATE_SUCCESS:'Uppdaterad!',
    LABEL_LIST_INSERT_SUCCESS:'Tillagt!',//@todo
	INFO_LIST_CANNOT_BE_EMPTY:'Fyll i text',//@todo


	INFO_OFFLINE_SYNC_NOT_POSSIBLE: 'Du måste vara online för att synka',


    HEADLINE_FEEDBACK: '',
   

    LOADING:'Loading',

    INFO_HOMEWORKS_UPDATED:'',
    INFO_PRACTICES_UPDATED:''


  })
  ;

  if (_APP_DEVELOPMENT_MODE) {
  	//$translateProvider.preferredLanguage('en');
  	$translateProvider.preferredLanguage('sv');
  }else {
  	$translateProvider.preferredLanguage('sv');
  }
  

});
