SERVICE_MODULE

.factory('BUPActivities', ['BUPConfig', 'BUPUtility', 'BUPUser', 'BUPLoader', '$rootScope', '$filter', 'authService', '$moment','UserAssignment','$timeout',
    function(BUPConfig, BUPUtility, BUPUser, BUPLoader, $rootScope, $filter, authService, $moment,UserAssignment,$timeout) {
        //var currentUserID = BUPUser.getCurrentUser().id;
        //console.log(currentUserID + " <-");


        var allActivities = [];
        var activities = [];
      
        //console.debug('setting the current activities');
        //console.debug(BUPUser.getCurrentUser());
        //console.log("all activities");
        //console.log(allActivities);



        //if individual user
        

        
 

        /*
        var activities = _.filter(allActivities, {
            //userid: 11 //currentUserID
            userid: BUPUser.getCurrentUser() && BUPUser.getCurrentUser().id
            //userid:Number($rootScope.currentUser.id)
        });
        */
        //var activities = _.filter(allActivities, activitiesFilterCriteria);
        //console.log("activities from the current user only");
        //console.log(activities);


        var usersFeedbacks = {
            practices: [],
            homeworks: []
        };





        var daily = [];
        window.daily = daily;

        var weekly = [];    
        window.weekly= weekly;

        /**
         * @todo
         * Questions: is  the saved activities only related to the repetitive practices???
         * what abou the homework list added???
         */

        var service = {

            initActivities: function() {
                var currentUser = BUPUser.getCurrentUser();
                if (!currentUser || !currentUser.isLoggedIn) return;
                
                if (window.localStorage['activities']) {
                    try {
                        allActivities = JSON.parse(window.localStorage['activities']);
                    } catch (e) {

                    }
                }
                

                var activitiesFilterCriteria = {};
                if($rootScope.RoleId && $rootScope.RoleId == 3){
                    activitiesFilterCriteria.meetingId = $rootScope.meetingId;
                }
                if (BUPUser.getCurrentUser() ) {
                     activitiesFilterCriteria.userid = parseInt(BUPUser.getCurrentUser().id,10);
                }

                window.activitiesFilterCriteria=activitiesFilterCriteria;

                activities = _.filter(allActivities, activitiesFilterCriteria);

                if (window.localStorage['usersFeedbacks']) {
                    try {
                        usersFeedbacks = angular.extend({
                            practices: [],
                            homeworks: []
                        }, JSON.parse(window.localStorage['usersFeedbacks']));
                    } catch (e) {

                    }
                }
                window.usersFeedbacks = usersFeedbacks;


                window.activities = activities;


                //if daily and weekly have already been calculate , they might need to be recalculated!
                //if (daily.length) {
                
                    service.groupActivitiesByDay();
                //}

                //if (weekly.length) {
                    service.groupActivitiesByWeek();
                //}
                /*
                console.error('Good now initialized');
                console.log('>>>>>>>>>>>>>>>>>>>>>>>>>');
                console.log('all activities');
                console.debug(allActivities);
                console.log('activities');
                console.debug(activities);
                console.log('daily');
                console.debug(daily);
                */
                // this.listen = function($scope) {
                //     $scope.$watch(function(){return someScopeValue},function(){
                //        //$scope.dosomestuff();
                //     });
                // };
                $rootScope.$broadcast('graphInit');

            },
            getAll: function(opt) {
                opt = opt || {};
                if (typeof activities == "object" && activities != null && activities.hasOwnProperty("length")) { //make sure it is a valid array
                    //controller is configured to expect a promist
                    var deferred = $q.defer();
                    deferred.resolve(activities);
                    // !opt.hideLoader && BUPLoader.show({
                    //     delay: 0
                    // });
                    return deferred.promise;

                } else {
                    return []; //return the empty array
                }
            },

            getCurrentUserActivities: function() {
                var user = BUPUser.getCurrentUser();
                return activities;
                var cua = activities.filter(function(activity) {
                    return activity.userid == user.id;
                });
                return cua;
            },
            dailyActivities: function() {
                //console.warn('daily data requested');
                //console.log(daily);
                return daily; 
                
            },
            //returns data in same format as dailyActivities but with remaining dates
            futureDailyActivities: function(daysCount) {
                daysCount = daysCount || 4;
                var today = $moment().startOf('day');
                var graphTomorrow = {
                    day: today.dayOfYear()+1,
                    week: today.isoWeek() +1
                };

                //great!!! in same year
                //just create empty array from graphStart.day to graphToday.day
                var futureDaily = _(graphTomorrow.day).range(graphTomorrow.day + daysCount).map(function(ind) {
                    var obj = {};
                    obj['count'] = 0;
                    obj['date'] = $moment().dayOfYear(ind).startOf('day').valueOf();
                    obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                    obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                    return obj;

                }).indexBy('dayIndex')
                .value();


                return futureDaily;
            },
            weeklyActivities: function() {
                return weekly;
            },
            futureWeeklyActivities: function(weeksCount) {
                weeksCount = weeksCount || 0;

                var today = $moment().startOf('day');


            
                  var graphStart = {
                    year: today.get('year'),
                    day: today.dayOfYear()+1,
                    week: today.isoWeek() +1
                };


                var futureWeekly = _(graphStart.week).range(graphStart.week + weeksCount).map(function(ind) {
                        var obj = {};
                        obj['count'] = 0;
                        obj['date'] = $moment().isoWeek(ind).startOf('day').valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('weekIndex')
                        .value();
                return futureWeekly;
            },
            /**
             * adds the activity to the persistent storage
             * 
             * @param {object} newActivity  
             * a compulsory paramter used to identify:
             *     1. how many times a particular task has been done 
             *     2. other user contributed values which needs to be tracked for each activity
             *
             * For eg rating rating value has to be saved for every entry, ie has to be tracked 
             * independently for each activity even for the same task
             *  
             *]
             * @param {[object]} userResponse [description]
             * Saved in one place for one task
             * eg when a task is done for first time it is saved, when the same task 
             * is done second time, it overwrites the old values
             */
            
            addActivity: function(newActivity,userResponse) {
                console.log(newActivity);

                // obj['date'] = $moment().isoWeek(ind).startOf('day').valueOf();
                //         obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                //         obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);

                var currDate =$moment().startOf('day');
                var defaultValues = {
                    date: currDate.format('YYYY-MM-DD'),
                    day: BUPUtility.dayIndex(currDate.valueOf()),
                    week: BUPUtility.weekIndex(currDate.valueOf()),
                    userid: Number(BUPUser.getCurrentUser().id),
                };



                //if individual user
                if($rootScope.RoleId && $rootScope.RoleId == 3){
                    defaultValues.meetingId = $rootScope.meetingId;
                }
                newActivity = angular.extend(defaultValues, newActivity);
                //console.log(newActivity);
                activities.push(newActivity);
                allActivities.push(newActivity);
                this.saveToLocalStorage();
                
                // if( BUPUser.getCurrentUser().saveDataInServer ) {
                //     newActivity
                // }

                //$timeout(function(){
                    $rootScope.$broadcast('newactivity', newActivity,activities);    
                //},1);
                

            },

            getActivitiesCountByTaskId: function(taskId) {
                //return _.filter(activities, { 'taskid': taskId }).length;
                return _.filter(activities, function(activity) {
                    return activity.taskid === Number(taskId);
                }).length;
                //function(num) { return num % 2 == 0; });
            },
            //
            getActivitiesCountByMeeting: function(meetingId) {
                //return _.filter(activities, { 'taskid': taskId }).length;
                return _.filter(activities, function(activity) {
                    return activity.meetingId === Number(meetingId);
                }).length;
                //function(num) { return num % 2 == 0; });
            },

            isHomeworkDone: function(homeworkId) {
                //console.warn('homework check request received: '+homeworkId);
                //console.log(activities);
                return _.filter(activities, function(activity) {
                    return activity.homeworkid && activity.homeworkid === Number(homeworkId);
                }).length !== 0;
            },

            getExposureRatingActivities: function(taskId) {

                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });

                return _.filter(repetitiveActivities, function(activity) {
                    return activity.taskid === Number(taskId);
                });
            },

            getActivityStartDate: function() {

                //var config = BUPConfig.getCurrentUserConfig();
                var config = BUPConfig.getCurrentUserConfig();

                if (config && config.startedDate)
                    return config.startedDate;

            },
            getRepetitiveActivities: function() {
                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });
                return repetitiveActivities;
            },

            groupActivitiesByDay: function() {
                var currentUser = BUPUser.getCurrentUser();
                if (!currentUser || !currentUser.isLoggedIn) return;
                //console.error('groupActivitiesByDay called');
                //console.error(currentUser);
                //console.time('dailyGraph'); 
                //console.debug(activities);

                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });

                //console.log(repetitiveActivities);

                var UserConfigStartDate = $moment(this.getActivityStartDate().timestamp);

                var graphStart = {
                    year: UserConfigStartDate.get('year'),
                    day: UserConfigStartDate.dayOfYear(),
                    week: UserConfigStartDate.isoWeek()
                };

                //custom date
                // var graphStart = {
                //     year: 2014, 
                //     day: 150, 
                //     week: UserConfigStartDate.isoWeek()
                // };
                var today = $moment().startOf('day');

                var graphToday = {
                    year: today.get('year'),
                    day: today.dayOfYear(),
                    week: today.isoWeek()
                };


                if (debugMode) window.activities = activities;


                //now need to create empty array with from graphStart till graphToday
                if (graphStart.year == graphToday.year) {
                    //great!!! in same year
                    //just create empty array from graphStart.day to graphToday.day
                    window._ua1 = daily = _(graphStart.day).range(graphToday.day + 1).map(function(ind) {
                        var obj = {};
                        obj['count'] = 0;
                        obj['date'] = $moment().dayOfYear(ind).startOf('day').valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('dayIndex')
                        .value();



                } else if (graphStart.year < graphToday.year) {
                    var day_indexes = [];

                    var fromYear = graphStart.year,
                        fromDayofYear = graphStart.day;


                    //for previous years
                    while (fromYear < graphToday.year) {
                        //minimum day of the year is 365 but also check for 366 
                        //If the range is exceeded, it will bubble up to the next years. 
                        //so check if the year value actually increases to make sure we have reached the last day of the year
                        while (fromDayofYear < 365 || $moment().set('year', fromYear).dayOfYear(fromDayofYear).get('year') === fromYear) {
                            day_indexes.push(fromYear + '_' + fromDayofYear);
                            ++fromDayofYear; //increase the "fromDayOfYear" value
                        }
                        fromDayofYear = 1;
                        ++fromYear;

                    }
                    //for current year till today 
                    for (fromDayofYear = 1; fromDayofYear <= (graphStart.day + 1); fromDayofYear++) {
                        day_indexes.push(fromYear + '_' + fromDayofYear);
                    }

                    if (debugMode) window._day_index_range = day_indexes;
                    if (debugMode) console.log(day_indexes);

                    window._ua2 = daily = _(day_indexes).map(function(ind) {
                        //console.log(ind);
                        var info = ind.split('_'),
                            year = info[0],
                            dayOfYear = info[1],
                            theMoment = $moment().year(year).dayOfYear(dayOfYear).startOf('day'),
                            obj = {};
                        obj['count'] = 0;
                        obj['date'] = theMoment.valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('dayIndex')
                        .value();



                    //console.log(daily);


                }



                //now done populating empty dates 


                //now time to add user data activities to them


                _(repetitiveActivities).countBy('date').forEach(function(count, timestampIndex) {
                    //console.log(timestampIndex);
                    var timestamp = $moment(timestampIndex).startOf('day').valueOf();
                    var found = _.findKey(daily, {
                        'date': timestamp
                    });
                    if (found) {
                        daily[found].count = count;
                    }
                    //console.log(found);


                })

                console.timeEnd('dailyGraph');
                return daily;
                //return theMoment.get('year') + '_' + theMoment.dayOfYear();
            },

            groupActivitiesByWeek: function() {




                // console.log("!!!!!!!!!!GETACTIVITIESBYWEEK CALLED!!!!!!!!!");
                // console.log("caller is " + arguments.callee.caller.toString());
                // console.time('weeklyGraph');


                var repetitiveActivities = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                });



                //custom date
                // var graphStart = {
                //     year: 2014, 
                //     day: 150, 
                //     week: UserConfigStartDate.isoWeek()
                // };



                var today = $moment().startOf('day');

                var graphToday = {
                    year: today.get('year'),
                    day: today.dayOfYear(),
                    week: today.isoWeek()
                };

                try {
                    var UserConfigStartDate = $moment(this.getActivityStartDate().timestamp);

                    var graphStart = {
                        year: UserConfigStartDate.get('year'),
                        day: UserConfigStartDate.dayOfYear(),
                        week: UserConfigStartDate.isoWeek()
                    };
                } catch (e) {
                    console.error('failed to get activity start date');
                    var graphStart = graphToday;
                    //return false;
                }



                if (debugMode) window.activities = activities;


                //now need to create empty array with from graphStart till graphToday
                if (graphStart.year == graphToday.year) {
                    //great!!! in same year
                    //just create empty array from graphStart.day to graphToday.day
                    window._uaw1 = weekly = _(graphStart.week).range(graphToday.week + 1).map(function(ind) {
                        var obj = {};
                        obj['count'] = 0;
                        obj['date'] = $moment().isoWeek(ind).startOf('day').valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('weekIndex')
                        .value();



                } else if (graphStart.year < graphToday.year) {
                    var week_indexes = [];

                    var fromYear = graphStart.year,
                        fromIsoWeek = graphStart.week;


                    //for previous years
                    while (fromYear < graphToday.year) {
                        //minimum week of the year is 52 but also check for more 
                        //If the range is exceeded, it will bubble up to the next years. 
                        //so check if the year value actually increases to make sure we have reached the last week of the year
                        while (fromIsoWeek < 52 || $moment().set('year', fromYear).isoWeek(fromIsoWeek).get('year') === fromYear) {
                            week_indexes.push(fromYear + '_' + fromIsoWeek);
                            ++fromIsoWeek; //increase the "fromDayOfYear" value
                        }
                        fromIsoWeek = 1;
                        ++fromYear;

                    }
                    //for current year till today 
                    for (fromIsoWeek = 1; fromIsoWeek <= (graphStart.week + 1); fromIsoWeek++) {
                        week_indexes.push(fromYear + '_' + fromIsoWeek);
                    }

                    //if (debugMode) window._week_index_range = week_indexes;
                    //if (debugMode) console.log(week_indexes);

                    window._ua2 = weekly = _(week_indexes).map(function(ind) {
                        //console.log(ind);
                        var info = ind.split('_'),
                            year = info[0],
                            week = info[1],
                            theMoment = $moment().year(year).isoWeek(week).startOf('day'),
                            obj = {};
                        obj['count'] = 0;
                        obj['date'] = theMoment.valueOf();
                        obj['dayIndex'] = BUPUtility.dayIndex(obj['date']);
                        obj['weekIndex'] = BUPUtility.weekIndex(obj['date']);
                        return obj;

                    }).indexBy('dayIndex')
                        .value();

                    //console.log(weekly);

                    
                }



                //now done populating empty dates 


                //now time to add user data activities to them


                //if (debugMode) window.daily = daily;
                //if (debugMode) console.log(daily);
                //window.weekly = weekly;
                var vals = _(repetitiveActivities).map(function(entry) {
                        entry['week'] = BUPUtility.weekIndex(entry.date);
                        //console.log(entry);

                        return entry;
                    }).countBy('week')
                    .forEach(function(count, weekIndexValue) {
                        if (weekly[weekIndexValue]) {
                            weekly[weekIndexValue].count = count;
                        }
                    })

                //.value();

                //console.log(vals.value());

                // .forEach(function(count, timestampIndex) {

                //     var timestamp = $moment(timestampIndex).startOf('day').valueOf();
                //     console.log(timestamp);
                //     console.log(count);
                //     var found = _.findKey(weekly, {
                //         'date': timestamp
                //     });
                //     if (found) {
                //         weekly[found].count = count;
                //     }
                //     console.log(found);


                // })

                console.timeEnd('weeklyGraph');
                return weekly;
                //return theMoment.get('year') + '_' + theMoment.dayOfYear();
            },


            getPracticeCountInTotal:function(){
                //only repetitive, repetitive activity has taskid, nonrepetitive has homeworkid
                return _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                }).length;
              
            },

            getPracticeCountThisWeek:function(){
                var currentWeekIndex = BUPUtility.weekIndex();
                return _.filter(activities, function(activity) {
                    return activity.week == currentWeekIndex && false !== !!activity.taskid;
                }).length;
            },

            getCurrentUserPracticeCount: function(){
                var response = {
                    total:0,
                    week:0
                };
                var currentWeekIndex = BUPUtility.weekIndex();
                response.total = _.filter(activities, function(anActivity) {
                    return false !== !!anActivity.taskid;
                }).length;
                response.week = _.filter(activities, function(activity) {
                    return activity.week == currentWeekIndex && false !== !!activity.taskid;
                }).length;

                return response;
            },


            saveToLocalStorage: function() {
                //???
                // overwriting the activities from the otherusers here
                // 
                //current activities are the activities 
                
                //this will remove the activities of the other users,data of which
                //are in allActivities but not in activities
                //
                
                //window.localStorage['activities'] = JSON.stringify(activities);
                
                //this will maintain the activities of all the users
                window.localStorage['activities'] = JSON.stringify(allActivities);
            },

            /*****************************************************************
             * @id: (int) id of the task
             * @type:  (string) type of the task , either homework or practice
             *****************************************************************
             **/
            getActivityByTaskId: function(taskId) {

                var deferred = $q.defer();

                //first ensure we have all the tasks
                this.getAllTasks().then(function(allTasks) {

                    //console.log('all  tasks (practices and homeworks) available.ensured!');

                    //console.log(allTasks);
                    //console.log('type=' + type);
                    //console.log(allTasks[type]);
                    //get the task type
                    //console.log()
                    var foundTasks = allTasks[type].filter(function(thetask) {
                        //console.log(thetask.id);
                        return thetask != null && parseInt(thetask.id, 10) == id;
                    });
                    //console.log(foundTasks);
                    //make sure the task has been found
                    var requiredTask = foundTasks.length ? foundTasks[0] : {};

                    deferred.resolve(requiredTask);


                });

                return deferred.promise;

            },


                        /*****************************************************************
             * @id: (int) id of the task
             * @type:  (string) type of the task , either homework or practice
             *****************************************************************
             **/
            getUserResponseByTaskId: function(id, type) {

                    console.time('getUserResponseByTaskId'+id+type);
                    console.log(usersFeedbacks);
                    console.log('type=' + type);
                    console.log(usersFeedbacks[type]);
                    var foundItems = usersFeedbacks[type].filter(function(theItem) {
                        return theItem != null && parseInt(theItem.id, 10) == id;
                    });

                    var userResponseForTheTask = false;

                    if (foundItems.length) {
                        userResponseForTheTask = foundItems[0];
                        //userResponseForTheTask = new UserAssignment(foundItems[0]);
                        console.debug('user contributed activity found!')
                    }
                    console.timeEnd('getUserResponseByTaskId'+id+type);
                    return userResponseForTheTask;

            },

            updateUserResponseByTaskId: function(id,type,response) {
                console.log(response);
                // var foundItems = usersFeedbacks[type].filter(function(theItem) {
                //     return theItem != null && parseInt(theItem.id, 10) == id;
                // });
                
                var foundIndex = _.findIndex(usersFeedbacks[type],function(theItem) {
                    return theItem != null && parseInt(theItem.id, 10) == id;
                });

               

                if (foundIndex < 0) {
                    //the file doesn't previously exists
                    //allFiles.push(theFile);
                    console.debug('adding fresh value to type '+type);
                    usersFeedbacks[type].push(new UserAssignment(response));
                    console.log(usersFeedbacks[type][0]);

                } else {
                    //update the same index 
                    console.debug('updating value to index '+foundIndex+' of type '+type);  
                    usersFeedbacks[type][foundIndex] = new UserAssignment(response);
                    console.log(usersFeedbacks[type][foundIndex]);
                    
                }




                // if (foundItems.length) {
                //     //foundItems[0] = response;
                //     foundItems[0] = new UserAssignment(response);
                //     console.log(foundItems[0]);
                //     console.log(usersFeedbacks[type]);
                  
                //     console.debug('existing updated in the user contributed activities');
                // }else {
                //     //usersFeedbacks[type].push(response);
                //     usersFeedbacks[type].push(new UserAssignment(response));

                //     //console.debug('new user contributed activity');
                // }
                //JSON.parse(window.localStorage['usersFeedbacks'])
                window.localStorage['usersFeedbacks'] =  JSON.stringify(usersFeedbacks);

            }

        };

        //service.groupActivitiesByDay();
        //service.groupActivitiesByWeek();

        $rootScope.$on('activitiesInit', service.initActivities);
        //service.initActivities();
        
        window.a = service;

        return service;
    }
])

SERVICE_MODULE



.factory('film_deep_breath02', [

    function() {


        var lib;

        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes
            var rect; // used to reference frame bounds

            // stage content:
            // 
            //  {src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"sounds/Andas_djupt_ovning_med_intro.mp3"},
                                 // {src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"sounds/Rakna_baklanges_ovning_med_intro.mp3"},
                                 // {src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"sounds/Skona_tanken_ovning_med_intro.mp3"}
            (lib.film_deep_breath02 = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    this.player = playSound("sounds/Andas_djupt_ovning_med_intro.mp3");
                }

                this.pause = function() {
                    console.log('expect the animation and sound to get paused here');
                    this.stop();
                    this.player.pause();
                }

                this.resume = function() {
                    this.play();
                    this.player.resume();
                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(839));

                // Andas Animation 
                this.CIRKEL = new lib.Symbol46();
                this.CIRKEL.setTransform(240, 240);
                this.CIRKEL.alpha = 0;
                this.CIRKEL._off = true;

                this.timeline.addTween(cjs.Tween.get(this.CIRKEL).wait(6).to({
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 57).wait(43).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 12).wait(6).to({
                    scaleX: 0.71,
                    scaleY: 0.71
                }, 89).wait(17).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 44).wait(69).to({
                    scaleX: 0.69,
                    scaleY: 0.69
                }, 53).to({
                    scaleX: 0.49,
                    scaleY: 0.49
                }, 14).wait(45).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 82).wait(42).to({
                    scaleX: 0.63,
                    scaleY: 0.63
                }, 46).to({
                    scaleX: 0.44,
                    scaleY: 0.44
                }, 17).wait(44).to({
                    scaleX: 1.62,
                    scaleY: 1.62
                }, 49).wait(28).to({
                    scaleX: 0.72,
                    scaleY: 0.72
                }, 53).to({
                    scaleX: 0.53,
                    scaleY: 0.53
                }, 9).wait(15));

                // Andas djupt bakgrund
                this.instance = new lib.ID_bakgrund_blue();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance
                    }]
                }).wait(840));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            // symbols:
            (lib.ID_bakgrund_blue = function() {
                this.initialize(img.ID_bakgrund_blue);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_green = function() {
                this.initialize(img.ID_bakgrund_green);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol46 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f().s("#FFFFFF").ss(32, 1, 1).p("ALKrJQEoEoAAGhQAAGikoEoQkoEomiAAQmhAAkokoQkokoAAmiQAAmhEokoQEokoGhAAQGiAAEoEog");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = rect = new cjs.Rectangle(-100.9, -100.9, 202, 202);
            p.frameBounds = [rect];

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});


        return lib.film_deep_breath02;
    }
])


.factory('film_nice_thought', [

    function() {


        var lib;
        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes
            var rect; // used to reference frame bounds

            // stage content:
            (lib.film_nice_thought = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    // playSound("Skona_tanken_ovning_med_intro");
                    this.player = playSound("sounds/Skona_tanken_ovning_med_intro.mp3");

                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(799));

                // Sköna tanken Lager 25
                this.instance = new lib.Symbol45();
                this.instance.setTransform(240, 240);

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(750).to({
                    alpha: 0
                }, 49).wait(1));

                // Sköna tanken Lager 24
                this.instance_1 = new lib.ID_bakgrund_green();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance_1
                    }]
                }).wait(800));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 480, 480);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            // symbols:
            (lib.ID_bakgrund_blue = function() {
                this.initialize(img.ID_bakgrund_blue);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_green = function() {
                this.initialize(img.ID_bakgrund_green);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol43 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlZFaQiQiQABjKQgBjJCQiQQCQiQDJABQDKgBCQCQQCQCQgBDJQABDKiQCQQiQCQjKgBQjJABiQiQg");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
            p.frameBounds = [rect];


            (lib.Symbol44 = function(mode, startPosition, loop) {
                this.initialize(mode, startPosition, loop, {});

                // Lager 1
                this.instance = new lib.Symbol43("synched", 0);
                this.instance.alpha = 0;

                this.timeline.addTween(cjs.Tween.get(this.instance).to({
                    alpha: 0.602
                }, 49).wait(115).to({
                    startPosition: 0
                }, 0).to({
                    alpha: 0
                }, 75).wait(1));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(-48.9, -48.9, 98, 98);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


            (lib.Symbol45 = function(mode, startPosition, loop) {
                this.initialize(mode, startPosition, loop, {});

                // Lager 22
                this.instance = new lib.Symbol44();
                this.instance.setTransform(148, -38.9);
                this.instance._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(589).to({
                    _off: false
                }, 0).wait(211));

                // Lager 21
                this.instance_1 = new lib.Symbol44();
                this.instance_1.setTransform(47, 121, 1.184, 1.184);
                this.instance_1._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(570).to({
                    _off: false
                }, 0).wait(230));

                // Lager 20
                this.instance_2 = new lib.Symbol44();
                this.instance_2.setTransform(-61.9, 129, 0.571, 0.571);
                this.instance_2._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(546).to({
                    _off: false
                }, 0).wait(254));

                // Lager 19
                this.instance_3 = new lib.Symbol44();
                this.instance_3.setTransform(148, 63);
                this.instance_3._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(519).to({
                    _off: false
                }, 0).wait(281));

                // Lager 18
                this.instance_4 = new lib.Symbol44();
                this.instance_4.setTransform(119, -113.9);
                this.instance_4._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(491).to({
                    _off: false
                }, 0).wait(309));

                // Lager 17
                this.instance_5 = new lib.Symbol44();
                this.instance_5.setTransform(-143.9, -42.9, 1.306, 1.306);
                this.instance_5._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(471).to({
                    _off: false
                }, 0).wait(329));

                // Lager 16
                this.instance_6 = new lib.Symbol44();
                this.instance_6.setTransform(-82.9, -139.9);
                this.instance_6._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(435).to({
                    _off: false
                }, 0).wait(365));

                // Lager 15
                this.instance_7 = new lib.Symbol44();
                this.instance_7.setTransform(124, 0);
                this.instance_7._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(406).to({
                    _off: false
                }, 0).wait(394));

                // Lager 14
                this.instance_8 = new lib.Symbol44();
                this.instance_8.setTransform(-161.9, 75, 0.755, 0.755);
                this.instance_8._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(376).to({
                    _off: false
                }, 0).wait(424));

                // Lager 13
                this.instance_9 = new lib.Symbol44();
                this.instance_9.setTransform(-137.9, -97.9, 0.694, 0.694);
                this.instance_9._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(344).to({
                    _off: false
                }, 0).wait(456));

                // Lager 12
                this.instance_10 = new lib.Symbol44();
                this.instance_10.setTransform(-6.9, -9.9, 1.673, 1.673);
                this.instance_10._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(319).to({
                    _off: false
                }, 0).wait(481));

                // Lager 11
                this.instance_11 = new lib.Symbol44();
                this.instance_11.setTransform(-118.9, 79, 0.612, 0.612);
                this.instance_11._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(293).to({
                    _off: false
                }, 0).wait(507));

                // Lager 10
                this.instance_12 = new lib.Symbol44();
                this.instance_12.setTransform(-13.9, -130.9, 1.347, 1.347);
                this.instance_12._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(261).to({
                    _off: false
                }, 0).wait(539));

                // Lager 9
                this.instance_13 = new lib.Symbol44();
                this.instance_13.setTransform(129, -48.9, 0.796, 0.796);
                this.instance_13._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(224).to({
                    _off: false
                }, 0).wait(576));

                // Lager 8
                this.instance_14 = new lib.Symbol44();
                this.instance_14.setTransform(-103.9, 10, 1.429, 1.429);
                this.instance_14._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(194).to({
                    _off: false
                }, 0).wait(606));

                // Lager 7
                this.instance_15 = new lib.Symbol44();
                this.instance_15.setTransform(-13.9, 110);
                this.instance_15._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(162).to({
                    _off: false
                }, 0).wait(638));

                // Lager 6
                this.instance_16 = new lib.Symbol44();
                this.instance_16.setTransform(68, -101.9, 1.245, 1.245);
                this.instance_16._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(133).to({
                    _off: false
                }, 0).wait(667));

                // Lager 5
                this.instance_17 = new lib.Symbol44();
                this.instance_17.setTransform(68, 0.1, 1.408, 1.408, 0, 0, 0, -7.8, -44.7);
                this.instance_17._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(109).to({
                    _off: false
                }, 0).wait(691));

                // Lager 4
                this.instance_18 = new lib.Symbol44();
                this.instance_18.setTransform(-69.9, 74, 0.735, 0.735);
                this.instance_18._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(79).to({
                    _off: false
                }, 0).wait(721));

                // Lager 3
                this.instance_19 = new lib.Symbol44();
                this.instance_19.setTransform(72, -26.9, 0.755, 0.755);
                this.instance_19._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(57).to({
                    _off: false
                }, 0).wait(743));

                // Lager 2
                this.instance_20 = new lib.Symbol44();
                this.instance_20.setTransform(-82.9, -64.9, 1.531, 1.531);
                this.instance_20._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(31).to({
                    _off: false
                }, 0).wait(769));

                // Lager 1
                this.instance_21 = new lib.Symbol44();
                this.instance_21._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(10).to({
                    _off: false
                }, 0).wait(790));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = rect = new cjs.Rectangle(0, 0, 0, 0);
            p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-48.9, -48.9, 98, 98), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 207, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 189), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 267, 250), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -139.9, 306, 272), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 295), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-157.9, -162.9, 306, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 322, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -162.9, 342, 322), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-173.9, -196.9, 342, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 367, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-198.9, -196.9, 372, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 381, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 356), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect = new cjs.Rectangle(-207.9, -196.9, 405, 376), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});


        return lib.film_nice_thought;
    }
])


.factory('film_countdown02', [

    function() {


        var lib;
        (function(lib, img, cjs) {

            var p; // shortcut to reference prototypes

            // stage content:
            (lib.film_countdown02 = function(mode, startPosition, loop) {
                if (loop == null) {
                    loop = false;
                }
                this.initialize(mode, startPosition, loop, {});

                // timeline functions:
                this.frame_0 = function() {
                    this.player = playSound("sounds/Rakna_baklanges_ovning_med_intro.mp3");
                }

                // actions tween:
                this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(810));

                // 99
                this.instance = new lib.Symbol42nr5();
                this.instance.setTransform(242.5, 235.4);
                this.instance.alpha = 0.23;
                this.instance._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance).wait(317).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 6).wait(469));

                // 98
                this.instance_1 = new lib.Symbol42nr6();
                this.instance_1.setTransform(242.5, 235.4);
                this.instance_1.alpha = 0.23;
                this.instance_1._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(293).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(494));

                // 97
                this.instance_2 = new lib.Symbol42nr7();
                this.instance_2.setTransform(242.5, 235.4);
                this.instance_2.alpha = 0.23;
                this.instance_2._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(269).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(518));

                // 96
                this.instance_3 = new lib.Symbol42nr8();
                this.instance_3.setTransform(242.5, 235.4);
                this.instance_3.alpha = 0.23;
                this.instance_3._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(245).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(542));

                // 95
                this.instance_4 = new lib.Symbolnr9();
                this.instance_4.setTransform(240, 235.4);
                this.instance_4.alpha = 0.23;
                this.instance_4._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(221).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(566));

                // 94
                this.instance_5 = new lib.Symbol42nr4();
                this.instance_5.setTransform(242.5, 235.4);
                this.instance_5.alpha = 0.23;
                this.instance_5._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(342).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 5).wait(445));

                // 93
                this.instance_6 = new lib.Symbol42nr3();
                this.instance_6.setTransform(242.5, 235.4);
                this.instance_6.alpha = 0.23;
                this.instance_6._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(366).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(423));

                // 92
                this.instance_7 = new lib.Symbol42nr2();
                this.instance_7.setTransform(242.5, 235.4);
                this.instance_7.alpha = 0.23;
                this.instance_7._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(388).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(401));

                // 91
                this.instance_8 = new lib.Symbol42nr1();
                this.instance_8.setTransform(242.5, 235.4);
                this.instance_8.alpha = 0.23;
                this.instance_8._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(410).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 58).wait(324));

                // 90
                this.instance_9 = new lib.Symbol41();
                this.instance_9.setTransform(242.5, 235.4);
                this.instance_9.alpha = 0;
                this.instance_9._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(32).to({
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 45).wait(124).to({
                    scaleX: 1.19,
                    scaleY: 1.19
                }, 5).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(590));

                // 89
                this.instance_10 = new lib.Symbol42nr1();
                this.instance_10.setTransform(242.5, 235.4);
                this.instance_10.alpha = 0.23;
                this.instance_10._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(715).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 1).wait(76));

                // 88
                this.instance_11 = new lib.Symbol42nr2();
                this.instance_11.setTransform(242.5, 235.4);
                this.instance_11.alpha = 0.23;
                this.instance_11._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(693).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(96));

                // 87
                this.instance_12 = new lib.Symbol42nr3();
                this.instance_12.setTransform(242.5, 235.4);
                this.instance_12.alpha = 0.23;
                this.instance_12._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(671).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(118));

                // 86
                this.instance_13 = new lib.Symbol42nr4();
                this.instance_13.setTransform(242.5, 235.4);
                this.instance_13.alpha = 0.23;
                this.instance_13._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(649).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(140));

                // 85
                this.instance_14 = new lib.Symbol42nr5();
                this.instance_14.setTransform(242.5, 235.4);
                this.instance_14.alpha = 0.23;
                this.instance_14._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(627).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(162));

                // 84
                this.instance_15 = new lib.Symbol41();
                this.instance_15.setTransform(242.5, 235.4, 0.757, 0.757);
                this.instance_15.alpha = 0;
                this.instance_15._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(488).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.09,
                    scaleY: 1.09,
                    alpha: 1
                }, 28).to({
                    scaleX: 1.19,
                    scaleY: 1.19
                }, 8).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(272));

                // 83
                this.instance_16 = new lib.Symbol42nr9();
                this.instance_16.setTransform(242.5, 235.4);
                this.instance_16.alpha = 0.23;
                this.instance_16._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(539).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(250));

                // 82
                this.instance_17 = new lib.Symbol42nr8();
                this.instance_17.setTransform(242.5, 235.4);
                this.instance_17.alpha = 0.23;
                this.instance_17._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(561).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(228));

                // 81
                this.instance_18 = new lib.Symbol42nr7();
                this.instance_18.setTransform(242.5, 235.4);
                this.instance_18.alpha = 0.23;
                this.instance_18._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(583).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(206));

                // 80
                this.instance_19 = new lib.Symbol42nr6();
                this.instance_19.setTransform(242.5, 235.4);
                this.instance_19.alpha = 0.23;
                this.instance_19._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(605).to({
                    _off: false
                }, 0).to({
                    scaleX: 1.19,
                    scaleY: 1.19,
                    alpha: 1
                }, 7).to({
                    scaleX: 1.03,
                    scaleY: 1.03,
                    alpha: 0.75
                }, 5).to({
                    scaleX: 0.81,
                    scaleY: 0.81,
                    alpha: 0
                }, 7).to({
                    _off: true
                }, 3).wait(184));

                // Lager 23
                this.instance_20 = new lib.Symbol45("synched", 0);
                this.instance_20.setTransform(240, 240);
                this.instance_20.alpha = 0;
                this.instance_20._off = true;

                this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(6).to({
                    startPosition: 0,
                    _off: false
                }, 0).to({
                    alpha: 1
                }, 57).wait(748));

                // Bakgrund
                this.instance_21 = new lib.ID_bakgrund_orange();

                this.timeline.addTween(cjs.Tween.get({}).to({
                    state: [{
                        t: this.instance_21
                    }]
                }).wait(811));

            }).prototype = p = new cjs.MovieClip();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            // symbols:
            (lib.ID_bakgrund_orange = function() {
                this.initialize(img.ID_bakgrund_orange);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720 = function() {
                this.initialize(img.stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 450, 470);


            (lib.under_Background = function() {
                this.initialize(img.under_Background);
            }).prototype = p = new cjs.Bitmap();
            p.nominalBounds = new cjs.Rectangle(0, 0, 480, 480);


            (lib.Symbol45 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f().s("#FFFFFF").ss(20, 0, 0, 4).p("ARIwZMgiPAAAMAAAAgzMAiPAAAg");

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-109.5, -104.9, 219.2, 210);


            (lib.Symbol42nr9 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjpHIQhPhGgLiHICvgRQAeCCBXAAQCGAAATkiQhRBAhrAAQhxAAhRhVQhQhUAAiUQAAiVBWhiQBXhhCaAAQCpAABdB5QBcB5AAEZQAAIMl1AAQh6AAhPhEgAhnk7QgfAtAABOQAABIAgAwQAgAwBBAAQA6AABFguIAAgLQAAh2glhRQglhRhLAAQgvAAgdAug");
                this.shape.setTransform(-0.1, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


            (lib.Symbol42kopia = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AgDGGQhaiIAAj+QAAj4BYiJQBViKCvAAQCvAABXCJQBZCIgBD6QABD/hbCHQhaCGiqAAQiqAAhYiGgACkkJQgbBdAACsQABCxAbBbQAbBbA+AAQBOAAAUh9QAVh8AAhuQAAiwgbhbQgbhbhAAAQg/AAgcBdgApUH/IAAijICNAAIAAqyIiVAAIAAikIF1AAIAANWICFAAIAACjg");
                this.shape.setTransform(4.9, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-55.7, -49.6, 121.2, 105);


            (lib.Symbol38 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AkKG3QhZhVAAiCQAAizC1hCQiWhVAAiWQAAh3BXhKQBYhKCVAAQCdAABVBJQBUBKAAB0QAACZiTBWQCxBBAAC5QAACHhbBQQhbBQivAAQiwAAhZhVgAhgBcQgnAsAABJQAABIAmArQAnApA7AAQA9AAAngpQAngrAAhIQAAhHgmgtQgngtg+AAQg6AAgnAsgAhUlKQgiAjAABFQAAA5AgAmQAhAmA1AAQA2AAAhgmQAhglAAg9QAAg8gfgmQgggmg5AAQgyAAgiAjg");
                this.shape.setTransform(0.1, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-35.5, -49.6, 71.4, 105);


            (lib.Symbol37 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AkVH9IAAikICBAAQAGiUBOjOQBLjQBfiEIkEAAIAACPIijAAIAAkuIJ7AAIAAB9QhNBchPDmQhPDkAACyICBAAIAACkg");
                this.shape.setTransform(0.6, 3.1);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-31.2, -47.8, 63.7, 101.9);


            (lib.Symbol36 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("Aj4GRQhbh9ABkKQgBkaBnh9QBmh+CuAAQD+AAAgEPIivAQQgeiChZAAQhFAAgnBSQgoBQgCCDQBHhDBrAAQB2AABQBRQBRBQAACaQAACshVBZQhWBYiNAAQi4AAhbh7gAh2BFQADCpAmA+QAjA+BBAAQAxAAAegsQAegsAAhWQABhRgkgpQglgpg0AAQg/AAg/Asg");
                this.shape.setTransform(0.6, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-33.3, -49.6, 68, 105);


            (lib.Symbol35 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjzG2QhVhNgTiEIDBgXQATCSB0AAQBAABAjgtQAjgtAAhRQABi1iDAAQhKAAgwBQIikhRIAeoCII6AAIAADvIiUAAIgPhMIkEAAIgND0QBQg5BbAAQCWAABSBhQBSBfAACLQAACUhYBlQhYBjixAAQiZAAhVhNg");
                this.shape.setTransform(-1.2, 3.7);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36, -47.8, 69.7, 103.2);


            (lib.Symbol34 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("Ag0IGIAAiWIBvAAIAAiDIl0AAQgahOgcg0IG0pwIDIAAIAAJnIBjAAIAACLIhjAAIAACDIBWAAIAACWgAjABiID7AAIAAlhg");
                this.shape.setTransform(-1.5, 2.2);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38.4, -49.6, 73.8, 103.7);


            (lib.Symbol33 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlbDIIDEgQQAECzCDAAQA5AAAmgrQAlgrAAhDQAAhBgkgrQglgshHAAIguABIAAipQBWAAAngnQAngnAAg5QAAgygdgfQgfggguAAQhtAAAACGIi+gOQAFiLBXhJQBXhJCFAAQCUAABUBIQBSBHABB9QgBCiiXBFQC8A8AAC2QAACQhhBRQhiBRiVAAQlGAAgZlEg");
                this.shape.setTransform(-1.2, 2.8);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36, -49.6, 69.7, 105);


            (lib.Symbol32 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AlJIGQAAiGAWhJQAVhJAyhFQAxhFBZhSQByhpAmgxQAkgzAAg/QAAgvgcgeQgcgfgnAAQg9AAgdAzQgdAygFBxIi7gJQAAiTAshLQAthKBLgiQBMgiBWAAQCSAABTBRQBTBRAAB4QAABAgZA3QgaA2glAmQgoAihhBGQiQBogmA+QglA+gHAuIEoAAIAAiEICjAAIgJEog");
                this.shape.setTransform(-1.1, 2.2);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 66.1, 103.7);


            (lib.Symbol31 = function() {
                this.initialize();

                // Lager 1
                this.shape = new cjs.Shape();
                this.shape.graphics.f("#FFFFFF").s().p("AjzH9IAAikICNAAIAAqyIiWAAIAAijIF0AAIAANVICFAAIAACkg");
                this.shape.setTransform(0.1, 3.1);

                this.addChild(this.shape);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-25.2, -47.8, 50.7, 101.9);


            (lib.Symbolnr9 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol42nr9();

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-34.2, -49.6, 68.3, 105);


            (lib.Symbol42nr8 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol38("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-37.5, -49.6, 71.4, 105);


            (lib.Symbol42nr7 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol37("synched", 0);
                this.instance.setTransform(0.5, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-30.7, -47.8, 63.7, 101.9);


            (lib.Symbol42nr6 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol36("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-35.3, -49.6, 68, 105);


            (lib.Symbol42nr5 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol35("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38, -47.8, 69.7, 103.2);


            (lib.Symbol42nr4 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol34("synched", 0);
                this.instance.setTransform(-3.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-42.4, -49.6, 73.8, 103.7);


            (lib.Symbol42nr3 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol33("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-38, -49.6, 69.7, 105);


            (lib.Symbol42nr2 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol32("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-36.2, -49.6, 66.1, 103.7);


            (lib.Symbol42nr1 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol31("synched", 0);
                this.instance.setTransform(-1.9, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-27.2, -47.8, 50.7, 101.9);


            (lib.Symbol41 = function() {
                this.initialize();

                // Lager 1
                this.instance = new lib.Symbol42kopia();
                this.instance.setTransform(-4.2, 0);

                this.addChild(this.instance);
            }).prototype = p = new cjs.Container();
            p.nominalBounds = new cjs.Rectangle(-60, -49.6, 121.2, 105);

        })(lib = lib || {}, images = images || {}, createjs = createjs || {});



        return lib.film_countdown02;
    }
])


.service('animationService', ['$rootScope', '$injector',
    function($rootScope, $injector) {
        //var canvas, stage, exportRoot,player;
        var stage, manifest;
        var resizeCanvas = function() {
            // browser viewport size
            var w = window.innerWidth;
            var h = window.innerHeight;

            // stage dimensions
            var ow = 480; // your stage width
            var oh = 480; // your stage height

            var keepAspectRatio = true;

            if (keepAspectRatio) {
                // keep aspect ratio
                var scale = Math.min(w / ow, h / oh);
                stage.scaleX = scale;
                stage.scaleY = scale;

                // adjust canvas size
                stage.canvas.width = ow * scale;
                stage.canvas.height = oh * scale;
            } else {
                // scale to exact fit
                stage.scaleX = w / ow;
                stage.scaleY = h / oh;

                // adjust canvas size
                stage.canvas.width = ow * stage.scaleX;
                stage.canvas.height = oh * stage.scaleY;
            }

            // update the stage
            stage.update()
        }
        var animationFile;

        var animationCompleted = function() {
            $rootScope.$broadcast('audio.ended');
        }
        window.as = this;

        window.addEventListener('resize', resizeCanvas);

        this.init = function(animationPackage, manifest) {
            this.canvas = document.getElementById("canvas");
            this.images = images || {};

            // this.manifest = [
            // 	{src:"images/ID_bakgrund_blue.jpg", id:"ID_bakgrund_blue"},
            // 	{src:"images/ID_bakgrund_green.jpg", id:"ID_bakgrund_green"},
            // 	{src:"images/ID_bakgrund_orange.jpg", id:"ID_bakgrund_orange"},
            // 	{src:"images/stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720.jpg", id:"stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720"},
            // 	{src:"images/under_Background.jpg", id:"under_Background"},
            // 	{src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"Andas_djupt_ovning_med_intro"},
            // 	{src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"Rakna_baklanges_ovning_med_intro"},
            // 	{src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"Skona_tanken_ovning_med_intro"}
            // ];

            this.manifest = manifest;

            animationFile = animationPackage;


            var loader = new createjs.LoadQueue(false);
            loader.installPlugin(createjs.Sound);
            loader.addEventListener("fileload", this.handleFileLoad);
            loader.addEventListener("complete", this.handleComplete);
            loader.loadManifest(this.manifest);
        };

        this.handleFileLoad = function(evt) {
            if (evt.item.type == "image") {
                this.images[evt.item.id] = evt.result;
            }
        }

        this.handleComplete = function() {
            // this.exportRoot = new lib.film_deep_breath02();
            this.exportRoot = new animationFile();

            stage = new createjs.Stage(this.canvas);
            stage.addChild(this.exportRoot);
            stage.update();
            window.st = stage;

            createjs.Ticker.setFPS(12);
            createjs.Ticker.addEventListener("tick", stage);
            resizeCanvas();
        }

        this.playSound = window.playSound = function(id, loop) {

            this.sound_id = id;


                
                console.log(id);
                my_media = new Media("file:///android_asset/www/"+id,
                        // success callback
                         function () { console.log("playAudio():Audio Success"); },
                        // error callback
                         function (err) { console.log("playAudio():Audio Error: " + err); }
                );
                       // Play audio
                my_media.play();
           
               
           

            this.player = createjs.Sound.play(id, createjs.Sound.INTERRUPT_EARLY, 0, 0, loop);
            this.player.addEventListener("complete", animationCompleted);
            return player;
        }

        this.destroy = function() {
            try {
                if(!!my_media){
                    my_media.stop();
                    my_media.release();
                }
                stage.removeAllChildren();
                createjs.Sound.removeAllEventListeners();
                createjs.Sound.removeAllSounds();
            } catch (e) {
                console.error(e);
            }

        }

        this.startAnimation = function(animationType, lib) {
            manifest = [
                                  //{src:"images/ID_bakgrund_blue.jpg", id:"ID_bakgrund_blue"},
                                  //{src:"images/ID_bakgrund_green.jpg", id:"ID_bakgrund_green"},
                                  //pos{src:"images/ID_bakgrund_orange.jpg", id:"ID_bakgrund_orange"},
                                  //{src:"images/stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720.jpg", id:"stockvectorpencildrawncirclesbubblesabstractvectorillustration101167720"},
                                  //{src:"images/under_Background.jpg", id:"under_Background"},
                                  {src:"sounds/Andas_djupt_ovning_med_intro.mp3", id:"sounds/Andas_djupt_ovning_med_intro.mp3"},
                                  {src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"sounds/Rakna_baklanges_ovning_med_intro.mp3"},
                                  {src:"sounds/Skona_tanken_ovning_med_intro.mp3", id:"sounds/Skona_tanken_ovning_med_intro.mp3"}
                              ];

            var animationpackageLib;
            console.log(animationType);
            switch (animationType) {
                case 3: // takna green






                    // manifest = [];

                    // manifest.push({
                    //     src: "sounds/Skona_tanken_ovning_med_intro.mp3",
                    //     id: "Skona_tanken_ovning_med_intro"
                    // });
                    // manifest.push({
                    //     src: "images/ID_bakgrund_green.jpg",
                    //     id: "ID_bakgrund_green"
                    // });

                    //animationPackage = lib2.film_nice_thought;
                    animationPackage = $injector.get('film_nice_thought');

                    break;

                case 1: //countdown rakna (orange)

                    // manifest = [];

                    // manifest.push({
                    //     src: "images/ID_bakgrund_orange.jpg",
                    //     id: "ID_bakgrund_orange"
                    // });

                     // manifest.push({src:"sounds/Rakna_baklanges_ovning_med_intro.mp3", id:"Rakna_baklanges_ovning_med_intro"});
                   

                    // animationPackage = lib3.film_countdown02;
                   	animationPackage = $injector.get('film_countdown02');
                    //animationPackage = lib3.film_countdown02;

                    break;

                case 2: //andas (blue) //film_deep_breath02
                    //default:
                    console.log("Called");
                    // manifest = [];
                    // manifest.push({
                    //     src: "images/ID_bakgrund_blue.jpg",
                    //     id: "ID_bakgrund_blue"
                    // });
                    // manifest.push({
                    //     src: "sounds/Andas_djupt_ovning_med_intro.mp3",
                    //     id: "Andas_djupt_ovning_med_intro"
                    // });
                    // animationPackage = lib1.film_deep_breath02;
                    animationPackage = $injector.get('film_deep_breath02');

                    break;

            }

            this.init(animationPackage, manifest);

        }
        //
        // window.onresize = function()
        // {
        //      this.onResize();
        // }

    }
]);

SERVICE_MODULE
.service('AuthService', [

    function() {

        var isAuthenticated = false; //false;

        this.setAuthenticated = function(value) {
            isAuthenticated = value;
        };

        this.isAuthenticated = function() {
            //return true;
            return isAuthenticated;
        };

    }
])

.factory('AuthenticationService', ['$rootScope', 'WEBSERVICE_BASE_URI', '$http', 'BUPUtility', 'authService', 'BUPLoader', 'BUPUser', 'BUPConfig', 'BUPWebservice', '$httpBackend','CordovaNetworkAsync',
    function($rootScope, WEBSERVICE_BASE_URI, $http, BUPUtility, authService, BUPLoader, BUPUser, BUPConfig, BUPWebservice, $httpBackend,CordovaNetworkAsync) {


        // var tasks = window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) || {
        //     practices: [],
        //     homeworks: []
        // };

        
        var service = {
            login: function(user,loginOption) {
                CordovaNetworkAsync.isOnline().then(function(isConnected) {
                    if (isConnected) {

                       //force remote login
                        service.doLogin(user,{forceRemoteLogin: true })


                    } else {
                        //normal login in which local login has the first preverence
                        service.doLogin(user,{forceRemoteLogin: false })
                    }

                }, function(err) {
                    console.log(err);
                });

            },
            doLogin: function(user, loginOption) {
                loginOption = loginOption || {
                    forceRemoteLogin: false //parameter to avoid trying to authenticate user locally 
                };

                console.log('authentication started')
                ///api/login
                ///
                console.log(user);

                if (!user.Username && !user.UserPassword) {

                    return false;
                }

                //make sure the user info exists locally
                //ie if the user has previously logged in
                var localUser = BUPUser.getCurrentUser();


                //use cases for local login and remote login
                //Go for local login when
                //1. informations like local user id username token are available
                //2. the username with which user is trying to login is available locally

                //Go for remote login when
                //1. when the username the user is trying to login is not avilable locally

                var tryLocalLogin = false;

                if (loginOption.forceRemoteLogin === false && !!localUser.id && !!localUser.username && !!localUser.tokenKey) {
                    tryLocalLogin = true;
                }

                if (tryLocalLogin === true) {
                    //more conditions to consider remote login 

                    // someone previouslylogged in but trying to login as a different user this time
                    if (String(user.Username).toLowerCase() !== String(localUser.username).toLowerCase()) {
                        tryLocalLogin = false;
                    }
                }


                //if (con)

                if (tryLocalLogin === true) {
                    console.log("loggin in locally");
                    //data available locally!
                    //do not require remote login ie can login without internet Internet

                    //sample localUser Object
                    // var curUser = {
                    //     id: data.UserId,
                    //     name: data.data.FullName,
                    //     username: data.data.UserName,
                    //     password: BUPUtility.encode(user.UserPassword),
                    //     roleId: data.data.RoleId,
                    //     roleName: data.data.RoleName,
                    //     tokenKey: data.TokenKey,
                    //     saveDataInServer: data.data.saveDataInServer,
                    //     isLoggedIn: true
                    // };
                    if (String(user.Username).toLowerCase() !== String(localUser.username).toLowerCase()) {
                        var errorMessage = "Invalid username";
                        //BUPInteraction.toast(errorMessage, 'short');
                        $rootScope.$broadcast('event:auth-login-failed', errorMessage);
                        return false;
                    } else if (user.UserPassword !== BUPUtility.decode(localUser.password)) {
                        var errorMessage = "Invalid password";
                        //BUPInteraction.toast(errorMessage, 'short');
                        $rootScope.$broadcast('event:auth-login-failed', errorMessage);
                        return false;
                    } else {
                        //local login successful


                        localUser.isLoggedIn = true;
                        BUPUser.setCurrentUserInfo(localUser);

                        $rootScope.currentUser = localUser;
                        $rootScope.currentUserConfig = BUPConfig.initUserConfig(localUser.id);

                        $http.defaults.headers.common.Authorization = localUser.tokenKey;

                        authService.loginConfirmed(localUser, function(config) { // Step 2 & 3
                            //config.headers.Authorization = data.user.authorizationToken;
                            config.headers.Authorization = localUser.tokenKey;
                            return config;
                        });


                    }



                } else {
                    console.log("loggin in remotely");

                    var params = BUPUtility.buildQuery(user);
                    $http.get(WEBSERVICE_BASE_URI + '/Login/ValidateUser?' + params, {
                        ignoreAuthModule: true
                    })
                        .success(function(data, status, headers, config) {
                            console.log(data);

                            if (data.status == "ok" && !!data.TokenKey) {
                                //$http.defaults.headers.common.Authorization = data.user.authorizationToken; // Step 1
                                $http.defaults.headers.common.Authorization = data.TokenKey; // Step 1

                                /*
                        Sample data obtained from the webservice:

                        {
                            TokenKey: "<tokenkey>",
                            UserId: "<userid>",
                            data:{
                                UserName:"Patient21",
                                saveDataInServer:'boolean true/false'
                            },
                            status: "ok"
                        }

                        */


                                var curUser = {
                                    id: data.UserId,
                                    name: data.data.FullName,
                                    username: data.data.UserName,
                                    password: BUPUtility.encode(user.UserPassword),
                                    roleId: data.data.RoleId,
                                    roleName: data.data.RoleName,
                                    tokenKey: data.TokenKey,
                                    saveDataInServer: data.data.saveDataInServer,
                                    useMeeting:Boolean(data.data.useMeeting),
                                    isLoggedIn: true
                                };

                                //FullName: "Patient 21"
                                //RoleId: 3
                                //RoleName: "Patient"
                                //UserName: "patient21"
                                //saveDataInServer: false



                                //BUPUser.setCurrentUserInfo(data.user);
                                BUPUser.setCurrentUserInfo(curUser);

                                $rootScope.currentUser = curUser;
                                $rootScope.currentUserConfig = BUPConfig.initUserConfig(curUser.id);


                                //$rootScope.currentUser = data.user;

                                // Need to inform the http-auth-interceptor that
                                // the user has logged in successfully.  To do this, we pass in a function that
                                // will configure the request headers with the authorization token so
                                // previously failed requests(aka with status == 401) will be resent with the
                                // authorization token placed in the header
                                authService.loginConfirmed(curUser, function(config) { // Step 2 & 3
                                    //config.headers.Authorization = data.user.authorizationToken;
                                    config.headers.Authorization = data.TokenKey;

                                    return config;
                                });

                                console.log("here");
                            } else {
                                console.log(data);
                                console.log("invalid user or token key not obtained");
                                $rootScope.$broadcast('event:auth-login-failed', status);
                            }

                        })
                        .error(function(data, status, headers, config) {
                            console.log('login failed?');
                            BUPLoader.hide();
                            console.log(data);
                            console.log(status);
                            console.log(headers);
                            $rootScope.$broadcast('event:auth-login-failed', status);
                        });
                }




                //end of locally authenticate

            },
            logout: function(user) {
                // $http.post('https://logout', {}, {
                //     ignoreAuthModule: true
                // }).
                // finally(function(data) {
                //     delete $http.defaults.headers.common.Authorization;
                //     $rootScope.$broadcast('event:auth-logout-complete');
                // });

                // $http.post('https://logout', {}, {
                //     ignoreAuthModule: true
                // })
                //     .
                // finally(function(data) {
                //     delete $http.defaults.headers.common.Authorization;
                //     $rootScope.$broadcast('event:auth-logout-complete');
                // });

                //delete the authentication token
                delete $http.defaults.headers.common.Authorization;
                $rootScope.$broadcast('event:auth-logout-complete');
                //remove the current user details

                var oldData = BUPUser.getCurrentUser();
                oldData.isLoggedIn = false;
                BUPUser.setCurrentUserInfo(oldData);

                //remove the old tasks
                //BUPWebservice.setTasks({});

            },
            loginCancelled: function() {
                authService.loginCancelled();
            },
            loggedIn: function() {

            },
            invalidateCurrentToken: function() {
                var oldData = BUPUser.getCurrentUser();
                oldData.tokenKey = '';
                BUPUser.setCurrentUserInfo(oldData);
            }
        };

        return service;

    }
])

.run(['$rootScope', '$state', 'AuthenticationService',  'BUPUser',  
    function($rootScope, $state, AuthenticationService, BUPUser ) {

        $rootScope.user = {};

        $rootScope.signIn = function() {
            AuthenticationService.login($rootScope.user);
        };
       
       $rootScope.$on('$stateChangeStart', function(event, curr, prev) {

            if (
                curr.requireLogin !== false 
                && 
                //!AuthService.isAuthenticated()
                 !BUPUser.isLoggedIn()
                ) {
              
                //console.log(BUPUser);
                //console.log(BUPUser.isLoggedIn());
                //console.log(curr.requireLogin);
                //console.log($rootScope.currentUser);

                 //$state.transitionTo('signin');
                 //console.debug('login required to access this page')
                 $rootScope.$broadcast('event:auth-loginRequired');
                 //BUPInteraction.toast('Login Required','short');
                 event.preventDefault();
            }
        });

        $rootScope.$on('event:auth-loginRequired', function(e, rejection) {
            //.log('event fired: event:auth-loginRequired');
            //BUPLoader.hide();
            $rootScope.$broadcast('loading:hide');
            AuthenticationService.invalidateCurrentToken();
            $state.go('signin');
        });


        $rootScope.$on('event:auth-logout-complete', function() {
            //console.log('event fired: event:auth-logout-complete');
            //console.log('time to refresh');
            
           

            /*
            $state.go('signin', {}, {
                //reload: true,
                //inherit: false
            });
            */
        });


        //  $rootScope.$on('old:event:auth-loginConfirmed', function() {
        //     //console.log('event fired: event:auth-loginConfirmed');
        //     //$scope.username = null;
        //     //$scope.password = null;
        //     // $state.go($state.$current, null, {
        //     //     reload: true
        //     // });
        //     $rootScope.loginModal.hide();

        //     window._a = $rootScope.currentUser = BUPUser.getCurrentUser();

        //     //track the earliest logged in date
        //     //console.log('time to init the config');

        //     $rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);
            
           


            
        // });

        

    }
]);

SERVICE_MODULE

/**
 * CordovaDialog AngularJS service
 * https://gist.github.com/rvanbaalen/9527418
 * com.apache.cordova.dialogs wrapper for AngularJS with fallback for testing without Cordova
 */
.service('CordovaDialog', ['$window', '$q',
    function($window, $q) {
        var notification = navigator.notification || {
            alert: function(message, alertCallback, title, buttonLabel) {
                return alertCallback($window.alert(title + '\n\n' + message + '\n\nButton: ' + buttonLabel));
            },
            confirm: function(message, confirmCallback, title, buttonLabels) {
                buttonLabels = buttonLabels || ['OK', 'Cancel'];

                return $window.confirm(message);
            },
            prompt: function(message, confirmCallback, title, buttonLabels, defaultValue) {
                return $window.prompt(title + '\n\n' + message + '\n\nButtons:\n' + buttonLabels.join('\n'), defaultValue);
            },
            beep: function(times) {
                var def = $q.defer();

                for (var i = 0; i <= times; i++) {
                    notification.alert('Beep ' + (i + 1));

                    if (i === times) {
                        def.resolve(times);
                    }
                }

                return def.promise;
            }
        };

        return {
            alert: function(message, title, buttonName) {
                var def = $q.defer();
                console.info('Alert triggered');
                var cb = function() {
                    console.info('Alert callback triggered');
                    def.resolve();
                };

                notification.alert(message, cb, title, buttonName);

                return def.promise;
            },

            confirm: function(message, title, buttonName) {
                var def = $q.defer();
                var cb = function(buttonIndex) {
                    switch (buttonIndex) {
                        case 1:
                            // First button is the confirm button
                            def.resolve(true);
                            break;
                        default:
                            def.reject(false);
                    }
                };

                var confirmed = notification.confirm(message, cb, title, buttonName);

                if (confirmed) {
                    def.resolve(true);
                } else {
                    def.resolve(false);
                }

                return def.promise;
            },

            prompt: function(message, title, buttonLabels, defaultValue) {
                var def = $q.defer();
                var cb = function(results) {
                    switch (results.buttonIndex) {
                        case 1:
                            // First button is the confirm button
                            def.resolve(results);
                            break;
                        default:
                            def.reject(false);
                    }
                };

                var confirmed = notification.prompt(message, cb, title, buttonLabels, defaultValue);

                if (confirmed && confirmed !== null) {
                    def.resolve(true);
                } else {
                    def.resolve(false);
                }

                return def.promise;
            },

            beep: function(times) {
                return notification.beep(times);
            }
        };
    }
])

;
        /**
         * @todo
         * research more on sounds
         * seems we got more robust libraries try best not to reinvent the wheel
         * try these
         * soundmanager2 ( looks like tries to break cross device incompativilites and bugs)
         * (good news we also have angular directive to this https://github.com/saygoweb/ng-soundmanager2)
         * www.schillmania.com/projects/soundmanager2
         */ 
        /**
         * html5 audio properties
         *
         * currentTime
         * duration
         * muted
         * paused
         * volume
         *
         */

        /*
.factory('BUPAudio', ['$document',
    function($document) {

        //reference http://html5doctor.com/native-audio-in-the-browser
        var audioElement = $document[0].createElement('audio');
        //supportsHtml5Audio = !! audioElement.canPlayType,
        //supportsMp3 = supportsHtml5Audio && "" != audioElement.canPlaytype("audio/mpeg")
        ;




        var seekbar = $document[0].getElementById('seekbar');

        //setup seekbar when the new sound is loaded (duration is changed)
        audioElement.ondurationchange = this.setupSeekbar;

        //when seekbar is updated reflect in the audio
        //seekbar.onchange = this.seekAudio;

        //as the audio goes on playing update the seekbar
        //audioElement.ontimeupdate = this.updateSeekbarPosition;





  
        return {
            audioElement: audioElement,
            setupSeekbar: function() {
                //seekbar.max = audioElement.duration;
            },
            seekAudio: function() {
                //audioElement.currentTime = seekbar.value;
            },
            updateSeekbarPosition: function() {
                console.log(seekbar.value);
                //seekbar.value = audioElement.currentTime;
            },
            load: function(filename) {
                audioElement.src = filename;
                audioElement.load(); //required for 'older' browsers



            },

            play: function(filename) {

                audioElement.play();
            },
            pause: function() {
                console.log("pausing todo");
                audioElement.pause();
                //audioElement.pause();
            }

        };
    }
])
*/

var DynamicTextClass = function(entry) {
    //the constructor function
    this.code = entry.Code;
    this.modified = entry.ModifyDateTime;
    this.key = entry.OverallTextName;
    this.value = entry.OverallTextDescription;
    return this;
};

SERVICE_MODULE

.value('DynamicTextClass', DynamicTextClass)
/**
 * Dynamic Text Service
 *
 * This service will get the different dynamic text from the server through a webservice
 * and serve the text based on a key
 *
 * This can have some fallback text in case the text has not been downloaded yet
 *
 * Dependency
 *  1. $rootScope.isOnline var to know if there is internet connection or not
 *  2.
 */

.factory('DynamicTextCache', ['$cacheFactory',
    function($cacheFactory) {
        return $cacheFactory('DynamicText');
    }
])


.factory('DynamicTextService', ['$q', '$http', 'DynamicTextCache', 'WEBSERVICE_BASE_URI', 'BUPUtility', 'CordovaNetworkAsync', '$rootScope', 'DynamicTextClass',
    function($q, $http, DynamicTextCache, WEBSERVICE_BASE_URI, BUPUtility, CordovaNetworkAsync, $rootScope, DynamicTextClass) {
        //var entries;    
        var service = {

            getAllFromLocalDB: function() {

                var entries = [];
                try {
                    entries = JSON.parse(window.localStorage['dynamicTexts']);
                } catch (e) {

                }
                return entries;
            },

            saveToLocalDB: function(entriesToSave) {
                window.localStorage['dynamicTexts'] = JSON.stringify(entriesToSave);
            },

            /**
             * [loadToCache description]
             * @param entries optonal if supplied save the same object to cache
             */
            loadToCache: function(entries) {

                var entries = entries || service.getAllFromLocalDB();
                for (var i = 0; i < entries.length; i++) {
                    /**
                     * eg
                     * WELCOME_SCREEN_TEXT_KEY: 'title as seen on the admin'
                     * WELCOME_SCREEN_TEXT: <actual description>
                     */
                    DynamicTextCache.put(entries[i]['code'] + '_KEY', entries[i]['key']);
                    DynamicTextCache.put(entries[i]['code'], entries[i]['value']);

                }
                console.debug('dynamic texts loaded to cache');

            },

            mergeRecentlyUpdatedTexts: function(recentlyUpdated) {
                if (!recentlyUpdated.length) return;

                var allEntries = service.getAllFromLocalDB();
                if (allEntries.length) {
                    angular.forEach(recentlyUpdated, function(freshEntry) {
                        // var index = 
                        var index = _.findIndex(allEntries, function(elem) {
                            return typeof elem['code'] !== "undefined" && elem['code'] === freshEntry['code'];
                        });
                        if (index > -1) {
                            allEntries[index] = freshEntry;
                        } else {
                            allEntries.push(freshEntry)
                        }
                    })
                } else {
                    allEntries = recentlyUpdated;
                }
                service.loadToCache(allEntries);
                service.saveToLocalDB(allEntries);

            },

            getLastModifiedDate: function() {
                var lastModified = 0; //lastResort
                var entries = service.getAllFromLocalDB();
                if (entries.length) {
                    var lastModifiedEntry = _.max(entries, function(entry) {
                        return parseInt(entry.modified, 10);
                    });
                    if (lastModifiedEntry.modified) {
                        lastModified = lastModifiedEntry.modified;
                    }
                }
                return lastModified;

            },

            fetchFromServer: function(all) {
                all = all || false;

                var deferred = $q.defer();
                CordovaNetworkAsync.isOnline()
                    .then(function(isConnected) {
                        if (isConnected) {
                            var lastModifiedDate = (all === false) ? service.getLastModifiedDate() : 0;
                            //@todo calculate lastModifiedDate

                            var webserviceurl = WEBSERVICE_BASE_URI + '/Login/OverallText?logdatetime=' + lastModifiedDate;
                            console.log('dynamic text fetching started');
                            $http.get(webserviceurl).then(function(rawResponse) {
                                if (rawResponse.data && rawResponse.data.status == "ok" && rawResponse.data.data && rawResponse.data.data.length) {
                                    var length = rawResponse.data.data.length;
                                    var entries = [];
                                    for (var i = 0; i < length; i++) {
                                        var entry = new DynamicTextClass(rawResponse.data.data[i]);
                                        entries.push(entry);
                                    }
                                    service.mergeRecentlyUpdatedTexts(entries);
                                    deferred.resolve(entries);
                                    console.log(length + ' updated dynamic texts found');

                                } else {
                                    deferred.reject('no new texts');
                                    console.log(' No new updated dynamic texts found');
                                }

                            });
                        } else {
                            deferred.reject('No internet to pull the dynamic texts');
                            console.log('dynamic texts fetching aborted: no internet');
                        }
                    });


                return deferred.promise;


            },

            getFallbackDynamicText: function(code) {
                var fallbacks = [{
                    "code": "ABOUT_THE_APP",
                    "key": "Om",
                    "value": "<p>Hemuppgiften underlättar din egen träning och repetition när du har en kontakt med behandlare inom Barn- och ungdomspsykiatrin (BUP) i Stockholm.<br> <br>I Hemuppgiften kan du: </p> <ul><li>Träna på & nbsp;nya beteenden </li> <li>Repetera innehåll eller övningar som du gått igenom med din behandlare </li> <li>Bli påmind om när du ska träna eller gå igenom hemuppgifter </li></ul><p><br>För att kunna använda det här verktyget behöver du användarnamn och lösenord som du får från din behandlare. </p>"
                }, {
                    "code": "WELCOME_SCREEN_TEXT",
                    "modified": 635511115519000000,
                    "key": "Welcome",
                    "value": "<h3 style=\"margin: 0px; line-height: 150%; font-family: arial; font-size: 17px;\">\n\t<strong>V&auml;lkommen!</strong></h3>\n<p>\n\tInneh&aring;llet i den h&auml;r appen kommer att ut&ouml;kas efter varje tr&auml;ff. Efter n&aring;gra g&aring;nger kommer du att kunna tr&auml;na p&aring; &rdquo;ilsked&auml;mpare&rdquo; som ska ligga &ouml;verst som tre knappar. Hemuppgifterna baseras p&aring; ditt eget inneh&aring;ll. Inst&auml;llningar f&ouml;r p&aring;minnelser g&ouml;r du p&aring; v&aring;ra m&ouml;ten. Ta kontakt om du har en fr&aring;ga eller om du vill &auml;ndra p&aring; n&aring;got inneh&aring;ll. N&auml;r en ny hemuppgift lagts in beh&ouml;ver du l&auml;sa igenom alla steg och i sista steget intygar du om texten st&auml;mmer eller inte.</p>\n"
                }, {
                    "code": "CORRECT_TEXT",
                    "modified": 634789912742366700,
                    "key": "Agree",
                    "value": "<p>  <strong>Ditt svar: </strong>St&auml;mmer</p> <p>  Nu &auml;r du klar med hemuppgiften, uppgifterna kommer att finnas kvar och du kan l&auml;sa igenom det n&auml;r du vill.</p> <p>  Efter n&auml;sta m&ouml;te kommer en ny hemuppgift.</p> "
                }, {
                    "code": "INCORRECT_TEXT",
                    "modified": 634807473868366700,
                    "key": "Do not agree",
                    "value": "<p>  <strong>Ditt svar: </strong>St&auml;mmer inte</p> <p>  Vid n&auml;sta m&ouml;te kan du &auml;ndra texten med din kontaktperson.</p> <p>  Nu &auml;r du klar med hemuppgiften, uppgifterna kommer att finnas kvar och du kan l&auml;sa igenom det n&auml;r du vill.</p> <p>  Efter n&auml;sta m&ouml;te kommer en ny hemuppgift.</p> "
                }, {
                    "code": "QUESTION_AT_THE_END",
                    "modified": 634807385954766700,
                    "key": "Ask at the end",
                    "value": "<p>  St&auml;mmer beskrivningen med<br />  det som vi gick igenom?</p> "
                }, {
                    "code": "FIRST_REMINDER_TEXT",
                    "modified": 634807388696000000,
                    "key": "first reminder ",
                    "value": "Det vi pratade om sist finns nu i din app. Klar att godkänna."
                }, {
                    "code": "SECOND_REMINDER_TEXT",
                    "modified": 635469797831200000,
                    "key": "second reminder ",
                    "value": "Det vi pratade om sist finns nu i din app. Klar att godkänna.."
                }, {
                    "code": "THIRD_REMINDER_TEXT",
                    "modified": 634807389278966700,
                    "key": "third reminder ",
                    "value": "Det vi pratade om sist finns nu i din app. Klar att godkänna."
                }, {
                    "code": "HOMEWORK_MESSAGE1",
                    "modified": 634836817294600000,
                    "key": "first homework ",
                    "value": "<p>  <strong>G&ouml;r den f&ouml;rsta hemuppgiften!</strong></p> <p>  Det h&auml;r &auml;r den f&ouml;rsta hemuppgiften, l&auml;s igenom och kontrollera att allt st&auml;mmer.</p> <p>  　</p> <p>  　</p> "
                }, {
                    "code": "FEEDBACK_TEXT1",
                    "modified": 634837268705533300,
                    "key": "Feedback text 5 percent ",
                    "value": "<p>  Bra att du har kommit ig&aring;ng!</p> "
                }, {
                    "code": "FEEDBACK_TEXT2",
                    "modified": 634872623291266700,
                    "key": "Feedback text 10 percent ",
                    "value": "<p>  Bra! Forts&auml;tt tr&auml;na!</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE2",
                    "modified": 634858907587766700,
                    "key": "anger Suppressors ",
                    "value": "<p>  <strong>Dags att b&ouml;rja tr&auml;na</strong>!</p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na och n&auml;r du vill ha p&aring;minnelser kom vi &ouml;verens om vid senaste tr&auml;ffen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE3",
                    "modified": 634807475208200000,
                    "key": "Homework message3 ",
                    "value": "<p>  <strong>3 Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om p&aring; v&aring;ra m&ouml;ten. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE4",
                    "modified": 634776632726266600,
                    "key": "Homework message4 ",
                    "value": "<p>  <strong>4 Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HELP_TEXT",
                    "modified": 635511115273300000,
                    "key": "If the app (in the symbol key) ",
                    "value": "<p>\n\ttest new fdfdf</p>\n"
                }, {
                    "code": "REMINDER",
                    "modified": 634775831397200000,
                    "key": "Reminder Text ",
                    "value": "Dags att träna på dina ilskedämpare."
                }, {
                    "code": "HOMEWORK_MESSAGE5",
                    "modified": 634776480000000000,
                    "key": "Homework message5 ",
                    "value": "<p>  <strong>5 Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE6",
                    "modified": 634776638625000000,
                    "key": "Homework message6",
                    "value": "<p>  <strong>6. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE7",
                    "modified": 634776638755799900,
                    "key": "Homework message7",
                    "value": "<p>  <strong>7. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE8",
                    "modified": 634776638854999900,
                    "key": "Homework message8",
                    "value": "<p>  <strong>8. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE9",
                    "modified": 634776638953133300,
                    "key": "Homework message9",
                    "value": "<p>  <strong>9. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "HOMEWORK_MESSAGE10",
                    "modified": 634776639044700000,
                    "key": "Homework message10",
                    "value": "<p>  <strong>10. Nu finns det tre ilsked&auml;mpare att tr&auml;na p&aring;! </strong></p> <p>  Nu finns tre ilsked&auml;mpare som tre knappar &ouml;verst p&aring; startsidan.</p> <p>  I f&ouml;rsta versionen inneh&aring;ller ilsked&auml;mparna ljudinstruktioner, efter ett tag, n&auml;r du l&auml;rt dig hur du g&ouml;r, ers&auml;tts de med ilsked&auml;mpare utan ljudinstruktioner.</p> <p>  Hur m&aring;nga g&aring;nger du ska tr&auml;na har du kommit &ouml;verens om med din kontaktperson. Varje g&aring;ng du beh&ouml;ver tr&auml;na f&aring;r du en p&aring;minnelse fr&aring;n mobilen.</p> "
                }, {
                    "code": "FEEDBACK_TEXT3",
                    "modified": 634872623630333300,
                    "key": "Feedback text 35 percent ",
                    "value": "<p>  Bra! Nu har du tr&auml;nat en tredjedel!</p> "
                }, {
                    "code": "FEEDBACK_TEXT4",
                    "modified": 634872623985033300,
                    "key": "Feedback text 60 percent ",
                    "value": "<p>  Tv&aring; tredjedelar! Bra jobbat!</p> "
                }, {
                    "code": "THIS_IS_TEST",
                    "modified": 635422906267500000,
                    "key": "this is test",
                    "value": "<p>\n\tthis is test</p>\n"
                }, {
                    "code": "FOURTH",
                    "modified": 635469832776600000,
                    "key": "Fourth",
                    "value": "this is fourth reminder"
                }, {
                    "code": "FEEDBACK_TEXT_70_PERCENT",
                    "modified": 635469833768700000,
                    "key": "Feedback text 70 Percent",
                    "value": "<p>\n\tgood going</p>\n"
                }, {
                    "code": "FEEDBACK_TEXT_100_PERCENT_",
                    "modified": 635469834439600000,
                    "key": "Feedback text 100 percent ",
                    "value": "<p>\n\tBra! Nu har du tr&auml;nat klart! Stapeln &auml;r full!</p>\n"
                }];

                var fallbackEntries = _.filter(fallbacks, function(fallbackEntry) {
                    return fallbackEntry.code == code;
                });
                if (fallbackEntries.length) {
                    return fallbackEntries[0];
                } else {
                    console.log('not found in fallback');
                    return {};
                }
            },

            getTitle: function(code) {
                if (!code.length) {
                    console.error('Invalid dynamic text code');
                    return '';
                }
                var internalCode = code + '_KEY';
                var title = DynamicTextCache.get(internalCode);
                if (title) {
                    console.log('found' + title);
                    return title;
                } else {
                    console.log('dynamic text looking for fallback for CODE :'+code)
                    var fallbackEntry = service.getFallbackDynamicText(code);
                    return fallbackEntry.key;

                }

            },

            getContent: function(code) {
                if (!code.length) {
                    console.error('Invalid dynamic text code');
                    return '';
                }
                var description = DynamicTextCache.get(code);
                if (description) {
                    return description;
                } else {
                    var fallbackEntry = service.getFallbackDynamicText(code);
                    return fallbackEntry.value;
                }

            },

        }
        window.dt = service;
        window.dc = DynamicTextCache;
        return service;
    }
])

.run(['DynamicTextService', '$rootScope', '$ionicPlatform','$ionicModal', 'CordovaNetworkAsync', 'BUPConfig',
    function(DynamicTextService, $rootScope, $ionicPlatform,$ionicModal, CordovaNetworkAsync, BUPConfig) {

        DynamicTextService.loadToCache();

        var checkForUpdate = function() {
            CordovaNetworkAsync.isOnline()
                .then(function(isConnected) {
                    if (isConnected) {
                        DynamicTextService.fetchFromServer();
                    }
                });
        };

        var showWelcomeMessage = function() {
            $ionicModal.fromTemplateUrl('html/partials/modals/welcome.modal.html', {
                scope: $rootScope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                
                $rootScope.welcomeModal = modal;
                $rootScope.welcomeModal.title = DynamicTextService.getTitle('WELCOME_SCREEN_TEXT');
                $rootScope.welcomeModal.content = DynamicTextService.getContent('WELCOME_SCREEN_TEXT');
                $rootScope.welcomeModal.close = function() {
                    $rootScope.welcomeModal.hide();
                    $rootScope.welcomeModal.remove();
                };
                $rootScope.welcomeModal.show();
                console.log($rootScope.welcomeModal)
                $rootScope.currentUserConfig.welcomed = true;
                BUPConfig.updateCurrentUserConfig($rootScope.currentUserConfig);
            });
        };

        $rootScope.$on('app.userready', function() {
            if ($rootScope.currentUserConfig && $rootScope.currentUserConfig.welcomed !== true) {
                showWelcomeMessage();
            }
            
        });

        $ionicPlatform.ready(function() {
            
            checkForUpdate();
            
            //$rootScope.currentUserConfig = BUPConfig.getCurrentUserConfig();
             


            // if (BUPUser.isLoggedIn()) {

            //     if ($rootScope.currentUserConfig && $rootScope.currentUserConfig.welcomed !== true) {
            //         showWelcomeMessage();
            //     }


            // }else {



            // }


            //console.debug(currentUserConfig);
        });

        $ionicPlatform.on('resume', checkForUpdate);


    }
])
SERVICE_MODULE
.factory('BUPFiles', ['$q', '$rootScope', 'BUPWebservice', '$cordovaFile', 'filesystem', 'BUPUtility',
    function($q, $rootScope, BUPWebservice, $cordovaFile, filesystem, BUPUtility) {
        var allFiles = [];
        if (window.localStorage['_files']) {
            try {
                allFiles = JSON.parse(window.localStorage['_files']);
                console.log(JSON.stringify(allFiles));
                //ionic.Platform.
                //$cordovaFile.createDir('SoundTasks',true);

            } catch (e) {

            }
        }

        try{
            
            if(ionic.Platform.isWebView()) {
                ionic.Platform.ready(function(){
                    $cordovaFile.createDir('SoundTasks',true);
                });
            }
            
            
        }catch(e){
            console.error(e);
        }
        var _private = {};


        window.$cordovaFile = $cordovaFile;

        _private.getFilesystem = function() {
            var q = $q.defer();
            // Note: The file system has been prefixed as of Google Chrome 12:
            window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 1024 * 1024, function(filesystem) {
                    window._filesystem = filesystem;
                    q.resolve(filesystem);
                },
                function(err) {
                    q.reject(err);
                });

            return q.promise;
        };




        //$rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask);

        var service = {
            /**
             * returns object with local and remote url
             * Important note: it doesn't check the local file is present or not
             *
             * @param  {[type]} fileUrl [description]
             * @return {[type]}         [description]
             */
            getFileOld: function(fileUrl) {
                //fileUrl = fileUrl.split(' ').join('_');

                var deferred = $q.defer();
                var promises = [];

                //deferred.resolve('http://109.74.12.180/Hemuppgiften2.0/Uploads/Sound/f2c1271d-ad4a-4f78-8e5d-79cf3dbdfd18_Rakna_baklanges_ovning_med_intro.mp3');
                var files = _.filter(allFiles, function(aFile) {
                    return aFile.remote == fileUrl
                });

                console.log('found files matching with ' + fileUrl + ' are');
                console.log(JSON.stringify(files));

                if (files.length) {

                    var theFile = files[files.length - 1];



                    //also check if the file exists
                    try {

                        // var dir = cordova.file.documentsDirectory || cordova.file.externalRootDirectory;
                        // $cordovaFile.checkFile(dir+theFile.local).then(
                        //     function(){
                        //         //file exists locally
                        //        deferred.resolve(theFile.local);
                        //     },
                        //     function(){
                        //         //file doesn't exists locally
                        //        deferred.resolve(theFile.remote);
                        //        //remove this entry
                        //     }
                        // );

                    } catch (e) {

                        // if we can't be sure the file doesn't exist locally then load it from the remote
                        //deferred.resolve(fileUrl);

                    }
                    deferred.resolve(theFile);


                    
                    if (files.length > 1) {


                        allFiles = _.uniq(allFiles,function(a){
                            return a.remote; 
                        });
                        service.saveToLocalStorage();

                        /*
                        console.log('lots of files!!!')
                        var i = 0;
                        for (i; i < files.length; i++) {
                            _.remove(files, files[i]);
                            console.log('removing ' + files[i].local);
                        }
                        */

                    }
                    
                    


                } else {

                    deferred.resolve({
                        local: null,
                        remote: fileUrl
                    });
                }

                return deferred.promise;

            },
            getFile: function(fileUrl){

                var deferred = $q.defer();
                var responseFile = {
                        local: null,
                        remote: fileUrl
                    };
                if(ionic.Platform.isWebView()) {
                    var dir;
                    if(ionic.Platform.isIOS()){
                        dir = cordova.file.documentsDirectory;
                    }else if(ionic.Platform.isAndroid()){
                        dir = cordova.file.externalRootDirectory;
                    }
                    var fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
                    $cordovaFile.checkFile('SoundTasks/'+fileName)
                    .then(function(){
                        responseFile.local = dir +'SoundTasks/'+ fileName;
                        deferred.resolve(responseFile);
                    },function(){
                        deferred.resolve(responseFile);
                    });

                }else{
                    deferred.resolve(responseFile);
                }
                
                return deferred.promise;

            },
            getFileOrig: function(fileUrl){
                var deferred = $q.defer();
                var fileName = fileURI.substring(fileURI.lastIndexOf('/') + 1);

               window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
               fileSystem.root.getDirectory("SoundTasks", {
                       create: true
               }, function(directory) {

                    var directoryReader = directory.createReader();
                    directoryReader.readEntries(function(entries) {
                        var i;
                        for (i=0; i<entries.length; i++) {
                            console.log(entries[i].name);
                        }
                    }, function (error) {
                        alert(error.code);
                    });

                   } );
                }, function(error) {
                   alert("can't even get the file system: " + error.code);
                });

            },
            saveFile: function(fileURI, saveOptions) {
                var deferred = $q.defer();
                if(!ionic.Platform.isWebView()) {
                    deferred.reject('escape saving in browser' );
                    return deferred.promise;
                }

                console.log('trying to save the file ' + fileURI);
                saveOptions = angular.extend({
                    forceFromServer: false,
                    forceSaveAgain: false
                }, saveOptions);


                //tasks 
                //1 download the file
                //2 save the local path with the corresponding url in the locastorage




                
                window.$cordovaFile = $cordovaFile;
                try {



                    console.log(fileURI);
                    window.fileURI = fileURI;


                    var fileName = fileURI.substring(fileURI.lastIndexOf('/') + 1);

                    var dir = cordova.file.documentsDirectory || cordova.file.externalRootDirectory;
                    var filePath = dir +'SoundTasks/'+ fileName;
                    //var filePath = fileName;




                    if (filePath && saveOptions.forceSaveAgain !== true ) {

                        //BUPUtility.UrlExists(filePath).then(function(resp){
                        service.getFile(fileURI).then(function(resp){
                            console.log(resp);
                            if( resp && resp.local){
                                console.log('url exists response');
                                console.log(resp);
                                console.debug("file: " + filePath + " has already been saved before! No need to save again");
                                deferred.reject('File:' + filePath + ' already exists.'); 
                                return deferred.promise;  

                            }else{
                                //file does not exists
                            }
                            
                            
                        },function(){
                            //alert('file:'+filePath+' already')
                            console.log('file doens\'t exist already so need to download it');
                            //cannot read the file (asume the file does not exist already)
                            //deferred.reject('File:' + filePath + ' already exists.'); 
                            //return deferred.promise;  
                        })

                        
                        

                    }

                    


                    // var files = _.filter(allFiles, function(aFile) {
                    //     return aFile.remote == fileURI
                    // });


                    $cordovaFile.downloadFile(fileURI, filePath, true).then(function(result) {
                        console.log('result of file download here:');
                        console.log(result);

                        //console.log('download complete to url: ' + result.toURL());
                        console.log('download complete to url: ' + result.fullPath);
                        //alert(fileURI+'='+filePath);
                        /*
                        $cordovaFile.checkFile(result.toURL()).then(function(result) {
                            //success!
                            console.log('File Success!');

                            deferred.resolve(result.toURL());

                        }, function(err) {
                            console.log(err);
                            deferred.reject('error reading the downloaded file!');
                            console.error('error reading the downloaded file!')
                        })
                        */

                        //deferred.resolve(result.toURL());
                        deferred.resolve(result.fullPath);


                    }, function(err) {
                        deferred.reject('error in downloading file');
                        console.log('error in downloading file');
                        console.log("error code" + err.code);
                        console.error(err);
                    });


                } catch (e) {
                    deferred.reject('some error occured' + e);

                }
                return deferred.promise;



            },
            saveFileOld: function(fileURI, saveOptions) {
                console.log('save file called');
                //if( !isPhoneGap) return;

                console.log('trying to save the file ' + fileURI);
                saveOptions = angular.extend({
                    forceFromServer: false,
                    forceSaveAgain: false
                }, saveOptions);


                //tasks 
                //1 download the file
                //2 save the local path with the corresponding url in the locastorage




                var deferred = $q.defer();
                window.$cordovaFile = $cordovaFile;
                try {



                    console.log(fileURI);
                    window.fileURI = fileURI;


                    //_private.getFilesystem().then(

                    var fileName = fileURI.substring(fileURI.lastIndexOf('/') + 1);

                    //fileName = fileName.split(' ').join('_');

                    //var dir = cordova.file.documentsDirectory;
                    //console.log(JSON.stringify(cordova.file));
                    

                    //console.log(JSON.stringify(allFiles));

                    //var dir = cordova.file.dataDirectory || cordova.file.externalRootDirectory;
                    var dir = cordova.file.documentsDirectory || cordova.file.externalRootDirectory;
                    var filePath = dir + fileName;
                    //var filePath = fileName;




                    if (filePath && saveOptions.forceSaveAgain !== true ) {

                        BUPUtility.UrlExists(filePath).then(function(resp){
                            console.log('url exists response');
                            console.log(resp);
                            console.debug("file: " + filePath + " has already been saved before! No need to save again");
                            deferred.reject('File:' + filePath + ' already exists.'); 
                            return deferred.promise;  

                        },function(){
                            //alert('file:'+filePath+' already')
                            console.log('file doens\'t exist already so need to download it');
                            deferred.reject('File:' + filePath + ' already exists.'); 
                            return deferred.promise;  
                        })

                        
                        

                    }

                    


                    var files = _.filter(allFiles, function(aFile) {
                        return aFile.remote == fileURI
                    });

                    //var localFilePath = fileSystem.root.toURL();

                    $cordovaFile.downloadFile(fileURI, filePath, true).then(function(result) {
                        console.log('result of file download here:');
                        console.log(result);

                        //console.log('download complete to url: ' + result.toURL());
                        console.log('download complete to url: ' + result.fullPath);
                        //alert(fileURI+'='+filePath);
                        /*
                        $cordovaFile.checkFile(result.toURL()).then(function(result) {
                            //success!
                            console.log('File Success!');

                            deferred.resolve(result.toURL());

                        }, function(err) {
                            console.log(err);
                            deferred.reject('error reading the downloaded file!');
                            console.error('error reading the downloaded file!')
                        })
                        */

                        //deferred.resolve(result.toURL());
                        deferred.resolve(result.fullPath);


                    }, function(err) {
                        deferred.reject('error in downloading file');
                        console.log('error in downloading file');
                        console.log("error code" + err.code);
                        console.error(err);
                    });


                } catch (e) {
                    deferred.reject('some error occured' + e);

                }
                return deferred.promise;



            },
            saveToLocalStorage: function() {
                allFiles = _.uniq(allFiles,function(a){
                    return a.remote
                });
                window.localStorage['_files'] = JSON.stringify(allFiles);
            },
            /**
             * This service automatically looks for an audio file in the current practice tasks and
             * saves them locally!
             * @return {[type]} [description]
             */
            audioDaemon: function() {
                console.log('running the audio daemon');
                //console.time('audioDaemon');

                var allTasks = BUPWebservice.getAllTasks({

                    forceFromServer: false,

                    isBackgroundTask: true

                }).then(function(existingTasks) {

                    if (!existingTasks.hasOwnProperty('practices')) {
                        existingTasks.practices = [];
                    }

                    window.existingTasks = existingTasks;

                    var soundFiles = _(existingTasks.practices).filter(function(soundTask) {
                        return soundTask.hasOwnProperty('type') && soundTask.type === "sound";
                    }).pluck('audio_file').valueOf();
                    console.log(soundFiles);
                    window.soundFiles = soundFiles;


                    angular.forEach(soundFiles);

                });

                //console.timeEnd('audioDaemon');

            },
            saveMp3sFromTasks: function(existingTasks) {
                
                if( !isPhoneGap) return;

                if (!existingTasks.hasOwnProperty('practices')) {
                    existingTasks.practices = [];
                }

                if (!existingTasks.practices.length) {

                    console.log('no sound tasks here!');
                    return;
                }

                console.log(JSON.stringify(allFiles));


                console.log('running saveMp3sFromTasks');
                window.existingTasks = existingTasks;

                var soundFiles = _(existingTasks.practices).filter(function(soundTask) {
                    return soundTask.hasOwnProperty('type') && soundTask.type === "sound";
                }).pluck('audio_file').valueOf();
                console.log(soundFiles);
                window.soundFiles = soundFiles;


                angular.forEach(soundFiles, function(soundFile) {


                    //service.saveFile(soundFile).then(function(soundFile) {
                    service.saveFile(soundFile).then(function(localFile) {
                        console.log('going to save the files');
                        var theFile = {
                            remote: encodeURI(soundFile),
                            local: localFile
                        };

                        var oldIndex = _.findIndex(allFiles, theFile);
                        console.log('old index:'+oldIndex);
                        
                        var oldRecord = _.filter(allFiles, function(a){
                            return a.remote == theFile.remote && a.local  == theFile.local;
                        });

                        if(!oldRecord.length) {

                            allFiles.push(theFile);
                            service.saveToLocalStorage();
                        } 

                        /*
                        //check if the localstorage record already has this one
                        if (oldIndex < 0) {
                            //the file doesn't previously exists
                            allFiles.push(theFile);
                        } else {
                            //update the same index   
                            allFiles[oldIndex] = theFile;

                        }

                        console.log(allFiles);
                        */




                        

                    }, function(e) {

                        console.error(e);
                    });
                    //});


                });



                //console.timeEnd('audioDaemon');

            }
        };
        window.serviceFile = service;

        $rootScope.$on('save_audio_locally', function(event, url,options) {
            
            options = options || {};

            if( !isPhoneGap) return;

            console.log('received event save_audio_locally for file:' + url);
            service.saveFile(url,options);


        });

        return service;

    }
]);
SERVICE_MODULE
/**
 * Service for user UI interaction
 * so that they can be controlled centrally
 *
 * gracefully fallback to other available UI when
 * run on web browser
 *
 * dependencies:
 * include ng-cordova.js with custom build including the dependencies
 *
 * @return {[type]} [description]
 */
.factory('BUPInteraction', ['$timeout', '$cordovaDevice', '$cordovaToast', '$cordovaDialogs', '$cordovaVibration', '$ionicLoading', '$ionicPopup', '$ionicActionSheet',
    function($timeout, $cordovaDevice, $cordovaToast, $cordovaDialogs, $cordovaVibration, $ionicLoading, $ionicPopup, $ionicActionSheet) {

        return {
            toast: function(message, duration, position) {
                if (!!message) {

                    duration = duration || 'short';
                    position = position || 'center';

                    //try {
                    if (window.ionic.Platform.isWebView()) {
                        //make sure the duration is always in string either 'short' or 'long'
                        if (duration === parseInt(duration)) {
                            //oh no value provided in integer
                            duration = duration >= 1000 ? 'long' : 'short';

                        }
                        try {

                            $cordovaToast.show(message, duration, position).then(function(success) {
                                // success
                            }, function(error) {
                                // error
                                console.error(message + 'could not be shown');

                            });

                        } catch (err) {
                            $timeout(function() {
                                //make sure the duration is always in number of milliseconds
                                if (duration === 'short') {
                                    duration = 800;
                                } else if (duration == 'long') {
                                    duration = 1500;
                                } else if (duration !== parseInt(duration)) {
                                    duration = 500; //if other invalid strings sent
                                    console.warn('unsupported duration value provided!');
                                }

                                $ionicLoading.show({
                                    template: message,
                                    noBackdrop: true,
                                    duration: duration
                                });


                            }, 0);
                        }


                    } else {
                        $timeout(function() {
                            //make sure the duration is always in number of milliseconds
                            if (duration === 'short') {
                                duration = 800;
                            } else if (duration == 'long') {
                                duration = 1500;
                            } else if (duration !== parseInt(duration)) {
                                duration = 500; //if other invalid strings sent
                                console.warn('unsupported duration value provided!');
                            }

                            $ionicLoading.show({
                                template: message,
                                noBackdrop: true,
                                duration: duration
                            });


                        }, 0);


                    }


                    //} catch (e) {

                    //alert(message);

                    //}



                }
            },
            /**
             * Uses ionic $ionicActionSheet show method
             * returns a function when called hides the action sheet
             *
             */
            confirm: function(actionSheetOpt) {
                return $ionicActionSheet.show(angular.extend({
                    destructiveText: 'Delete',
                    titleText: 'Are you sure?',
                    cancelText: 'Cancel',
                    cancel: function() {
                        // add cancel code..
                    },
                    destructiveButtonClicked: function() {
                        return true;
                    },
                    buttonClicked: function(index) {
                        return true;
                    }
                }, actionSheetOpt));
            },

            /**
             * Alert
             * @param object option
             *  title: Title of the alert
             *  message: Actual Message of the alert box
             *  label: Ok Text for closing the alert box
             *  callback: the callback function fired after
             *
             * @todo implement dynamic label for ok text
             */
            alert: function(option) {
                option = angular.extend({
                    title: '',
                    message: '',
                    callback: angular.noop
                }, option);

                var popup = $ionicPopup.alert({
                    title: option.title,
                    template: option.message
                });
                popup.then(function(res) {
                    option.callback();
                });

                if(option.autoClose) {
                    $timeout(function(){
                        popup.close();
                    },option.autoClose);
                }
            }

        };
    }
])
SERVICE_MODULE

.factory('BUPLoader', ['$rootScope', '$ionicLoading','$translate',
    function($rootScope, $ionicLoading,$translate) {
        var loading,loadingText = 'Laddar';
        $translate('LABEL_LOADING'). then ( function  (text) {
            loadingText = text;
        });
        return {
            show: function(opt) {
                if (loading) return;

                loading = true;

                opt = opt || {};
                var defaultopt = {
                    template: '<i class="icon ion-loading-c"></i>&nbsp;'+loadingText,
                    noBackdrop:false,
                    delay:250
                };

                if (opt.text) opt.template = '<i class="icon ion-loading-c"></i>&nbsp;' + opt.text;
                
                //if (opt.text) opt.template = '<i class="icon ion-loading-c"></i>&nbsp;' + opt.text;

                var finalOption = angular.extend(defaultopt, opt);
                //console.debug(finalOption);

                //$rootScope.loading = 
                $ionicLoading.show(finalOption);
            },
            
            hide: function() {
                loading = false;
                //$rootScope.loading.hide();
                $ionicLoading.hide();
            }
        }
    }
])

.run(['$rootScope', 'BUPLoader', function ($rootScope,BUPLoader) {
    
        $rootScope.$on('loading:show', function(event,data) {
            data = data || {};
            BUPLoader.show(data);
        })

        $rootScope.$on('loading:hide', function() {
            BUPLoader.hide();
        })

}])
SERVICE_MODULE
/**
 * CordovaNetwork AngularJS service
 * https://gist.github.com/rvanbaalen/9527418
 * com.apache.cordova.network-information wrapper for AngularJS with fallback for testing without Cordova
 */
.factory('CordovaNetwork', ['$rootScope', '$ionicPlatform',
    function($rootScope, $ionicPlatform) {
        // Get Cordova's global Connection object or emulate a smilar one
        var Connection = window.Connection || {
            'ETHERNET': 'ETHERNET',
            'WIFI': 'WIFI',
            'CELL_2G': 'CELL_2G',
            'CELL_3G': 'CELL_3G',
            'CELL_4G': 'CELL_4G',
            'CELL': 'CELL',
            'EDGE': 'EDGE',
            'UNKNOWN': 'unknown'
        };

        // Get Cordova's global navigator.connection object or emulate one
        var networkConnection = navigator.connection || {
            type: 'UNKNOWN'
        };

       
        $ionicPlatform.ready(function() {

            if (window.addEventListener) {
                window.addEventListener("online", function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Online', {});
                }, false);
                window.addEventListener("offline", function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Offline', {});
                }, false);
            } else {
                document.body.ononline = function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Online');
                };
                document.body.onoffline = function() {
                    $rootScope.$broadcast('Cordova.NetworkStatus.Offline');
                };
            }
        });

        return {
            isOnline: function() {
                var blnReturn = false;

                switch (this.getStatus()) {
                    case Connection.ETHERNET:
                    case Connection.WIFI:
                    case Connection.CELL_2G:
                    case Connection.CELL_3G:
                    case Connection.CELL_4G:
                    case Connection.CELL:
                        blnReturn = true;
                        break;
                    case 'UNKNOWN':
                        //blnReturn = false;
                        if (window.ionic.Platform.isWebView()) {
                            blnReturn = false;
                        } else {
                            blnReturn = navigator.onLine || false;
                        }
                        break;
                }

                return blnReturn;
            },

            getStatus: function() {
                return networkConnection.type;
            }
        };
    }
])



.factory('CordovaNetworkAsync', ['$ionicPlatform', '$q',
    function($ionicPlatform, $q) {
        // Get Cordova's global Connection object or emulate a smilar one
        var Connection = window.Connection || {
            "CELL": "cellular",
            "CELL_2G": "2g",
            "CELL_3G": "3g",
            "CELL_4G": "4g",
            "ETHERNET": "ethernet",
            "NONE": "none",
            "UNKNOWN": "unknown",
            "WIFI": "wifi"
        };

        var asyncGetConnection = function() {
            var q = $q.defer();
            $ionicPlatform.ready(function() {
                if (navigator.connection) {
                    q.resolve(navigator.connection);
                } else {
                    //q.reject('navigator.connection is not defined');
                    q.resolve(
                        {
                            type: 'UNKNOWN'
                        }
                    );
                }
            });
            return q.promise;
        };
        return {
            isOnline: function() {
                return asyncGetConnection().then(function(networkConnection) {
                    var isConnected = false;

                    switch (networkConnection.type) {
                        case Connection.ETHERNET:
                        case Connection.WIFI:
                        case Connection.CELL_2G:
                        case Connection.CELL_3G:
                        case Connection.CELL_4G:
                        case Connection.CELL:
                            isConnected = true;
                            break;
                        case 'UNKNOWN':
                        default:
                            //blnReturn = false;
                            // if (window.ionic.Platform.isWebView()) {
                            //     blnReturn = false;
                            // } else {
                            //     blnReturn = navigator.onLine || false;
                            // }
                            

                            isConnected = navigator.onLine || false;
                            //cannot be relied however!!!
                            
                            // isConnectedSync = function(){

                                
                            //     try{
                            //         var http = new XMLHttpRequest();
                            //         http.open('HEAD', 'http://google.com', true);
                            //         http.send();
                            //         return http.status!=404;

                            //     }catch(e) {

                            //         return false;
                            //     }
                                

                            // }();
                            break;
                    }
                    return isConnected;
                });
            }
        };
}])

.run(['$rootScope', 'CordovaNetworkAsync' ,'CordovaNetwork',function($rootScope,CordovaNetworkAsync,CordovaNetwork){
	
	$rootScope.isOnline = CordovaNetwork.isOnline();

	CordovaNetworkAsync.isOnline()
	.then(function(isConnected) {
        $rootScope.isOnline = isConnected;
    }, function(err) {
    	$rootScope.isOnline = false;
    });

    //update
    
    $rootScope.$on('Cordova.NetworkStatus.Offline', function(event, data) {
        // $rootScope.isOnline = CordovaNetwork.isOnline();
        $rootScope.isOnline = false;

    });
    $rootScope.$on('Cordova.NetworkStatus.Online', function(event, data) {
        // $rootScope.isOnline = CordovaNetwork.isOnline();
        $rootScope.isOnline = true;
    });

}])

SERVICE_MODULE
.run(['$rootScope','BUPUser','BUPWebservice','WEBSERVICE_BASE_URI','$http', function ($rootScope,BUPUser,BUPWebservice,WEBSERVICE_BASE_URI,$http) {

    $rootScope.$on('newactivity',function(event,activity){
        var currentUser =  BUPUser.getCurrentUser();

        console.log(currentUser);
        if(!currentUser || !currentUser.roleId || !currentUser.saveDataInServer ){
            console.log('no saing in the server!');
            return;
        }

        //console.log('preparing to save data on the server')
        
        if( activity.homeworkid || activity.type && activity.type=='homework' ) {
            if(!activity.homeworkid) {
                activity.type = 'practice';
            }
            var homeworkType = (activity.homeworkid)?'homeworks':'practices';
            var defaultValues = {}

            //var newActivity = angular.extend(defaultValues, activity);
            console.log(homeworkType)
            BUPWebservice.getTaskById(activity.taskid,homeworkType).then(function(homeworkDetail){
                //console.log(homeworkDetail);
                //console.log(newActivity);
                activity.homeworkData = homeworkDetail;
                console.log(activity);

                $http.post(WEBSERVICE_BASE_URI + '/Login/SaveMeetingData',activity).then(function(rawResponse) {
                    console.debug(rawResponse);
                },function(err){
                    console.err(err);
                });

            })

        }else {

            $http.post(WEBSERVICE_BASE_URI + '/Login/SaveMeetingData',activity).then(function(rawResponse) {
                    console.debug(rawResponse);
                },function(err){
                    console.err(err);
                });

        }

       

        //console.log($rootScope);
      
        console.error(activity);
    });

    $rootScope.$on('homeworkdoneagain',function(event,activity) {
        var currentUser =  BUPUser.getCurrentUser();

        
        if(!currentUser || !currentUser.roleId || !currentUser.saveDataInServer ){
            console.log('no saing in the server!');
            return;
        }
        console.log('meeting id '+ $rootScope.meetingId );
        var defaultValues = {
            date: moment().startOf('day').format('YYYY-MM-DD'),
            userid: Number(BUPUser.getCurrentUser().id),
            meetingId : $rootScope.meetingId
        };
        var newActivity = angular.extend(defaultValues, activity);
        /*
            
            if($rootScope.RoleId && $rootScope.RoleId == 3){
                defaultValues.meetingId = $rootScope.meetingId;
            }
            
            date: $moment().startOf('day').format('YYYY-MM-DD'),
            userid: Number(BUPUser.getCurrentUser().id),
       
         */
    });

}])

.factory('BUPSync', ['$q','BUPWebservice', 'BUPUser', 'BUPFiles', 'BUPInteraction', '$rootScope', 'CordovaNetwork', 'CordovaNetworkAsync','$translate',
    function($q, BUPWebservice, BUPUser, BUPFiles, BUPInteraction, $rootScope, CordovaNetwork, CordovaNetworkAsync,$translate) {



        return {

            /**
             * [manualSync this function is fired when the user clicks sync button in the home navbar]
             * @return {[type]} [description]
             *
             * things to be done here:
             * 1. get the task list (forcefully from the server)
             * 2. update the task list and update the localstorage value
             * 3. abort gracefully when the internet connection is not
             */
            manualSync: function(opt) {

                if (!BUPUser.isLoggedIn()) {
                    console.error("not logged in to sync");
                    return false;
                }
                opt = opt || {};
                var defaultOpt = {
                    silentSync: false
                };
                opt = angular.extend(defaultOpt, opt);

                console.log("silentsync(background)=" + opt.silentSync);


                // if (CordovaNetwork.isOnline()) {
                //     BUPWebservice.getAllTasks({
                //         forceFromServer: true,
                //         isBackgroundTask: opt.silentSync
                //     });
                // } else {
                //     opt.silentSync === false && BUPInteraction.toast('Cannot sync in offline mode!!!');
                //     console.warn("cannot sync in offline mode");


                // }

                CordovaNetworkAsync.isOnline().then(function(isConnected) {
                    if (isConnected) {
                        BUPWebservice.getAllTasks({
                            forceFromServer: true,
                            isBackgroundTask: opt.silentSync
                        });


                    } else {
                        if (opt.silentSync === false) {
                            $translate('INFO_OFFLINE_SYNC_NOT_POSSIBLE'). then ( function  (text) {
                                BUPInteraction.toast(text, 'short');
                            });
                            //BUPInteraction.toast('Cannot sync in offline mode!!!');
                        }
                        console.warn("cannot sync in offline mode");
                    }

                }, function(err) {
                    console.log(err);
                });


                var currentUser = BUPUser.getCurrentUser();
                /**
                 * things to be done in the
                 */
            },

            /**
             * timely sync to check for updates
             *
             * @param  {[type]} opt [description]
             * @return {[type]}     [description]
             */
            regularSync: function(opt) {

            },


            backgroundSync: function(){

                var deferred = $q.defer();
                if (!BUPUser.isAuthenticated()) {
                    deferred.reject('User has not been authenticated to sync the data!');
                }else {
                    CordovaNetworkAsync.isOnline().then(function(isConnected) {
                        if (isConnected) {
                            return BUPWebservice.getAllTasks({
                                forceFromServer: true,
                                isBackgroundTask: true
                            });
                        }else{
                            deferred.reject('User is offline!');
                        }

                    }, function(err) {
                        deferred.reject('User is offline!');
                    });

                }
                return deferred.promise;

            },






            sync: function(opt) {

            }


        };
    }
])

.run(['$rootScope', 'BUPSync', 'CordovaNetworkAsync', 'BUPUser','$ionicPlatform', function ($rootScope,BUPSync,CordovaNetworkAsync,BUPUser, $ionicPlatform) {
    
    var sync = function(withSilence){
        BUPSync.manualSync({
            silentSync: withSilence
        });
    };
    $rootScope.$on('app.userready',function(event,data){
        var silentSync = true;
        if( data && data.manualLogin===true) {
            silentSync = false;
        }
        sync(silentSync);
    });

    //$ionicPlatform.on('resume', checkForUpdate);
    $rootScope.$on('Cordova.NetworkStatus.Online', function(event, data) {
        sync(true);
    });

    $ionicPlatform.on('resume', function(){
        sync(true);
    });


    //http://109.74.12.180/HemuppgiftenDev/webservice/Login/TreatmentDataArray?TokenKey=c54c8c79-d7cc-4cda-b32b-66862e526b26
    
    /*

    if (ionic.Platform.isWebView()) {
        try{

            var Fetcher = window.plugins.backgroundFetch;
            // Your background-fetch handler.
            var fetchCallback = function() {
                
                
                
                console.log('BackgroundFetch initiated');

                BUPSync.backgroundSync().finally(function() {
                     var backgroundExecutionCount = localStorage.getItem('bgExeCount') || 0;
                     ++ backgroundExecutionCount;
                    localStorage.setItem('bgExeCount', backgroundExecutionCount);

                    Fetcher.finish();
                    // Always execute this on both error and success
                });

            }

            Fetcher.configure(fetchCallback);

        }catch(e){
            console.error(e);
        }
        

    }

    */





}])
var UserAssignment = function(homework){
	//the constructor function
	var ua = this;

	//is repetive if it has icon
	ua.isRepetitive = !!homework.icon;

	//id
	ua.id = homework.id;

	ua.homeworkSlides = [];

	//homework
	angular.forEach(homework.homeworkSlides, function(slide) {
		//console.log(slide);
		var userSlide = {
            slidId:slide.slidId
        };
		switch(slide.type) {
			case "normal-text":
			     //console.log(slide.value);
				userSlide.value = slide.value || "";
			break;
			
			case "reorder":
                console.debug(slide.list);

                //do not store all the slided, save only those which has been changed!
                
                var changedList = [];
                _.forEach(slide.list,function(theList){
                    if (theList.added || theList.updated || theList.reordered || theList.trashed) {
                        changedList.push(theList);
                    }
                });

                // userSlide.list = slide.list;
                userSlide.list = changedList;
                
                // if(slide.updated) {
                //     userSlide.updated = slide.updated;
                // }if(slide.reordered) {
                //     userSlide.reordered = slide.reordered;
                // }if(slide.edited) {
                //     userSlide.edited = slide.edited;
                // }if(slide.edited) {
                //     userSlide.edited = slide.edited;
                // }
                //userSlide.list = slide.list;
				//userSlide = slide;
			break;

			case "form":
			case "dynamic-button":
			default:

			break;
		}
		ua.homeworkSlides.push(userSlide);
	});
	//console.log(ua);
	return ua;
};

SERVICE_MODULE

.value('UserAssignment',UserAssignment)

.factory('BUPUser', ['$rootScope', 'authService',
    function($rootScope, authService) {

        //private var
        var user = {};
        var forceReAuthenticate = function() {
            //console.log('re authentication required');
            $rootScope.$broadcast('event:auth-loginRequired');
            //$rootScope.$emit('event:auth-loginRequired');

        };

        

        var service = {
            init:function(){
                if (window.localStorage['user']) {
                    try {
                        user = JSON.parse(window.localStorage['user']);
                        // if(service.isLoggedIn()){
                        //     $rootScope.$broadcast('app.userready');
                        // }
                        //window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) ||
                    } catch (e) {
                        console.error('Could not read the user data from the local storage');
                        forceReAuthenticate();
                    }
                } else {

                    //console.log('user detail not found in the local storage');
                    forceReAuthenticate();

                }
            },
            getCurrentUser: function(authentication) {
                if (user !== null && typeof user == "object" && user.hasOwnProperty('tokenKey') && !!user.tokenKey) {
                    //console.log('current user info is ');
                    //console.log(user);
                    window._u = user;
                    return user;

                } else {

                    console.error('user info not found');
                    return false;
                    //forceReAuthenticate();

                    //$rootScope.$broadcast('event:auth-loginRequired');
                    //authenticateion required

                }
            },
            setCurrentUserInfo: function(userObj) {
                user = userObj;
                //$rootScope.currentUser = userObj;
                //$rootScope.currentUser = curUser;
                //$rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);
                //$rootScope.currentUserConfig = BUPConfig.initUserConfig(userObj.id);

                window.localStorage['user'] = JSON.stringify(userObj);
            },
            isLoggedIn: function() {

                if (user !== null && typeof user == "object" && user.hasOwnProperty('tokenKey') && !!user.tokenKey && !!user.isLoggedIn) {
                    // if ($rootScope.currentUser) {
                    //     return true;
                    // }
                    // return false;
                    return true;

                }

                return false;

            },

            /**
             * if a user has a valid token key but not necessarily logged in=true
             */
            isAuthenticated: function() {

                if (user !== null && typeof user == "object" && user.hasOwnProperty('tokenKey') && !!user.tokenKey ) {
                    // if ($rootScope.currentUser) {
                    //     return true;
                    // }
                    // return false;
                    return true;

                }

                return false;

            },
            locallyAuthenticate: function(checkUser) {


            }

        };

        // if(service.isUs){
        //     // $rootScope.$broadcast('app.userready');
        // }

        return service;
    }
])

//persistent configuration details

.factory('BUPConfig', ['BUPUtility', 'BUPUser', '$moment', '$rootScope',

    function(BUPUtility, BUPUser, $moment, $rootScope) {

        /**
         * note this config is always per user basis. 
         * ie, config of one user never applies to another user
         * stored config is actually an array
         * sample:
            var config = [{
                userId: 11,
                startedDate: "2014-06-15"
            }];
         */


        var config = [];

        var service = {
            /**
             * [readDataFromLocalStorage read the data from  the local storage into the private variable "config"]
             * @return {[type]} [description]
             */
            readDataFromLocalStorage: function() {
                //debugMode && console.log('reading data from the local storage for user config')
                if (window.localStorage['config']) {
                    try {
                        config = JSON.parse(window.localStorage['config']);
                    } catch (e) {}
                } else {
                    //if this object does not exist already then create that object and also save in localstorage

                }
            },

            /**
             * [updateDataToLocalStorage write the content of private variable "config" to the localstorage]
             * @return {[type]} [description]
             */
            updateDataToLocalStorage: function() {
                window.localStorage['config'] = JSON.stringify(config);
            },

            /**
             * BUPConfig.check() i
             * @param  {[int]} userId [id of the user to check]
             * @return {[void]}        doesn't return anything
             * checks if the user related config is set or not.
             * Ensures that must have config eg "userid","startedDate" is there
             * If not it is created and stored
             */
            initUserConfig: function(userId) {
                // var userConfig = _.filter(config, function(configEntry) {
                //     return configEntry.userId && configEntry.userId == userId && configEntry.startedDate;
                // });
                // if (userConfig.length) {
                //     debugMode && console.log('reading previous config');
                //     userConfig = userConfig[0];
                // } 

                if (!userId) return;
                var userConfig = this.get(userId);

                if (userConfig === false) {

                    //debugMode && console.log('setting a new config');
                    var today = $moment().startOf('day');

                    userConfig = {
                        userId: userId,
                        startedDate: {
                            timestamp: $moment(today).valueOf(),
                            dayIndex: BUPUtility.dayIndex(today),
                            weekIndex: BUPUtility.weekIndex(today)
                        }
                    };
                    config.push(userConfig);
                    this.updateDataToLocalStorage();
                }

                return userConfig;

            },

            get: function(userId) {
                if (!userId) return;
                //first check if it exists
                var userConfig = _.filter(config, function(configEntry) {
                    return configEntry.userId && configEntry.userId == userId && configEntry.startedDate;
                });
                if (userConfig.length) {
                    //debugMode && console.log('reading previous config');
                    userConfig = userConfig[0];

                    //make sure the userConfig has all the required values
                    if (!userConfig.startedDate) {
                        userConfig['startedDate'] = {
                            timestamp: $moment(today).valueOf(),
                            dayIndex: BUPUtility.dayIndex(today),
                            weekIndex: BUPUtility.weekIndex(today)
                        };
                    }



                    window._uc = userConfig;
                    return userConfig;
                } else {
                    return false;
                }
                debugMode && console.warn('user config not available!!!');
                return false;
            },
            getCurrentUserConfig: function() {

                var currentUser = BUPUser.getCurrentUser();
                if (currentUser && currentUser.id) {
                    return this.get(currentUser.id);
                }
            },
            
            updateUserConfig: function(userId,newConfig){
                var index = _.findIndex(config, function(userObj) {
                    return typeof userObj['userId'] !== "undefined" && userObj['userId'] === userId;
                });
                if (index > -1) {
                    config[index] = newConfig;
                } else {
                    config.push(newConfig);
                }
                this.updateDataToLocalStorage();
            },
            updateCurrentUserConfig:function(newConfig) {
                
                var currentUser = BUPUser.getCurrentUser();
                if (currentUser && currentUser.id) {
                    this.updateUserConfig(currentUser.id,newConfig);
                }

            },
            set: function(newConfig) {
                config = newConfig;
                this.updateDataToLocalStorage();
            }


        };

        service.readDataFromLocalStorage();

        return service;
    }
])

.run(['BUPUser','$rootScope','BUPConfig', function (BUPUser,$rootScope,BUPConfig) {
    BUPUser.init();
    if(BUPUser.isLoggedIn()) {
        $rootScope.currentUser = BUPUser.getCurrentUser();
        $rootScope.currentUserConfig = BUPConfig.initUserConfig($rootScope.currentUser.id);
        $rootScope.$broadcast('app.userready',{manualLogin:false});
    }
}])
SERVICE_MODULE
/**
 * Utility Service
 * @return {[type]} [description]
 */

.service('BUPUtility', ['$moment', '$document', '$q','$http',


    function($moment, $document, $q, $http) {
        window.BUPUtility = this;
        /**
         * find out the unique day index
         * @uses moment().dayOfYear() to get the unique day of the year
         * @returns string prefixed with "{currentyear}_" to make sure it is valid for multiple years too
         */
        this.dayIndex = function(date) {
            var theMoment = $moment(date);
            return theMoment.get('year') + '_' + theMoment.dayOfYear();
        };

        /**
         * find out the unique week index
         * @uses moment().isoWeek() to get the unique week number of the year
         * @returns string prefixed with "{currentyear}_" to make sure it is valid for multiple years too
         */
        this.weekIndex = function(date) {
            //if( typeof this.weekIndex.count == 'undefined') this.weekIndex.count = 0;
            //++this.weekIndex.count;
            //console.error("weekIndex called count: "+this.weekIndex.count);
            //console.time('weekIndex');
            var theMoment = $moment(date);
            var res= theMoment.get('year') + '_' + theMoment.isoWeek();
            //console.timeEnd('weekIndex');
            return res;
        };

        /**
         * suitable building query string out of a javascript object
         * eg for making GET Request in webservice
         */

        this.buildQuery = function(obj, prefix) {
            var str = [];
            for (var p in obj) {
                var k = prefix ? prefix + "[" + p + "]" : p,
                    v = obj[p];
                str.push(typeof v == "object" ?
                    serialize(v, k) :
                    encodeURIComponent(k) + "=" + encodeURIComponent(v));
            }
            return str.join("&");
        };


        //https://developer.mozilla.org/en-US/docs/Web/API/window.btoa
        this.encode = function(str) {
            //utf8_to_b64
            return window.btoa(encodeURIComponent(escape(str)));
        };

        this.decode = function(str) {
            //b64_to_utf8
            return unescape(decodeURIComponent(window.atob(str)));
        };

        this.deviceInfo = function() {

            var info = {
                pushID: "",
                model: '', //ionic.Platform.platform()+'_'+ionic.Platform.version(),
                platform: "browser",
                uuid: ""
            }
            info = angular.extend(info, ionic.Platform.device());

            // var device = ionic.Platform.device();
            // info.model = device.model;
            // info.platform = device.platform;
            // info.uuid = device.uuid;

            return info;

        };


        this.UrlExists = function(url) {
            
            window.$http = $http;
            
            var deferred = $q.defer();

            $http.head(url,{cache: false})
            .then(function(response){
                if (response.status == 404) {
                    deferred.reject(response.status);
                    
                }else{
                    deferred.resolve();
                }
            },function(e){
                deferred.reject(e);
            });

            return deferred.promise;



            return $http.head(url,{cache: true})
            .then(function(response){
                if( response.status == 404) {
                   
                    return $q.reject(response);
                    // console.log('file not found');
                    //return $q.reject('404');
                    
                } 
                //console.log(response)
                return response;
                
            },function(e){
                //throw e;
                //console.error(e);
                return $q.reject(e);
                //console.error(e);
            });

            // var http = new XMLHttpRequest();
            // http.open('HEAD', url, false);
            // http.send();
            // return http.status != 404;
        };


        this.findNested = function(obj, key, memo) {
          var self = this;
          _.isArray(memo) || (memo = []);
          _.forOwn(obj, function(val, i) {
            if (i === key) {
              memo.push(val);
            } else {
              self.findNested(val, key, memo);
            }
          });
          return memo;
        };


    }
])

.run(['$http', '$templateCache','$state','$q','BUPUtility',
    function($http, $templateCache,$state,$q,BUPUtility) {
        return;
        /**
         * Template prefetching
         *
         */
        var templates = BUPUtility.findNested($state.get(),'templateUrl'),
            promises = [];

        var modals = ['html/partials/modals/welcome.modal.html','html/partials/modals/welcome.modal.html','html/partials/modals/homework_add_edit_list.html'];
        modals.forEach(function(m){
            templates.push(m);
        }); 

        //html/partials/homework.html
      

        templates.forEach(function (c,i) {
            if ($templateCache.get(c)){
             console.log('template '+ i+ ' already loaded!');
             return; //prevent the prefetching if the template is already in the cache
            }
            promises[i] = $http.get(c).success(function (t) {
              $templateCache.put(c, t);
            });
        });

        $q.all(promises)
        .then(function(resp){
            //all the template loadings has been completed
            console.log('all templates loaded');
            try{
                navigator.splashscreen.hide();
            
            }catch(e){

            }
            //console.timeEnd('templatesPrefetch');

        },function(){
            //if failed then also remove the splash screen
            try{

                navigator.splashscreen.hide();
            
            }catch(e){

            }
            //console.timeEnd('templatesPrefetch');
        });

        

        
    }
])
;
SERVICE_MODULE
/**
 * A simple example service that returns some data.
 * authService is the service from angular-http-auth module
 *
 */
.factory('tasksCache', function($cacheFactory) {
    return $cacheFactory('tasksCache');
})
.factory('BUPWebservice', ['$q', '$http','$cacheFactory', 'authService', 'WEBSERVICE_BASE_URI', 'BUPUtility', 'BUPUser','BUPActivities', 'BUPLoader', '$rootScope',
    function($q, $http,$cacheFactory ,authService, WEBSERVICE_BASE_URI, BUPUtility, BUPUser,BUPActivities, BUPLoader, $rootScope) {
        // Might use a resource here that returns a JSON array



        // var tasks = {
        //     practices: [],
        //     homeworks: []
        // };
        //read the tasks from the local storage or empty


        // var tasks = window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) || {
        //     practices: [],
        //     homeworks: []
        // };


        var tasks = {
            practices: [],
            homeworks: []
        };



        if (window.localStorage['tasks']) {
            try {
                tasks = JSON.parse(window.localStorage['tasks']);
                //window.localStorage['tasks'] && JSON.parse(window.localStorage['tasks']) ||
            } catch (e) {

            }
        }

        return {

            // authenticate: function(user) {
            //     BUPLoader.show({
            //         text: 'Authenticating...'
            //     });
            //     return $http.post('/api/login', user).then(function(response) {
            //         BUPLoader.hide();
            //         return response.data;
            //     });

            // },

            getAllTasksOld: function(opt) {
                var defaultOpts = {
                    forceFromServer: false,
                    isBackgroundTask: false
                };
                opt = angular.extend(defaultOpts, opt); //opt || {fromServer:false};
                // return $http.get('/api/practices').then(function(response) {
                //     practices = response.data;
                //     return practices;
                // });
                //if it has already been requested before get the same data
                if (opt.forceFromServer === false && typeof tasks == "object" && tasks != null && tasks.hasOwnProperty("practices") && tasks.hasOwnProperty("homeworks") && (tasks.practices.length || tasks.homeworks.length)) {

                    //controller is configured to expect a promist
                    var deferred = $q.defer();
                    //console.log('the existing all tasks are');
                    //console.log(tasks);
                    //deferred.resolve(practices);
                    deferred.resolve(tasks);

                    //temporary for testing only 
                    //$rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask);

                    //opt.showLoader && BUPLoader.hide();
                    return deferred.promise;

                } else {
                    // !opt.hideLoader && BUPLoader.show({
                    //     delay: 0
                    // });


                    var params = BUPUtility.buildQuery({
                        'TokenKey': '' + BUPUser.getCurrentUser().tokenKey
                    });
                    ///api/practices
                    return $http.get(WEBSERVICE_BASE_URI + '/Login/TreatmentDataArray?' + params).then(function(rawResponse) {
                        
                        var response = rawResponse.data.data[0];

                        //console.log('the backend all task respnse is');
                        //console.log(rawResponse);
                        //console.log(response);

                        //$rootScope.$broadcast('event:auth-loginRequired');

                        //practices = response.data;
                        if (response === "invalid") {

                            $rootScope.$broadcast('event:auth-loginRequired');

                        } else if (response && response.practices && response.homeworks) {

                            // tasks = {
                            //     practices: response.practices,
                            //     homeworks: response.homeworks
                            // }
                            tasks = response;

                            // tasks.practices = response.data.tasks.practices;
                            // tasks.homeworks = response.data.tasks.homeworks;

                            window.localStorage['tasks'] = JSON.stringify(tasks);
                            //!opt.hideLoader && BUPLoader.hide();

                            //if (opt.forceFromServer === true) {
                            //this is the response after request for forcefully get the data from the server
                            //(not the local storage). so time to broadcast this new data
                            //console.log('time to make the braodcast')
                            $rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask);
                            //}
                            return tasks;
                        } else {

                            $rootScope.$broadcast('event:auth-loginRequired');
                        }

                        //return practices;


                    });
                }

            },
            getAllTasks: function(opt) {
                var defaultOpts = {
                    forceFromServer: false,
                    isBackgroundTask: false,
                    cachePreferred:false
                };
                opt = angular.extend(defaultOpts, opt); 

                //if it has already been requested before get the same data
                //if (opt.forceFromServer === false && typeof tasks == "object" && tasks != null && tasks.hasOwnProperty("practices") && tasks.hasOwnProperty("homeworks") && (tasks.practices.length || tasks.homeworks.length)) {

                if (opt.cachePreferred===true) {
                    var deferred = $q.defer();
                    deferred.resolve(tasks);
                    return deferred.promise;
                }

                    //controller is configured to expect a promist
                    var params = BUPUtility.buildQuery({
                        'TokenKey': '' + BUPUser.getCurrentUser().tokenKey
                    });

                    if(opt.forceFromServer) {
                        //delete cache
                        $httpDefaultCache = $cacheFactory.get('$http');
                        $httpDefaultCache.remove(WEBSERVICE_BASE_URI + '/Login/TreatmentDataArray?' + params);
                    }


                    ///api/practices
                    return $http.get(WEBSERVICE_BASE_URI + '/Login/TreatmentDataArray?' + params,{cache:!!!opt.forceFromServer})
                    .then(function(rawResponse) {
                        if( rawResponse.status!=200) { //rawResponse.data != "ok" ||
                            var deferred = $q.defer();
                            deferred.resolve(tasks);
                            return deferred.promise;
                        }
                        //console.log(JSON.stringify(rawResponse));
                        var response = rawResponse.data.data[0];

                        //console.log('the backend all task respnse is');
                        //console.log(rawResponse);
                        //console.log(response);

                        //$rootScope.$broadcast('event:auth-loginRequired');

                        //practices = response.data;
                        if (response === "invalid") {

                            $rootScope.$broadcast('event:auth-loginRequired');

                        } else if (response && response.practices && response.homeworks) {

                            // tasks = {
                            //     practices: response.practices,
                            //     homeworks: response.homeworks
                            // }
                            tasks = response;

                            // tasks.practices = response.data.tasks.practices;
                            // tasks.homeworks = response.data.tasks.homeworks;

                            window.localStorage['tasks'] = JSON.stringify(tasks);
                            //!opt.hideLoader && BUPLoader.hide();

                            //if (opt.forceFromServer === true) {
                            //this is the response after request for forcefully get the data from the server
                            //(not the local storage). so time to broadcast this new data
                            //console.log('time to make the braodcast')
                      
                            $rootScope.$broadcast('BUP.tasks_updated', tasks, opt.isBackgroundTask,opt.forceFromServer);
                            //}
                            return tasks;
                        } else {

                            $rootScope.$broadcast('event:auth-loginRequired');
                        }

                        //return practices;


                    },function(err) {
                        var deferred = $q.defer();
                        deferred.resolve(tasks);
                        $rootScope.$broadcast('BUP.tasks_loaded', tasks);
                        return deferred.promise;

                    });
            

            },
            setTasks: function(newTasks) {
                tasks = newTasks;
                window.localStorage['tasks'] = JSON.stringify(newTasks);
                //console.log('now the tasks are');
                //console.log(tasks);
            },

            /*****************************************************************
             * @id: (int) id of the task
             * @type:  (string) type of the task , either homework or practice
             *****************************************************************
             **/
            getTaskById: function(id, type) {

                var deferred = $q.defer();
                console.time('getAllTasks');
                //first ensure we have all the tasks
                this.getAllTasks({cachePreferred:true}).then(function(allTasks) {
                    console.timeEnd('getAllTasks');
                    //console.log('all  tasks (practices and homeworks) available.ensured!');

                    //.log(allTasks);
                    //.log('type=' + type);
                    //console.log(allTasks[type]);
                    //get the task type
                    //console.log()
                    console.time('foundTasks');
                    var foundTasks = allTasks[type].filter(function(thetask) {
                        //console.log(thetask.id);
                        return thetask != null && parseInt(thetask.id, 10) == id;
                    });
                    console.timeEnd('foundTasks');

                    //var userExtendedTask = BUPActivities.getUserResponseByTaskId(id,type);
                    
                    var requiredTask = foundTasks.length ? foundTasks[0] : {};

                    //var mergedTask = angular.extend(requiredTask, userExtendedTask);

                    //var mergedTask = _.merge(requiredTask, userExtendedTask);


                    //console.log(mergedTask);
                    deferred.resolve(requiredTask);
                    //deferred.resolve(mergedTask);


                });

                return deferred.promise;

            },

            getHomeworkById: function(id) {

                return this.getTaskById(id, 'homeworks');

            },

            getPracticeById: function(id) {

                return this.getTaskById(id, 'practices');

            },
            /**
             * Experimental function 
             * @param  {[type]} id     [description]
             * @param  {[type]} type   [description]
             * @param  {[type]} newVal [description]
             * @return {[type]}        [description]
             */
            updateTaskById: function(id,type,newVal) {
                var foundTasks = tasks[type].filter(function(thetask) {
                    //console.log(thetask.id);
                    return thetask != null && parseInt(thetask.id, 10) == id;
                });

                if(foundTasks.length) {
                    //console.log('tasks updated in the localstorage')
                    foundTasks[0] = newVal;

                    window.localStorage['tasks'] = JSON.stringify(tasks);

                } 
            },

            registerCustomGroupUser: function(customGroupUser) {
                var params = BUPUtility.buildQuery(customGroupUser);
                var sampleSuccessReturn = {
                    status: "ok",
                    data: {
                        UserId: 22,
                        UserName: "customuserName",
                        RoleId: 5,
                        RoleName: "Group Patient",
                        saveDataInserver: false,
                        TokenKey: "asampletokenkey"
                    }

                };

                var sampleErrorReturn = {
                    status: "error",
                    data: {
                        message: "Username already taken, please choose another"
                    }
                };

                //sending static data but as a javascript promise similar to $http response

                // var deferred = $q.defer();
                // deferred.resolve(sampleSuccessReturn);
                // return deferred.promise;

                return $http.get(WEBSERVICE_BASE_URI + '/Login/InsertGroupUser?' + params).then(function(rawResponse) {
                    if (rawResponse.status === 200) {
                        var response = rawResponse.data.data;
                        if (response.Status) {
                            response.status = response.Status;
                        }
                        return response;
                        //console.log(rawResponse);
                        //console.log(response);
                        //return sampleSuccessReturn;
                        //return response;
                    } else {

                    }

                });
            }




            //
        }
    }
])